<?php
App::uses('AppModel', 'Model');

class Config extends AppModel {
	var $useTable = 'config';	// table name
	public $primaryKey = 'id';	// primary key (ID) [add]

	// Tabledit による User 編集
	public function saveByLite ($post, $id) {
		// データ登録用の連想配列を作成
		$update_data = array(
//			'upd_date' => date('Y/m/d H:i:s'),
		);
		foreach (array('key', 'value', 'memo') as $key) {
			if (array_key_exists($key, $post))
				$update_data[$key] = $post[$key];
		}
		// 更新のときは ID をセット
		if ($id) $update_data[ $this->primaryKey ] = $id;

		// データ登録 (MySQL)
		return $this->save($update_data);
	}
}
