<?php
App::uses('AppModel', 'Model');
App::import('Model', 'Target');	// import other Model

class MessageRead extends AppModel {
	var $useTable = 'message_read';	// table name
	public $primaryKey = 'mess_read_id';	// primary key (ID)

	// Relation
	public $belongsTo = array(
		'Message' => array(
			'className' => 'Message',
			'foreignKey' => 'mess_id',
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'username',
		),
	);

	// 既読として保存
	public function saveAsRead ($id, $username = null) {
		return $this->saveToSwitch('read', 1, $id, $username);
	}

	// 返信済として保存
	public function saveAsRes ($id, $username = null) {
		return $this->saveToSwitch('res', 1, $id, $username);
	}

	// 既読/返信として保存
	public function saveToSwitch ($key, $value, $id, $username = null) {
		if (!$key or !$id or !$username) return;		// (複合主キー)
		$pk = $this->primaryKey;	// 主キー

		// 既存データを取得
		$item = $this->find('item', array(
			'conditions' => array(
				'MessageRead.mess_id' => $id,
				'MessageRead.username' => $username,
			),
			'recursive' => -1,
		));

		// データがないときは「新規」
		if (empty($item)) {
			return $this->save(array(
				'mess_id' => $id,
				'username' => $username,
				$key => $value,
			));
		}
		// 値が異なるときは「更新」
		else if (isset($item[$pk])
			and (!isset($item[$key]) or $item[$key] != $value))
		{
			return $this->save(array(
				$pk => $item[$pk], $key => $value,
			));
		}
	}
}
