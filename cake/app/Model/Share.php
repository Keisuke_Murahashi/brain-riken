<?php
App::uses('AppModel', 'Model');

class Share extends AppModel {
	var $useTable = 'share';	// table name
	public $primaryKey = 'share_id';	// primary key (ID) [add]

	public $belongsTo = array(
			// 公開申請情報
			'Group' => array(
					'className' => 'Group',
					'foreignKey' => 'group',
			)
	);

	// 共有データ作成、保存
	public function saveAsAccept ($post) {
		if (empty($post['data_id'])) return false;

		return $this->save(array(
			'data_id' => $post['data_id'],
			'group'   => $post['group'],
		));
	}

	// ステータスを "取り下げ" に設定
	public function saveAsWithdraw ($post) {
		if (empty($post['data_id'])) return false;
		if (empty($post['group'])) return false;

		return $this->updateAll(
				array ( 'delflg' => 1 ),
				array ( 'data_id' => $post['data_id'], 'group' => $post['group'])
		);
	}

	// 共有先グループ名を取得
	public function getSharedGroup ($data_id) {
		$item = $this->find('items', array(
				'conditions' => array('Share.data_id' => $data_id, 'Share.delflg' => 0),
				'fields' => array('Group.id', 'Group.name'), 'recursive' => 1,
		));
		$grp = array();
		foreach ($item as $val) {
			$grp[ $val['Group']['id'] ] =
				isset($val['Group']['name']) ? $val['Group']['name'] :
				                               $val['Group']['id'];
		}
		return $grp;
	}

}
