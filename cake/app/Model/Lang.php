<?php
App::uses('AppModel', 'Model');

class Lang extends AppModel {
	var $useTable = 'lang';	// table name
	public $primaryKey = 'lang_id';	// primary key (ID) [add]
}
