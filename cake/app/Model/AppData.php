<?php
App::uses('AppModel', 'Model');
App::import('Model', 'User');	// import other Model

class AppData extends AppModel {
	var $useTable = 'app';	// table name
	public $primaryKey = 'app_id';	// primary key (ID)

	// Relation
	public $belongsTo = array(
		'Dataset' => array(
			'className' => 'Dataset',
			'foreignKey' => 'data_id',
		),
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group',
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'creator',
			'fields' => array('username', 'name', 'detail', 'home_dir'),
		),
		'UserAuth' => array(
			'className' => 'UserAuth',
			'foreignKey' => 'creator',
			'fields' => array('username', 'group', 'role'),
		),
		'AuthUser' => array(
			'className' => 'User',
			'foreignKey' => 'authorizer',
			'fields' => array('username', 'name', 'detail', 'home_dir'),
		),
		'AuthUserAuth' => array(
			'className' => 'UserAuth',
			'foreignKey' => 'authorizer',
			'fields' => array('username', 'group', 'role'),
		),
	);

	// ID を指定して取得
	public function findById($id) {
		return $this->find('item', array(
			'conditions' => array($this->name .'.'. $this->primaryKey => $id)
		));
	}

	// POST データからデータ登録
	public function saveByPost ($post, $username = null) {
		// データ登録用の連想配列を作成
		$new_data = array(
			'data_id'  => $post['data_id'],
			'detail'   => $post['detail'],
			'authorizer'    => $post['select'],
			'creator'  => $username,	// ログインユーザー名
			'app_type' => $post['app_type'],
			// 'status'   => '申請中',
			'status'   => 'Processing',
			'date'     => date('Y/m/d H:i:s'),
			'group'    => $post['group'],
		);

		//メタデータ/データセット確認、既に共有(承認)されているか
		$this->save($new_data);

		return $this->getLastInsertID();	// $post['app_id'];
	}

	// ステータスを "承認済" に設定
	public function saveAsAccept ($post) {
		if (empty($post['app_id'])) return false;

		return $this->save(array(
			'app_id' => $post['app_id'],
			// NIJC 'status' => '承認済',
			'status' => 'Accepted',
		));
	}

	// ステータスを "棄却" に設定
	public function saveAsReject ($post) {
		if (empty($post['app_id'])) return false;

		return $this->save(array(
			'app_id' => $post['app_id'],
			// NIJC 'status' => '棄却',
			'status' => 'Rejected',
			//'delflg' => 1,
		));
	}
	// ステータスを "棄却" 、delfigを１に設定
	public function saveAsRejectForPublish($post){
		if (empty($post['app_id'])) return false;

		return $this->save(array(
				'app_id' => $post['app_id'],
				// NIJC 'status' => '棄却',
				'status' => 'Rejected',
				'delflg' => 1,
		));

	}
	// ステータスを "取り下げ" に設定
	public function saveAsWithdraw ($post) {
		// 共有承認取り下げ
		if (!empty($post['data_id']) and !empty($post['group'])) {
		// NIJC get group ids

/*		$groups = array();
		foreach ($post['group'] as $groupname) {
                        array_push($groups, getGroupId($groupname));
		}
*/
		// $groups = getGroupId($post['group']);
			// delflg => 1
			$this->updateAll(
					array ( 'delflg' => 1 ),
					array ( 'AppData.data_id' => $post['data_id'], 'AppData.group' => $post['group'])
					//array ( 'AppData.data_id' => $post['data_id'],
					//	'OR' => array('AppData.group' => $groups))
			);

			// status => '取り下げ'
			$this->updateAll(
					// NIJC escape is required for updateAll
					//  array ( 'status' => '"'.'取り下げ'.'"' ),
					array ( 'AppData.status' => "'Withdrawn'" ),
					// NIJC array ( 'AppData.data_id' => $post['data_id'], 'AppData.group' => $post['group'], 'app_type' => '共有')
					array ( 'AppData.data_id' => $post['data_id'], 'AppData.group' => $post['group'], 'OR' => array('app_type' => 'Share', 'app_type' => 'WithdrawalOfShare'))
					//array ( 'AppData.data_id' => $post['data_id'], 
					//	'OR' => array('AppData.group' => $groups), 'app_type' => '共有')
			);
		}
		// 申請取り下げ
		if (!empty($post['app_id'])) {
			// status => '取り下げ'
			return $this->save(array(
					'app_id' => $post['app_id'],
					'delflg'   => 1,
					// NIJC 'status' => '取り下げ'
					'status' => 'Withdrawn'
			));
		}
		return true;
	}

	// ステータスを "取り下げ" に設定 (公開用)
	public function saveAsWithdrawForPublish ($post) {
		if (empty($post['data_id'])) return false;

		$item = $this->find('item', array(
				'fields' => 'app_id', 'recursive' => -1,
				'conditions' => array('data_id' => $post['data_id'], 'app_type' => 'Release', 'delflg' => 0),
		));

		return $this->save(array(
				'app_id' => $item['app_id'],
				'data_id' => $post['data_id'],
				'app_type' => 'Release',
				'delflg' => 1,
		));
	}

	// 申請タイプを取得
	public function getAppType ($app_id) {
		$item = $this->find('item', array(
				'fields' => 'app_type', 'recursive' => -1,
				'conditions' => array('app_id' => $app_id),
				//'conditions' => array('app_id' => $app_id, 'delflg' => 0),
		));
		return isset($item['app_type']) ? $item['app_type'] : null;
	}

	// 共有先を取得
	public function getGroup ($app_id) {
		$item = $this->find('item', array(
				'fields' => 'group', 'recursive' => -1,
				'conditions' => array('app_id' => $app_id),
				//'conditions' => array('app_id' => $app_id, 'delflg' => 0),
		));
		return isset($item['group']) ? $item['group'] : null;
	}

	// NIJC Retrieve all group id from data_id for share
	public function getShareGroup ($data_id) {
		$item = $this->find('list', array(
				'fields' => 'group', 'recursive' => -1,
				'conditions' => array('data_id' => $data_id, 'delflg' => 0),
		));
//		return isset($item) ? array_values($item) : null;
		return isset($item) ? array_unique( array_values($item) ) : null;	// 20160708
	}

	// データセットの ID を取得
	public function getDataId ($app_id) {
		$item = $this->find('item', array(
				'fields' => 'data_id', 'recursive' => -1,
				'conditions' => array('app_id' => $app_id),
				//'conditions' => array('app_id' => $app_id, 'delflg' => 0),
		));
		return isset($item['data_id']) ? $item['data_id'] : null;
	}

	// 申請者 を取得
	public function getCreator ($app_id) {
		$item = $this->find('item', array(
				'fields' => 'creator', 'recursive' => -1,
				'conditions' => array('app_id' => $app_id),
				// 'conditions' => array('app_id' => $app_id, 'delflg' => 0),
		));
		return isset($item['creator']) ? $item['creator'] : null;
	}

	// 承認者 を取得
	public function getAuthorizer ($app_id) {
		$item = $this->find('item', array(
				'fields' => 'authorizer', 'recursive' => -1,
				'conditions' => array('app_id' => $app_id),
				//'conditions' => array('app_id' => $app_id, 'delflg' => 0),
		));
		return isset($item['authorizer']) ? $item['authorizer'] : null;
	}

	// ユーザーがデータを承認可能かどうかチェック
	public function is_editable ($id, $username) {
		// データセット（IDなら取得しにいく）
		$item = (is_array($id)) ? $id : $this->findById($id);

		// （ログイン）ユーザー情報（IDなら取得しにいく）
		$user = $username;
		if (!is_array($user)) {
			$Users = new User();
			$user = $Users->findById($username);
		}
		if (!$item or !$user) return;	// null

		// Dataset.authorizer が同ユーザーであれば true
		if ($item['authorizer'] == $user['username']) return true;

		// またはシステム管理者
		if (isset($user['UserAuth'])
		and $user['UserAuth']['role'] == 'SystemAdmin') return true;

		// またはグループ管理者、システム管理者
		if (isset($user['UserAuth'])
		and isset($item['UserAuth'])
		// NIJC and $user['UserAuth']['role'] == 'GroupAdmin'
		and ($user['UserAuth']['role'] == 'GroupAdmin' or $user['UserAuth']['role'] == 'Leader')
		and $item['UserAuth']['group'] == $user['UserAuth']['group']) return true;
			// (*memo: 自グループ内の GroupAdmin と Leader が編集できる？) - 20160708

		return false;
	}
}
