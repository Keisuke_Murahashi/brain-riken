<?php
App::uses('AppModel', 'Model');
App::import('Model', 'User');	// import other Model

class Group extends AppModel {
	var $useTable = 'group';	// table name
	public $primaryKey = 'id';	// primary key (ID)

	// Relation
	public $hasMany = array(
		'UserAuth' => array(
			'className' => 'UserAuth',
			'foreignKey' => 'group',
		)
	);

	// POST データからデータ登録
	public function saveByPost ($post, $username = null) {
		// データ登録用の連想配列を作成
		$new_data = array(
			'name'   => $post['name'],
			'detail' => $post['detail'],
			'admin'  => $post['admin'],
			'status' => $post['status'],
		);
		// 保存
		return $this->save($new_data);
	}


	// Tabledit による User 編集
	public function saveByLite ($post, $id) {
		if (empty($id)) return;	// グループID
		$pk = $this->primaryKey;	// primaryKey

		// データ登録用の連想配列を作成
		$update_data = array(
			$pk        => $id,	// グループID
			'name'     => $post['name'],
			'detail'   => $post['detail'],
		);
		// データ登録 (MySQL)
		return $this->save($update_data);
	}


	// ユーザーがグループを編集可能かどうかチェック
	public function is_editable ($id, $username) {
		$pk = $this->primaryKey;	// primaryKey

		// （ログイン）ユーザー情報（IDなら取得しにいく）
		$user = $username;
		if (!is_array($user)) {
			$Users = new User();
			$user = $Users->findById($username);
		}
		if (!$user or empty($user['UserAuth'])) return;	// null

		// グループ指定がないとき
		if (is_null($id)) {
			// システム管理者 / グループ管理者
			if ($user['UserAuth']['role'] == 'SystemAdmin'
			// NIJC
			or  $user['UserAuth']['role'] == 'Leader'
			or  $user['UserAuth']['role'] == 'GroupAdmin') return true;
		}

		// データセット（IDなら取得しにいく）
		$group = (is_array($id)) ? $id : $this->findById($id);
		if (!$group) return;	// null

		// グループID
		$group_id = isset($group[$pk]) ? $group[$pk] :
				  ( isset($group['Group']) ? $group['Group'][$pk] : null );
		if (!$group_id) return;	// null

		// システム管理者
		if ($user['UserAuth']['role'] == 'SystemAdmin') return true;

		// またはグループ管理者、システム管理者
		// NIJC if ($user['UserAuth']['role'] == 'GroupAdmin'
		if (($user['UserAuth']['role'] == 'GroupAdmin' or $user['UserAuth']['role'] == 'Leader')
		and $user['UserAuth']['group'] == $group_id) return true;
			// (*memo: 自グループ内の GroupAdmin と Leader が編集できる？) - 20160708

		return false;
	}

	// グループID を指定して取得
	public function findById($id, $options = array()) {
		return $this->find('item', array_merge(array(
				'conditions' => array($this->name .'.'. $this->primaryKey => $id)
		), $options));
	}
	// グループ名 を指定して取得
	public function findByName($name, $options = array()) {
		return $this->find('item', array_merge(array(
				'conditions' => array($this->name .'.name' => $name)
		), $options));
	}

	// グループID からグループ名を取得 - 20160708
	public function getName ($id) {
		$item = $this->find('item', array(
			'fields' => 'name', 'recursive' => -1,
			'conditions' => array($this->primaryKey => $id),
		));
		if (isset($item['name'])) return $item['name'];
		return;
	}
}
