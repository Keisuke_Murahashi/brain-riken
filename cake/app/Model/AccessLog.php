<?php
App::uses('AppModel', 'Model');

class AccessLog extends AppModel {
	var $useTable = 'log';	// table name
	public $primaryKey = 'log_id';	// primary key (ID) [add]
}
