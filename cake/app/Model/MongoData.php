<?php
App::uses('AppMongoModel', 'Model');

class MongoData extends AppMongoModel {
	var $useTable = 'bminds';	// table name
	public $primaryKey = '_id';	// primary key (ID)
}
