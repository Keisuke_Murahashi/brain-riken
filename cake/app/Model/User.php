<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
	var $useTable = 'user';	// table name
	public $primaryKey = 'username';	// primary key (ID) [add]

	// Relation
	public $hasOne = array(
		'UserAuth' => array(
			'className' => 'UserAuth',
			'foreignKey' => 'username',
			'dependent' => true,
		)
	);

	// バリデート
	public $validate = array(
		'username' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'ユーザ名は必須です。'
			)
		),
		'password' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'パスワードは必須です。'
			)
		),
/*
		'role' => array(
			'valid' => array(
				'rule' => array('inList', array('admin', 'author')),
				'message' => 'Please enter a valid role',
				'allowEmpty' => false
			)
		)
*/
	);

	// Active Directory 使用時、LDAP で取得した値とリンクするフィールド名
	// .. User テーブルのフィールド => LDAP で取得したキー
	public $ldap_fields = array(
		'name'   => 'displayname',
		'detail' => 'description',
	);

	// パスワードを暗号化
	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$passwordHasher = new BlowfishPasswordHasher();
			$this->data[$this->alias]['password'] = $passwordHasher->hash(
				$this->data[$this->alias]['password']
			);
		}
		return true;
	}

	// Find 結果を整形
	public function afterFind($items, $primary = false) {
		foreach ($items as $i => $item) {
			if (isset($item['UserAuth'])) {
				$is_group = !empty($item['UserAuth']['group']);

				foreach (array('group', 'role') as $key) {
					$items[$i][$key] = @$item['UserAuth'][$key];
//						implode("\n", Hash::extract($item['UserAuth'], '{n}.'. $key));
				}
				if (isset($item['UserAuth']['Group'])) {
					$items[$i]['group_name'] = @$item['UserAuth']['Group']['name'];
//						implode("\n", Hash::extract($item['UserAuth'], '{n}.Group.name'));
				}
				// 管理権限 (グループ設定時)
				if (isset($item['UserAuth']['Group'])) {
					$items[$i]['is_admin'] =
						(  $item['UserAuth']['role'] == 'SystemAdmin'
						|| $item['UserAuth']['role'] == 'Leader'      && $is_group
						|| $item['UserAuth']['role'] == 'GroupAdmin'  && $is_group );
					$items[$i]['is_applicant'] =
						(  $item['UserAuth']['role'] == 'SystemAdmin'
						|| $item['UserAuth']['role'] == 'Leader'      && $is_group
						|| $item['UserAuth']['role'] == 'GroupAdmin'  && $is_group
						// NIJC || $item['UserAuth']['role'] == 'Curator'     && $is_group );
						|| $item['UserAuth']['role'] == 'Reviewer'     && $is_group );
				} else {
					$items[$i]['is_admin']     = false;
					$items[$i]['is_applicant'] = false;
				}
			} else {
				$items[$i]['is_admin'] = false;
				$items[$i]['is_applicant'] = false;
			}
		}
		return $items;
	}

	// ID を指定して取得
	public function findById($username) {
		return $this->find('item', array(
			'conditions' => array($this->name .'.'. $this->primaryKey => $username)
		));
	}

	// ユーザー名を取得
	public function getName ($username) {
		$item = $this->find('item', array(
			'fields' => 'name', 'recursive' => -1,
			'conditions' => array($this->primaryKey => $username),
		));
		if (isset($item['name'])) return $item['name'];
		return;
	}

	// ユーザーディレクトリを取得
	public function getHomeDir ($username) {
		$item = $this->find('item', array(
			'fields' => 'home_dir', 'recursive' => -1,
			'conditions' => array($this->primaryKey => $username),
		));
		if (isset($item['home_dir'])) return preg_replace('![\\\/]$!', '', $item['home_dir']);
		return;
	}

	// Tabledit による User 編集
	public function saveByLite ($post, $id) {
		if (empty($id)) return;	// ユーザーID
		$pk = $this->primaryKey;	// primaryKey

		// データ登録用の連想配列を作成
		$update_data = array(
			'User' => array(
				$pk        => $id,	// ユーザーID
				'name'     => $post['name'],
				'detail'   => $post['detail'],
				'home_dir' => $post['home_dir'],
			),
		);
		// [UserAuth]
		if (!empty($post['group']) or !empty($post['role'])) {
			$update_data['UserAuth'] = array_merge(
				array(
//					'admin' => $post['admin'],
				), empty($post['group']) ? array() : array(
					'group' => $post['group'],
				), empty($post['role'])  ? array() : array(
					'role'  => $post['role'],
				)
			);
		}
		// データ登録 (MySQL)
		return $this->saveAll($update_data);
	}

	// Active Directory による User 情報編集
	public function saveByLDAP ($post, $id) {
		// データ登録用の連想配列を作成
		$new_data = array($this->primaryKey => $id);	// ユーザーID
		$pk = $this->primaryKey;	// primaryKey

		// 既にユーザー情報があれば返す
		$count = $this->find('count', array(
			'conditions' => $new_data,
			'recursive' => -1,
		));
		if ($count) return;

		// LDAP で取得した値とリンクする
		foreach ($this->ldap_fields as $key => $ldap_key) {
			if (isset($post[$ldap_key])) $new_data[$key] = $post[$ldap_key];
		}
		$insert_data = array(
				'User' => array(
						$pk        => $id,
						'name'     => $new_data['name'],
						'detail'   => $new_data['name'],
						'home_dir' => null,
				),
				'UserAuth' => array(
						'username' => $id,
						'group'    => null,
						'role'     => null,
						//'admin'    => $post['admin'],
				),
		);
		// データ登録 (MySQL)
		return $this->saveAll($insert_data);
	}

	// Tabledit による User 編集
	public function saveAsNew ($post) {
		// データ登録用の連想配列を作成して登録
		return $this->save(array(
			'User' => array(
				'username' => $post['username'],
				'password' => $post['password'],
				'name'     => $post['name'],
				'status'   => 'active',
			),
		));
	}
}
