<?php
App::uses('AppModel', 'Model');
App::import('Model', 'MongoData');	// import other Model
App::import('Model', 'AppData');	// - 20160708

class Dataset extends AppModel {
	var $useTable = 'dataset';	// table name
	public $primaryKey = 'data_id';	// primary key (ID) (add)

	// Relation
	public $hasMany = array(
		// 共有・公開申請情報
		'AppData' => array(
			'className' => 'AppData',
			'foreignKey' => 'data_id',
			'conditions' => array('delflg' => 0),
			'dependent' => true,
		),
		// 共有情報
		'Share' => array(
			'className' => 'Share',
			'foreignKey' => 'data_id',
			'fields' => array('group'),
			'conditions' => array('delflg' => 0),
			'dependent' => true,
		),
		// 共有情報
		'Publish' => array(
			'className' => 'Publish',
			'foreignKey' => 'data_id',
			'fields' => array('app_id'),
			// NIJC
			//'conditions' => array('status' => 'Release', 'delflg' => 0),
			'conditions' => array('delflg' => 0),
			'dependent' => true,
		),
		// 共有取り下げ情報 (データロック) - 20160708
		'AppLock' => array(
			'className' => 'AppData',
			'foreignKey' => 'data_id',
			'conditions' => array('app_type' => 'WithdrawalOfShare', 
									'status' => 'Accepted', 'delflg' => 1),
		),
	);

	// Relation
	public $hasOne = array(
		// 公開申請情報
		'PublishAppData' => array(
			'className' => 'AppData',
			'foreignKey' => 'data_id',
			'conditions' => array('app_type' => 'Release', 'delflg' => 0),
		)
	);

	// Relation
	public $belongsTo = array(
		// オーナー情報
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'owner',
			'fields' => array('username', 'name', 'detail', 'home_dir'),
		),
		'UserAuth' => array(
			'className' => 'UserAuth',
			'foreignKey' => 'owner',
			'fields' => array('username', 'group', 'role'),
		),
	);

	// relate MongoDB
	public $hasOneMongo = array(
		'Mongo' => array(
			'className' => 'MongoData',
			'foreignKey' => 'data_id',
		)
	);

	// Find 結果を整形
	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			// 共有・公開申請のステータスを数える
			$count = array();
			if (isset($val['AppData'])) {
				foreach ($val['AppData'] as $app) {
					$type   = $app['app_type'];
					$status = $app['status'];
					if (empty($count[$type][$status])) {
						$count[$type][$status] = 1;
					} else {
						$count[$type][$status]++;
					}
				}
			}
			$results[$key]['AppCount'] = $count;

			// 公開状態のステータスをコピー
			$results[$key]['publish_status'] =
				isset($val['PublishAppData']) ?
					  $val['PublishAppData']['status'] : null;

			// データロック (共有取り下げ情報) - 20160708
			$results[$key]['locked'] =
				isset($val['AppLock']) ? count($val['AppLock']) : null;
		}
		return $results;
	}

	// MongoDB のデータとリンクしながら find (override)
	public function find($type = 'first', $query = array()) {
		$results = parent::find($type, $query);		// override find

		//+ MongoDB のデータとリンク +//

		// 取得形式が "list", "neighbors" のときは返す
		if ($type == 'list' or $type == 'neighbors') return $results;

		// 取得する階層数 (recursive == -1 のときは返す)
		$recursive = isset($query['recursive']) ? $query['recursive'] : $this->recursive;
		if (empty($results) or $recursive <= 0) return $results;

		// 準備
		$pk = $this->primaryKey;	// primaryKey

		// データが single か list か
		$is_single = $type == 'first' || $type == 'item'
		          || ! ctype_digit( join('', array_keys($results)) );
		if (!empty($query['as'])) $is_single = false;	// IDキー指定のとき

		// MongoDB と連携
		foreach ($this->hasOneMongo as $relkey => $rel) {
			$Mongo = new $rel['className'];	// Class MongoDB

			// $type = first 関連のとき
			if ($is_single) {
				$id = isset($results[$pk]) ? $results[$pk]
				                           : $results[ $this->name ][$pk];
				$results[$relkey] = $Mongo->find('item', array(
					'conditions' => array($rel['foreignKey'] => $id),
				));
			}
			// $type = all 関連のとき
			else {
				foreach (array_keys($results) as $i) {
					$id = isset($results[$i][$pk]) ? $results[$i][$pk]
					                               : $results[$i][ $this->name ][$pk];
					$results[$i][$relkey] = $Mongo->find('item', array(
						'conditions' => array($rel['foreignKey'] => $id),
					));
				}
			}
		}
		return $results;
	}

	// ID を指定して取得
	public function findById($id, $options = array()) {
		return $this->find('item', array_merge(array(
			'conditions' => array($this->name .'.'. $this->primaryKey => $id)
		), $options));
	}

	// ユーザーが共有しているデータを取得
	public function findWithShared ($type, $op = array()) {
		// 基本式
		$options = array(
			// JOIN UserAuth .. 2016/03/05
			'joins' => array(array(
				'table' => 'share', 'alias' => 'Share',
				'type' => 'INNER',
				'conditions' => array('Share.data_id = Dataset.data_id')
			), array(
				'table' => 'user_auth', 'alias' => 'SharedUserAuth',
				'type' => 'INNER',
				'conditions' => array('SharedUserAuth.group = Share.group')
			)),
			'fields' => '*', 'recursive' => 1,
			'conditions' => array('and' => array()),
		);

		// WHERE 条件
		if (isset($op['conditions'])) {
			$options['conditions'] = array('and' => array_merge(
				$op['conditions']
			));
			unset($op['conditions']);	// 後で merge が重複するので削除
		}
		// WHERE 条件 (ログインユーザーのものに限るかどうか)
		if (array_key_exists('username', $op)) {
			// UserAuth condition .. 2016/03/05
			$options['conditions'] = array('and' => array_merge(
				array('SharedUserAuth.username' => $op['username']),
				$options['conditions']['and']
			));
		}
		// ID があれば
		if (array_key_exists('id', $op)) {
			$options['conditions'] = array('and' => array_merge(
				array($this->name .'.'. $this->primaryKey => $op['id']),
				$options['conditions']['and']
			));
		}
		// find
		return $this->find($type, array_merge($options, $op));
	}

	// get next ID
	public function getNextID ($group_name = null, $want_prefix = false) {
		$max_id = 0;

		// get max data_id number
		$data = $this->find('first', array(
			'fields'=> array('MAX(RIGHT('. $this->primaryKey .', 6)) AS max_id'),
			//'conditions' => array('data_type' => $group_name),
			'recursive' => -1,
		));
		foreach ($data as $dt) {
			if (isset($dt['max_id'])) $max_id = intval($dt['max_id'], 10);
		}

		// ID を返す (接頭辞付, 数値のみ)
		if ($want_prefix) {
			return sprintf('%s_%06d', mb_strtolower($group_name), $max_id + 1);
		} else {
			return sprintf('%010d', $max_id + 1);
		}
	}

	// JSON デコードデータからデータ登録
	// NIJC(20160628)
	//public function saveByPost ($post, $username = null, $id = null) {
	public function saveByPost ($post, $username = null, $id = null, $old_dir_name = null) {
		if (empty($post['data_type'])) return;

		// ディレクトリパス取得
		if (!isset($post['dir_name'])) $post['dir_name'] = '';

		// NIJC(20160624): renamed uploader to submitter
		$post["submitter"] = $username;
		// "uploader" にユーザー名を付加
		//if (empty($post['uploader'])) {
		//	$post['uploader'] = array( array('name' => $username) );
		//} else if (empty($post['uploader'][0]['name'])) {
		//	$post['uploader'][0]['name'] = $username;
		//}

		// ID発行(データタイプごと)
		$post['data_id'] = $id ?: $this->getNextID($post['data_type']);

		// MongoDB と連携 (保存)
		foreach ($this->hasOneMongo as $relkey => $rel) {
			$Mongo = new $rel['className'];	// Class MongoDB

			// 前のデータがあれば削除
			if ($id) {	// update / delete
				$Mongo->deleteAll(array($rel['foreignKey'] => $id));
			}
			// 新規データを追加
			$Mongo->save($post);
		}

		// データ登録用の連想配列を作成
		// NIJC(20160628)
		//$new_data = array(
		//	'data_id'   => $post['data_id'],
		//	'dir_name'   => $post['dir_name'],
		//	'owner'     => $username,	// ログインユーザー名
		//);
		//foreach (array('dir_name', 'data_type') as $k) {
		//	if (isset($post[$k])) $new_data[$k] = $post[$k];
		//}
		$new_data = array(
			'data_id' => $post['data_id'],
			'owner'   => $username,
			'upd_date' => date('Y/m/d H:i:s')	// NIJC(20160713)
		);
		if (isset($post['data_type'])) {
			$new_data['data_type'] = $post['data_type'];
		}
		if (!$old_dir_name) {
			$new_data['dir_name'] = $post['dir_name'];
		}

		// データ登録 (MySQL)
		return $this->save($new_data);
	}

	// JSON からデータ登録
	// NIJC(20160628)
	//public function saveByJSON ($json, $username = null, $id = null) {
	public function saveByJSON ($json, $username = null, $id = null, $old_dir_name = null) {
		// 文字化けするかもしれないのでUTF-8に変換
		$json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');

		// オブジェクト毎にパース
		// trueを付けると連想配列として分解して格納
		$post = json_decode($json, true);

		// JSON デコードデータからデータ登録
		// NIJC(20160628)
		//return $this->saveByPost($post, $username, $id);
		return $this->saveByPost($post, $username, $id, $old_dir_name);
	}

	// フィールドを更新
	public function saveTo ($data_id, $name, $value) {
		return $this->save(array('data_id' => $data_id, $name => $value));
	}

	// ディレクトリの関連付けを設定
	public function saveToDirName ($data_id, $dir_name, $ori_dir_name = null) {
		// 元となるデータセットのディレクトリが更新された際は、メタデータの dir_name を更新する？
		if ($ori_dir_name) {
			foreach ($this->hasOneMongo as $relkey => $rel) {
				$Mongo = new $rel['className'];	// Class MongoDB

				// データを更新
				$Mongo->updateAll(
					array('dir_name' => $ori_dir_name),	// data
					array('data_id'  => $data_id)	// where
				);
			}
		}

		// データ登録用の連想配列を作成
		$new_data = array(
			'data_id'  => $data_id,
			'dir_name' => $dir_name,
		);
		if ($ori_dir_name) $new_data['original_dir_name'] = $ori_dir_name;

		// データ更新 (MySQL)
		return $this->save($new_data);
	}

	// NIJC(20160701): ディレクトリの関連付け解除
	public function saveForUnlink($data_id) {
		foreach ($this->hasOneMongo as $relkey => $rel) {
			$Mongo = new $rel['className'];
			$Mongo->updateAll(
				array('dir_name' => ''),
				array('data_id'  => $data_id)
			);
		}

                $new_data = array(
                        'data_id'           => $data_id,
                        'dir_name'          => '',
			'original_dir_name' => ''
                );
                return $this->save($new_data);
	}

	// 現在のデータセットディレクトリを取得
	public function getDatasetDir ($data_id) {
		$item = $this->find('item', array(
			'fields' => 'dir_name', 'recursive' => -1,
			'conditions' => array($this->primaryKey => $data_id),
		));
		if (isset($item['dir_name'])) return $item['dir_name'];
		return;
	}

	// 元のデータセットディレクトリを取得
	public function getOriginalDir ($data_id) {
		$item = $this->find('item', array(
			'fields' => 'original_dir_name', 'recursive' => -1,
			'conditions' => array($this->primaryKey => $data_id),
		));
		if (isset($item['original_dir_name'])) return $item['original_dir_name'];
		return;
	}

	// データセットのデータタイプを取得
	public function getDataType ($data_id) {
		$item = $this->find('item', array(
			'fields' => 'data_type', 'recursive' => -1,
			'conditions' => array($this->primaryKey => $data_id),
		));
		if (isset($item['data_type'])) return $item['data_type'];
		return;
	}

	// データセットの所有者を取得
	public function getOwner ($data_id) {
		$item = $this->find('item', array(
			'fields' => 'owner', 'recursive' => -1,
			'conditions' => array($this->primaryKey => $data_id),
		));
		if (isset($item['owner'])) return $item['owner'];
		return;
	}

	// ユーザーがデータを編集可能かどうかチェック
	public function is_editable ($id, $username) {
		// データセット（IDなら取得しにいく）
		$item = (is_array($id)) ? $id : $this->findById($id);

		// （ログイン）ユーザー情報（IDなら取得しにいく）
		$user = $username;
		if (!is_array($user)) {
			$Users = new User();
			$user = $Users->findById($username);
		}
		if (!$item or !$user) return;	// null

		// Dataset.owner が同ユーザーであれば true
		if ($item['owner'] == $user['username']) return true;

		// またはシステム管理者
		if (isset($user['UserAuth'])
		and $user['UserAuth']['role'] == 'SystemAdmin') return true;

		// またはグループ管理者、システム管理者
		if (isset($user['UserAuth'])
		and isset($item['UserAuth'])
		// NIJC and $user['UserAuth']['role'] == 'GroupAdmin'
		and $user['UserAuth']['role'] != ''
		and $item['UserAuth']['group'] == $user['UserAuth']['group']) return true;
			// (*memo: 自グループ内の誰しもが編集できる？) - 20160708

		return false;
	}

	// データが申請中/承認済かどうかチェック
	public function is_app ($id) {
		// データセット（IDなら取得しにいく）
		$item = (is_array($id)) ? $id : $this->findById($id);

		// 公開/共有状態の数をチェック
		$counts = isset($item['AppCount']) ? $item['AppCount'] : array();

		// 申請中の状態であるか
		// if (!empty($counts['Release']['申請中'])
		// or  !empty($counts['Release']['承認済'])
		// or  !empty($counts['共有']['申請中'])
		// or  !empty($counts['共有']['Accepted'])) return true;
		if (!empty($counts['Release']['Processing'])
		or  !empty($counts['Release']['Accepted'])
		or  !empty($counts['Share']['Processing'])
		or  !empty($counts['Share']['Accepted'])) return true;

		return false;
	}

	// データセットディレクトリが外部グループ間共有のパスであるか
	public function is_dir_intergroup ($data_id) {
		return $this->getDatasetDir($data_id) == sprintf(DIR_INTERGROUP, $data_id);
	}

	// データセットディレクトリが自グループ内共有のパスであるか
	public function is_dir_intragroup ($data_id, $group_id = null) {
		if (!$group_id) {
			$item = $this->findById($data_id, array('recursive' => 0));
			$group_id = isset($item['UserAuth']['group']) ? $item['UserAuth']['group'] : null;
		}
		if (!$group_id) return;

		return $this->getDatasetDir($data_id) == sprintf(DIR_INTRAGROUP, $group_id, $data_id);
	}

	// データがロックされているか
	public function is_locked ($id) {
		// データがロックされているか判別するクラスとオブジェクト
		$rel = $this->hasMany['AppLock'];
		$lock = new $rel['className'];	// Class Lock

		// データ取得
		$item = $lock->find('item', array(
			'conditions' => array($rel['foreignKey'] => $id) + $rel['conditions'],
			'recursive' => -1,
		));
		return !empty($item);
	}

}
