<?php
App::uses('AppModel', 'Model');

class Publish extends AppModel {
	var $useTable = 'publish';	// table name
	public $primaryKey = 'app_id';	// primary key (ID) [add]

	// ID を指定して取得
	public function findById($id) {
		return $this->find('item', array(
			'conditions' => array($this->name .'.'. $this->primaryKey => $id)
		));
	}

	// POST データからデータ登録
	public function saveByPost ($post, $username = null) {

		// まとめられた Experiment データがあれば分割
		if (isset($post['exp_list'])) {
			$exp_list = explode("|", $post['exp_list']);
			foreach (array('theme_id', 'sub_theme_id', 'experiment_id',
							'theme', 'sub_theme', 'experiment') as $i => $name) {
				$post[$name] = isset($exp_list[$i]) ? $exp_list[$i] : null;
			}
		}

		// データ登録用の連想配列を作成
		$new_data = array(
			'app_id'    => $post['app_id'],
			'data_id'   => $post['data_id'],
			'applicant' => $username,		// ログインユーザー名
			'status'    => '申請中',

			// Experiment data
			'theme_id'      => $post['theme_id'],
			'sub_theme_id'  => $post['sub_theme_id'],
			'experiment_id' => $post['experiment_id'],
			'theme'         => $post['theme'],
			'sub_theme'     => $post['sub_theme'],
			'experiment'    => $post['experiment'],
		);

		//メタデータ/データセット確認、既に共有(承認)されているか
		$this->save($new_data);

		return $post['app_id'];
	}

	// ステータスを "承認済" に設定
	public function saveAsAccept ($post) {
		if (empty($post['app_id'])) return false;

		return $this->save(array(
				'app_id' => $post['app_id'],
				// NIJC 'status' => '承認済',
				'status' => 'Accepted',
		));
	}

	// ステータスを "棄却" に設定
	public function saveAsReject ($post) {
		if (empty($post['app_id'])) return false;

		return $this->save(array(
				'app_id' => $post['app_id'],
				// NIJC 'status' => '棄却',
				'status' => 'Rejected',
		));
	}

	// ステータスを "取り下げ" に設定
	public function saveAsWithdraw ($data_id) {
		if (empty($data_id)) return false;

		return $this->updateAll(
				// NIJC array ( 'status' => "'" . '取り下げ' . "'" ),
				array ( 'status' => "'" . 'Withdrawn' . "'" ),
				array ( 'data_id' => $data_id)
		);
	}

}
