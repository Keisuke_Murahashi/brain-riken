<?php
App::uses('AppModel', 'Model');

class AppMongoModel extends AppModel {
	// DB接続設定をMongoDBに切り替え
	var $useDbConfig = 'default_mongo';

	// ORマッパーを使えるように変更
	var $actsAs = array(
		'Mongodb.SqlCompatible'
	);
}
