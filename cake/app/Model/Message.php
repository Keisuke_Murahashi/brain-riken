<?php
App::uses('AppModel', 'Model');
App::import('Model', 'Target');	// import other Model

class Message extends AppModel {
	var $useTable = 'message';	// table name
	public $primaryKey = 'mess_id';	// primary key (ID)

	// Relation
	public $hasMany = array(
		'Target' => array(
			'className' => 'Target',
			'foreignKey' => 'mess_id',
			'dependent' => true,
		)
	);

        // Valiadte required fields
        public $validate = array(
                'title' => array(
                        'required' => array(
                                'rule' => 'notEmpty',
                                'message' => 'タイトルは必須です。'
                        )
                ),
                'body' => array(
                        'required' => array(
                                'rule' => 'notBlank',
                                'message' => 'メッセージ本文は必須です。'
                        )
                )
        );

	// POST データからデータ登録
	public function saveByPost ($post, $username = null) {
		// データ登録用の連想配列を作成
		$new_data = array(
			'mess_type' => $post['mess_type'],
			'title'     => $post['title'],
			'content'   => $post['content'],
			'category'  => isset($post['category']) ? $post['category'] : '',
			'author'    => $username,	// ログインユーザー名
			'date'      => date('Y/m/d H:i:s'),
		);
		// 保存
		$this->save($new_data);

		// message ID
		$post['mess_id'] = $this->getLastInsertID();

		// Target があれば
		if (isset($post['target'])) {
			// 対象ユーザーの重複を取り除く
			$post['target'] = array_unique($post['target']);

			// Target Model
			$TargetModel = new Target();

			// target テーブルにも保存
			foreach ($post['target'] as $target) {
				$TargetModel->saveByPost(array(
					'mess_id' => $post['mess_id'],
					'target' => $target
				));
			}
		}
		return $post['mess_id'];
	}

	// Find 結果を整形
	public function afterFind($results, $primary = false) {
		$type_title = array(
			'' => '',
			'user' => '個人宛て通知',
			'portal' => '皆様へのお知らせ',
			'system' => 'システムメッセージ',
		);
//		$ctgr_class = array(
//			'' => '', 'News' => 'fa-folder', 'Message' => 'fa-envelope-o',
//		);
		foreach ($results as $key => $val) {
			if (empty($val['Message'])) continue;

			// add category_class name
//			$results[$key]['Message']['category_class'] =
//				$ctgr_class[ isset($val['Message']['category']) ?
//				                   $val['Message']['category']  : '' ];

			// add mess_type title
			$results[$key]['Message']['mess_type_title'] =
				$type_title[ isset($val['Message']['mess_type']) ?
				                   $val['Message']['mess_type']  : '' ];

			// add target as string
			$results[$key]['Message']['targets'] = isset($val['Target']) ?
					implode("\n", Hash::extract($val['Target'], '{n}.target')) : '';
		}
		return $results;
	}

	// Join Condition .. 対象ユーザーを指定して取得
	public function joinsTarget($username) {
		return array(
			'table' => 'target', 'alias' => 'Target',
			'type' => 'INNER',
			'conditions' => array(
				'Target.mess_id = Message.mess_id',
				"Target.target = '" . $username ."'")
		);
	}

	// Join Condition .. 既読情報と一緒に取得
	public function joinsRead($username) {
		return array(
			'fields' => array('read', 'res'),
			'table' => 'message_read', 'alias' => 'MessageRead',
			'type' => 'LEFT',
			'conditions' => array(
				'MessageRead.mess_id = Message.mess_id',
				"MessageRead.username = '" . $username ."'")
		);
	}

	// Find .. 対象ユーザーを指定して取得
	public function findByTarget($type = 'first', $username, $query = array()) {
		$query += array('joins' => array(), 'fields' => '*');

		// add join
		$query['joins'][] = $this->joinsTarget($username);

		// find
		return $this->findWithRead($type, $username, $query);
	}

	// Find .. 既読情報と一緒に取得
	public function findWithRead($type = 'first', $username = null, $query = array()) {
		$query += array('joins' => array(), 'fields' => '*',
						'conditions' => array());

		// add join
		$query['joins'][] = $this->joinsRead($username);

		// make read/unread condition
		if (isset($query['read'])) {
			$query['conditions'][] = $query['read'] ?
				array(
					'MessageRead.read' => 1
				) : array('OR' => array(
						array('MessageRead.read' => null),
						array('MessageRead.read' => 0),
				));
		}

		// find
		return $this->findByMonth($type, $query);
	}

	// Find .. 年月を指定して取得
	public function findByMonth($type = 'first', $query = array()) {
		// make date condition
		$conditions = array();
		$year  = isset($query['year'])  ? intval($query['year'])  : 0;
		$month = isset($query['month']) ? intval($query['month']) : 0;
		if ($year and $month) {
			$conditions["DATE_FORMAT(date, '%Y/%m')"] =
				sprintf('%04d/%02d', $year, $month);
		} else if ($year) {
			$conditions["DATE_FORMAT(date, '%Y')"] =
				sprintf('%04d', $year);
		}
		// merge where conditions
		$query['conditions'] = array_merge(
			$conditions,
			isset($query['conditions']) ? $query['conditions'] : array()
		);
		// find
		return $this->find($type, $query);
	}

	// Count .. 対象ユーザーが未読なメッセージの件数を取得
	public function countByTarget($username, $query = array()) {
		$query += array('recursive' => 0, 'callbacks' => false);

		// find as count
		return $this->findByTarget('count', $username, $query);
	}

	// Count .. 既読情報と一緒にメッセージの件数を取得
	public function countWithRead($username, $query = array()) {
		$query += array('recursive' => 0, 'callbacks' => false);

		// find as count
		return $this->findWithRead('count', $username, $query);
	}

	// Count .. 年月ごとに件数を取得
	public function countsByMonth($type = 'first', $username = null, $query = array()) {
		// バーチャルフィールドを追加
		$this->virtualFields['cnt'] = 0;
		$this->virtualFields['month'] = 0;

		// 基本式
		$options = array(
			'fields'=> array(
				"DATE_FORMAT(date, '%Y/%m') AS Message__month",
				'COUNT(*) AS Message__cnt',
			),
			'group' => array("DATE_FORMAT(date, '%Y/%m')"),
			'order' => 'Message__month DESC',
			'recursive' => 0, 'callbacks' => false,
		);

		// 対象ユーザーの指定
		if ($username) {
			$options['joins'] = array( $this->joinsTarget($username) );
		}

		// find
		return $this->find($type, array_merge($options, $query));
	}
}
