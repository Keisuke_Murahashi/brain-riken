<?php
App::uses('AdminController', 'Controller');
App::import('Vendor', 'Util');

class SearchController extends AdminController {
	public $uses = array('Message', 'Dataset', 'DataType', 'MongoData','UserAuth');

	public function index ($q = null) {
		// GET/POST
		if ($this->request->is('get'))  return $this->query($q);
		if ($this->request->is('post')) return $this->search($q);
	}

	// search by keyword (method="GET")
	public function query ($keyword = null) {
		$vars = array('keyword', 'count_all',
						'data_metadata', 'data_pages', 'data_documents');

		// 検索キーワード
		if (!$keyword) $keyword = $this->request->query('keyword');

		// search in metadata
		$data_metadata = $this->_query_metadata($keyword);

		// search in static pages
		$data_pages = $this->_query_pages($keyword);

		// search in document files
		$data_documents = $this->_query_documents($keyword);

		// カウント
		$count_all = count($data_metadata) + count($data_pages) + count($data_documents);

		// View に変数をまとめてセット
		$this->set(compact($vars));
	}

	// search by conditions (method="POST")
	public function search ($q = null) {
		$vars = array('conditions', 'count_all', 'data_types', 'data_metadata');
		$post = $this->request->data;

		// 検索条件を抽出 (以前)
		$conditions = array();

		// 検索条件書式その２のとき
		if (isset($post['data_type']) and isset($post['field'])) {
			$type = $post['data_type'];

			// 順番に検索条件にセット
			foreach ($post['field'] as $i => $field) {
				if (!$field or !isset($post['value'][$i])) continue;

				$value = trim($post['value'][$i]);
				if ($value != '') $conditions[] = array(
					'data_type' => $type,
					'field' => $field,
					'value' => $value,
				);
			}
		}
		// 検索条件書式その１のとき
		else {
			foreach ($post as $type => $data) {
				if (!is_array($data)) continue;	// 値が配列のときに限る

				foreach ($data as $field => $value) {
					$value = trim($value);
					if ($value != '') $conditions[] = array(
						'data_type' => $type,
						'field' => $field,
						'value' => $value,
					);
				}
			}
		}

		// search in metadata
		$data_metadata = $this->_search_metadata($conditions);

		// カウント
		$count_all = count($data_metadata);

		// 検索条件 : データタイプ一覧
		$data_types = $this->DataType->getDataTypes();

		// View に変数をまとめてセット
		$this->set(compact($vars));
	}

	// search in metadata (method="POST")
	public function _search_metadata ($conditions) {
		$username = $this->Auth->user('username');	// ログインユーザー名
		$group_id = $this->getGroupId();	// ログインユーザーのグループID (短縮)

		// データセット (Mongo データはまだ不要) (ID をキーに)
		$data_asset = $this->Dataset->find('items', array(
				'joins' => array(array(
						'table' => 'share', 'alias' => 'Share',
						'type' => 'INNER',
						'conditions' => array('Share.data_id = Dataset.data_id', 'Share.delflg = 0')
				)),
				'conditions' => array('Share.group' =>  $group_id),
			'recursive' => 0, 'as' => true,
		));

		// 部分一致検索をしたいフィールド
		$like_fields = preg_split("/[\s,]+/", Configure::read('Site.search_like_fields') ?: 'title,subject');
		$like_fields = array_fill_keys($like_fields, true);

		// make where conditions
		$where = array();
//		$where[] = array('owner' => $username);	// in my dataset

		foreach ($conditions as $cond) {
			$c = array();
			// data_type
			if ($cond['data_type'] and $cond['data_type'] != 'ALL'
			                       and $cond['data_type'] != 'DataType') {
				$c['data_type'] = $cond['data_type'];
			}
			// any field
			if ($cond['data_type'] and $cond['data_type'] != 'Field') {
				$c[ $cond['field'] ] = $cond['value'];

				// as LIKE
				if (array_key_exists($cond['field'], $like_fields)) {
					$c[ $cond['field'] ] = new MongoRegex(
						'/'. preg_quote($cond['value'], '/') .'/'
					);
				}
			}
			$where[] = $c;
		}

		// and/or
		$andor = strtolower($this->request->data('iro'));
		$andor = '$'. ($andor ?: 'and');

		// MongoDB での検索条件
		$wheres = array(
			'data_id' => array('$in' => array_keys($data_asset)),
		);
		if ($where) $wheres[$andor] = $where;

		// MongoDB で全文検索 (※要 "text" インデックス)
		$data_meta = $this->MongoData->find('items', array(
			'conditions' => $wheres,
		));

		// 検索結果のデータを統合する
		$data_metadata = array();
		foreach ($data_meta as $i => $meta) {
			$item = $data_asset[ $meta['data_id'] ];
			$item['Mongo'] = $meta;
			$data_metadata[] = $item;
		}
		return $data_metadata;
	}

	// search in metadata (method="GET")
	public function _query_metadata ($keyword) {
		$username = $this->Auth->user('username');	// ログインユーザー名
		$group_id = $this->getGroupId();	// ログインユーザーのグループID (短縮)

		// データセット (Mongo データはまだ不要) (ID をキーに)
		$data_asset = $this->Dataset->find('items', array(
				'joins' => array(array(
						'table' => 'share', 'alias' => 'Share',
						'type' => 'INNER',
						'conditions' => array('Share.data_id = Dataset.data_id', 'Share.delflg = 0')
				)),
				'conditions' => array('Share.group' =>  $group_id),
			'recursive' => 0, 'as' => true,
		));

		// MongoDB での検索条件
		$wheres = array(
			'data_id' => array('$in' => array_keys($data_asset)),
		);
		if ($keyword) $wheres['$text'] = array('$search' => $keyword);

		// MongoDB で全文検索 (※要 "text" インデックス)
		$data_meta = $this->MongoData->find('items', array(
			'conditions' => $wheres,
		));

		// 検索結果のデータを統合する
		$data_metadata = array();
		foreach ($data_meta as $i => $meta) {
			$item = $data_asset[ $meta['data_id'] ];
			$item['Mongo'] = $meta;
			$data_metadata[] = $item;
		}
		return $data_metadata;
	}


	// search in static pages (method="GET")
	public function _query_pages ($keyword) {
		// get JSON from FESS
		$search_doc_limit = Configure::read('Site.search_doc_limit') ?: 100;

		//$url = "http://localhost:8080/fess/json?query=$keyword&num=100";
		$url = FESS . sprintf("/json?query=%s&num=%d", urlencode($keyword), $search_doc_limit);
		$json = file_get_contents($url);
		if (empty($json)) return array();

		// decode JSON
		$json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
		$result = json_decode($json, true);
		if (empty($result) or empty($result["response"]["result"])) return array();

		// レコード件数があるか
		$result_count = isset($result["response"]["recordCount"]) ?
		                      $result["response"]["recordCount"]  : 0;
		if ($result_count <= 0) return array();

		// HTMLのみ結果を抽出
		return $this->_find_pages_with($result, $this->Auth->user('username'));
	}

	// search in document files (method="GET")
	public function _query_documents ($keyword) {
		// get JSON from FESS
		$search_doc_limit = Configure::read('Site.search_doc_limit') ?: 100;

		//$url = "http://localhost:8080/fess/json?query=$keyword&num=100";
		$url = FESS . sprintf("/json?query=%s&num=%d", urlencode($keyword), $search_doc_limit);
		$json = file_get_contents($url);
		if (empty($json)) return array();

		// decode JSON
		$json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
		$result = json_decode($json, true);
		if (empty($result) or empty($result["response"]["result"])) return array();

		// レコード件数があるか
		$result_count = isset($result["response"]["recordCount"]) ?
		                      $result["response"]["recordCount"]  : 0;
		if ($result_count <= 0) return array();

		// オーナーを検索しながらデータを返す (共有されたデータセットのみ)
		return $this->_find_documents_with($result, $this->Auth->user('username'));
	}

	// find documents with owner
	public function _find_documents_with ($result, $username = null) {
		$result_items = array();

		// オーナー検索対象データ
		$dataset = array();

		// URL RegExp
		$url_reg_intra = str_replace('%s', '([0-9A-Za-z]+)', DIR_INTRAGROUP);
		$url_reg_inter = str_replace('%s', '([0-9A-Za-z]+)', DIR_INTERGROUP);

		// 検索結果からデータIDを取得
		$data_ids = array();
		foreach ($result["response"]["result"] as $i => $item) {
			// URL slash
			if (strpos($item['url'], '\\')) {
				$item['url'] = str_replace('\\', '/', $item['url']);
			}

			// URL match
			$match = array();
			if (preg_match('!'. $url_reg_intra .'!', $item['url'], $match)
			or  preg_match('!'. $url_reg_inter .'!', $item['url'], $match)) {
				// データIDを設定 (データID はおそらく URL の後方)
				$data_ids[] = $item['data_id'] = array_pop($match);
				$result_items[] = $item;
			}
		}
		if (count($data_ids) == 0) return array();	// 該当する検索結果なし

		// 共有データのみ
		if (isset($username)) {
			// 自・所属グループに共有されたデータセット
			$dataset = $this->Dataset->findWithShared('items', array(
				'fields' => array('data_id', 'dir_name', 'owner'),
				'username' => $username,
				'recursive' => 0, 'as' => 'data_id',
				'id' => array_unique($data_ids),	// データID (重複しない)
			));
		}
		// 全データ
		else {
			// get Dataset (データのオーナー決定用)
			$dataset = $this->Dataset->find('items', array(
				'fields' => array('data_id', 'dir_name', 'owner'),
				'recursive' => -1, 'as' => 'data_id',
				'id' => array_unique($data_ids),	// データID (重複しない)
			));
		}

		// ローカルパスをあらかじめ変換
		foreach (array_keys($dataset) as $i) {
			$dataset[$i]['dir_name_lower'] =
				strtolower( str_replace('\\', '/', $dataset[$i]['dir_name']) );
		}

		// 返り値とする新しい配列を作る
		$data_documents = array();
		foreach ($result_items as $i => $item) {
			// 対応する Dataset (共有データ) (見つからなければスキップ)
			if (empty($item['data_id']) or empty($dataset[ $item['data_id'] ])) continue;
			$dataset_item = $dataset[ $item['data_id'] ];

			//+ 返り値の要素として
			$ret = array();

			// 対応する Dataset から
			foreach (array('data_id', 'owner', 'dir_name', 'dir_name_lower') as $k) {
				$ret[$k] = $dataset_item[$k];
			}

			// 対応する検索結果情報から (urldecode)
			foreach (array('title', 'url', 'site', 'contentDescription',
			               'digest', 'mimetype', 'id', 'urlLink') as $k) {
				$ret[$k] = urldecode($item[$k]);
			}

			// path .. delete "file:///" from URL
			$ret['path'] = preg_replace('!^file:/+!', '', $ret['url']);
			if (!file_exists($ret['path']) and file_exists('/'. $ret['path'])) {
				$ret['path'] = '/'. $ret['path'];
			}

			// real path
			$ret['realpath'] = realpath($ret['path']);

			// 本来のパスが得られなければスキップ
			// .. ※日本語ファイルなどは文字コードの違いで見つけられない場合あり
 			if (!$ret['realpath']) continue;
//			if (stripos($ret['path'], $ret['dir_name_lower']) === false) continue;

			// file base name
			$ret['filename'] = basename($ret['path']);

			// file base path .. データ基本ディレクトリ名を除いたファイル名
			$ret['filepath'] = $ret['path'];
//			$ret['filepath'] = str_ireplace($ret['dir_name_lower'],   '', $ret['filepath']);
			$ret['filepath'] = preg_replace('!'. $url_reg_intra .'!', '', $ret['filepath']);
			$ret['filepath'] = preg_replace('!'. $url_reg_inter .'!', '', $ret['filepath']);
			$ret['filepath'] = preg_replace('!^/+!', '', $ret['filepath']);

			// file updated date
			$utime = isset($item['lastModified']) ? strtotime($item['lastModified']) : null;
			$ret['file_date'] = $utime ? date('Y/m/d H:i:s', $utime) : null;

			// file updated date
			$ret['icon'] = Util::icon_class($item['filetype_s']);

			// add item to items
			$data_documents[] = array_merge($item, $ret);
		}
		return $data_documents;
	}

	// find html pages
	public function _find_pages_with ($result, $username = null) {

		// 新しい配列を作る
		$data_pages = array();
		foreach ($result["response"]["result"] as $i => $item) {
			// content-type="text/html" 以外は不要
			if ($item['mimetype'] != 'text/html') continue;

			// urldecode
			foreach (array('title', 'url', 'site', 'contentDescription',
					'digest', 'mimetype', 'id', 'urlLink', 'filetype_s') as $k) {
					$item[$k] = urldecode($item[$k]);
			}
			// add item to items
			$data_pages[] = array_merge($item);
		}
		return $data_pages;
	}
}
