<?php
App::uses('AdminController', 'Controller');

class DownloadController extends AdminController {
	public $uses = array('Dataset');

	// download
	public function index ($data_id = null, $filename = null) {
		$username = $this->Auth->user('username');	// ログインユーザー名
		$dataset = array();

		// View を使用しない
		$this->autoRender = false;

		// ID, ファイル名を取得
		if (!$data_id)  $data_id  = $this->request->data('id');
		if (!$data_id)  $data_id  = $this->request->query('id');
		if (!$filename) $filename = $this->request->data('file');
		if (!$filename) $filename = $this->request->query('file');

		// ID, ファイル名が無ければ返す
		if (!$data_id)  return $this->_error_redirect(l('_no_dl_data'));
		if (!$filename) return $this->_error_redirect(l('_no_dl_file_name'));

		//++ 指定されたダウンロードファイルに対して、ユーザーがアクセス可能かチェック

		// 共有データのみ取得
		if (isset($username)) {
			// 自・所属グループに共有されたデータセット
			$dataset = $this->Dataset->findWithShared('item', array(
				'fields' => array('data_id', 'dir_name', 'owner'),
				'id' => $data_id,
				'username' => $username,
				'recursive' => 0,
			));
		}

		// 共有データがない == ユーザーがアクセス可能でなかったようなら返す
		if (!$dataset) return $this->_error_redirect(l('_no_priv_ref_file'));
		if (!$dataset['dir_name']) return $this->_error_redirect(l('_no_dir_set'));

		// ディレクトリトラバーサル
		$filename = str_replace(array("\x00", '\\'), array('', '/'), $filename);
		if (strpos($filename, '../') !== false)
			return $this->_error_redirect(l('_bad_path'));

		// ファイルのパスを決定
		$filepath = $dataset['dir_name'] .'/'. $filename;

		// ちゃんとしたアドレスにする (偽装URLの解決)
		$filepath = realpath($filepath);

		// ファイルが存在しないようなら返す
		if (!$filepath or !file_exists($filepath))
			return $this->_error_redirect(l('_no_file'));

		// ファイル内容を取得
		$contents = file_get_contents($filepath);

		//++ ダウンロード開始

		// ファイル名を指定
//		header('Content-Disposition: attachment; filename="'. $filename .'"');
		$this->response->download( str_replace('/', '-', $filename));

		// 出力して終了 (Content-Length も出力)
		$this->response->body($contents);
	}

	// download (フルパス用 - 不使用)
	private function _index_20160301 ($url = null) {
		$username = $this->Auth->user('username');	// ログインユーザー名
		$dataset = array();

		// View を使用しない
		$this->autoRender = false;

		// ファイル名を取得
		if (!$url) $url = $this->request->data('file');
		if (!$url) $url = $this->request->query('file');

		// ファイル名が無ければ返す
		if (!$url) return $this->_error_redirect(l('_no_dl_file_name'));

		// ディレクトリトラバーサル
		$url = str_replace(array("\x00", '\\'), array('', '/'), $url);
		if (strpos($url, '../') !== false)
			return $this->_error_redirect(l('_bad_path'));

		// ファイルの存在をチェック
		$filepath = file_exists($url) ? $url : preg_replace('!^/+!', '', $url);

		// ちゃんとしたアドレスにする (偽装URLの解決)
		$filepath = realpath($filepath);
		$filepath_slash = str_replace('\\', '/', $filepath);	// スラッシュ式に変換

		//++ 指定されたダウンロードファイルに対して、ユーザーがアクセス可能かチェック

		// 共有データのみ取得
		if (isset($username)) {
			// 自・所属グループに共有されたデータセット
			$dataset = $this->Dataset->findWithShared('items', array(
				'fields' => array('data_id', 'dir_name', 'owner'),
				'username' => $username,
				'recursive' => 0,
			));
		}
		if (!$dataset) return $this->_error_redirect(l('_no_shared_dataset'));

		// アクセス可能かチェック
		$is_accessible = false;

		//+ データの持ち主を、データのパスで判断する

		// compare path to get data_id
		if ($filepath and file_exists($filepath)) {
			foreach ($dataset as $j => $row) {
				if (empty($row['dir_name'])) continue;

				// ローカルパスをあらかじめ変換
				$dir_name = str_replace('\\', '/', $row['dir_name']);

				// パス検索 (大文字小文字区別しない)
				$match = stripos($filepath_slash, $dir_name);

				//Util::debug(array($match, $filepath_slash, $j, $dir_name), '_search.txt');	// debug

				if ($match === 0) {	// 見つかったら終了
					$is_accessible = true;
					break;
				}
			}
		} else {
			//Util::debug('No file .. '. $filepath_slash, '_search.txt');	// debug
		}

		// ユーザーがアクセス可能でなかったようなら返す
		if (!$is_accessible) return $this->_error_redirect(
			l('_no_dl_file')."\n".
			l('_no_priv_ref_file_or_mov_del'));

		// ファイルが存在しないようなら返す
		if (!$filepath or !file_exists($filepath))
			return $this->_error_redirect(l('_no_file'));

		// ファイル内容を取得
		$contents = file_get_contents($filepath);

		//++ ダウンロード開始
		$filename = basename($filepath);

		// ファイル名を指定
//		header('Content-Disposition: attachment; filename="'. $filename .'"');
		$this->response->download($filename);

		// 出力して終了 (Content-Length も出力)
		$this->response->body($contents);
	}

	// エラーリダイレクト
	protected function _error_redirect ($error) {
		$this->Flash->error($error);	// エラーメッセージ

		// リファラーに遷移
		$this->redirect($this->referer(), 205);
	}
}
