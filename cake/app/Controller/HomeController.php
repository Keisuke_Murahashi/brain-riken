<?php
App::uses('AdminController', 'Controller');

class HomeController extends AdminController {
	public $uses = array('Message', 'Dataset', 'DataType');

	public function index () {
		$username = $this->Auth->user('username');	// ログインユーザー名

		// var names to set
		$vars = array(
			'data_user', 'data_every', 'data_system', 'count_unread',
			'data_types', 'data_shared', 'data_asset',
		);

		// お知らせ (ユーザー通知) (対象:ユーザー)
		$data_user = $this->Message->findByTarget('items', $username, array(
			'conditions' => array('mess_type' => 'user'),
			'limit' => Configure::read('Site.home_message_limit') ?: 5,
			'order' => array('date' => 'desc'),
		));

		// お知らせ (皆様への通知) (対象:全員)
		$data_every = $this->Message->findWithRead('items', $username, array(
			'conditions' => array('mess_type' => 'portal'),
			'limit' => Configure::read('Site.home_message_limit') ?: 5,
			'order' => array('date' => 'desc'),
		));

		// お知らせ (システムメッセージ) (対象:ユーザー)
		$data_system = $this->Message->findByTarget('items', $username, array(
			'conditions' => array('mess_type' => 'system'),
			'limit' => Configure::read('Site.home_message_limit') ?: 5,
			'order' => array('date' => 'desc'),
		));

		// 対象ユーザーが未読なメッセージの件数を取得
		$count_unread = array(
			'user'   => $this->Message->countByTarget($username, array(
				'conditions' => array('mess_type' => 'user'),
				'read' => false,
			)),
			'portal' => $this->Message->countWithRead($username, array(
				'conditions' => array('mess_type' => 'portal'),
				'read' => false,
			)),
			'system' => $this->Message->countByTarget($username, array(
				'conditions' => array('mess_type' => 'system'),
				'read' => false,
			)),
		);

		// 検索条件 : データタイプ一覧
		$data_types = $this->DataType->getDataTypes();

		// Myアセット
		$data_asset = $this->Dataset->find('items', array(
				'conditions' => array('owner' => $username),
				'limit' => Configure::read('Site.home_myassets_limit') ?: 5,
				'order' => array('Dataset.upd_date' => 'desc'),
		));
//		$data_asset = Util::getAppInfo($data_asset, true);

		//$data_asset = $this->Dataset->find('items', array(
		//	// LEFT JOIN
		//	'joins' =>  array(array(
		//		'table' => 'app', 'alias' => 'AppData',
		//		'type' => 'LEFT',
		//		'conditions' => array('AppData.data_id = Dataset.data_id'),
		//	)),
		//	'fields' => '*', 'recursive' => -1,
		//	'conditions' => array('owner' => $username),
		//	'limit' => Configure::read('Site.home_myassets_limit') ?: 5,
		//	'order' => array('Dataset.upd_date' => 'desc'),
		//));

		// データセット (Mongo データも自動連係で取得)
		$data_shared = $this->Dataset->findWithShared('items', array(
			'username' => $username,
			'limit' => Configure::read('Site.home_dataset_limit') ?: 5,
			'order' => array('Dataset.upd_date' => 'desc'),
		));

		// View に変数をセット
		$this->set(compact($vars));
	}
}
