<?php
App::uses('AdminController', 'Controller');

class DocsBaseController extends AdminController {
    public $autoLayout = true;
    public $uses = array();

    protected function index () {
        $this->autoRender = true;
    }

    protected function docs() 
    {
        $this->response->file(APP . 'docs' . DS . implode(DS, $this->request->params['pass']));
        return $this->response;	
    }
}
