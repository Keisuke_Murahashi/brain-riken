<?php
App::uses('AdminController', 'Controller');
class ArchivesController extends AdminController {
	public $uses = array('Message');

	public function index ($year = null, $month = null) {
		$username = $this->Auth->user('username');	// ログインユーザー名

		// var names to set
		$vars = array(
			'data_user', 'data_every', 'data_system', 'count_unread',
			'count_month_user', 'count_month_every', 'count_month_system',
		);

		// POST 通信のとき
		if ($this->request->is('post')) {
			return $this->_post();
		}

		// ID 指定があるときは page へ
		if (!$year)  $year  = $this->request->param('year');
		if (!$month) $month = $this->request->param('month');

		// お知らせ (ユーザー通知) (対象:ユーザー)
		$data_user = $this->Message->findByTarget('items', $username, array(
			'conditions' => array('mess_type' => 'user'),
			'order' => array('date' => 'desc'),
			'year' => $year, 'month' => $month,
		));

		// お知らせ (皆様への通知) (対象:全員)
		$data_every = $this->Message->findWithRead('items', $username, array(
			'conditions' => array('mess_type' => 'portal'),
			'order' => array('date' => 'desc'),
			'year' => $year, 'month' => $month,
		));

		// お知らせ (システムメッセージ) (対象:ユーザー)
		$data_system = $this->Message->findByTarget('items', $username, array(
			'conditions' => array('mess_type' => 'system'),
			'order' => array('date' => 'desc'),
			'year' => $year, 'month' => $month,
		));

		// 対象ユーザーが未読なメッセージの件数を取得
		$count_unread = array(
			'user'   => $this->Message->countByTarget($username, array(
				'conditions' => array('mess_type' => 'user'),
				'read' => false, 'year' => $year, 'month' => $month,
			)),
			'portal' => $this->Message->countWithRead($username, array(
				'conditions' => array('mess_type' => 'portal'),
				'read' => false, 'year' => $year, 'month' => $month,
			)),
			'system' => $this->Message->countByTarget($username, array(
				'conditions' => array('mess_type' => 'system'),
				'read' => false, 'year' => $year, 'month' => $month,
			)),
		);

		// 月別にカウント (ユーザー通知)
		$count_month_user = $this->Message->countsByMonth('items', $username, array(
			'conditions' => array('mess_type' => 'user'),
		));

		// 月別にカウント (皆様への通知)
		$count_month_every = $this->Message->countsByMonth('items', null, array(
			'conditions' => array('mess_type' => 'portal'),
		));

		// 月別にカウント (システムメッセージ)
		$count_month_system = $this->Message->countsByMonth('items', $username, array(
			'conditions' => array('mess_type' => 'system'),
		));

		// View に変数をセット
		$this->set(compact($vars));
	}

	// リダイレクト
	protected function _redirect () {
		// トップページに遷移
		$this->redirect(array('controller' => 'Archives', 'action' => 'index'), 303);
	}

	// POST 通信のとき、振り分け
	protected function _post () {
		// メッセージ登録
		if ($this->request->data('submit')) $this->create();

		// リダイレクト
		$this->_redirect();
	}

	// メッセージ登録
	public function create () {
		if (!$this->request->is('post')) $this->_redirect();	// リダイレクト

		// POST データに、別名と固定値を設定
		$post = $this->request->data;
		$post['content'] = $post['body'];

		// POST データからデータ登録
		$this->Message->saveByPost($post, $this->Auth->user('username'));
	}
}
