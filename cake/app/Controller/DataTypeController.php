<?php
App::uses('AdminController', 'Controller');

class DataTypeController extends AdminController {
	public $uses = array('DataType');

	// For Ajax .. データタイプから対応する JSON フィールド名の線形リストを取得
	public function ajax_select ($type = null) {
		$vars = 'json_keys';	// var names to set (string/array)

		// post data
		if (!$type) $type = $this->request->data('type');
		if (!$type) $type = $this->request->query('type');

		// データタイプを元に、JSON スキーマから各 JSON フィールド名の連想配列を取得
		$json_names = $this->DataType->getJsonKeys($type);
		$json_keys = array_keys( Hash::flatten($json_names) );

		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}
}
