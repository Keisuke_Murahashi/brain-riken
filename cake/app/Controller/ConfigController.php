<?php
App::uses('AdminController', 'Controller');

class ConfigController extends AdminController {
	public $uses = array('Config', 'Dictionary', 'DataFilter', 'DataType', 'Group', 'UserAuth');

	// 一覧
	public function index () {
		if (!$this->is_editable_group()) return;	// 管理者かどうかチェック

		// POST のときは更新へ
//		if ($this->request->is('post')) return $this->post();

		// 設定データを全取得
		$this->set('data_config', $this->Config->find('items'));	// View にセット

		// 辞書データを全取得
		$this->set('data_dictionary', $this->Dictionary->find('items', array(
			'order' => array('id DESC'),
		)));	// View にセット
	}

	// 更新
	public function ajax ($action = null, $id = null) {
		if (!$this->is_editable_group()) return;	// 管理者かどうかチェック

		$vars = 'post';	// var names to set (string/array)
		$post = $this->request->data;	// POST データ

		if ($this->request->is('post')) {	// Method == POST
			// 引数
			if (!$action) $action = $this->request->data('action');
			if (!$id)     $id     = $this->request->data('id');

			// action による振り分け
			if (!$id) {
				// none
			} else if (strtolower($id) == 'new') {
				if ($action == 'edit') {
					$this->Config->saveByLite($post, null);
					$post['action'] = 'new';
				}
			} else if ($action == 'edit') {
				$this->Config->saveByLite($post, $id);
			} else if ($action == 'delete') {
				$this->Config->delete($id);
			} else if ($action == 'restore') {
//				$this->Config->restore($id);
			}
		}
		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}

	// データフィルター設定
	public function filter ($group_id = null) {		// 選択グループID
		if (!$this->is_editable_group()) return;	// 管理者かどうかチェック

		// var names to set
		$vars = array('data_req_fields', 'data_schemes', 'data_group', 'group_id', 'group');

		// グループ選択
		$username = $this->Auth->user('username');	// ログインユーザー名
		$data_group = array();

		// システム管理者の場合、グループ選択可能
		if ($this->UserAuth->isSystemAdmin($username)) {
			// グループ選択
			$data_group = $this->Group->find('items', array('recursive' => -1));
		}
		// 選択グループID (既定)
		if (!$group_id) $group_id = $this->getGroupId();	// ログインユーザーのグループID (短縮)

		// 選択グループを取得
		$group = $this->Group->findById($group_id, array('recursive' => -1));

		// 検索条件 : データタイプ一覧
		$data_types = $this->DataType->find('items');

		// データタイプごとの JSON フィールド名の連想配列をセット
		$data_schemes = array();
		foreach ($data_types as $dt) {
			$type = $dt['data_type'];

			// データタイプを元に、JSON スキーマから各 JSON フィールド名の連想配列を取得
			$json_names = $this->DataType->getJsonKeys($dt);
			$data_fields = Hash::flatten($json_names);

			// 公開必須なフィールド名は除く
			$data_req_fields = $this->DataFilter->required_fields;
			foreach ($data_req_fields as $key => $memo) {
				unset($data_fields[$key]);
			}

			// このグループの設定データを取得
			$data_filter = $this->DataFilter->find('items', array(
				'conditions' => array(
					'group' => $group_id, 'data_type' => $type,
				),
				'as' => 'key',
			));

			// データを追加
			$data_schemes[$type] = array(
				'fields' => $data_fields,
				'filter' => $data_filter,
			);
		}


		// View に変数をセット
		$this->set(compact($vars));
	}

	// 更新
	public function ajax_filter ($action = null, $id = null) {
		if (!$this->is_editable_group()) return;	// 管理者かどうかチェック

		$vars = 'update_data';	// var names to set (string/array)
		$update_data = array();

		$post = $this->request->data;	// POST データ
		$post['group'] = $this->getGroupId();	// ログインユーザーのグループID (短縮)
		$post_keys = array();

		// 引数
		if (!$action) $action = $this->request->data('action');
		if (!$id)     $id     = $this->request->data('id');

		if ($this->request->is('post') and isset($post['group'])) {	// Method == POST
			// すべてにチェックのとき
			if ($id == 'all') {
				// データタイプを元に、JSON スキーマから各 JSON フィールド名の連想配列を取得
				$json_names = $this->DataType->getJsonKeys($post['data_type']);
				$data_fields = Hash::flatten($json_names);

				// 公開必須なフィールド名は除く
				foreach ($this->DataFilter->required_fields as $key => $memo) {
					unset($data_fields[$key]);
				}

				// 設定するキー名
				$post_keys = array_keys($data_fields);
				$id = null;
			}
			// ひとつチェックのとき
			else {
				if (isset($post['key'])) $post_keys[] = $post['key'];
			}
		}
		if ($post_keys) {
			// キーごとに設定
			foreach ($post_keys as $post_key) {
				// ID が既に登録があるか確認 (なければ新規作成になる)
				$post_id = $id ?: $this->DataFilter->getIdByKey(
										$post['group'], $post['data_type'], $post_key);

				// action による振り分け
				if ($action == 'edit') {
					$res = $this->DataFilter->saveByLite(
								array_merge($post, array('key' => $post_key)), $post_id);
					$res['update'] = date('Y/m/d H:i:s');	// 現在時刻を設定
					$update_data[] = $res;
				} else if ($action == 'delete') {
					$updates[] = $this->DataFilter->delete($id);
				} else if ($action == 'restore') {
//					$updates[] = $this->Config->restore($id);
				}
			}
		}

		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}

	// ユーザーがグループ設定を編集可能かどうかチェック
	protected function is_editable_group() {
		$username = $this->Auth->user('username');	// ログインユーザー名
		$group_id = $this->getGroupId();	// ログインユーザーのグループID (短縮)

		// ユーザーがグループを編集可能かどうかチェック
		if (!$this->Group->is_editable($group_id, $username)) {
			$this->Flash->error(l('_no_priv_edit_grp'));
			return $this->_redirect();
		}
		return true;
	}

	// リダイレクト
	protected function _redirect () {
		// トップページに遷移
		$this->redirect('/', 303);
	}
}
