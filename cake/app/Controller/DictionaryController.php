<?php
App::uses('AdminController', 'Controller');

class DictionaryController extends AdminController {
	public $uses = array('Dictionary');

	// 一覧
	public function index () {
		// POST のときは更新へ
//		if ($this->request->is('post')) return $this->edit();

		// 辞書データを全取得
		$this->set('data_dictionary', $this->Dictionary->find('items'));	// View にセット
	}

	// For Ajax
	public function ajax ($term = null) {
		$vars = 'data_list';	// var names to set (string/array)
		$data_list = array();

		// post data
		if (!$term) $term = $this->request-> data('term');
		if (!$term) $term = $this->request->query('term');

		// get data
		if ( ($term) && (mb_strlen($term) >= 5 )){
			$data_list = $this->Dictionary->find('list', array(
				'conditions' => array(
					'term LIKE' => str_replace('%', '', $term) .'%'
				),
				'fields' => 'term',
				'order' => array('term'),
			));
//			$data_list = array_values($data_list);
		}
		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}

	// 更新
	public function edit ($action = null, $id = null) {
		$vars = 'post';	// var names to set (string/array)
		$post = $this->request->data;	// POST データ

		if ($this->request->is('post')) {	// Method == POST
			// 引数
			if (!$action) $action = $this->request->data('action');
			if (!$id)     $id     = $this->request->data('id');

			// action による振り分け
			if (!$id) {
				// none
			} else if (strtolower($id) == 'new') {
				if ($action == 'edit') {
					$this->Dictionary->saveByLite($post, null);
					$post['action'] = 'new';
				}
			} else if ($action == 'edit') {
				$this->Dictionary->saveByLite($post, $id);
			} else if ($action == 'delete') {
				$this->Dictionary->delete($id);
			} else if ($action == 'restore') {
//				$this->Config->restore($id);
			}
		}
		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}
}
