<?php
App::uses('DocsBaseController', 'Controller');

class HowToUseController extends DocsBaseController {
    public function index () {
        parent::index();
    }

    public function docs() 
    {
	return parent::docs();
    }
}
