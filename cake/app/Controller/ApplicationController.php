<?php
App::uses('AdminController', 'Controller');

class ApplicationController extends AdminController {
	public $uses = array('AppData', 'AppHistory', 'UserAuth', 'Share', 'Publish',
							// NIJC 'Dataset', 'DataFilter');
							'Dataset', 'DataFilter', 'Group');

	// POST 通信のとき、振り分け
	protected function _post () {
		// 申請承認
		if (isset($this->request->data['accept'])) $this->accept();

		// 申請棄却
		if (isset($this->request->data['reject'])) $this->reject();

		// 申請取り下げ
		if (isset($this->request->data['withdraw_user'])) $this->withdraw_user();

		// 申請承認取り下げ
		if (isset($this->request->data['withdraw'])) $this->withdraw();

		// リダイレクト
		$this->_redirect();
	}

	public function index () {
		$username = $this->Auth->user('username');	// ログインユーザー名

		// POST 通信のとき
		if ($this->request->is('post')) {
			return $this->_post();
		}

		// 申請一覧（受信）(GroupAdmin or SystemAdmin)
		// NIJC added delflg
                // $gcoptions = array('*, GROUP_CONCAT(distinct AppData.group) AS groupid, GROUP_CONCAT(distinct AppData.app_id) AS appid');
                $this->AppData->virtualFields = ['groupid' => 'GROUP_CONCAT(distinct AppData.group)', 'appid' => 'GROUP_CONCAT(distinct AppData.app_id)'];
		// (*memo: 重複しない group_id を取得したい？ - 20160708)

		if ($this->UserAuth->isSystemAdmin($username)) { // SystemAdminは他ユーザーの申請データも表示
			$data_list = $this->AppData->find('items', array(
				'conditions' => array('and' => array(
					array('not' => array(
						// NIJC 'AppData.status' => array('承認済', '棄却', '取り下げ')
						'AppData.status' => array('Accepted', 'Rejected', 'Withdrawn')
					)),
					'delflg' => 0
				)),
				// NIJC 'fields' => '*', 'recursive' => 1,
				'fields' => ['*', 'groupid', 'appid'],
				'group' => 'AppData.data_id',
				'order' => array('date' => 'desc'),
				'recursive' => 1,
			));
		}else{
			$data_list = $this->AppData->find('items', array(
				'conditions' => array('and' => array(
					array('AppData.authorizer' => $username),
					array('not' => array(
						// NIJC 'AppData.status' => array('承認済', '棄却', '取り下げ')
						'AppData.status' => array('Accepted', 'Rejected', 'Withdrawn')
					)),
					'delflg' => 0
				)),
				// NIJC 'fields' => '*',
				'fields' => ['*', 'groupid', 'appid'],
				'group' => 'AppData.data_id',
				'order' => array('date' => 'desc'),
				'recursive' => 1,
			));
		}
// var_dump($data_list); exit();
		// (*memo: 重複しない group_id ($～[0]['groupid']) を分割し、それぞれ各グループ名を取得したい？)
		// (*memo: ※ただしグループ名は $item['Group']['name'] にも入っている - 20160708)
		// NIJC replace id to name
	        $gids = array();
		for ($i = 0; $i< count($data_list); $i++) {
        		$gids = explode(",", $data_list[$i]['groupid']);
                	for ($j = 0; $j< count($gids); $j++) {
                        	$gid = $this->Group->findById($gids[$j], array('recursive' => -1));
				$gids[$j] = @ $gid['name'];
                	}
			$data_list[$i]['groupid'] = implode(",", $gids);
			$data_list[$i]['appid'] =  $data_list[$i]['appid'];
		}

		// NIJC merge 
		$this->set('data_list', $data_list);	// View にセット

		// 申請一覧（受信）(User)
		$data_list2 = $this->AppData->find('items', array(
			'conditions' => array('and' => array(
				//array('creator' => $username),
				array('AppData.creator' => $username),
				array('not' => array(
					// NIJC 'AppData.status' => array('承認済', '棄却', '取り下げ')
					'AppData.status' => array('Accepted', 'Rejected', 'Withdrawn')
				)),
				'delflg' => 0
			)),
			// NIJC 'fields' => $gcoptions,
			'fields' => ['*', 'groupid', 'appid'],
			'group' => 'AppData.data_id',
			'order' => array('date' => 'desc'),
			//'fields' => '*', 'recursive' => 1,
			'recursive' => 1,
		));

	        $gids = array();
		for ($i = 0; $i< count($data_list2); $i++) {
        		$gids = explode(",", $data_list2[$i]['groupid']);
                	for ($j = 0; $j< count($gids); $j++) {
                        	$gid = $this->Group->findById($gids[$j], array('recursive' => -1));
				$gids[$j] = @ $gid['name'];
                	}
			$data_list2[$i]['groupid'] = implode(",", $gids);
			$data_list2[$i]['appid'] =  $data_list[$i]['appid'];
		//  ~~~~~~~~~~~                 ~~~~~~~~~~ typo ??
		// (*memo: $data_list2 と $data_list の順番に関連性はあるのか？ - 20160708)
		}
		$this->set('data_list2', $data_list2);	// View にセット

                //$gcoptions = array('*, GROUP_CONCAT(distinct AppHistory.app_id) AS appid');
               $this->AppHistory->virtualFields = ['appid' => 'GROUP_CONCAT(distinct AppHistory.app_id order by AppHistory.app_id)', 'actid' => 'GROUP_CONCAT(distinct AppHistory.act_id order by AppHistory.act_id)'];
		// 承認履歴一覧 (GroupAdmin or SystemAdmin)
		$data_history = $this->AppHistory->find('items', array(
				'conditions' => array('AppHistory.authorizer' => $username,
						//		array('not' => array(
					//'AppHistory.action' => '申請取り下げ'
				),
				// NIJC
				'fields' => ['*', 'appid', 'actid'],
				'group' => 'AppHistory.act_date',
				'order' => array('act_date' => 'desc'),
				'recursive' => 0,
		));
//  var_dump($this->AppHistory->getDataSource()->getLog()); exit();
		$this->set('data_history', $data_history);	// View にセット

		// 承認履歴一覧 (User)
		$data_history2 = $this->AppHistory->find('items', array(
				'conditions' => array('AppData.creator' => $username),
				//'AppHistory.action' => '申請取り下げ'),
				// NIJC
				'fields' => ['*', 'appid', 'actid'],
				'group' => 'AppHistory.act_date',
				'order' => array('act_date' => 'desc'),
				'recursive' => 0,
		));
		$this->set('data_history2', $data_history2);	// View にセット
// var_dump($data_history2); exit();
	}

	// For Ajax (AppData)
	public function ajax ($id = null) {
		$vars = 'data_item';	// var names to set (string/array)

		// post data
		if (!$id) $id = $this->request->data('id');
		if (!$id) $id = $this->request->query('id');
		// NIJC retrieve shared group
                // $gcoptions = array('*, GROUP_CONCAT(distinct AppData.group) AS groupid, GROUP_CONCAT(distinct AppData.app_id) AS appid');
		$ids = array();
		$ids = explode(",", $id);
		$this->AppData->virtualFields = ['groupid' => 'GROUP_CONCAT(distinct AppData.group order by AppData.group)', 'appid' => 'GROUP_CONCAT(distinct AppData.app_id order by AppData.app_id)'];

		$data_item = $this->AppData->find('items', array(
			'conditions' => array('app_id' => explode(",", $id), 'delflg' => 0),
			'recursive' => 1,
                        'fields' => ['*', 'groupid', 'appid'],	// NIJC
                        'group' => 'AppData.data_id',		// NIJC
		));
		// NIJC get group name

                $gids = array();
                for ($i = 0; $i< count($data_item); $i++) {
                        $gids = explode(",", $data_item[$i]['groupid']);
                        for ($j = 0; $j< count($gids); $j++) {
                                $gid = $this->Group->findById($gids[$j], array('recursive' => -1));
                                $gids[$j] = $gid['name'];
                        }
                        $data_item[$i]['groupid'] = implode(",", $gids);
		}

		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}

	// For Ajax (AppHistory)
	public function ajax2 ($id = null) {
		$vars = 'data_item';	// var names to set (string/array)

		// post data
		if (!$id) $id = $this->request->data('id');
		if (!$id) $id = $this->request->query('id');
		// NIJC 20160706
		$id = explode(",", $id);
		// get data
		// NIJC 20160706
		$this->AppHistory->virtualFields = ['appid' => 'GROUP_CONCAT(distinct AppHistory.app_id order by AppHistory.app_id)', 'actid' => 'GROUP_CONCAT(distinct AppHistory.act_id order by AppHistory.act_id)'];
		$data_item = $this->AppHistory->find('items', array(
				'conditions' => array('act_id' => $id),
				// NIJC
                                'fields' => ['*', 'appid', 'actid'],
                                'group' => 'AppHistory.act_date',
				'recursive' => -1,
		));

		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}

	// リダイレクト
	protected function _redirect () {
		// トップページに遷移
		$this->redirect(array('controller' => 'Application', 'action' => 'index'), 303);
	}

	// 申請承認
	public function accept ($id = null) {
		if (!$this->request->is('post')) return $this->_redirect();	// リダイレクト
		$username = $this->Auth->user('username');	// ログインユーザー名

		// POST データ
		$post = $this->request->data;

		// ID チェック
		if (isset($post['app_id'])) $id = $post['app_id'];
		// NIJC for multi group if (empty($id) or !is_numeric($id)) {
		if (empty($id)) {
			return $this->_redirect();
		}
		$post['app_id']  = $id;

		// ユーザーがデータを承認可能かどうかチェック
		// NIJC
//		foreach ($id as $appid) {
		foreach (explode(",", $id) as $appid) {	// 20160708 - to array
		// if (!$this->AppData->is_editable($id, $username)) {
		   if (!$this->AppData->is_editable($appid, $username)) {
			$this->Flash->error(l('_no_priv_app_rej'));
			return $this->_redirect();
		   }
		}
		// 対応するデータセットの ID とグループを取得

		// NIJC
		//$post['data_id'] = $this->AppData->getDataId($id);
		$appid = explode(",", $id);
		$post['data_id'] = $this->AppData->getDataId($appid[0]);
		// NIJC	$post['group']   = $this->AppData->getGroup($id);
		$groupids   = array_unique($this->AppData->getShareGroup($post['data_id']));
			// (*memo: data_id に対するすべての共有申請を取得？ - 20160708)

		// 共有・Releaseタイプ
		// NIJC $app_type = $this->AppData->getAppType($id);
		$app_type = $this->AppData->getAppType($appid[0]);
		//                                     ~~~~~~~~~
		// (*memo: 最初の"申請タイプ"をすべての"申請タイプ"としてよいのか？ - 20160708)
		// トランザクション開始
		$Transaction = $this->Transaction->begin();
//		try {
			// NIJC if ($app_type == '共有') {
			if ($app_type == 'Share') {
				// 共有データ作成、保存
				// NIJC for multi group
			   foreach($groupids as $groupid) {			
				$post['group'] = $groupid;
				$this->Share->saveAsAccept($post)
					or throw_exception( l('_share') );
				$this->Share->create();
			   }
					// (*memo: data_id に対するすべての共有申請を共有化？)

				// NIJC(20160628)
				//$ori_dir_name = $this->Dataset->getOriginalDir($data_id);
				//$cur_dir_name = $this->Dataset->getDatasetDir($data_id);
				$ori_dir_name = $this->Dataset->getOriginalDir($post['data_id']);
				$cur_dir_name = $this->Dataset->getDatasetDir($post['data_id']);

				// ↓常時呼び出すように変更 .. 20160511
//				// 初めての外部グループ間共有かどうか (データセットディレクトリが外部グループ間でない)
//				if (!$this->Dataset->is_dir_intergroup($post['data_id'])) {
				if ($ori_dir_name!='' and $cur_dir_name!='') {
					$owner_name  = $this->Dataset->getOwner($post['data_id']);	// データセットのオーナー
					$owner_group = $this->UserAuth->getGroupId($owner_name);	// オーナーのグループID

					// 外部グループ間共有としてデータセットディレクトリを移動
					// NIJC for multi group
					// $this->_move_dir_intergroup($post['data_id'], $owner_group, $owner_name, $post['group'])
					$this->_move_dir_intergroup($post['data_id'], $owner_group, $owner_name, $groupids)
						or throw_exception(l('_api_have_problem'));
				}
//				}
			} else if ($app_type == 'Release') {
				// Release側の画像変換 API を呼び出す POST 送信
				$res = $this->post_public_imgcnv($id);

				// Release側の画像変換 API で失敗したとき (パラメータ等に不備がある場合)
				if (!$res) {
					// Error: Release側の画像変換 API の通信に失敗しました。
					throw_exception(l('_fail_send_public_imgcnv'));
				}

				// Release情報を POST 送信
				$res = $this->post_public_data($id);

				// エラーメッセージ
				if (!$res or empty($res['status'])) {
					throw_exception(l('_fail_send_public_info'));
				} elseif ($res['status'] != '200') {	// != 200 OK
					throw_exception('Error: '. $res['message']);
				}

				// Releaseデータのステータスを変更
				$this->Publish->saveAsAccept($post)
					or throw_exception( l('_publish') );

			// NIJC } else if ($app_type == '共有承認取り下げ') {
			} else if ($app_type == 'WithdrawalOfShare') {
				// (*memo: 20160708 確認)
				// ・data_id に対するすべての共有状態のグループの共有を解除 (?)
				// ・自グループ内共有も解除 (!?)
				// ・二度と共有申請行うことができないようにロック
				// ・二度とディレクトリ関連付けを行うことができないようにロック
				// ・ディレクトリ関連付けの解除は行わない (後に手動で行う)

				// 共有データのステータスを変更 (delflg=1)
				// NIJC 20160706 for multiple groups

				if (!empty($this->Dataset->getDatasetDir($post['data_id']))) {
				// [API] 外部グループ間共有の解除 - 20160708
				   	$owner_name  = $this->Dataset->getOwner($post['data_id']);	// データセットのオーナー
				   	$owner_group = $this->UserAuth->getGroupId($owner_name);	// オーナーのグループID

				// [API] 外部グループ間共有としていたデータセットを解除し、自グループ内共有に
				   	$this->_discharge_intergroup($post['data_id'], $owner_group, $owner_name, $groupids)
					   or throw_exception(l('_api_have_problem'));
				}

				$appids = explode(",", $post['app_id']);
				for ($i=0; $i < count($appids); $i++) {
					$post['group'] = $groupids[$i];
					$post['app_id'] = $appids[$i];
						// (*memo: $appids の $groupids の並び順は必ずしも関連しないはず？) - 20160708

					$this->Share->saveAsWithdraw($post)
					   or throw_exception( l('_share_withdraw') );
                                	$this->Share->create();

				// 申請データのステータス変更（取り下げ、delflg=1）
					$this->AppData->saveAsWithdraw($post)
					   or throw_exception( l('_app_withdraw') );
                                	$this->AppData->create();
				}
					// (*memo: × data_id に対するすべての共有状態を解除？ - 20160713修正)

				// 申請データのステータス変更（取り下げ、delflg=1）
				// $this->AppData->saveAsWithdraw($post)
				//	or throw_exception( l('_app_withdraw') );

			} else if ($app_type == 'WithrawalOfRelease') {
				// Release情報を POST 送信
				$res = $this->get_public_data($post['data_id']);

						// エラーメッセージ
				if (!$res or empty($res['status'])) {
					throw_exception(l('_fail_send_public_info'));
				} elseif ($res['status'] != '200') {	// != 200 OK
					throw_exception('Error: '. $res['message']);
				}

				// Releaseデータのステータスを変更 (delflg=1)
				$this->Publish->saveAsWithdraw($post)
					or throw_exception( l('_publish_withdraw') );

				// 申請データのステータス変更（取り下げ、delflg=1）
				$this->AppData->saveAsWithdrawForPublish($post)
					or throw_exception( l('_app_withdraw') );

			}

			// change status to "Accepted"
			// NIJC for multi group	
                        // foreach($groupids as $groupid) {
                        foreach($appid as $app_id) {
                                //$post['group'] = $groupid;
                                $post['app_id'] = $app_id;
				$this->AppData->saveAsAccept($post)
					or throw_exception( l('_approved') );
				$this->AppData->create();

			//+ 承認履歴用 +//

			// POST データに、別名と固定値を設定
			   $history = array_merge($post, array(
				// NIJC 'action' => '承認',
				'action' => 'Accepted',
			   ));
			// POST データからデータ登録
				$this->AppHistory->saveByPost($history, $username)
					or throw_exception( l('_app_history') );
				$this->AppHistory->create();
			}

			// システムメッセージ本文作成
			$msg = 'Application ID: '. $post['app_id'] . "\n";
			$msg .= 'Date: '. date('Y/m/d H:i:s') . "\n";
			$msg .= 'Data Id: '. $post['data_id'] . "\n";
			$msg .= 'App Type: '. $app_type . "\n";		// 20160708
			$msg .= 'Authorizer: '. $username . "\n";
			//$msg .= $this->AppData->getAppType($id) .'先：'. $this->AppData->getGroup($id) . "\n";
			$msg .= 'Comment: '. $post['comment'] . "\n";

			// システムメッセージ登録 (送信先, タイトル, メッセージ, 申請ID)
			// NIJC $this->messageTo($this->AppData->getCreator($post['app_id']), 'Accepted (' . $app_type .')', $msg, $post['app_id']);
			$this->messageTo($this->AppData->getCreator($post['app_id']), 'Request for Approval ('. $post['app_id']. ') was accepted', $msg, $post['app_id']);

			// (トランザクション) コミット
			$Transaction->commit();

			// send Accepted message to submitter 
			// NIJC $this->Flash->success(sprintf(l('_comp_app'), $app_type, $id));
			$this->Flash->success(sprintf(l('_comp_app'), $id));
		//}
		// 例外発生時
		//catch (Exception $e) {
			// (トランザクション) ロールバック
		//	$Transaction->rollback();

			// エラーメッセージ
			// NIJC $this->Flash->error(sprintf(l('_err_ocur_app'), $app_type, $id) .
		//	$this->Flash->error(sprintf(l('_err_ocur_app'), $id) .
		//						($e->getMessage() ? ' ('. $e->getMessage() .')' : ''));
		//}
	}

	// 申請棄却
	public function reject () {
		if (!$this->request->is('post')) return $this->_redirect();	// リダイレクト
		$username = $this->Auth->user('username');	// ログインユーザー名

		// POST データ
		$post = $this->request->data;

		// ID チェック
		if (isset($post['app_id'])) $id = $post['app_id'];
		// NIJC if (empty($id) or !is_numeric($id)) {
		if (empty($id)) {
			return $this->_redirect();
		}

		// ユーザーがデータを承認可能かどうかチェック

        // NIJC
//		foreach ($id as $appid) {
		foreach (explode(",", $id) as $appid) {	// 20160708 - to array
		// NIJC if (!$this->AppData->is_editable($id, $username)) {
                   if (!$this->AppData->is_editable($appid, $username)) {
			$this->Flash->error(l('_no_priv_app_rej'));
			return $this->_redirect();
		   }
		}

		// Releaseデータのステータスを変更
		if ($this->AppData->getAppType($id) == 'Release'){
			$this->Publish->saveAsReject($post);
			$this->AppData->saveAsRejectForPublish($post);
		}else{
			// ステータスを "承認済" に設定
			// NIJC update multiple entry
			$appids = array();
			$appids = explode(",", $post['app_id']);
		 	foreach ($appids as $appid) {
			   	$post['app_id'] = $appid;	
				$this->AppData->saveAsReject($post);
				$this->AppData->create();
			}
		}

		//+ 承認履歴用 +//

		// POST データに、別名と固定値を設定
		$history = array_merge($post, array(
			// NIJC 'action' => '棄却',
			'action' => 'Rejected',
		));
		// POST データからデータ登録
	 	// NIJC for multi group
		foreach  ($appids as $appid) {
			$history['app_id'] = $appid;
			$this->AppHistory->saveByPost($history, $username);
			$this->AppHistory->create();
		}
		// システムメッセージ本文作成
		$msg = 'Application ID：'. $post['app_id'] . "\n";
		$msg .= 'Date: '. date('Y/m/d H:i:s') . "\n";
		$msg .= 'Data ID: '. $this->AppData->getDataId($id) . "\n";
		$msg .= 'Authorizer: '. $username . "\n";
		//$msg .= $this->AppData->getAppType($id) .'先：'. $this->AppData->getGroup($id) . "\n";
		$msg .= 'Comment: '. $post['comment'] . "\n";

		// システムメッセージ登録 (送信先, タイトル, メッセージ, 申請ID)
		// $this->messageTo($this->AppData->getCreator($id), 'Rejected (' . $this->AppData->getAppType($id) .')', $msg, $app_id);
//		$this->messageTo($this->AppData->getCreator($id), 'Request for Approval (Rejected)' , $msg, $app_id);
		$this->messageTo($this->AppData->getCreator($id), 'Request for Approval (Rejected)' , $msg, $post['app_id']);	// - 20160708

		// 完了メッセージ
		// NIJC $this->Flash->success(sprintf(l('_comp_rej'), $this->AppData->getAppType($id), $id));
		$this->Flash->success(sprintf(l('_comp_rej'), $id));
	}

	// 申請取り下げ
	public function withdraw_user ($id = null) {
		if (!$this->request->is('post')) return $this->_redirect();	// リダイレクト
		$username = $this->Auth->user('username');	// ログインユーザー名

		// POST データ
		$post = $this->request->data;

		// ID チェック
		if (isset($post['app_id'])) $id = $post['app_id'];
		// NIJC 20160706 if (empty($id) or !is_numeric($id)) {
		if (empty($id)) {
			return $this->_redirect();
		}

		// 共有・Releaseタイプ
		$app_type = $this->AppData->getAppType($id);

		// 申請ステータスを "取り下げ" に設定
		// NIJC 20160706
		$appids = array();
		$appids = explode(",", $id);
		foreach($appids as $appid) {
                	$post['app_id'] = $appid;
			$this->AppData->saveAsWithdraw($post);
                	$this->AppData->create();

		   if ($app_type == 'Release') {
			// Releaseステータスを "取り下げ" に設定
			$this->Publish->saveAsWithdraw($this->AppData->getDataId($id));
                	$this->Publish->create();
		   }

		//+ 承認履歴用 +//

		// POST データに、別名と固定値を設定
			$history = array_merge($post, array(
				// NIJC 'action' => '申請取り下げ',
				'action' => 'Withdrawn',
			));
		// POST データからデータ登録
		// NIJC 20160706 
			$this->AppHistory->saveByPost($history, $username);
                	$this->AppHistory->create();
		} // endforeach
		// システムメッセージ本文作成
		$dataids = array();
		//$msg = 'Application ID: '. $post['app_id'] . "\n";
		$msg = 'Application ID: '. $id . "\n";
		$msg .= 'Date：'. date('Y/m/d H:i:s') . "\n";
		foreach ($appids as $appid) {
			$dataids[] = $this->AppData->getDataId($appid);
		}
		// $msg .= 'Data Id: '. $this->AppData->getDataId($id) . "\n";
		$msg .= 'Data Id: '. implode(",",$dataids) . "\n";
		$msg .= 'Applicant: '. $username . "\n";
		//$msg .= $this->AppData->getAppType($id) .'先：'. $this->AppData->getGroup($id) . "\n";
		$msg .= 'Comment: '. $post['comment'] . "\n";

		// システムメッセージ登録 (送信先, タイトル, メッセージ, 申請ID)
		$this->messageTo($this->AppData->getAuthorizer($id), 'Request for Withdraw (' . $id .') was Accepted', $msg, $id);

		// 完了メッセージ
		// NIJC $this->Flash->success(sprintf(l('_comp_wtd'), $this->AppData->getAppType($id), $id));
		$this->Flash->success(sprintf(l('_comp_wtd'), $id));
	}
	// 承認申請取り下げ
	public function withdraw ($id = null) {

		if (!$this->request->is('post')) return $this->_redirect();	// リダイレクト
		$username = $this->Auth->user('username');	// ログインユーザー名

		// POST データ
		$post = $this->request->data;

			// ID チェック
		if (isset($post['act_id'])) $id = $post['act_id'];
		// NIJC if (empty($id) or !is_numeric($id)) {
		if (empty($id)) {
			return $this->_redirect();
		}

		//申請タイプ設定
		// NIJC $data = $this->AppData->findById($post['app_id']);
		$data = $this->AppData->findById($post['app_id']);
		// if ( $data['app_type'] == '共有' ) {
		if ( $data['app_type'] == 'Share' ) {
			// $app_type = '共有承認取り下げ';
			$app_type = 'WithdrawalOfShare';
		} else if ( $data['app_type'] == 'Release' ) {
			$app_type = 'WithdrawalOfRelease';
		} else {
			$this->Flash->error(l('_no_wtd'));
			return $this->_redirect();
		}

		//申請されたデータの有無を確認
		// NIJC
		$groups = array_unique ($this->AppData->getShareGroup($data['data_id']));
			// (*memo: 共有されているすべてのグループが対象)

		$app_data = $this->AppData->find('items', array(
				'conditions' => array(
						//'AppData.creator' => $this->Auth->user('username'),
						// NIJC 'AppData.group' => $data['group'],
						'AppData.group' => $groups,
						'AppData.data_id' => $data['data_id'],
						'AppData.app_type' => $app_type,
						'delflg' => 0,
						// 'AppData.status' => '申請中'),
						'AppData.status' => 'Processing'),
				'recursive' => 2, 'callbacks' => true,
		));

		if (empty($app_data)) {
			//historyのステータスを承認取り下げに変更
			//$this->AppHistory->saveAsWithdraw($post);
			//申請データ作成
			//共有 or Release
		   $app_id = array();
		   foreach ($groups as $groupid) {
			$new_data = array(
					'data_id'  => $data['data_id'],
					'detail'   => $post['comment'],
					'select'   => $post['authorizer'],
					'creator'  => $username,	// ログインユーザー名
					'app_type' => $app_type,
					// 'status'   => '申請中',
					'status'   => 'Processing',
					'date'     => date('Y/m/d H:i:s'),
					// NIJC 'group'    => $data['group'],
					'group'    => $groupid,
			);

			// 申請情報を保存
			$app_id[] = $this->AppData->saveByPost($new_data, $username);
			$this->AppData->create();
		   } // foreach group_id
			// システムメッセージ本文作成
			$msg = 'Application ID: '. implode(",", $app_id) . "\n";
			// $msg .= $app_type . 'Date: '. date('Y/m/d H:i:s') . "\n";
			$msg .= 'Date: '. date('Y/m/d H:i:s') . "\n";
			$msg .= 'Data Id: '. $data['data_id'] . "\n";
			$msg .= 'Applicant: '. $username . "\n";
			//$msg .= $this->AppData->getAppType($id) .'先：'. $this->AppData->getGroup($id) . "\n";
			//$msg .= 'Comment: '. $post['comment'] . "\n";

			// システムメッセージ登録 (送信先, タイトル, メッセージ, 申請ID)
			// NIJC $this->messageTo($post['authorizer'], 'Request for Approval (' . ltrim("_", $this->Util->getAppType($app_type)) .')', $msg, $id);
			$this->messageTo($post['authorizer'], 'Request for Approval (' . implode(",", $app_id) .')', $msg, $id);


			// 完了メッセージ
			// $this->Flash->success(sprintf(l('_sent_app'), $app_type, $app_id));
			$this->Flash->success(sprintf(l('_sent_app'), implode(",", $app_id)));
		}else{

			//申請データがあれば中断
			// NIJC $this->Flash->error(sprintf(l('_aldy_app'), $app_type, $data['group']));
			$this->Flash->error(sprintf(l('_aldy_app'), implode(",", $data['group'])));
		}
	}

	// Release側の画像変換API を呼び出す POST 送信
	public function post_public_imgcnv ($app_id, $publish = null) {
		$username  = $this->Auth->user('username');	// ログインユーザー名
		$data_id   = $this->AppData->getDataId($app_id);	// 対応するデータセットの ID を取得
		$data_type = $this->Dataset->getDataType($data_id);	// データセットのデータタイプを取得

		// 必要なデータがなければ返す
		if (!$data_id or !$data_type) return;

		// 送信先 URL
		$url = ConnectionManager::$config->default_api['public_image_convert'];

		// Release側の画像変換 API に POST 送信するパラメータ
		$param = array(
			'username'    => $username,	// ActiveDirectory認証のユーザー名
			'datasetId'   => $data_id,	// 移動対象となるデータセットのID
			'dataType'    => $data_type,	// 移動対象となるデータセットのデータタイプ
			'signature'   => '',
		);
		// Release側の画像変換 API に POST 送信
		$result = curl_post_contents($url, $param);

		// 結果 (成功していたら status == 200)
		return !!$result;
	}

	// Release情報を POST 送信
	public function post_public_data ($app_id, $publish = null) {

		// 対応するRelease先情報を取得
		if (!$publish) $publish = $this->Publish->findById($app_id);

		// 必要なデータがなければ返す
		if (empty($publish['data_id'])) return;

		// Release先情報にログインユーザー名をセット
		$publish['applicant'] = $this->Auth->user('username');	// ログインユーザー名
//		$publish['applicant'] = 'test_matsuda'; // Tentative

		// 送信先 URL
		$url = ConnectionManager::$config->default_api['public_addDataset'];
		$url = preg_replace_callback('/\{\$(\w+)\}/', function ($matches) use ($publish) {
			return isset($publish[ $matches[1] ]) ? $publish[ $matches[1] ] : '';
		}, $url);

		// データを取得
		$data_item = $this->Dataset->findById($publish['data_id']);
		unset($data_item['Mongo']["_id"]);

		// 必要なデータがなければ返す
		if (empty($data_item['data_type']) or empty($data_item['Mongo'])) return;

		// Release可能なデータをフィルタリング (データフィルター)
		$group_id = $this->UserAuth->getGroupId($this->Auth->user('username'));
		$data_safety = $this->DataFilter->filterOn($group_id, $data_item['data_type'], $data_item['Mongo']);

		// JSON 形式の文字列にする
		$data_json = json_encode($data_safety);

		// PUBLIC_API に POST 送信
//		$result = file_post_contents($url, array('json' => $data_json));
		$result = curl_post_contents($url, array('json' => $data_json));

		// [デバッグ] 結果を出力
		//Util::debug($result, '_test_post_public_result.txt');

		// 結果をデコードして返す
		$result_hash = $result ? json_decode($result, true) : null;
		return isset($result_hash['meta']) ? $result_hash['meta'] : $result;
	}
	public function get_public_data ($data_id, $publish = null) {

		// 必要なデータがなければ返す
		if (empty($data_id)) return;

		// Release先情報にログインユーザー名をセット
		$publish['applicant'] = $this->Auth->user('username');	// ログインユーザー名
		$publish['data_id'] = $data_id;	// データID

		// 送信先 URL
		//$publish['applicant'] = 'test_matsuda'; // Tentative
		$url = ConnectionManager::$config->default_api['public_deletePublishPost'];
		$url = preg_replace_callback('/\{\$(\w+)\}/', function ($matches) use ($publish) {
			return isset($publish[ $matches[1] ]) ? $publish[ $matches[1] ] : '';
		}, $url);

		// PUBLIC_API に GET 送信
		$result = curl_get_contents($url);

		// [デバッグ] 結果を出力
		//Util::debug($result, '_test_post_public_result.txt');

		// 結果をデコードして返す
		$result_hash = $result ? json_decode($result, true) : null;
		return isset($result_hash['meta']) ? $result_hash['meta'] : $result;

	}

}
