<?php
App::uses('DocsBaseController', 'Controller');

class UserGuideController extends DocsBaseController {
    public function index () {
        parent::index();
    }

    public function docs() 
    {
	return parent::docs();
    }
}
