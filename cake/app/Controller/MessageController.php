<?php
App::uses('AdminController', 'Controller');

class MessageController extends AdminController {
	public $uses = array('Message', 'MessageRead', 'User', 'Group');

	// 一覧
	public function index ($id = null) {
		if (!$id) $id = $this->request->param('id');

		// POST 通信のとき
		if ($this->request->is('post')) $this->_post($id);

		// ID 指定があるときは page へ
		if ($id) return $this->page($id);

		// フォーム付一覧ページへ
		return $this->form();
	}

	// 返信用
	public function reply ($id = null) {
		// ID が見つからないときは返す
		if (!$id) $id = $this->request->data('mess_id');
		if (!$id) return $this->_redirect();

		// フォーム付一覧ページへ
		return $this->form($id);
	}

	// フォーム付一覧
	public function form ($id = null) {
		$username = $this->Auth->user('username');	// ログインユーザー名

		// var names to set
		$vars = array(
			'id', 'data_reply', 'data_items',
			'data_usernames', 'data_group',
			'data_user', 'data_every', 'data_system', 'count_unread',
		);

		// ID 指定があるときは返信用とする
		if (!$id) $id = $this->request->data('mess_id');

		// 返信用メッセージ取得
		$data_reply = (!$id) ? null : $this->Message->find('item', array(
			'conditions' => array('mess_id' => $id),
		));
		// メッセージ一覧
		$data_items = $this->Message->find('items', array(
			'conditions' => array(
				'author' => $this->Auth->user('username'),
				'mess_type' => array('user', 'portal'),
			),
			'order' => array('date' => 'desc'),
			'callbacks' => true,
		));

		// ユーザー選択
		$data_usernames = $this->User->find('items', array(
			'fields' => array('id', 'username', 'name'),
		));
		// ユーザー選択 (グループ)
		$data_group = $this->Group->find('items', array(
			'fields' => '*', 'recursive' => 2,
		));

		// お知らせ (ユーザー通知) (対象:ユーザー)
		$data_user = $this->Message->findByTarget('items', $username, array(
			'conditions' => array('mess_type' => 'user'),
			'order' => array('date' => 'desc'),
		));

		// お知らせ (皆様への通知) (対象:全員)
		$data_every = $this->Message->findWithRead('items', $username, array(
			'conditions' => array('mess_type' => 'portal'),
			'order' => array('date' => 'desc'),
		));

		// お知らせ (システムメッセージ) (対象:ユーザー)
		$data_system = $this->Message->findByTarget('items', $username, array(
			'conditions' => array('mess_type' => 'system'),
			'order' => array('date' => 'desc'),
		));

		// 対象ユーザーが未読なメッセージの件数を取得
		$count_unread = array(
			'user'   => $this->Message->countByTarget($username, array(
				'conditions' => array('mess_type' => 'user'),
				'read' => false,
			)),
			'portal' => $this->Message->countWithRead($username, array(
				'conditions' => array('mess_type' => 'portal'),
				'read' => false,
			)),
			'system' => $this->Message->countByTarget($username, array(
				'conditions' => array('mess_type' => 'system'),
				'read' => false,
			)),
		);

		// View に変数をセット
		$this->set(compact($vars));

		// set template (== index)
		$this->render('index');
	}

	// 詳細
	public function page ($id = null) {
		if (!$id) $this->_redirect();	// リダイレクト

		// メッセージ詳細
		$data_items = $this->Message->find('items', array(
			'conditions' => array('mess_id' => $id),
		));
		// Target を結合
		foreach ($data_items as $i => $item) {
			$targets = array();
			foreach ($item['Target'] as $tmp) {
				$targets[] = $tmp['target'];
			}
			$data_items[$i]['targets'] = implode(',', $targets);
		}
		$this->set('data_items', $data_items);	// View にセット

		// 既読として保存
		$this->MessageRead->saveAsRead($id, $this->Auth->user('username'));

		$this->render('page');	// set template
	}

	// For Ajax
	public function ajax ($id = null) {
		$vars = 'data_item';	// var names to set (string/array)

		// post data
		if (!$id) $id = $this->request->data('id');
		if (!$id) $id = $this->request->query('id');
		if (!$id) return;

		// get data
		$data_item = $this->Message->find('items', array(
			'conditions' => array('mess_id' => $id),
			'callbacks' => true,
		));

		// 既読として保存
		$this->MessageRead->saveAsRead($id, $this->Auth->user('username'));

		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}

	// リダイレクト
	protected function _redirect ($action = 'index', $id = null) {
		// トップページに遷移
		$this->redirect(array('controller' => 'Message', 'action' => $action, $id), 303);
	}

	// POST 通信のとき、振り分け
	protected function _post ($id = null) {
		// メッセージ登録
		if (isset($this->request->data['submit'])) $this->create($id);

		// メッセージ返信
		if (isset($this->request->data['reply'])) return $this->reply($id);

		// リダイレクト
		$this->_redirect();
	}

	// メッセージ登録
	public function create ($id = null) {
		if (!$this->request->is('post')) return $this->_redirect();	// リダイレクト
		$username = $this->Auth->user('username');	// user ID

		// POST データに、別名と固定値を設定
		$post = $this->request->data;
		$post['content'] = $post['body'];

		// メッセージタイプが "portal" のときは ターゲットユーザー を削除
		if (!$post['mess_type'] or $post['mess_type'] == 'portal') {
			$post['target'] = null;
		}

		// 前後の空白を削除
		foreach (array('title', 'content') as $key) {
			$post[$key] = empty($post[$key]) ? ''
						: trim( mb_convert_kana($post[$key], 's', 'UTF-8') );
		}

		// タイトルがなかったときは「No Subject」を付ける
		if (empty($post['title'])) $post['title'] = '(No subject)';

		// メッセージがなかったときは返す
		if (empty($post['content'])) {
			$this->Flash->warn(l('_make_body'));
			return $this->_redirect($id ? 'reply' : '', $id);	// リダイレクト
		}

		// POST データからデータ登録
		$this->Message->saveByPost($post, $username);

		// ID があれば返信として保存
		if ($id) $this->MessageRead->saveAsRes($id, $username);

		// メッセージ
//		$this->Flash->success(($id ? '返信' : '新規投稿') .'が完了しました。');
		$this->Flash->success(l('_send_mess'));

		// リダイレクト
		$this->_redirect();
	}
}
