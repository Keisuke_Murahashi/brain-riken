<?php
App::uses('Component', 'Controller');

// 共通処理
class CommonComponent extends Component {
//	public $uses = array('Share');

	// このコントローラーのオブジェクトを変数に
//	public function initialize (Controller $Controller) {
//		$this->Controller = $Controller;
//	}

	// データにダウンロード API 用の設定をセット
	public function set_download_info (&$data_item, $is_intergroup = false) {
		// 外部グループ間共有 ?
		if ($is_intergroup) {
			// データセットダウンロード用の基本 URL を決定
			$data_item['download_url'] =
				ConnectionManager::$config->default_api['download_url_intergroup'];

			// データセットダウンロード用のパラメーター
			$data_item['download_id'] = $data_item['data_id'];
		}
		// 自グループ内共有 ?
		else {
			// データセットダウンロード用の基本 URL を決定
			$data_item['download_url'] =
				ConnectionManager::$config->default_api['download_url_intragroup'];

			// データセットダウンロード用のパラメーター
			$data_item['download_id'] = $data_item['UserAuth']['group'] .':'. $data_item['data_id'];
		}
	}

}

