<?php
// For Admin
App::uses('AppController', 'Controller');
App::import('Vendor', 'Util');

class AdminController extends AppController {
	public $uses = array('User', 'UserAuth', 'Dataset');
	protected $autoLogin = true;
//	public $uses = array('User', 'Lang');	// extends
//	public $autoLayout = false;
	public $layout = 'default_brain_riken';
	public $components = array(
		'RequestHandler',	// for .json
		'Flash',	// for message
		'Auth' => array(	// for Admin
			'loginAction' => array(
				'controller' => 'Login',
				'action' => 'index',
//				'admin' => true,
			),
			'loginRedirect' => array(
				'controller' => '',
				'action' => 'index',
			),
			'logoutRedirect' => array(
				'controller' => 'Login',
				'action' => 'index',
			),
			'authenticate' => array(
				'Form' => array(
					'passwordHasher' => 'Blowfish'
				)
			)
		),
		'Common'	// Common compornents .. for download, other
	);

	// run before each action
	public function beforeFilter() {
		parent::beforeFilter();

		// ブラウザのキャッシュを無効にする
		$this->response->disableCache();	// no cache

		// アクセスログにアクセス情報を保存
		// NIJC
		if ( env('REMOTE_ADDR') != '10.10.151.21' ) { // zabbix
		   $this->AccessLog->save(array(
			'username'   => $this->Auth->user('username'),
			'controller' => $this->name,
			'action'     => $this->action,
			'pass'       => implode(',', $this->request->param('pass')),
			'uri'        => env('REQUEST_URI'),
			'referer'    => env('HTTP_REFERER'),
			'ip'         => env('REMOTE_ADDR'),
			'host'       => env('REMOTE_HOST'),
			'agent'      => env('HTTP_USER_AGENT'),
		   ));
		}
		// 

		// Auth ログイン
		//$user_info = $this->Auth->user();
		$user = $this->User->find('items', array(
			'recursive' => 2, 'callbacks' => true,
			'conditions' => array('User.username' => $this->Auth->user('username'))
		));
		$user_info = @$user[0]; //暫定対応②
		$this->set('user', $user_info);

		// ログイン認証済み？
		if ($this->autoLogin) {
/*
			// アクセス時間を記録 (セッションを更新)
			$this->Session->write('atime', time());

			// ログインユーザー名
			$username = $this->Auth->user('username');

			// 未ログイン
			if (!$username) {
				// アクセスされたURL (※ログインページ以外)
				if ($this->name != 'Login') {
					$this->Session->write('referer', env('REQUEST_URI'));
				}
				// リダイレクト
//				$this->response->disableCache();	// no cache
				$this->redirect('/Login/', 303);	// ログイン画面へ
			}
			// ユーザー情報
			$user = $this->User->find('first', array(
				'conditions' => array(
					$this->User->name .'.'. $this->User->primaryKey => $username
				),
			));
			$this->set('user', $user);
*/
		}
	}

	// save system message
	// .. システムメッセージ登録 (送信先, タイトル, メッセージ, 申請ID)
	protected function messageTo($target, $title, $content = null,
						$dst_id = null, $username = null, $type = 'system') {
		// save
		if ($target and $title) {
			return $this->Message->saveByPost(array(
				'mess_type' => $type,
				'title'     => $title,
				'content'   => $content,
				'category'  => $dst_id,
				'target'    => is_array($target) ? $target : array($target),
			), $username ?: $this->Auth->user('username'));
		}
		return;
	}

	// ログインユーザーのグループ名を取得する（短縮メソッド）
	protected function getGroupId() {
		// ログインユーザーグループ
		return $this->UserAuth->getGroupId( $this->Auth->user('username') );
	}

	// ユーザーがデータを編集可能かどうかチェック
	protected function is_editable_dataset($data_id, $flash = true) {
		$username = $this->Auth->user('username');	// ログインユーザー名
		$group_id = $this->UserAuth->getGroupId($username);	// ログインユーザーのグループID

		// ログインユーザーのグループが未設定時
		if (!$group_id) {
			if ($flash) $this->Flash->error(l('_grp_not_set'));
			return false;
		}

		// データセット (Mongo データも自動連係で取得)
		$data_item = is_array($data_id) ? $data_id : $this->Dataset->findById($data_id);

		// データセットが見つからないとき
		if (!$data_item) {
			if ($flash) $this->Flash->error(l('_not_get_data'));
			return false;
		}
		// ユーザーがデータを編集可能かどうかチェック
		if (!$this->Dataset->is_editable($data_item, $username)) {
			if ($flash) $this->Flash->error(l('_no_priv_edit_data'));
			return false;
		}
		// データが申請中/承認済かどうかチェック
		if ($this->Dataset->is_app($data_item)) {
			if ($flash) $this->Flash->error(l('_no_priv_edit_data_by_status'));
			return false;
		}
		return true;
	}

	// 自グループ内共有としてデータセットディレクトリを移動 (ファイル転送API と連携)
	protected function _move_dir_intragroup ($data_id, $dir_name, $ori_dir_name = null) {
		$username = $this->Auth->user('username');	// ログインユーザー名
		$group_id = $this->getGroupId();	// ログインユーザーのグループID (短縮)

		// API 送信先 URL
		$url = ConnectionManager::$config->default_api['move_dir_intragroup'];

		// グループ管理者のユーザー名の一覧
		$admin_users  = $this->UserAuth->getAdminUserNamesByGroup($group_id);

		// グループの一般ユーザー名の一覧
		$normal_users = $this->UserAuth->getNormalUserNamesByGroup($group_id);

		// PUBLIC_API に POST 送信するパラメータ
		$param = array(
			'username'    => $username,	// ActiveDirectory認証のユーザー名
			'groupId'     => $group_id,	// グループの内部ID
			'datasetId'   => $data_id,	// 移動対象となるデータセットのID
			'datasetDir'  => $dir_name,	// 移動対象となるデータセットのディレクトリー名
			'adminUsers'  => implode("\t", $admin_users),	// グループ管理者のユーザー名
			'memberUsers' => implode("\t", $normal_users),	// グループの一般ユーザー名
			'signature'   => '',
		);
		// ファイルやディレクトリーの元々のパス
		// (まだデータセットIDに移動していない場合は、このパラメター指定を省略)
		if ($ori_dir_name) $param['originalDir'] = $ori_dir_name;

		// PUBLIC_API に POST 送信
		$result = curl_post_contents($url, $param);

		// 結果 (JSON) をデコード (成功していたら status == 200)
//		$result_hash = $result ? json_decode($result, true) : null;
//		if ($result_hash and isset($result_hash['status']) and $result_hash['status'] == 200) {
		if ($result == 'OK') {
			// 新しいデータセットディレクトリ
			$new_dir_name = sprintf(DIR_INTRAGROUP, $group_id, $data_id);

			// ディレクトリの関連付けを設定
			$this->Dataset->saveToDirName($data_id, $new_dir_name, $dir_name);
			return true;
		}
		return false;
	}

	// 外部グループ間共有としてデータセットディレクトリを移動 (ファイル転送API と連携)
	protected function _move_dir_intergroup ($data_id, $owner_group_id, $owner_name, $other_group_id = null) {
		$username = $this->Auth->user('username');	// ログインユーザー名

		// API 送信先 URL
		$url = ConnectionManager::$config->default_api['move_dir_intergroup'];

		// 同じグループのユーザー名の一覧
		$group_users = $this->UserAuth->getUserNamesByGroup($owner_group_id);

		// 外部グループのユーザー名の一覧を取得 (同じグループのユーザーは除く) - 20160708
		$other_users = $this->_get_other_group_users($data_id, $owner_group_id, $other_group_id);
/*
		// 外部グループのユーザー名の一覧を取得
		$other_users = array();
		if ($other_group_id) {	// 引数に外部グループのIDがあればそれを
			// NIJC for multi group
			foreach($other_group_id as $group_id) {	
			// $other_users = $this->UserAuth->getUserNamesByGroup($other_group_id);
				$users = $this->UserAuth->getUserNamesByGroup($group_id);
				foreach ($users as $user) {
					array_push($other_users, $user);
				}
			}
		} else {	// なければ DB から外部の共有グループを取得
			// 共有先グループ名を取得
			$shared_group = $this->Share->getSharedGroup($data_id);
			unset( $shared_group[ $owner_group_id ] );	// 同じグループは不要

			// 外部グループのユーザー名の一覧
			if (count($shared_group))
				$other_users = $this->UserAuth->getUserNamesByGroup( array_keys($shared_group) );
		}
*/

		// PUBLIC_API に POST 送信するパラメータ
		$param = array(
			'username'   => $username,	// ActiveDirectory認証のユーザー名
			'datasetId'  => $data_id,	// 移動対象となるデータセットのID
			'ownerUser'  => $owner_name,	// そのデータセットの最初の登録者のユーザー名
			'groupId'    => $owner_group_id,	// グループの内部ID
			'groupUsers' => implode("\t", $group_users),	// 同じグループ内のユーザー名
			'otherUsers' => implode("\t", $other_users),	// 相手グループ内のユーザー名
			'signature'  => '',
		);

		// PUBLIC_API に POST 送信
		$result = curl_post_contents($url, $param);

		// 結果 (JSON) をデコード (成功していたら status == 200)
//		$result_hash = $result ? json_decode($result, true) : null;
//		if ($result_hash and isset($result_hash['status']) and $result_hash['status'] == 200) {
		if ($result == 'OK') {
			// 新しいデータセットディレクトリ
			$new_dir_name = sprintf(DIR_INTERGROUP, $data_id);

			// ディレクトリの関連付けを設定
			$this->Dataset->saveToDirName($data_id, $new_dir_name);
			return true;
		}
		return false;
	}

        // NIJC(20160701)
        	// (*memo: 自グループ内共有としていたデータセット共有を解除し、ディレクトリの関連付けも解除する)
        protected function _discharge_intragroup($data_id, $ori_dir_name, $owner_user) {
                $username = $this->Auth->user('username');
                $group_id = $this->getGroupId();
                $admin_users  = $this->UserAuth->getAdminUserNamesByGroup($group_id);
                $normal_users = $this->UserAuth->getNormalUserNamesByGroup($group_id);

                $param = array(
                        'username'    => $username,
                        'datasetId'   => $data_id,
                        'groupId'     => $group_id,
                        'originalDir' => $ori_dir_name,
                        'ownerUser'   => $owner_user,
                        'adminUsers'  => implode("\t", $admin_users),
                        'memberUsers' => implode("\t", $normal_users),
                        'signature'   => ''
                );

                $url = ConnectionManager::$config->default_api['discharge_intragroup'];
                $result = curl_post_contents($url, $param);
                if ($result == 'OK') {
                        $this->Dataset->saveForUnlink($data_id);
                        return true;
                }
                return false;
        }

	// 外部グループ間共有としていたデータセット共有を解除し、自グループ内共有とする (ファイル転送API と連携) - 20160708
	protected function _discharge_intergroup ($data_id, $owner_group_id, $owner_name, $other_group_id = null) {
		$username = $this->Auth->user('username');	// ログインユーザー名
		$group_id = $this->getGroupId();	// ログインユーザーのグループID (短縮)

		// 同じグループのユーザー名の一覧
		$group_users = $this->UserAuth->getUserNamesByGroup($owner_group_id);

		// 外部グループのユーザー名の一覧を取得 (同じグループのユーザーは除く)
		$other_users = $this->_get_other_group_users($data_id, $owner_group_id, $other_group_id);

		// PUBLIC_API に POST 送信するパラメータ
		$param = array(
			'username'   => $username,	// ActiveDirectory認証のユーザー名
			'datasetId'  => $data_id,	// 移動対象となるデータセットのID
			'groupId'    => $owner_group_id,	// グループの内部ID
			'otherUsers' => implode("\t", $other_users),	// 相手グループ内のユーザー名
			'signature'  => '',
		);

		// PUBLIC_API に POST 送信
		// API 送信先 URL
		$url = ConnectionManager::$config->default_api['discharge_intergroup'];
		$result = curl_post_contents($url, $param);
		// 結果 (JSON) をデコード (成功していたら status == 200)
		if ($result == 'OK') {
			// 戻すデータセットディレクトリ
			$new_dir_name = sprintf(DIR_INTRAGROUP, $group_id, $data_id);

			// ディレクトリの関連付けを設定
			$this->Dataset->saveToDirName($data_id, $new_dir_name);
			return true;
		}
		return false;
	}

	// 外部グループのユーザー名の一覧を取得 (同じグループのユーザーは除く)
	protected function _get_other_group_users ($data_id, $owner_group_id, $other_group_id = null) {
		$other_users = array();
		if ($other_group_id) {	// 引数に外部グループのIDがあればそれを
			// 念のため、自グループのグループIDが外部グループのリストにあれば、それを削除する
			if (is_array($other_group_id)) {
				$idx = array_search($owner_group_id, $other_group_id);
				if ($idx !== false)	unset( $other_group_id[ $idx ] );	// 同じグループは不要
			}

			// 外部グループのユーザー名の一覧
			$other_users = $this->UserAuth->getUserNamesByGroup($other_group_id);

		} else {	// 外部グループの指定がなければ DB から外部の共有グループを取得
			// 共有先グループ名を取得
			$shared_group = $this->Share->getSharedGroup($data_id);
			unset( $shared_group[ $owner_group_id ] );	// 同じグループは不要

			// 外部グループのユーザー名の一覧
			if (count($shared_group))
				$other_users = $this->UserAuth->getUserNamesByGroup( array_keys($shared_group) );
		}
		return $other_users;
	}


}
