$(function() {
	// Flash メッセージがあれば、10秒後に非表示
	var flash_id = '.alert.flash';
	if ($(flash_id).length) {
		$(flash_id).css('cursor', 'pointer')
			.on('click', function () { $(this).slideUp('normal') });

		// 10秒後に非表示
//		setTimeout(function () { $(flash_id).slideUp('slow') }, 10 * 1000);
	}

	// table link (to use data-table)
	$(document).on('click', 'tr[data-href]', function (e) {
		location.href = $(this).data('href') || $(this).attr('data-href');
	}).on('click', 'td[data-href]', function (e) {
		if (!$(this).hasClass('tabledit-edit-mode'))	// ! edit-mode
			location.href = $(this).data('href') || $(this).attr('data-href');
	}).on('click', 'tr[data-href] a', function (e) {
		e.stopPropagation();
		return true;
	});

	// コメントリセット
	$('button.reset-comment').on('click', function (e) {
		$(this).closest('form')
			.find('textarea[name="comment"],textarea[name="detail"]').val('');
	});
});

// checkbox 操作
$(function() {
	// check on checkbox/radio in table on click table-row
	$(document).on('click', 'tr.click-checked', function (e) {
		$('input:radio', this).prop('checked', true).triggerHandler('change');
		$('input:checkbox', this).each(function () {
			$(this).prop('checked', !$(this).is(':checked'));
		}).triggerHandler('change');
	});
	// stop event bubbling
	$(document).on('click', 'tr.click-checked input:radio,'+
							'tr.click-checked input:checkbox', function (e) {
		e.stopPropagation();
	});

	// check all on checkbox in table on click checkbox in table-head
	$(document).on('click', 'thead input:checkbox', function (e) {
		var checked = $(this).is(':checked');
		$(this).closest('table').find('input:checkbox').prop('checked', checked);
		e.stopPropagation();
	});
});

// jQuery.dataTable の「テーブルにデータがない時の colspan="0"」に対応する
// jQuery.dataTable の「"Search", "Pager" をスクロールさせない」に対応する
$(function() {
	// 少し遅延して実行 (dataTable の構築待ち)
	setTimeout(function () {
		// 「テーブルにデータがない時の colspan="0"」に対応する
		$('table.dataTable').each(function () {
			var $empty_td = $('td[colspan="0"]', this);
			if ($empty_td.length > 0) {
				$empty_td.attr('colspan',
					$('thead tr', this).eq(0).find('th,td').length);
			}
		});

		// 「"Search", "Pager" をスクロールさせない」に対応する
		// .. <table.dataTable> を囲む要素を作る
		$('table.dataTable').each(function () {
			var $wrapper = $('<div class="dataTables_box"></div>');
			$(this).after($wrapper).appendTo($wrapper);
		});
	}, 10);
});

// キーワードで翻訳する .. get translated word by user Language
// .. 出力ターゲット（第二引数）があれば、そちらに出力する
function Trans (name, $target) {
	if (!name) return;

	// キャッシュされた翻訳データがあればそれを取得
	if (Lang.hash && Lang.hash[name]) {
		if ($target) $target.text(Lang.hash[name]);
		return Lang.hash[name];
	}

	// HTML 内に同名のキャッシュされた要素があればそれを取得
	// .. ID の最初に "_" が一つ余計につくことに注意
	var val = $('#_'+ name).val() || $('#_'+ name).text();
	if (val) {
		if ($target) $target.text(val);
		return val;
	}

	// なければ、Ajax で取得してきてから出力する (※要ターゲット)
	if ($target) return Lang(name, $target);

	// なければ、キーワードをキャメルケースにして返す
	return name.replace(/_(.)/g, function (s, w) { return w.toUpperCase(); });
}
var l = Trans;	// alternate function

// Ajax で翻訳データを取得してくる
// .. 引数があれば、取得してきたデータを元に、ターゲット要素に出力する
function Lang (name, $target) {
	// キャッシュされた翻訳データがあればそれを取得して返す
	if (name && Lang.hash && Lang.hash[name]) {
		if ($target) $target.text(Lang.hash[name]);
		return Lang.hash[name];
	}
	// 一度翻訳データを取得していたら返す
	if (Lang.hash) return '';

	// 初期化
	Lang.hash = {};

	// 翻訳データを取得 (非同期)
	$.ajax({
		url: "/Lang/ajax.json",	// get JSON on PHP
		dataType: 'json',
		success: function (data) {
			Lang.hash = data;

			// 引数があれば、ターゲット要素に出力する
			if (name && $target && Lang.hash[name]) {
				$target.text( Lang.hash[name] );
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('Error : ' + errorThrown);
			Lang.hash = null;
		}
	});
	return;
}
Lang.hash = null;
