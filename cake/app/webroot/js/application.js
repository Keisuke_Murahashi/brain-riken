// 翻訳データを取得
if (window.Lang) Lang();
//////////////////////////////////////////////////////////////////////
$(function () {
	$('#appadminbtn1,#appadminbtn2,#appadminbtn4').on('click', function(event){
	//$('#sharebtn,#publishbtn').click(function(){
	var button = $(this);
	var btntype = button.attr("id");
	if (btntype == "appadminbtn4"){
		var	app_id = $("input[name='rdo_app3']:checked").val();
	}else{
		var	app_id = $("input[name='rdo_app1']:checked").val();
	}

	if (app_id === undefined){
		//alert("app_idが選択されていません。")
		alert(l('_select_data_from_waiting_list'))
		//event.preventDefault();
		return;
	}

	$.ajax({
			type: "POST",
			url: "/Application/ajax.json",	// get JSON on PHP
			data: { id: app_id },
			dataType: 'json',
			cache : false,
			async: true,
			success: function(data){
				search_msg_data = data;
				search_html = search_json(search_msg_data, btntype);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				// NIJC(20160701)
				//alert("失敗しました");
				//alert('Error : ' + errorThrown);
				alert(l("_failed_to_communicate_to_server"));

			}
		}).done(function(data) {
			//処理
			if (btntype == "appadminbtn1"){
				$("#p-lg1").modal("show");
			}else if (btntype == "appadminbtn2"){
				$("#p-lg2").modal("show");
			}else if (btntype == "appadminbtn4"){
				$("#p-lg4").modal("show");
			}

	   });
	});

	function search_json(search_msg_data, btntype){
		for (var i = 0, I = search_msg_data.length; i < I; i++) {
			var item = search_msg_data[i];
			var search_html2 = "";

console.log(search_msg_data);
			// NIJC  search_html2 += '<tr><th class="col-xs-3 col-sm-3 col-md-3 success">' + l('_app_id') + '</th><td class="col-xs-9 col-sm-9 col-md-9">' + item['app_id'] + '</td></tr>';
			search_html2 += '<tr><th class="col-xs-3 col-sm-3 col-md-3 success">' + l('_app_id') + '</th><td class="col-xs-9 col-sm-9 col-md-9">' + item['appid'] + '</td></tr>';
			search_html2 += '<tr><th class="col-xs-3 col-sm-3 col-md-3 success">' + l('_data_id') + '</th><td class="col-xs-9 col-sm-9 col-md-9">' + item['data_id'] + '</td></tr>';
			search_html2 += '<tr><th class="col-xs-3 col-sm-3 col-md-3 success">' + l('_app_type') + '</th><td class="col-xs-9 col-sm-9 col-md-9">' + item['app_type'] + '</td></tr>';
			search_html2 += '<tr><th class="col-xs-3 col-sm-3 col-md-3 success">' + l('_creator') + '</th><td class="col-xs-9 col-sm-9 col-md-9">' + item['creator'] + '</td></tr>';
			search_html2 += '<tr><th class="col-xs-3 col-sm-3 col-md-3 success">' + l('_app_date') + '</th><td class="col-xs-9 col-sm-90 col-md-9">' + item['date'] + '</td></tr>';
			// if (item['app_type'] == '公開'){
			if (item['app_type'] == 'Release'){
				var target = item['group'];
			}else{
				// NIJC var target =  item['Group']['name'];
				var target =  item['groupid'];
			}
			search_html2 += '<tr><th class="col-xs-3 col-sm-3 col-md-3 success">' + l('_app_target') + '</th><td class="col-xs-9 col-sm-9 col-md-9">' + target + '</td></tr>';
		}
		if (btntype == "appadminbtn1"){
			var modal = $("#p-lg1");  //モーダルを取得
			modal.find('#table_app1').html(search_html2);
		}else if (btntype == "appadminbtn2"){
			var modal = $("#p-lg2");  //モーダルを取得
			modal.find('#table_app2').html(search_html2);
		}else if (btntype == "appadminbtn4"){
			var modal = $("#p-lg4");  //モーダルを取得
			modal.find('#table_app4').html(search_html2);
		}

		// NIJC modal.find('.modal-body input#app_id').val(item['app_id']); //inputタグにも表示
		modal.find('.modal-body input#app_id').val(item['appid']); //inputタグにも表示
		modal.find('.modal-body input#data_id').val(item['data_id']); //inputタグにも表示

		return search_html2;
	}
});
////////////////////////////////////////////////////////////////////

$(function () {

	//申請承認取り下げモーダル
	$('#appadminbtn3').on('click', function(event){

		var ajax_data;
		var	act_id = $("input[name='rdo_app2']:checked").val();
		if (act_id === undefined){
			//alert("act_idが選択されていません。")
			alert(l('_select_data_from_done_list'))
			//event.preventDefault();
			return;
		}
		$.ajax({
				type: "POST",
				url: "/Application/ajax2.json",	// get JSON on PHP
				data: { id: act_id },
				dataType: 'json',
				cache : false,
				success: function(data){
					ajax_data = data;
					set_app_data(data); //申請データをセット
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					// NIJC(20160701)
					//alert("失敗しました");
					//alert('Error : ' + errorThrown);
					alert(l("_failed_to_communicate_to_server"));
				}
	   }).done(function(data) {
			// if (ajax_data[0]['action'] != '承認'){
			if (ajax_data[0]['action'] != 'Accepted'){
				alert(l('_select_approved_data'))
				//event.preventDefault();
				return;
			}else{
				$.ajax({
					type: "POST",
					url: "/UserManagement/ajax.json",	// get JSON on PHP
					data: {},
					dataType: 'json',
					cache : false,
					async: true,
					success: function(data){
						search_msg_data = data;
						search_html = search_json(search_msg_data); // 承認者リストをセット
					},
					error: function(XMLHttpRequest, textStatus, errorThrown){
						// NIJC(20160701)
						//alert("失敗しました");
						//alert('Error : ' + errorThrown);
						alert(l("_failed_to_communicate_to_server"));
					}
				}).done(function(data) {
					//処理
					$("#p-lg5").modal("show");
			   });
			}
	   });

		function set_app_data(ajax_data){
			var app_data = ajax_data[0];
			var button = $(this);
			var btntype = $(this).attr("id");
console.log(app_data);

			var modal = $("#p-lg5");  //モーダルを取得
			var search_html2 = '';
			// search_html2 += '<tr><th class="col-xs-2 col-sm-2 col-md-2 success">' + l('_act_id') + '</th><td class="col-xs-10 col-sm-10 col-md-10">' + app_data['act_id'] + '</td></tr>';
			search_html2 += '<tr><th class="col-xs-2 col-sm-2 col-md-2 success">' + l('_act_id') + '</th><td class="col-xs-10 col-sm-10 col-md-10">' + app_data['actid'] + '</td></tr>';
			// search_html2 += '<tr><th class="col-xs-2 col-sm-2 col-md-2 success">' + l('_app_id') + '</th><td class="col-xs-10 col-sm-10 col-md-10">' + app_data['app_id'] + '</td></tr>';
			search_html2 += '<tr><th class="col-xs-2 col-sm-2 col-md-2 success">' + l('_app_id') + '</th><td class="col-xs-10 col-sm-10 col-md-10">' + app_data['appid'] + '</td></tr>';
			search_html2 += '<tr><th class="col-xs-2 col-sm-2 col-md-2 success">' + l('_authorizer') + '</th><td class="col-xs-10 col-sm-10 col-md-10">' + app_data['authorizer'] + '</td></tr>';
			search_html2 += '<tr><th class="col-xs-2 col-sm-2 col-md-2 success">' + l('_action') + '</th><td class="col-xs-10 col-sm-10 col-md-10">' + app_data['action'] + '</td></tr>';
			search_html2 += '<tr><th class="col-xs-2 col-sm-2 col-md-2 success">' + l('_comment') + '</th><td class="col-xs-10 col-sm-10 col-md-10">' + app_data['comment'] + '</td></tr>';
			search_html2 += '<tr><th class="col-xs-2 col-sm-2 col-md-2 success">' + l('_act_date') + '</th><td class="col-xs-10 col-sm-10 col-md-10">' + app_data['act_date'] + '</td></tr>';

			modal.find('#table_app5').html(search_html2);

			//NIJC modal.find('.modal-body input#app_id').val(app_data['app_id']); //inputタグにも表示
			//NIJC  modal.find('.modal-body input#act_id').val(app_data['act_id']); //inputタグにも表示
			modal.find('.modal-body input#app_id').val(app_data['appid']); //inputタグにも表示
			modal.find('.modal-body input#act_id').val(app_data['actid']); //inputタグにも表示
		}

	});

	function search_json(search_msg_data){
		var search_html = "";
		for (var i = 0, I = search_msg_data['authorizer'].length; i < I; i++) {
			var item = search_msg_data['authorizer'][i];
			search_html += '<option value="' + item['username'] + '">'+
								item['name'] + ' (' + item['group_name'] + ') ' + '[' + l(item['role']) + ']' + '</option>';
		}
		var modal = $("#p-lg5");  //モーダルを取得
		search_html = '<select class="form-control col-md-12" name="authorizer">' + search_html + '</select>';
		modal.find('.modal-body #search_data').html(search_html); //inputタグにも表示

		return search_html;
	}

});

$(function () {
	$('#table1').dataTable({
		"language": {
            "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
        },
		"order": [],			// DO NOT touch the order of query
		bLengthChange:false
	});
});

$(function () {
	$('#table2').css('width', '100%').dataTable({
		"language": {
            "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
        },
		"order": [],
		bLengthChange:false
	});
});

$(function () {
	$('#table3').dataTable({
		"language": {
            "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
        },
		"order": [],
		bLengthChange:false
	});
});
$(function () {
	$('#table4').css('width', '100%').dataTable({
		"language": {
            "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
        },
		"order": [],
		bLengthChange:false,
		// NIJC  "order": [[ 4, 'desc' ]]
	});
});

$(function () {
	//$(".appbtn").click(function(event){
	$(".appbtn").on('click', function(event){
	var button = $(this);
	var app_id = button.data('app_id') //data-whatever の値を取得
		$.ajax({
				type: "POST",
/*
				url: "/ajax/app_hist_list.php",
				data: {
					input1: app_id,
					input2: ""
				},
*/
				url: "/Application/ajax.json",	// get JSON on PHP
				data: { id: app_id },
				dataType: 'json',
				cache : false,
				async: true,
				success: function(data){
					search_msg_data = data;
					search_html = search_json(search_msg_data);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					// NIJC(20160701)
					//alert("失敗しました");
					//alert('Error : ' + errorThrown);
					alert(l("_failed_to_communicate_to_server"));
				}
	   }).done(function(data) {
			//処理
			$("#p-lg3").modal("show");
	   });
	});

	function search_json(search_msg_data){

		var search_html2 = '';

		for(var i in search_msg_data){
			var row = search_msg_data[i];
			//for (var key in row) {
				//search_html2 += '<li class="list-group-item"><strong>' + key + '：</strong>' + row[key] + '</li>';
				//search_html2 += '<tr><th class="col-xs-2 col-sm-2 col-md-2 success">' + key + '</th><td class="col-xs-10 col-sm-10 col-md-10">' + row[key] + '</td></tr>';
				search_html2 += '<tr><th class="col-xs-3 col-sm-3 col-md-3 success">' + l('_app_id') + '</th><td class="col-xs-9 col-sm-9 col-md-9">' + row['app_id'] + '</td></tr>';
				search_html2 += '<tr><th class="col-xs-3 col-sm-3 col-md-3 success">' + l('_data_id') + '</th><td class="col-xs-9 col-sm-9 col-md-9">' + row['data_id'] + '</td></tr>';
				search_html2 += '<tr><th class="col-xs-3 col-sm-3 col-md-3 success">' + l('_app_type') + '</th><td class="col-xs-9 col-sm-9 col-md-9">' + row['app_type'] + '</td></tr>';
				search_html2 += '<tr><th class="col-xs-3 col-sm-3 col-md-3 success">' + l('_creator') + '</th><td class="col-xs-9 col-sm-9 col-md-9">' + row['creator'] + '</td></tr>';
				search_html2 += '<tr><th class="col-xs-3 col-sm-3 col-md-3 success">' + l('_app_date') + '</th><td class="col-xs-9 col-sm-90 col-md-9">' + row['date'] + '</td></tr>';
				// NIJC if (row['app_type'] == '公開'){
				if (row['app_type'] == 'Release'){
					var target = row['group'];
				}else{
					var target =  row['Group']['name'];
console.log(row);
				}
				search_html2 += '<tr><th class="col-xs-3 col-sm-3 col-md-3 success">' + l('_app_target') + '</th><td class="col-xs-9 col-sm-9 col-md-9">' + target + '</td></tr>';

			//};
		};

		var modal = $("#p-lg3");  //モーダルを取得
		//modal.find('.list-group').html(search_html2);
		modal.find('#table_app3').html(search_html2);
		return search_html2;
	}
});

$(function(){
    $('a[data-toggle="tab"]').each(function(i){
        if($(this).parent().attr("class") == 'active'){
            //alert($(this).attr("href"));
        }
    });
});

// NIJC(20160622)
$(function() {
    $(".nav-tabs a").each(function(target) {
        if ($(this).parent().attr("class") == "active") {
            toggle_appadminbtn($(this).attr("href"));
        }
    });

    $(".nav-tabs a").on("shown.bs.tab", function(event) {
        toggle_appadminbtn($(event.target).attr("href"));
    });

    function toggle_appadminbtn(href) {
        if (href == "#tab1") {
            $("#appadminbtn1").show();
            $("#appadminbtn2").show();
            $("#appadminbtn3").hide();
        } else if (href == "#tab2") {
            $("#appadminbtn1").hide();
            $("#appadminbtn2").hide();
            $("#appadminbtn3").show();
        }
    }
});
