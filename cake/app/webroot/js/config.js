// 翻訳データを取得
if (window.Lang) Lang();

/* Config */

$(function(){
	if ($('#table-config').length == 0) return;

	// 設定の更新
	$('#table-config').Tabledit({
		url: '/Config/ajax.json',	// get JSON on PHP
		autoFocus: true,	// 編集開始時にオートフォーカス？
		editButton: false,	// 編集ボタンを表示？ (false のときはクリックで編集)
		saveButton: false,	// 編集中に保存ボタンを表示？
		deleteButton: true,	// 削除ボタンを表示？
		restoreButton: false,	// 削除後、復元ボタンを表示？
		buttons: {
			edit: {
				class: 'btn btn-sm btn-success',
				html: '<span class="glyphicon glyphicon-pencil"></span>',
				action: 'edit'
			},
			delete: {
				class: 'btn btn-sm btn-default',
				html: '<span class="glyphicon glyphicon-trash"></span>',
				action: 'delete'
			}
		},
		columns: {
			identifier: [0, 'id'],	// ID
			editable: [	// set editable fields [<td> index, field-name]
				[1, 'key'], [2, 'value'], [3, 'memo']
			]
		},
		onDraw: function() {
		},
		onSuccess: function(data, textStatus, jqXHR) {
			// Reload this page if action "new"
			if (data && (data.action == 'new' || data.action == 'delete')) location.reload();
		},
		onFail: function(jqXHR, textStatus, errorThrown) {
			alert(textStatus);
			alert(errorThrown);
		},
		onAlways: function() {
		},
		onAjax: function(action, serialize) {
		}
	});
});

/* Dictionary */

$(function(){
	if ($('#table-dictionary').length == 0) return;

	// 辞書の更新
	$('#table-dictionary').Tabledit({
		url: '/Dictionary/edit.json',	// get JSON on PHP
		autoFocus: true,	// 編集開始時にオートフォーカス？
		editButton: false,	// 編集ボタンを表示？ (false のときはクリックで編集)
		saveButton: false,	// 編集中に保存ボタンを表示？
		deleteButton: true,	// 削除ボタンを表示？
		restoreButton: false,	// 削除後、復元ボタンを表示？
		buttons: {
			edit: {
				class: 'btn btn-sm btn-success',
				html: '<span class="glyphicon glyphicon-pencil"></span>',
				action: 'edit'
			},
			delete: {
				class: 'btn btn-sm btn-default',
				html: '<span class="glyphicon glyphicon-trash"></span>',
				action: 'delete'
			}
		},
		columns: {
			identifier: [0, 'id'],	// ID
			editable: [	// set editable fields [<td> index, field-name]
				[1, 'term'], [2, 'memo']
			]
		},
		onDraw: function() {
		},
		onSuccess: function(data, textStatus, jqXHR) {
			// Reload this page if action "new"
			if (data && (data.action == 'new' || data.action == 'delete')) location.reload();
		},
		onFail: function(jqXHR, textStatus, errorThrown) {
			alert(textStatus);
			alert(errorThrown);
		},
		onAlways: function() {
		},
		onAjax: function(action, serialize) {
		}
	});

	// dataTable
	$('#table-dictionary').dataTable({
		"language": {
            "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
        },
		bLengthChange : false,
		order : [ [0, 'desc'] ],
		columnDefs: [
			{ type: 'html-num-fmt', targets: 0 }
		]
	});
});

/* DataFilter setting */

$(function(){
	if ($('form.filter-setting').length == 0) return;
	var highlight = '#fffad0';	// ハイライトカラー

	// チェックボックスを ON にしたとき、データを設定しに行く
	$('form.filter-setting input.key:checkbox').on('change', function () {
		var $this = $(this);
		var post_data = {
			id        : $this.data('id'),
			key       : $this.data('key') || $this.attr('name'),
			value     : $this.is(':checked') ? parseInt( $this.val() ) : 0,
			data_type : $this.data('type') ||
					    $this.closest('form').find('input[name="data_type"]').val(),
			action    : 'edit'
		};
		// チェック状態を通信
		return ajax_filter.call(this, post_data);
	});

	// 「全てにチェック」を ON にしたとき、すべてのチェックを ON にし、データを設定しに行く
	$('.check-all').on('click', function () {
		if (!confirm(l('_confirm_to_check_all'))) return false;	// confirm

		// すべてのチェック状態を通信
		return check_all_filter.call(this, true);
	});

	// 「全てのチェックをはずす」を ON にしたとき、すべてのチェックを OFF にし、データを設定しに行く
	$('.uncheck-all').on('click', function () {
		if (!confirm(l('_confirm_to_uncheck_all'))) return false;	// confirm

		// すべてのチェック状態を通信
		return check_all_filter.call(this, false);
	});

	// すべてのチェック状態を通信
	function check_all_filter (checked) {
		var post_data = {
			id        : 'all',
			key       : null,
			value     : checked ? 1 : 0,
			data_type : $(this).closest('form').find('input[name="data_type"]').val(),
			action    : 'edit'
		};
		if (!post_data.data_type) return false;

		// チェック状態を通信
		return ajax_filter.call(this, post_data);
	}

	// チェック状態を通信
	function ajax_filter (post_data) {
		console.log('ajax_filter.post', post_data);

		// 通信して設定
		$.ajax({
			url: '/Config/ajax_filter.json',
			type: "POST",
			data: post_data,
			dataType: 'json',
			cache : false,
			context: this,
			success: function (results) {
				console.log('ajax_filter.result', results);
				if (!results || typeof results != 'object') { alert(results); return; }	// エラー

				// <form>
				var $form = $(this).closest('form');

				// チェックを付ける
				for (var i = 0, I = results.length; i < I; i++) {
					var result = results[i];
					var $check = $form.find('input:checkbox[name="'+ result.key +'"]');

					// チェックを付ける
					$check.prop('checked', result.value > 0)
							.data('id', result.id)	// ID を設定

					// 日付の更新
					var $tr = $check.closest('tr').stop(true);	// <TR>
					$tr.find('td.date').text(result.update);

					// ハイライト
					$tr.animate({ 'background-color': highlight }, 400, 'swing', function () {
						var $tr = $(this).stop(true);
						$tr.animate({ 'background-color': $tr.data('bgcolor') || '#ffffff' }, 1000, 'swing');
					});
				}
			},
			error: function () {
				// NIJC(20160701)
				//alert("通信に失敗しました");
				alert(l("_failed_to_communicate_to_server"));
			}
		});
		return false;
	}

	// フォームの送信は無効
	$('form.filter-setting').on('submit', function () { return false; });

	// ハイライト用に、現在の <tr> の背景色をキャッシュ
	$('form.filter-setting tr').each(function () {
		var $tr = $(this);
			$tr.data('bgcolor', $tr.css('background-color'));
	});
});

$(function(){
	// グループ選択
	$('#config_select_group').on('change', function () {
		location.href = '/Config/filter/'+ $(this).val();
	});
});
