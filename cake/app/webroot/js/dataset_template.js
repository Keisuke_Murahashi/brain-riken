// NIJC(20160629): load message resource
if (window.Lang) {
	Lang();
};

$(window).on('load', function() {
	// NIJC(20160714)
	//$('input:text').autocomplete({
	//	source: '/Dictionary/ajax.json'
	//});

	// NIJC(20160624): removed
	//$('#editor div.well button.json-editor-btn-collapse').click();
});

$(function() {
	var schema;
	if(window.location.href.match('[?&]schema=([^&]+)')) {
		try {
			schema = JSON.parse(
						LZString.decompressFromBase64(
							window.location.href.match('[?&]schema=([^&]+)')[1]));
		}
		catch(e) {
			console.log('invalid starting schema');
		}
	}

	// Default starting schema
	if (!schema) {
		var schema_string = $('#json_scheme').val();
		if (schema_string) schema = eval('('+ schema_string +')');
	}

	// Divs/textareas on the page
	var $schema = document.getElementById('schema');
	var $output = document.getElementById('output');
	var $editor = document.getElementById('editor');
	var $validate = document.getElementById('validate');

	// Buttons
	var $set_schema_button = document.getElementById('setschema');
	var $set_value_button = document.getElementById('setvalue');

	var jsoneditor;

	var updateDirectLink = function() {
		var url = window.location.href.replace(/\?.*/,'');

		url += '?schema='+LZString.compressToBase64(JSON.stringify(schema));
		url += '&value='+LZString.compressToBase64(JSON.stringify(jsoneditor.getValue()));

		for(var i in JSONEditor.defaults.options) {
			if(!JSONEditor.defaults.options.hasOwnProperty(i)) continue;
			if(JSONEditor.defaults.options[i]===false) continue;
			else if(JSONEditor.defaults.options[i]===true) {
				url += '&'+i;
			}
			else {
				url += '&'+i+'='+JSONEditor.defaults.options[i];
			}
		}

		var direct_link = document.getElementById('direct_link');
		if (direct_link) direct_link.href = url;
	};

	// get JSON
	var _startval = null;				// NIJC(20160624): rename var
	var json_string = $('#data_json').val();
	if (json_string) _startval = eval('('+ json_string +')');

	var reload = function(keep_value) {
		var startval = (jsoneditor && keep_value)? jsoneditor.getValue() : window.startval;
		window.startval = undefined;

		if(jsoneditor) jsoneditor.destroy();
		jsoneditor = new JSONEditor($editor,{
			schema: schema,
			//startval: hoge		// NIJC(20160623)
			startval: _startval,		// NIJC(20160624)
			keep_oneof_values: false,	// NIJC(20160623)
			show_errors: "always"		// NIJC(20160623)
		});
		window.jsoneditor = jsoneditor;

		// When the value of the editor changes, update the JSON output and validation message
		jsoneditor.on('change',function() {
			var json = jsoneditor.getValue();

			$output.value = JSON.stringify(json,null,2);

			var validation_errors = jsoneditor.validate();
			// Show validation errors if there are any
			if(validation_errors.length) {
				$validate.value = JSON.stringify(validation_errors,null,2);
			}
			else {
				$validate.value = 'valid';
			}

			//updateDirectLink();		// NIJC(20160707): element for direct link is missing
		});

		// NIJC(20160623)
		$(".row > .col-md-12 > .form-group > .control-label, .row > .col-md-12 > label", $editor).each(function(index, target) {
			var label = $(target);
			if (label.length) {
				label.replaceWith("<h3>" + label.text() + "</h3>");
			}
		});
                $(".row td > label", $editor).each(function(index, target) {
                        var label = $(target);
                        if (label.length) {
                                label.text("");
                        }
                });
	};

	// Start the schema and output textareas with initial values
	$schema.value = JSON.stringify(schema,null,2);
	$output.value = '';

	// When the 'update form' button is clicked, set the editor's value
	$set_value_button.addEventListener('click',function() {
		jsoneditor.setValue(JSON.parse($output.value));
	});

	// Update the schema when the button is clicked
	$set_schema_button.addEventListener('click',function() {
		try {
			schema = JSON.parse($schema.value);
		}
		catch(e) {
			alert('Invalid Schema: '+e.message);
			return;
		}

		reload();
	});

	// Set the theme by loading the right stylesheets
	var setTheme = function(theme,no_reload) {
		theme = theme || '';

		var mapping = {
			html: '',
			bootstrap2: '//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css',
			bootstrap3: '//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css',
			foundation3: '//cdnjs.cloudflare.com/ajax/libs/foundation/3.2.5/stylesheets/foundation.css',
			foundation4: '//cdn.jsdelivr.net/foundation/4.3.2/css/foundation.min.css',
			foundation5: '//cdn.jsdelivr.net/foundation/5.0.2/css/foundation.min.css',
			jqueryui: '//code.jquery.com/ui/1.10.3/themes/south-street/jquery-ui.css'
		};

		if(typeof mapping[theme] === 'undefined') {
			theme = 'bootstrap3';
			document.getElementById('theme_switcher').value = theme;
		}

		JSONEditor.defaults.options.theme = theme;

		document.getElementById('theme_stylesheet').href = mapping[theme];
		document.getElementById('theme_switcher').value = JSONEditor.defaults.options.theme;

		if(!no_reload) reload(true);
	};

	// Set the icontheme
	// Set the theme by loading the right stylesheets
	var setIconlib = function(iconlib,no_reload) {
		iconlib = iconlib || '';
		var mapping = {
			foundation2: '//cdnjs.cloudflare.com/ajax/libs/foundicons/2.0/stylesheets/general_foundicons.css',
			foundation3: '//cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css',
			fontawesome3: '//cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.css',
			fontawesome4: '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css'
		};

		JSONEditor.defaults.options.iconlib = iconlib;

		document.getElementById('icon_stylesheet').href = mapping[iconlib] || '';
		document.getElementById('icon_switcher').value = JSONEditor.defaults.options.iconlib;

		if(!no_reload) reload(true);
	};

	var refreshBooleanOptions = function(no_reload) {
		var boolean_options = document.getElementById('boolean_options').children;
		for(var i=0; i<boolean_options.length; i++) {
			JSONEditor.defaults.options[boolean_options[i].value] = boolean_options[i].selected;
		}
		if(!no_reload) reload(true);
	};

	// Change listeners for options
	document.getElementById('theme_switcher').addEventListener('change',function() {
		setTheme(this.value);
	});
	document.getElementById('icon_switcher').addEventListener('change',function() {
		setIconlib(this.value);
	});
	document.getElementById('object_layout').addEventListener('change',function() {
	   JSONEditor.defaults.options.object_layout = this.value;
		reload(true);
	});
	document.getElementById('show_errors').addEventListener('change',function() {
	   JSONEditor.defaults.options.show_errors = this.value;
		reload(true);
	});
	document.getElementById('boolean_options').addEventListener('change',function() {
		refreshBooleanOptions();
	});

	// Get starting value from url
	if(window.location.href.match('[?&]value=([^&]+)')) {
	  window.startval = JSON.parse(LZString.decompressFromBase64(window.location.href.match('[?&]value=([^&]+)')[1]));
	}

	// Set options from direct link
	setTheme((window.location.href.match(/[?&]theme=([^&]+)/)||[])[1] || 'bootstrap3', true);

	setIconlib((window.location.href.match(/[?&]iconlib=([^&]*)/)||[null,'fontawesome4'])[1], true);

	document.getElementById('object_layout').value = (window.location.href.match(/[?&]object_layout=([^&]+)/)||[])[1] || 'normal';
	JSONEditor.defaults.options.object_layout = document.getElementById('object_layout').value;

	document.getElementById('show_errors').value = (window.location.href.match(/[?&]show_errors=([^&]+)/)||[])[1] || 'interaction';
	JSONEditor.defaults.options.show_errors = document.getElementById('show_errors').value;

	var boolean_options = document.getElementById('boolean_options').children;
	for(var i=0; i<boolean_options.length; i++) {
		if(window.location.href.match(new RegExp('[?&]'+boolean_options[i].getAttribute('value')+'([&=]|$)'))) {
			boolean_options[i].selected = true;
		}
	}
	refreshBooleanOptions(true);

	reload();

	//var $output = document.getElementById('output');

	//document.getElementById('scheme').value = JSON.stringify(foo,null,2);
	//document.getElementById('output').value = JSON.stringify(hoge,null,2);

	// NIJC(20160629): add validation when fixed==true and submitted
	$("#form_update").on("submit", function(event) {
		if (jsoneditor) {
			var json = jsoneditor.getValue();
			if (json && json["fixed"] == "true") {
				var errors = jsoneditor.validate();
				if (errors && errors.length) {
					alert(l("_metadata_have_invalid_format"));
					return false;
				}
			}
		}
	});
});
