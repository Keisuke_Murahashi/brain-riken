
function add(){
	var selindex1 = document.selectform.typeSelect.selectedIndex;
	var selvalue1 = document.selectform.typeSelect.value;
	var selindex2 = document.selectform.condSelect.selectedIndex;
	var selvalue2 = document.selectform.condSelect.value;

	var a = document.getElementById('test');
	//a.innerHTML += 'DataType: ' + selvalue1 + '/ Field:' + selvalue2 + '：';
	var info = document.createTextNode('DataType: ' + selvalue1 + '/ Field:' + selvalue2 + '：');
	a.appendChild(info);
	var input = document.createElement("input");
	 input.setAttribute("type","text");
	 input.setAttribute("class", "form-control");
	 input.setAttribute("name",selvalue1 + '[' + selvalue2 + ']');
	 document.getElementById("test").appendChild(input);
	 var br = document.createElement("br");
	 document.getElementById("test").appendChild(br);

}


$(function(){
	$(".aaa").click(function(event){
		var button = $(this);
		var mess_id = button.data('mess_id') //data-whatever の値を取得
		$.ajax({
				type: "POST",
/*
				url: "/ajax/message.php",
				data: {
					input1: mess_id,
					input2: ''
				},
*/
				url: "/Message/ajax.json",		// get JSON on PHP
				data: { id: mess_id },
				dataType: 'json',
				cache : false,
				async: true,
				context: this,
				success: function(data){
					search_msg_data = data;
					search_html = search_json(search_msg_data);

					// クラスを追加
					$('i.fa.read', this).addClass('fa-check');
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					// NIJC(20160701)
					//alert("失敗しました");
					alert('Error : ' + textStatus);
				}
	   }).done(function(data) {
			//処理
			$("#p-lg1").modal("show");
	   });
	   return false;
	});
	function search_json(search_msg_data){

		var search_html2 = '';
		for(var i in search_msg_data){
			var row = search_msg_data[i];
			var str = row["content"];
			search_html2 += '<table class="table table-bordered">';
			search_html2 += '<tbody>';
			search_html2 += '<tr>';
			search_html2 += '<th class="col-xs-2 col-sm-2 col-md-2 success">' + l('_message_id') + '</th>';
			search_html2 += '<td class="col-xs-10 col-sm-10 col-md-10">' + row["mess_id"] + '</td>';
			search_html2 += '</tr>';
			search_html2 += '<tr>';
			search_html2 += '<th class="col-xs-2 col-sm-2 col-md-2 success">' + l('_message_type') + '</th>';
			search_html2 += '<td class="col-xs-10 col-sm-10 col-md-10">' + row["mess_type_title"] + '</td>';
			search_html2 += '</tr>';
			search_html2 += '<tr>';
			search_html2 += '<th class="col-xs-2 col-sm-2 col-md-2 success">' + l('_sender') + '</th>';
			search_html2 += '<td class="col-xs-10 col-sm-10 col-md-10">' + row["author"] + '</td>';
			search_html2 += '</tr>';
			search_html2 += '<tr>';
			search_html2 += '<th class="col-xs-2 col-sm-2 col-md-2 success">' + l('_target') + '</th>';
			search_html2 += '<td class="col-xs-10 col-sm-10 col-md-10">' + row["targets"] + '</td>';
			search_html2 += '</tr>';
			search_html2 += '</tbody>';
			search_html2 += '</table>';
				//search_html2 += '<ul class="list-group">';
				//search_html2 += '<li class="list-group-item"><strong>mess_id：</strong>' + row["mess_id"] + '</li>';
				//search_html2 += '<li class="list-group-item"><strong>mess_type：</strong>' + row["mess_type_title"] + '</li>';
				//search_html2 += '<li class="list-group-item"><strong>target：</strong>' + row["targets"] + '</li>';
				//search_html2 += '</ul>';
				search_html2 += '<div class="panel panel-default">';
				search_html2 += '<div class="panel-heading"><h2 class="panel-title">' + row["title"] + '</h2></div>';
				search_html2 += '<div class="panel-body">';
				search_html2 += '<p>' + str.replace(/\r?\n/g, '<br>') + '</p>';
				search_html2 += '</div>';
				//search_html2 += '<div class="panel-footer text-right"><small class="text-muted"><i class="fa fa-clock-o"></i>' + row["date"] + '&nbsp; |&nbsp; <i class="fa fa-pencil"></i>' + row["author"] + '&nbsp; |&nbsp; <i class="fa ' + row["fa"] + '"></i>' + row["category_class"] + '</small></div>';
				search_html2 += '<div class="panel-footer text-right"><small class="text-muted"><i class="fa fa-clock-o"></i>' + row["date"] + '&nbsp; |&nbsp; <i class="fa fa-pencil"></i>' + row["author"] + '</small></div>';
				search_html2 += '</div>';
				search_html2 += '<input type="hidden" id="mess_id" name="mess_id" value="' + row["mess_id"] + '"/>'
		};
		var modal = $("#p-lg1");  //モーダルを取得
		modal.find('#mess').html(search_html2);

		// システムメッセージのときは「返信」ボタン不要
		(row['mess_type'] == 'system') ?
			modal.find(':submit').hide() :
			modal.find(':submit').show();

		return search_html2;
	}
});


$(function(){
   $('mess-form').on('submit', function(e){
		e.preventDefault();
		//alert("submit");
		var $form = $(this);
		$.ajax({
			url: $form.attr('action'),
			type: "POST",
			data: $("mess-form").serialize(),
			cache : false,
			success: function(data){
				alert("Successfully submitted.")
			}
		});
   });
});
