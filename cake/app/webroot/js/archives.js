$(function(){
	$('.table_id').css('width', '100%').dataTable({
		"language": {
            "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
        },
		bLengthChange:false
	});
});

$(function(){
	$('a[data-monthly]').on('click', function () {
		var monthly_id = $(this).data('monthly');
		$('.monthly').not(monthly_id).hide();	// other
		if (monthly_id) $(monthly_id).fadeIn();	// target
	});
	$('li.active a[data-monthly]').triggerHandler('click');

	// change tab by URL hash
	if (location.hash) {
		var $elm = $('a[href="'+ location.hash +'"]');
		if ($elm.data('toggle') == 'tab') $elm.click();
	}
});
