<?php
	try{
		$dbh = getDBH();
		// NIJC $sql = "select a.dir_name, a.data_id, a.data_type, b.status status1, c.status  status2 from dataset a left outer join app b on a.data_id = b.data_id and b.app_type = '共有' left outer join app c on a.data_id = c.data_id and c.app_type = 'Release' where a.owner = ?";
		$sql = "select a.dir_name, a.data_id, a.data_type, b.status status1, c.status  status2 from dataset a left outer join app b on a.data_id = b.data_id and b.app_type = 'Share' left outer join app c on a.data_id = c.data_id and c.app_type = 'Release' where a.owner = ?";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array($username));

		$no = 1;
		$title = "";
		$subject = "";
		$contact_person = "";
		$shared_date = "";

		while($row = $stmt -> fetch(PDO::FETCH_ASSOC)) {
			$dir_name = htmlspecialchars($row['dir_name']);
			$data_id = htmlspecialchars($row['data_id']);
			$data_type = htmlspecialchars($row['data_type']);
			$status1 = htmlspecialchars($row['status1']);
			$status2 = htmlspecialchars($row['status2']);

			$mongo = new MongoClient();
			$db = $mongo->test;
			$col = $db->test1;
			$cursor = $col->find(array( 'data_id' => $data_id ));
			//$col->insert($obj);
			//$cursor = $col->find();

			foreach ($cursor as $doc) {
				if (!is_array($doc["title"]) and isset($doc["title"])) $title = htmlspecialchars($doc["title"]);
				if (!is_array($doc["subject"]) and isset($doc["subject"])) $subject = htmlspecialchars($doc["subject"]);
				if (!is_array($doc["contact_person"]) and isset($doc["contact_person"])) $contact_person = htmlspecialchars($doc["contact_person"]);
				if (!is_array($doc["shared_date"]) and isset($doc["shared_date"])) $shared_date = htmlspecialchars($doc["shared_date"]);
			}
			echo "<tr><td><input type=\"radio\" name=\"rdo1\" value=\"$data_id\"></td><td>$dir_name</td><td>$data_id</td><td>$data_type</td><td>$shared_date</td><td>$title</td><td>$subject</td><td>$contact_person</td><td><span class=\"label label-default\">$status1</span></td><td><span class=\"label label-default\">$status2</span></td><td><button class=\"btn btn-xs btn-success\" type=\"button\" onclick=\"location.href='DatasetDetail.php?data_id=$data_id'\">detail</button></td></tr>";
			$no = $no + 1;
		}
	}catch (PDOException $e){
		print('Error:'.$e->getMessage());
		die();
	}
	$dbh = null;
