<?php
	$user_name = $_SESSION['username'];
	$dbh = getDBH();

	$app_id = $_POST['app_id'];
	$comment = $_POST['comment'];

	//申請データ更新
	// NIJC $sql = "UPDATE app SET status = '承認済' where app_id = ?";
	$sql = "UPDATE app SET status = 'Accepted' where app_id = ?";
	$stmt = $dbh->prepare($sql);
	$stmt->execute(array($app_id));


	//承認データ作成
	$act_id = getActID();
	$sql = "INSERT INTO app_history(act_id, app_id, authorizer, action, comment, act_date) VALUES (?, ?, ?, ?, ?, CURRENT_TIMESTAMP())";
	$stmt = $dbh->prepare($sql);
	$stmt->execute(array($act_id, $app_id, $user_name, '承認', $comment));

	$dbh = null;
