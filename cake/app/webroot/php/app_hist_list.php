<?php
	try{
		$dbh = getDBH();
		$username = $_SESSION['username'];
		$sql = "select a.act_id, a.app_id, a.authorizer, a.action, a.comment, a.act_date, b.data_id from app_history a, app b where a.app_id = b.app_id";
		$stmt = $dbh->prepare($sql);
		$stmt->execute();

		$no = 1;

		while($row = $stmt -> fetch(PDO::FETCH_ASSOC)) {
			$act_id = htmlspecialchars($row['act_id']);
			$app_id = htmlspecialchars($row['app_id']);
			$authorizer = htmlspecialchars($row['authorizer']);
			$action = htmlspecialchars($row['action']);
			$comment = htmlspecialchars($row['comment']);
			$act_date = htmlspecialchars($row['act_date']);
			$data_id = htmlspecialchars($row['data_id']);
			echo "<tr><td><input type=\"radio\" name=\"rdo1\" value=\"$app_id\"></td><td>$act_id</td><td>$app_id</td><td>$authorizer</td><td>$act_date</td><td><span class=\"label label-default\">$action</span></td><td>$comment</td><td><button class=\"btn btn-xs btn-success\" type=\"button\" id=\"myBtn\" data-app_id=\"$app_id\">app</button><button class=\"btn btn-xs btn-success\" type=\"button\" onclick=\"location.href='DatasetDetail.php?data_id=$data_id'\">dataset</button></td></tr>";
			$no = $no + 1;
		}

	}catch (PDOException $e){
		print('Error:'.$e->getMessage());
		die();
	}
	$dbh = null;
