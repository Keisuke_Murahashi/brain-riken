<?php
	try{
		$dbh = getDBH();
		$username = $_SESSION['username'];
		$sql = "select a.dir_name, a.data_id, a.data_type, b.app_type, b.status from dataset a left outer join app b on a.data_id = b.data_id where a.owner = ? LIMIT 5";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array($username));
		$no = 1;
		$dir_name = "";
		$data_id = "";
		$data_type = "";
		$app_type = "";
		$status = "";
		while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
			//foreach ($dbh->query($sql) as $row) {
			$dir_name = htmlspecialchars($result['dir_name']);
			$data_id = htmlspecialchars($result['data_id']);
			$data_type = htmlspecialchars($result['data_type']);
			$app_type = htmlspecialchars($result['app_type']);

			$status = htmlspecialchars($result['status']);
			echo "<tr data-href=\"DatasetDetail.php?data_id=$data_id\"><td>$no</td><td>$dir_name</td><td>$data_id</td><td>$data_type</td><td><span class=\"label label-default\">$app_type</span></td><td>$status</td></tr>";
			$no = $no + 1;
		}
	}catch (PDOException $e){
		print('Error:'.$e->getMessage());
		die();
	}
	$dbh = null;