<?php
	$user_name = $_SESSION['username'];
	$dbh = getDBH();

	$app_id = $_POST['app_id'];
	$comment = $_POST['comment'];

	//申請データ更新
	// NIJC  $sql = "UPDATE app SET status = '棄却' where app_id = ?";
	$sql = "UPDATE app SET status = 'Rejected' where app_id = ?";
	$stmt = $dbh->prepare($sql);
	$stmt->execute(array($app_id));


	//承認データ作成
	$act_id = getActID();
	$sql = "INSERT INTO app_history(act_id, app_id, authorizer, action, comment, act_date) VALUES (?, ?, ?, ?, ?, CURRENT_TIMESTAMP())";
	$stmt = $dbh->prepare($sql);
	// NIJC $stmt->execute(array($act_id, $app_id, $user_name, '棄却', $comment));
	$stmt->execute(array($act_id, $app_id, $user_name, 'Rejected', $comment));

	$dbh = null;
