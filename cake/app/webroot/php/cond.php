<?php
	try{
		$dbh = getDBH();
		$sql = "select distinct type from cond order by type";
		$stmt = $dbh->prepare($sql);
		$stmt->execute();
		echo "<option value=\"DataType\">DataType</option>";
		while($row = $stmt -> fetch(PDO::FETCH_ASSOC)) {
			$type = htmlspecialchars($row['type']);
			echo "<option value=\"$type\">$type</option>";

		}

	}catch (PDOException $e){
		print('Error:'.$e->getMessage());
		die();
	}
	$dbh = null;
