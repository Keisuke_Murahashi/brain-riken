<?php

// Basic example of PHP script to handle with jQuery-Tabledit plug-in.
// Note that is just an example. Should take precautions such as filtering the input data.

header('Content-Type: application/json');

$input = filter_input_array(INPUT_POST);

$mysqli = new mysqli('localhost', 'test', '', 'test2');

if (mysqli_connect_errno()) {
	echo json_encode(array('mysqli' => 'Failed to connect to MySQL: ' . mysqli_connect_error()));
	exit;
}
//
if ($input['action'] === 'edit') {
	$sql="UPDATE `group` SET `id`='" . $input['group_name'] . "', name='" . $input['group_name'] . "', detail='" . $input['detail'] . "' WHERE `id`='" . $input['group_name'] . "'";
	$mysqli->query($sql);
} else if ($input['action'] === 'delete') {
	//$mysqli->query("DELETE from `group` WHERE `id`='" . $input['group_name'] . "'");
	$sql="UPDATE `group` SET `status`='deleted' WHERE `id`='" . $input['group_name'] . "'";
	//$sql="UPDATE `group` SET `status`='deleted' WHERE `id`='group_c'";
	$mysqli->query($sql);
} else if ($input['action'] === 'restore') {
	//$mysqli->query("UPDATE test2 SET deleted='0' WHERE id='" . $input['id'] . "'");
}

mysqli_close($mysqli);

echo json_encode($input);
