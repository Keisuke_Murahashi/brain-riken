<?php
header('Content-Type: application/json');
require( "../lib/function.php" );

	$mess_id = $_POST["input1"];

	try{
		$dbh = getDBH();

		$sql = "select a.mess_id, a.mess_type, a.title, a.content, a.author, a.category, a.date from message a where mess_id = ? ";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array($mess_id));

			while($row = $stmt -> fetch(PDO::FETCH_ASSOC)) {
			$mess_id = htmlspecialchars($row['mess_id']);
			$mess_type = htmlspecialchars($row['mess_type']);
			if ($mess_type == "user"){
				$mess_type = "お知らせ";
			}else{
				$mess_type = "研究ポータルについて";
			}
			$title = htmlspecialchars($row['title']);
			$content = htmlspecialchars($row['content']);
			$author = htmlspecialchars($row['author']);
			$category = htmlspecialchars($row['category']);
			switch ($category) {
				case "News":
					$fa = "fa-folder";
					break;
				case "Message":
					$fa = "fa-envelope-o";
					break;
			}
			$date = htmlspecialchars($row['date']);;
		}

		$sql = "select a.mess_id, a.target from target a where mess_id = ? ";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array($mess_id));

		$no = 1;
		while($row = $stmt -> fetch(PDO::FETCH_ASSOC)) {
			if ($no == 1){
				$target = htmlspecialchars($row['target']);
			}else{
				$target = $target . "," . htmlspecialchars($row['target']) ;
			}
			$no = $no + 1;
		}

		$array_data[1] = array(
				"mess_id" => $mess_id,
				"mess_type" => $mess_type,
				"title" => $title,
				"content" => $content,
				"author" => $author,
				"category" => $category,
				"fa" => $fa,
				"date" => $date,
				"target" => $target
		);

	}catch (PDOException $e){
		print('Error:'.$e->getMessage());
		die();
	}
	$dbh = null;
	echo json_encode($array_data);
