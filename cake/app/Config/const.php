<?php
$config = array();

//+ ディレクトリのフォーマット

// ユーザ定義定数
define('JSON_SCHEME_DIR', WWW_ROOT .'lib/json/scheme/');
define('JSON_DATA_DIR',   WWW_ROOT .'lib/json/data/');

// 自グループ間共有時 .. sprintf(DIR_INTRAGROUP, $group_id, $data_id)
define('DIR_INTRAGROUP', '/gpfs/home/intragroup/%s/dataset/%s');

// 外部グループ間共有時 .. sprintf(DIR_INTERGROUP, $data_id)
define('DIR_INTERGROUP', '/gpfs/home/intergroup/dataset/%s');

// ユーザーホームディレクトリ .. sprintf(DIR_INTERGROUP, $data_id)
define('DIR_USER_HOME', '/gpfs/home/%s/transfer');
//define('DIR_USER_HOME', 'c:\test\%s\transfer');

//+ URL

// 呼び出し方:    echo FESS;
define("FESS","http://localhost:8080/fess");

