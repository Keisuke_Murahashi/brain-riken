				<!-- Myアセット -->
				<div class="panel panel-success"><!-- panel -->
					<div class="panel-heading"><span class="glyphicon glyphicon-briefcase"></span>&nbsp; <?php el('_my_assets'); ?>

							<a class="pull-right" data-toggle="collapse" href="#p2"><font color="#ffffff"><i class="fa fa-caret-square-o-down"></i></font></a>
					</div>
					<div class="panel-body collapse in" id="p2">
						<div class="table-responsive">
							<table class="table table-striped table-hover table-condensed">
								<thead>
									<!-- <tr><strong>申請一覧</strong></tr> -->
									 <tr>
										<th>#</th>
										<th><?php el('_data_id'); ?></th>
										<th><?php el('_data_type'); ?></th>
										<th><?php el('_dataset_dir'); ?></th>
										<th><?php el('_upd_date'); ?></th>
										<th><?php el('_title'); ?></th>
										<th><?php el('_subject'); ?></th>
										<th><?php el('_contact_person'); ?></th>
										<th><?php el('_fixed'); ?></th>
										<th><?php el('_share'); ?></th>
										<th><?php el('_publish'); ?></th>
									</tr>
								</thead>
								<tbody>
<?php
	foreach ($data_asset as $i => $dt) :
		$mng = $dt['Mongo'] + array('title' => '', 'subject' => '', 'contact_person' => '');
		$cnt = $dt['AppCount'];
		$color = Util::getLabelColor($dt['publish_status']);
?>

									<!-- <tr data-href="/DatasetDetail/?data_id=$data_id"> -->
									<!-- <tr data-href="/Dataset/detail/<?php eh($dt['data_id']); ?>"> -->
									<tr>
										<td><?php echo $i + 1; ?></td>
										<!-- <td><?php eh($dt['data_id']); ?></td> -->
										<td><a href="/Dataset/detail/<?php echo $dt['data_id']; ?>"><?php eh($dt['data_id']); ?></a></td>
										<td><?php eh($dt['data_type']); ?></td>
										<td><?php eh($dt['dir_name']); ?></td>
										<td><?php eh($dt['upd_date']); ?></td>
<?php echo $this->element('dataset_list_mongo', array('mng' => $mng));	// Mongoデータ共通 ?>
												<!-- <td><span class="label label-default"><?php el($ap['app_type']); ?></span></td> -->
												<!-- <td><?php el($ap['status']); ?></td> -->
										<td>
<!-- NIJC -->
<?php echo $this->element('item_status', array('cnt' => @$cnt)); ?>

										</td>
										<!-- <td><span class="label label-default label-warning"><?php el($dt['pb_st1']); ?></span><span class="label label-default label-info"><?php el($dt['pb_st2']); ?></span><span class="label label-default label-danger"><?php el($dt['pb_st3']); ?></span></td> -->
										<td><span class="label label-default label-<?php echo $color; ?>"><?php el($dt['publish_status']); ?></span></td>
									</tr>

<?php
	endforeach;
?>
									</tbody>
							</table>
						</div>
						<a href="/MyDataset/" class="pull-right"><?php el('_more'); ?></a>
					</div>
				</div><!-- panel -->
				<!-- /Myアセット -->
