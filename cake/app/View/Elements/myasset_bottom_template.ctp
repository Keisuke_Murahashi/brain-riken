<!-- テンプレート登録 -->
<div class="modal fade" id="p-lg5">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title"><?php el('_set_template'); ?></h4>
				<h5><?php el('_set_dataset_as_template'); ?></h5>
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<p><?php el('_target_data'); ?></p>
						<ul class="list-group">
							<li class="list-group-item"><strong>data_id : </strong>t1wi_000004</li>
							<li class="list-group-item"><strong>data_type : </strong>rs-fMRI</li>
							<li class="list-group-item"><strong>title : </strong>Adult marmoset, individual brain, in vivo, T1WI 9.4 Tesla</li>
						</ul>
						<input class="input-xxlarge form-control" id="" type="text" placeholder="Template Name...">
					</div>
				</div>
				<br>
				<button type="button" class="btn btn-success btn-block" data-dismiss="modal"><?php el('_run_template'); ?></button>
				<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?php el('_cancel'); ?></button>
			</div>
		</div>
	</div>
</div>
