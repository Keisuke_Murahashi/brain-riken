<!-- メタデータ登録 - 大サイズ用モーダル -->
<div class="modal fade" id="p-lg5">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title">メタデータ登録</h4>
				<h5>下記データを登録します。よろしいですか？</h5>
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="table-responsive">
						<table class="table" id="table1">
							<thead>
								<tr><th>ファイル名：Metadata.csv</th></tr>
							</thead>
							<tbody>
								<tr class="success"><td>-----------</td><td>OK</td></tr>
								<tr class="success"><td>keyitem,col1,col2,col3,col3</td><td>OK</td></tr>
								<tr class="success"><td>/home/user/scp/Experimental1,col1,col2,co3</td><td>OK</td></tr>
								<tr class="danger"><td>/home/user/scp/Experimental1/data_1,col1,col2,col3</td><td>データ型不正(col1)</td></tr>
								<tr class="success"><td>/home/user/scp/Experimental1/data_2,col1,col2,col3</td><td>OK</td></tr>
								<tr class="success"><td>/home/user/scp/Experimental1/data_3,col1,col2,col3</td><td>OK</td></tr>
								<tr class="danger"><td>/home/user/scp/Experimental1/doc_data_1,col1,col2,col3</td><td>文字数オーバー(col2)</td></tr>
								<tr class="success"><td>/home/user/scp/Experimental1/doc_data_2,col1,col2,col3</td><td>OK</td></tr>
							</tbody>
						</table>
						</div>
					</div>
				</div>
				<br>
				<button type="button" class="btn btn-success btn-block" data-dismiss="modal">登録</button>
				<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?php el('_cancel'); ?></button>
			</div>
		</div>
	</div>
</div>
