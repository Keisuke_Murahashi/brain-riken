
<!-- データ一覧 -->
<!-- NIJC(20160627)
			<form action="<?php eh(ConnectionManager::$config->default_api['download_url_any']); ?>"  method="post" id="sidebar_download" class="nav download">
-->
			<form action="/MyDataset/download" method="post" id="sidebar_download" class="nav download">
					<div class="submit form-inline text-right">
						<input type="hidden" name="username" value="<?php eh($user['username']); ?>" />
						<input type="hidden" name="targets" value="" />
						<input type="hidden" name="download_url" value="<?php eh(ConnectionManager::$config->default_api['download_url_any']); ?>" />
<!-- NIJC(20160630) -->
						<input type="hidden" name="language" value="<?php echo lang(); ?>" />
                                                <button type="submit" class="btn btn-success" title="<?php el('_download'); ?>">
							<?php el('_download_all'); ?></button>
					</div>
			</form>
	<div class="panel panel-success">
		<div class="panel-heading"><i class="fa fa-list"></i>&nbsp; <?php el('_dataset_list'); ?>
			<a class="pull-right white" data-toggle="collapse" href="#p1">
				<i class="fa fa-caret-square-o-down"></i></a>
		</div>
		<div class="panel-body collapse in" id="p1">

			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#taba" data-toggle="tab"><span class="glyphicon glyphicon-briefcase"></span>&nbsp;<?php el('_my_assets'); ?></a>
				</li>
				<li>
					<a href="#tabb" data-toggle="tab"><i class="fa fa-share-alt"></i>&nbsp;<?php el('_share_in_group'); ?></a>
				</li>
				<li>
					<a href="#tabc" data-toggle="tab"><i class="fa fa-share-alt"></i>&nbsp;<?php el('_share_between_groups'); ?></a>
				</li>
				<?php  if ($user['UserAuth']['role'] == 'SystemAdmin') : ?>

				<li>
					<a href="#tabd" data-toggle="tab"><i class="fa fa-share-square-o"></i>&nbsp;<?php el('_all_items'); ?></a>
				</li>
                                <?php endif; ?>
			</ul>
			<div class="tab-content">
				<!-- Myアセット -->
				<div class="tab-pane active fade in" id="taba">
					<br>
<?php
					// Myアセット
					echo $this->element('myasset_form_data_contents_myasset', array('data' => $data['myasset']));
?>
				</div>
				<!-- グループ内共有 -->
				<div class="tab-pane fade in" id="tabb">
					<br>
<?php
					// Myアセット
					echo $this->element('myasset_form_data_contents_mygroup', array('data' => $data['intragroup']));
?>
				</div>
				<!-- グループ間共有 -->
				<div class="tab-pane fade in" id="tabc">
					<br>
<?php
					// Myアセット
					echo $this->element('myasset_form_data_contents_other', array('data' => $data['intergroup']));
?>
				</div>
				<?php  if ($user['UserAuth']['role'] == 'SystemAdmin') : ?>

				<!-- 公開 -->
				<div class="tab-pane fade in" id="tabd">
					<br>
<?php
					// Myアセット
					echo $this->element('myasset_form_data_contents_public', array('data' => $data['public']));
?>
				</div>
				<?php endif; ?>

			</div>

		</div>
	</div>
<!-- データ一覧 -->
