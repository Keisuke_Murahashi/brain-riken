<!-- 申請取り下げ -->
<div class="modal fade" id="p-lg4">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title"><?php el('_withdraw_application'); ?></h4>
				<h5><?php el('_withdraw_application_of_dataset'); ?></h5>
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<p><?php el('_target_data'); ?></p>
						<ul class="list-group">
							<li class="list-group-item"><strong>dir_name : </strong>/user1/dir1</li>
							<li class="list-group-item"><strong>data_id : </strong>t1wi_000004</li>
							<li class="list-group-item"><strong>data_type : </strong>rs-fMRI</li>
							<li class="list-group-item"><strong>title : </strong>Adult marmoset, individual brain, in vivo, T1WI 9.4 Tesla</li>
						</ul>
					</div>
				</div>
				<button type="button" class="btn btn-success btn-block" data-dismiss="modal"><?php el('_run_withdrawal'); ?></button>
				<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?php el('_cancel'); ?></button>
			</div>
		</div>
	</div>
</div>
