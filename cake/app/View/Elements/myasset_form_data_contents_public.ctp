			<div class="table-responsive">
				<table class="table table-striped table-hover table_id">
					<thead>
						<tr>
							<th>#</th>
							<th><?php el('_data_id'); ?></th>
							<th><?php el('_data_type'); ?></th>
							<th><?php el('_dataset_dir'); ?></th>
							<th><?php el('_upd_date'); ?></th>
							<th><?php el('_creator'); ?></th>
							<th><?php el('_title'); ?></th>
							<th><?php el('_subject'); ?></th>
							<th><?php el('_contact_person'); ?></th>
							<th><?php el('_share'); ?></th>
							<th><?php el('_publish'); ?></th>
							<!-- <th><?php el('_detail'); ?></th> -->
							<th><?php el('_download'); ?></th>
						</tr>
					</thead>
					<tbody>
<?php
	foreach ($data as $i => $dt) :
		$mng = $dt['Mongo'] + array('title' => '', 'subject' => '', 'contact_person' => '');
		$cnt = $dt['AppCount'];
		$color = Util::getLabelColor($dt['publish_status']);
?>
							<tr class="click-checked">
								<td><?php echo $i + 1; ?>
									<?php if ($dt['locked']) : ?><i class="fa fa-lock locked" title="<?php el('_locked'); ?>"></i><?php endif; ?>
								</td>
								<td><a href="/Dataset/detail/<?php echo $dt['data_id']; ?>"><?php eh($dt['data_id']); ?></a></td>
								<td><?php eh($dt['data_type']); ?></td>
								<td><label class="download text-nowrap" title="<?php el('_download_queue'); ?>">
									<button type="button" class="download btn btn-default btn-xs fa fa-download"
										data-action="<?php eh($dt['download_url']); ?>"
										data-targets="<?php eh($dt['download_id']); ?>"
										data-username="<?php eh($user['username']); ?>"
									></button>
									<?php eh($dt['dir_name']); ?></label></td>
								<td><?php eh(date('Y-m-d H:i:s', strtotime($dt['upd_date']))); ?></td>
								<td><?php eh($dt['owner']); ?></td>
<?php echo $this->element('dataset_list_mongo', array('mng' => $mng));	// Mongoデータ共通 ?>
								<td>
<!-- NIJC -->
<?php echo $this->element('item_status', array('cnt' => @$cnt)); ?>
								</td>
								<td><span class="label label-default label-<?php echo $color; ?>"><?php el($dt['publish_status']); ?></span></td>
								<!-- <td><a href="/Dataset/detail/<?php echo $dt['data_id']; ?>" class="btn btn-xs btn-success"><?php el('_detail'); ?></a></td> -->
								<td><button type="button" class="btn btn-default btn-sm download"
										data-action="<?php eh($dt['download_url']); ?>"
										data-targets="<?php eh($dt['download_id']); ?>"
										data-username="<?php eh($user['username']); ?>"
									><?php el('_download_queue'); ?></button></td>
							</tr>
<?php
	endforeach;
?>
					</tbody>
				</table>
			</div>
