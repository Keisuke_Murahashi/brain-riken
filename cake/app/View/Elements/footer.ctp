<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<?php echo $this->Html->script('jquery.min'); ?>

<?php echo $this->Html->script('bootstrap.min'); ?>

<?php echo $this->Html->script('common'); ?>

<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="/assets/js/vendor/holder.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/assets/js/ie10-viewport-bug-workaround.js"></script>

<?php echo $this->fetch('script'); ?>

