		<!-- 申請一覧 -->
		<div class="panel panel-success">
			<div class="panel-heading">
				<i class="fa fa-history"></i>&nbsp; <?php el('_approved_history'); ?>
				<a class="pull-right white" data-toggle="collapse" href="#ppp2">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>

			<div class="panel-body collapse in" id="ppp2">
				<form name="app-form" class="form-inline" enctype="multipart/form-data" action="" method="post">
					<!--
						<input class="input-xxlarge form-control" id="id_textBox1" type="text" placeholder="Directory Name...">&nbsp;
						<input type="button" class="btn btn-mini btn-primary" onclick="add();" value="新規追加">
					-->
					<!-- <select id="Select2" onchange="changeSelect()" class="form-control"> -->
					<!--	<option value=""><?php el('_select_action'); ?></option> -->
					<!--	<option value="#p-lg1"><?php el('_approve'); ?></option> -->
					<!--	<option value="#p-lg2"><?php el('_cancel'); ?></option> -->
						<!-- <option value="#p-lg1">保留</option> -->
					<!-- </select> -->
				<!-- <input type="submit" name="submit_app" class="btn btn-mini btn-success" type="submit" value="実行"> -->
					<!-- 	<a class="btn btn-mini btn-success" href="" id="appLink2" data-toggle="modal"><?php el('_run'); ?></a> -->
					<div class="btn-group">
						<button type="button" class="btn btn-success" id="appadminbtn1" data-toggle="modal" data-target="#p-lg1"><i class="fa fa-check-circle"></i>&nbsp; <?php el('_approve'); ?></button>
						<button type="button" class="btn btn-success" id="appadminbtn2" data-toggle="modal" data-target="#p-lg2"><i class="fa fa-external-link"></i>&nbsp; <?php el('_cancel'); ?></button>
					</div>

				</form>
				<br>
				<div class="table-responsive"><!-- table-responsive -->
					<table class="table table-striped table-hover" id="table2">
						<thead>
							<tr>
								<th>#</th>
								<th><?php el('_act_id'); ?></th>
								<th><?php el('_app_id'); ?></th>
								<th><?php el('_data_id'); ?></th>
								<th><?php el('_authorizer'); ?></th>
								<th><?php el('_act_date'); ?></th>
								<th><?php el('_action'); ?></th>
								<th><?php el('_comment'); ?></th>
								<!-- <th><?php el('_detail'); ?></th> -->
							</tr>
						</thead>
						<tbody>
<?php
	foreach ($data_history as $i => $dt) :
?>
							<tr>
								<td><input type="radio" name="rdo1" value="<?php eh($dt['app_id']); ?>"></td>
								<td><?php eh($dt['act_id']); ?></td>
								<td><a class="appbtn" data-app_id="<?php eh($dt['app_id']); ?>"><?php eh($dt['app_id']); ?></a></td>
								<td><a href="/Dataset/detail/<?php eh($dt['data_id']); ?>"><?php eh($dt['data_id']); ?></a></td>
								<td><?php eh($dt['authorizer']); ?></td>
								<td><?php eh($dt['act_date']); ?></td>
								<td><span class="label label-default"><?php el($dt['action']); ?></span></td>
								<td><?php eh($dt['comment']); ?></td>
								<!-- <td> -->
									<!-- <button type="button" data-app_id="<?php eh($dt['app_id']); ?>" class="btn btn-xs btn-success appbtn"><?php el('_app'); ?></button> -->
<?php	if ($dt['data_id']) : ?>
									<!-- <a href="/Dataset/detail/<?php eh($dt['data_id']); ?>" class="btn btn-xs btn-success"><?php el('_dataset'); ?></a> -->
<?php	endif; ?>
								<!-- </td> -->
							</tr>
<?php
	endforeach;
?>
						</tbody>
					</table>
				</div><!-- /table-responsive -->
			</div><!-- /panel-body -->
		</div><!-- /panel -->
