		<!-- ユーザー一覧 -->
<?php if ($user['role'] == 'SystemAdmin') : ?>
		<div class="panel panel-info">
		<div class="panel-heading"><i class="fa fa-user"></i>&nbsp; <?php el('_user_managements'); ?>&nbsp;(<?php el('_for_system_admin'); ?>)
<?php else : ?>
		<div class="panel panel-success">
		<div class="panel-heading"><i class="fa fa-user"></i>&nbsp; <?php el('_user_managements'); ?>
<?php endif; ?>
				<!-- <div class="pull-right"><i class="fa fa-caret-square-o-down"></i></div> -->
				<!-- <button class="pull-right btn-primary" data-toggle="collapse" data-target="#p1"><i class="fa fa-caret-square-o-down"></i></button> -->
				<a class="pull-right white" data-toggle="collapse" href="#p1">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>

			<div class="panel-body collapse in" id="p1">
				<div class="table-responsive">
					<table class="table table-striped table-hover <?php echo ($user['role'] == 'SystemAdmin' ? 'root ' : ''); echo ($user['is_admin'] ? 'editable' : ''); ?>" id="table1">
						<thead>
							<tr>
								<th>#</th>
								<th><?php el('_user_id'); ?></th>
								<th><?php el('_user_name'); ?></th>
								<th><?php el('_detail'); ?></th>
								<th><?php el('_home_dir'); ?></th>
								<th><?php el('_group_name'); ?></th>
								<th><?php el('_role'); ?></th>
							</tr>
						</thead>
						<tbody>
<?php
	foreach ($data_users as $i => $dt) :
?>
							<tr class="<?php	// システム管理者か、グループ管理者で自グループに所属するシステム管理者以外のユーザー
										echo ($user['role'] == 'SystemAdmin' ||
										       ($dt['role'] != 'SystemAdmin' && $dt['group'] == $user['group'])) ? '' : 'not'; ?>">
								<td><?php echo $i + 1; ?></td>
								<td><?php eh($dt['username']); ?></td>
								<td><?php eh($dt['name']); ?></td>
								<td><?php eh($dt['detail']); ?></td>
								<td><?php eh($dt['home_dir']); ?></td>
								<td><?php echo nl2br(h(@$dt['group_name'])); ?></td>
								<td><?php echo nl2br(h($dt['role'])); ?></td>
							</tr>
<?php
	endforeach;
?>
						</tbody>
					</table>
				</div><!--/table-responsive-->
			</div><!--/panel-body-->
		</div><!--/panel-->
