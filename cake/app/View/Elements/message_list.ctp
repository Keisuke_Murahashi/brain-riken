		<!-- ドキュメント一覧 -->
		<div class="panel panel-success">
			<div class="panel-heading"><i class="fa fa-history"></i>&nbsp;
				<?php el('_past_messages'); ?>

				<a class="pull-right white" data-toggle="collapse" href="#ppp2">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>
			<div class="panel-body collapse in" id="ppp2">

				<div class="table-responsive"><!-- table-responsive -->
					<table class="table table-striped table-hover" id="table_id">
						<thead>
							<tr>
								<!-- <th><?php el('_message_id'); ?></th>  -->
								<th>No.</th>
								<th><?php el('_message_type'); ?></th>
								<th><?php el('_target'); ?></th>
								<th><?php el('_message_title'); ?></th>
								<th><?php el('_sent_date'); ?></th>
							</tr>
						</thead>
						<tbody>
<?php
	// icon / mess_type
//	$icons = array('News' => 'fa-folder', 'Message' => 'fa-envelope-o');
	$types = array('user' => l('_user_message'), 'portal' => l('_portal_message'),
					'system' => l('_system_message'));

	foreach ($data_items as $i => $dt) :
?>
							<!-- <tr data-href="/Message/<?php eh($dt['mess_id']); ?>"> -->
								<tr>
								<!-- <td><?php eh($dt['mess_id']); ?></td> -->
								<td><?php echo $i + 1; ?></td>
								<td><?php eh(@$types[ $dt['mess_type'] ]); ?></td>
								<td><?php eh($dt['targets']); ?></td>
								<td><a href="/Message/<?php eh($dt['mess_id']); ?>"><?php eh($dt['title']); ?></a></td>
								<td><?php eh($dt['date']); ?></td>
								<!-- <td><a href="/Message/<?php eh($dt['mess_id']); ?>" class="btn btn-xs btn-success"><?php el('_detail'); ?></a></td> -->
							<!--
								<td><i class="fa <?php echo $icons[ $dt['category'] ]; ?>"></i><?php eh($dt['category']); ?></td>
							-->
							</tr>
<?php
	endforeach;
?>
						</tbody>
					</table>
				</div><!-- /table-responsive -->
			</div><!-- /panelbody -->
		</div><!-- /panel -->
