<div class="modal fade" id="modal-unlink">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title"><?php el('_unlink_dir'); ?></h4>
				<h5><?php el('_confirm_to_discharge_intragroup'); ?></h5>
				<div class="row">
					<form action="" name="unlink-form" class="form-group col-xs-12 col-md-12 col-sm-12" method="post">
<p><?php el('_target_data'); ?></p>
						<ul class="list-group">
							<li class="list-group-item"><strong></strong></li>
						</ul>
						<input type="hidden" id="data_id" name="data_id"/>
                                                <button type="submit" name="unlink_dir" class="btn btn-success btn-block"><?php el('_run_unlink'); ?></button>
                                                <button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?php el('_cancel'); ?></button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
