		<!-- 申請一覧 -->
		<button class="btn btn-info" id="appadminbtn1" <?php if (!$user['is_admin']) echo 'disabled="disabled"'; ?>><i class="fa fa-check-circle"></i>&nbsp; <?php el('_approve'); ?></button>
		<button class="btn btn-info" id="appadminbtn2" <?php if (!$user['is_admin']) echo 'disabled="disabled"'; ?>><i class="fa fa-external-link-square"></i>&nbsp; <?php el('_reject'); ?></button>
		<button class="btn btn-info" id="appadminbtn3" <?php if (!$user['is_admin']) echo 'disabled="disabled"'; ?>><i class="fa fa-external-link"></i>&nbsp; <?php el('_withdraw_approved_application'); ?></button>

		<br><br>
		<div class="panel panel-info">
			<div class="panel-heading">
				<i class="fa fa-pencil-square-o"></i>&nbsp;
				<?php el('_applications_list'); ?>&nbsp;(<?php el('_for_admin'); ?>)

				<a class="pull-right white" data-toggle="collapse" href="#p1">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>
			<div class="panel-body collapse in" id="p1">
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-spinner"></i>&nbsp; <?php el('_waiting_for_approval'); ?></a></li>
	<li><a href="#tab2" data-toggle="tab"><i class="fa fa-check-square-o"></i>&nbsp; <?php el('_done'); ?></a></li>
</ul><!-- /nav-tabs -->
<div class="tab-content">
	<div class="tab-pane active fade in" id="tab1">
			<!-- <h5><i class="fa fa-spinner"></i>&nbsp; <?php el('_waiting_for_approval'); ?></h5> -->
<br>
				<div class="table-responsive">
						<table class="table table-striped table-hover" id="table1">
<?php // NIJC put table header
	echo $this->element('application_table_header');
?>
							<tbody>
<?php
	foreach ($data_list as $i => $dt) :
	$color = Util::getLabelColor($dt['status']);
	$color2 = Util::getLabelColor($dt['app_type']);
	$app_type = Util::getAppType($dt['app_type']);
?>
							<tr class="click-checked">
								<td><input type="radio" name="rdo_app1" value="<?php eh($dt['appid']); ?>"></td>
								<td><?php eh($dt['appid']); ?></td>
								<td><a href="/Dataset/detail/<?php eh($dt['data_id']); ?>"><?php eh($dt['data_id']); ?></a></td>
								<td><span class="label label-default label-<?php echo $color2; ?>">
	<?php el($app_type); ?>
								</span></td>
								<td><?php eh($dt['creator']); ?></td>
								<td><?php eh(( mb_substr($dt['app_type'], 0, 2) == "Release" ) ? el('_publish') : $dt['groupid']); ?></td>
								<td><?php eh($dt['authorizer']); ?></td>
								<td><?php eh($dt['date']); ?></td>
								<td><span class="label label-default label-<?php echo $color; ?>"><?php el('_applying'); ?></span></td>
								<td><?php eh($dt['detail']); ?></td>
								<!-- <td><a href="/Dataset/detail/<?php eh($dt['data_id']); ?>" class="btn btn-xs btn-success"><?php el('_detail'); ?></a></td> -->
							</tr>
<?php
	endforeach;
?>
							</tbody>
						</table>
					</div><!-- /table-responsive -->
	</div><!-- /tab-pane -->
	<div class="tab-pane fade in" id="tab2">
			<!-- <h5><i class="fa fa-check-square-o"></i>&nbsp; <?php el('_done'); ?></h5> -->
	<br>
				<div class="table-responsive"><!-- table-responsive -->
					<table class="table table-striped table-hover" id="table2">
<?php // NIJC put table header
	echo $this->element('application_table_header2');
?>
						<tbody>
<?php
	foreach ($data_history as $i => $dt) :
	$color = Util::getLabelColor($dt['action']);
	$status_label = Util::getStatusLabel($dt['action']);
?>
							<tr>
							<td><?php if($dt['action'] == "Accepted") : ?><input type="radio" name="rdo_app2" value="<?php eh($dt['actid']); ?>"><?php endif; ?></td>
								<!-- td><?php eh($dt['act_id']); ?></td -->
								<td><a class="appbtn" data-app_id="<?php eh($dt['appid']); ?>"><?php eh($dt['appid']); ?></a></td>
								<td><a href="/Dataset/detail/<?php eh($dt['data_id']); ?>"><?php eh($dt['data_id']); ?></a></td>
								<td><?php eh($dt['authorizer']); ?></td>
								<td><?php eh($dt['act_date']); ?></td>
								<td><span class="label label-default label-<?php echo $color; ?>">
		<?php el($status_label); ?>
								</span></td>
								<td><?php eh($dt['comment']); ?></td>
								<!-- <td> -->
									<!-- <button type="button" data-app_id="<?php eh($dt['app_id']); ?>" class="btn btn-xs btn-success appbtn"><?php el('_app'); ?></button> -->
<?php	if ($dt['data_id']) : ?>
									<!-- <a href="/Dataset/detail/<?php eh($dt['data_id']); ?>" class="btn btn-xs btn-success"><?php el('_dataset'); ?></a> -->
<?php	endif; ?>
								<!-- </td> -->
							</tr>
<?php
	endforeach;
?>
						</tbody>
					</table>
				</div><!-- /table-responsive -->
</div><!-- /tab-pane -->
</div><!-- /tab-content -->

			</div><!-- /panel-body -->
		</div><!-- /panel -->
