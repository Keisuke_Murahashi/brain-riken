<div class="modal fade" id="modal-invalid">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title"><?php el('_metadata_invalid'); ?></h4>
				<h5><?php el('_metadata_have_invalid_format'); ?></h5>
				<div class="row">
					<form action="/Dataset/template/" name="invalid-form" class="form-group col-xs-12 col-md-12 col-sm-12">
						<div class="message note note-warning"></div>
						<a href="/Dataset/template/" class="submit btn btn-warning btn-block">
							<i class="fa fa-pencil"></i>&nbsp;
							<?php el('_edit_metadata'); ?></a>
						<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?php el('_cancel'); ?></button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
