<?php if (!empty($message)) : // エラーメッセージがあれば ?>
		<div class="message note note-warning">
<?php	if (!is_array($message)) : ?>
			<p><?php echo $message; ?></p>
<?php	else : ?>
			<dl>
<?php		foreach ($message as $mess) : ?>
				<dt><?php echo @$mess['property']; ?> (<?php echo @$mess['constraint']; ?>)</dt>
				<dd><?php eh(@$mess['message']); ?></dd>
<?php		endforeach; ?>
			</dl>
<?php	endif; ?>
		</div>
<?php endif; ?>
