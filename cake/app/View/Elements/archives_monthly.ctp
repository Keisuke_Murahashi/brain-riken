				<!-- 月別 -->
				<ul id="monthly-user" class="list-group monthly user active">
<?php
	foreach ($count_month_user as $cnt) :
		$t = strtotime($cnt['month'] .'/01');
?>
					<li class="list-group-item">
						<a href="/Archives/<?php echo $cnt['month']; ?>">
							<?php echo date('Y', $t) . l('_year') . date('n', $t) .l('_month'); ?>
							<span class="count"><?php printf('(%d)', $cnt['cnt']); ?></span></a>
					</li>
<?php
	endforeach;
?>
				</ul>
				<ul id="monthly-every" class="list-group monthly every">
<?php
	foreach ($count_month_every as $cnt) :
		$t = strtotime($cnt['month'] .'/01');
?>
					<li class="list-group-item">
						<a href="/Archives/<?php echo $cnt['month']; ?>#tab2">
							<?php echo date('Y', $t) . l('_year') . date('n', $t) .l('_month'); ?>
							<span class="count"><?php printf('(%d)', $cnt['cnt']); ?></span></a>
					</li>
<?php
	endforeach;
?>
				</ul>
				<!-- /月別 -->
