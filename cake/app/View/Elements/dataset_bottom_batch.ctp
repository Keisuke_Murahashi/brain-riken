<!-- メタデータ一括登録 - 大サイズ用モーダル -->
<div class="modal fade" id="p-lg4">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title">メタデータ一括登録</h4>
<hr>
				<div class="panel panel-success">
					<div class="panel-heading">メタデータファイル
						<div class="pull-right">
							<i class="fa fa-caret-square-o-down"></i>
						</div>
					</div>
					<div class="panel-body">
						<p>メタデータファイル</p>
						<form class="form-inline" enctype="multipart/form-data" action="" name="form" method="post">
							<div class="form-group">
								<div class="input-group">
									<input type="file" id="file_input" name="file_input" style="display: none;">
									<div class="input-group col-md-12">
										<input id="dummy_file" type="text" class="form-control input-xxlarge" placeholder="select file..." disabled>
									</div>
									<span class="input-group-btn">
											<button class="btn btn-default" type="button" onclick="$('#file_input').click();"><i class="glyphicon glyphicon-folder-open"></i></button>
									</span>
								</div>
							</div>
						</form>
					</div>
				</div>
				<button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#p-lg5">登録</button>
				<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?php el('_cancel'); ?></button>
			</div>
		</div>
	</div>
</div>

