<?php
if (isset($import_script)) {
	$in = array('inline' => false);

	// jQuery Cookie
	if (in_array('cookie', $import_script)) {
		$this->Html->script('jquery.cookie', $in);	// script
	}
	// jQuery Migrate
	if (in_array('migrate', $import_script)) {
		$this->Html->script('jquery-migrate-1.2.1.min', $in);	// script
	}
	// jQuery UI
	if (in_array('ui', $import_script)) {
		$this->Html->css('jquery-ui', null, $in);	// css
		$this->Html->script('jquery-ui.min', $in);	// script
	}
	// TreeGrid
	if (in_array('treegrid', $import_script)) {
	    $this->Html->script('jquery.treegrid', $in);	// script
	    $this->Html->script('jquery.treegrid.bootstrap3', $in);	// script
		$this->Html->css('jquery.treegrid', null, $in);	// css
	}
	// Tabledit
	if (in_array('tabledit', $import_script) or in_array('tableedit', $import_script)) {
	    $this->Html->script('jquery.tabledit.min', $in);	// script
	}
	// DataTables
	if (in_array('datatables', $import_script)) {
		//$this->Html->css('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', null, $in);	// css
		$this->Html->css('jquery.dataTables.min', null, $in);	// css
		$this->Html->css('dataTables.bootstrap.min', null, $in);	// css
		//$this->Html->script('//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js', $in);	// script

		$this->Html->script('jquery.dataTables', $in);	// script
		$this->Html->script('jquery.dataTables.min', $in);	// script
		$this->Html->script('jquery.dataTables.currency', $in);	// script
	}
	// MultiSelect
	if (in_array('multiselect', $import_script)) {
		$this->Html->css('jquery.multiselect', null, $in);	// css
		$this->Html->script('jquery.multiselect', $in);	// script
	}
	// Prettify
	if (in_array('prettify', $import_script)) {
//		$this->Html->css('prettify', null, $in);	// css
		$this->Html->script('prettify', $in);	// script
	}
	// JSON Editor
	if (in_array('jsoneditor', $import_script)) {
		echo "\t".'<link rel="stylesheet" id="theme_stylesheet">'."\n";
		echo "\t".'<link rel="stylesheet" id="icon_stylesheet">'."\n";
//		$this->Html->css('jsoneditor', null, $in);	// css
		$this->Html->script('jsoneditor', $in);	// script
		$this->Html->script('lz-string', $in);	// script
	}
}
?>