<?php $ctrl = $this->name; ?>
		<!-- サイドメニュー -->
		<div class="col-sm-3 col-md-2 sidebar">
			<ul class="nav nav-sidebar">
				<li class="<?php if ($ctrl == 'Home') echo 'active'; ?>">
					<a href="/"><span class="glyphicon glyphicon-home"></span>&nbsp;
						<?php el('_top', 'トップ'); ?> <span class="sr-only">(current)</span></a></li>
				<li class="<?php if ($ctrl == 'MyDataset') echo 'active'; ?>">
					<a href="/MyDataset/"><span class="glyphicon glyphicon-briefcase"></span>&nbsp;
						<?php el('_item_management', 'アイテム管理'); ?></a></li>
		<?php if (isset($user['UserAuth']['role']) && $user['UserAuth']['role'] != '') : ?>
				<li class="<?php if ($ctrl == 'Application') echo 'active'; ?>">
					<a href="/Application/"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;
						<?php el('_applications', '申請/承認'); ?></a></li>
		<?php endif; ?>
				<li class="<?php if ($ctrl == 'Message') echo 'active'; ?>">
					<a href="/Message/"><span class="glyphicon glyphicon-envelope"></span>&nbsp;
						<?php el('_my_messages', 'メッセージ管理'); ?></a></li>
				<!-- <li class="<?php if ($ctrl == 'Archives') echo 'active'; ?>">
					<a href="/Archives/"><span class="glyphicon glyphicon-save-file"></span>&nbsp;
						<?php el('_archives', '受信メッセージ'); ?></a></li> -->
				<li class="<?php if ($ctrl == 'UserManagement') echo 'active'; ?>">
					<a href="/UserManagement/"><span class="glyphicon glyphicon-user"></span>&nbsp;
						<?php el('_users_managements', 'ユーザー/グループ管理'); ?></a></li>
				<li class="<?php if ($ctrl == 'Help') echo 'active'; ?>">
					<a href="/HowToUse/"><span class="fa fa-book"></span>&nbsp;
						<?php el('_howtouse', '利用ガイド'); ?></a></li>
				<li class="<?php if ($ctrl == 'Help') echo 'active'; ?>">
					<a href="/UserGuide/"><span class="fa fa-exclamation-circle"></span>&nbsp;
						<?php el('_userguide', 'User Guide'); ?></a></li>
				<li class="<?php if ($ctrl == 'Contact') echo 'active'; ?>">
					<a href="/Contact/"><span class="fa fa-envelope-o"></span>&nbsp;
						<?php el('_contact', 'コンタクト'); ?></a></li>
						<!-- <li class="<?php if ($ctrl == 'Template') echo 'active'; ?>">
					<a href="/Template/"><span class="glyphicon glyphicon-edit"></span>&nbsp;
						<?php el('title_jsoneditor', 'JSON editor'); ?></a></li> -->
				<!-- <li class="<?php if ($ctrl == 'SearchResult') echo 'active'; ?>">
					<a href="/SearchResult/"><span class="glyphicon glyphicon-file"></span>&nbsp;
						<?php el('title_search', '検索結果'); ?></a></li> -->
			</ul>
		</div> <!-- /サイドメニュー -->
