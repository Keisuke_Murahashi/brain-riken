				<!-- お知らせ -->
				<div id="home-messages" class="panel panel-success"><!-- panel -->
					<!-- panel head -->
					<div class="panel-heading"><span class="glyphicon glyphicon-envelope"></span>&nbsp; <?php el('_received_messages'); ?>

						<!-- <div class="pull-right btn-primary" data-toggle="collapse" data-target="#p1"><i class="fa fa-caret-square-o-down"></i></div> -->
						<a class="pull-right white" data-toggle="collapse" href="#p1">
							<i class="fa fa-caret-square-o-down"></i></a>
					</div><!-- panel head -->

					<!-- panel body -->
					<div class="panel-body collapse in" id="p1">

						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#taba" data-toggle="tab"><i class="fa fa-user"></i>&nbsp;<?php el('_user_message'); ?></a>
								<span class="count"><?php echo @$count_unread['user'] ?: 0; ?></span>
							</li>
							<li>
								<a href="#tabb" data-toggle="tab"><i class="fa fa-users"></i>&nbsp;<?php el('_portal_message'); ?></a>
								<span class="count"><?php echo @$count_unread['portal'] ?: 0; ?></span>
							</li>
							<!-- NIJC
							<li>
								<a href="#tabc" data-toggle="tab"><i class="fa fa-flag-o"></i>&nbsp;<?php el('_system_message'); ?></a>
								<span class="count"><? //php echo @$count_unread['system'] ?: 0; ?></span>
							</li>
							NIJC -->
						</ul>
						<div class="tab-content">
							<!-- 個人宛て通知 -->
							<div class="tab-pane active fade in" id="taba">
								<br>
<?php
	// contents
	echo $this->element('home_news_contents', array('data' => $data_user));
?>
							</div>
							<!-- 皆様へのお知らせ -->
							<div class="tab-pane fade in" id="tabb">
								<br>
<?php
	// contents
	echo $this->element('home_news_contents', array('data' => $data_every));
?>
							</div>
							<!-- システムメッセージ -->
							<div class="tab-pane fade in" id="tabc">
								<br>
<?php
	// contents
	echo $this->element('home_news_contents', array('data' => $data_system));
?>
							</div>
						</div>

						<a href="/Message/" class="pull-right"><?php el('_more'); ?></a>
					</div><!-- panel body -->
				</div><!-- panel -->
				<!-- /お知らせ -->
