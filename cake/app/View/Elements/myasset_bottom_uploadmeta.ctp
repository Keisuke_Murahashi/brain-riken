<!-- メタデータ登録 -->
<div class="modal fade" id="p-lg6">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title"><?php el('_register_metadata'); ?></h4>
				<!-- <h5><?php el('_share_application_of_dataset'); ?></h5> -->
				<h5>メタデータファイルを選択して登録するか、フォーム入力で登録するか選択して下さい。</h5>
				<br>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tabfile" data-toggle="tab"><i class="fa fa-file-o"></i>&nbsp; <?php el('_upload_metadata'); ?></a></li>
							<li><a href="#tabform" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp; <?php el('_input_metadata'); ?></a></li>
						</ul>
						<div class="tab-content">
							<!-- ファイルで登録 -->
							<div class="tab-pane active" id="tabfile">
								<br>
								<form class="form-inline mb20" enctype="multipart/form-data" action="" name="form" method="post">
								<div class="form-group">
									<div class="input-group">
										<input type="file" id="file_input" name="file_input" style="display: none;">
										<div class="input-group col-md-12">
											<input id="dummy_file" type="text" class="form-control input-xxlarge" placeholder="<?php el('_select_file'); ?>" disabled>
										</div>
										<span class="input-group-btn">
											<button class="btn btn-default" type="button" onclick="$('#file_input').click();"><i class="glyphicon glyphicon-folder-open"></i></button>
										</span>
										<input type="submit" name= "meta_reg" class="btn btn-mini btn-success" value="<?php el('_upload'); ?>">
									</div>
								</div>
								</form>
							</div>
							<!-- フォームで登録 -->
							<div class="tab-pane" id="tabform">
							<br>
								<!--  <form id="id_form1" name="form1" action="" -->
								<form id="id_form1" name="form1" action="/Dataset/add_new"
								class="form-inline clearfix mb20" method="post" enctype="multipart/form-data">
								<div class="form-group">
									<!-- <input class="input-xxlarge form-control" id="id_textBox1" type="text" placeholder="<?php el('_input_directory_name'); ?>" name="dir_name">&nbsp; -->
									<select id="Select0" class="form-control" name="data_type">
										<option value=""><?php el('_select_datatype'); ?></option>
										<option value="T1WI">T1WI</option>
										<option value="T2WI">T2WI</option>
										<option value="DWI">DWI</option>
										<option value="rs-fMRI">rs-fMRI</option>
										<option value="Tracer">Tracer</option>
									</select>
									<input type="submit" name= "dir_add" class="btn btn-mini btn-success" value="<?php el('_add_dataset'); ?>">
								</div>
								</form>
							</div>
	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
