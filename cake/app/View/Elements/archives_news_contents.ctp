				<div class="col-xs-12 col-sm-12  col-md-12">
					<div class="table-responsive"><!-- table-responsive -->
						<table class="table table-striped table-hover table_id">
							<thead>
								<tr>
									<th>No.</th>
									<!-- <th><?php el('_To'); ?></th> -->
									<th><?php el('_sender'); ?></th>
									<th><?php el('_message_title'); ?></th>
									<th><?php el('_receive_date'); ?></th>
								</tr>
							</thead>
							<tbody>
<?php
	// icon
	$icons = array('News' => 'fa-folder', 'Message' => 'fa-envelope-o');

	// new
	$new_days = floor(strtotime(date('Y-m-d')) / 86400)
	          - Configure::read('Site.message_new_days');

	foreach ($data as $i => $dt) :
		$read = $dt['MessageRead'];
?>
								<!-- <tr data-href="/Message/<?php eh($dt['mess_id']); ?>"> -->
								<tr>
									<td><i class="read fa <?php
	echo $read['res'] ? 'fa-reply' : ($read['read'] ? 'fa-check' : '')
										?>"></i>
										<?php echo $i + 1; ?></td>
									<!-- <td><?php eh($dt['targets']); ?></td> -->
									<td><?php eh($dt['author']); ?> </td>
									<td><a href="/Message/<?php eh($dt['mess_id']); ?>"><?php eh($dt['title']); ?></a></td>
									<td><?php eh(date('Y-m-d H:i', strtotime($dt['date']))); ?>
<?php if (floor(strtotime($dt['date'])/86400) > $new_days) : ?>
										<small class="new">new</small>
<?php endif ?>
									</td>
								</tr>
<?php
	endforeach;
?>
							</tbody>
						</table>
					</div><!-- /table-responsive -->
				</div><!-- /cols -->
