				<!-- データセット検索 -->
				<div class="panel panel-success"><!-- panel -->
					<div class="panel-heading">
						<span class="glyphicon glyphicon-search"></span>&nbsp; <?php el('_search'); ?>

						<!-- <button class="pull-right btn-primary" data-toggle="collapse" data-target="#ppp4"><i class="fa fa-caret-square-o-down"></i></button> -->
						<a class="pull-right white" data-toggle="collapse" href="#p4">
							<i class="fa fa-caret-square-o-down"></i></a>
					</div><!-- /panel-heading -->

					<div class="panel-body collapse in" id="p4">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-key"></i>&nbsp;<?php el('_by_keyword'); ?></a></li>
							<li><a href="#tab2" data-toggle="tab"><i class="fa fa-keyboard-o"></i>&nbsp;<?php el('_by_conditions'); ?></a></li>
						</ul><!-- /nav-tabs -->

						<div class="tab-content">
							<div class="tab-pane active fade in" id="tab1">
								<br>
								<form name="form1" enctype="multipart/form-data" id="id_form1" action="/Search/" method="get">
									<div class="form-group">
									<!--
										<label for="name1">検索対象：</label>&nbsp;
										<label class="radio-inline"><input type="radio" name="iro" checked="checked">データセット</label>
										<label class="radio-inline"><input type="radio" name="iro">メタデータ</label>
									-->
									</div>
									<div class="input-group">
										<input type="text" class="form-control" placeholder="<?php el('_input_keyword'); ?>" name="keyword">
										<span class="input-group-btn">
										<!-- <button class="btn btn-default" type="submit" onclick="location.href='/Search/'"> -->
										<button class="btn btn-default" type="submit" name="submit1">
										<i class='glyphicon glyphicon-search'></i>
										</button>
										</span>
									</div>
								</form>
							</div><!-- /tab-pane -->

							<div class="tab-pane fade in" id="tab2">
								<br>
<?php echo $this->element('search_condition');	// データセット検索 ?>
							</div><!-- /tab-pane -->

<?php /* // 旧データセット検索 .. ~ 20160310
							<div class="tab-pane fade in" id="tab3">
								<br>
								<form name="selectform" enctype="multipart/form-data" id="select_form2" action="/Search/" method="post">
									<div class="form-inline form-group">
										<select id="typeSelect" class="form-control">
<?php foreach ($data_types as $dt) : ?>
											<option value="<?php eh($dt['type']); ?>"><?php eh($dt['type']); ?></option>
<?php endforeach; ?>
										</select>
										<select id="condSelect" class="form-control">
										</select>
										<div class="btn-group">
											<input type="button" class="btn btn-sm btn-success" onclick="add();" value="<?php el('_add_condition'); ?>">
											<input type="button" class="btn btn-sm btn-success" value="<?php el('_clear'); ?>">
										</div>
									</div>

									<label class="radio-inline">
										<input type="radio" name="iro" value="and" checked="checked">AND</label>
									<label class="radio-inline">
										<input type="radio" name="iro" value="or">OR</label>&nbsp;
									<button class="btn btn-success btn-sm " type="submit" name="submit2">
										<span class="glyphicon glyphicon-search"></span>&nbsp;<?php el('_search'); ?>
									</button>

									<br>
									<div id="test">

									</div>
								</form>
							</div><!-- /tab-pane -->
*/ ?>
						</div><!-- /tab-content -->
					</div><!-- /panelbody -->
				</div><!-- /panel -->
				<!-- /データセット検索 -->
