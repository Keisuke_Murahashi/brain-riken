<!-- 共有申請 -->
<div class="modal fade" id="p-lg3">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title"><?php el('_apply_share'); ?></h4>
				<h5><?php el('_share_application_of_dataset'); ?></h5>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<form name="share-form" class="form-group col-md-12" enctype="multipart/form-data" action="" method="post">
							<h5><?php el('_target_data'); ?></h5>
							<ul class="list-group">
								<li class="list-group-item"><strong></strong></li>
							</ul>
							<h5><?php el('_target_group'); ?></h5>
							<div id="search_data2"></div>
							<h5><?php el('_authorizer'); ?></h5>
							<div id="search_data"></div>
							<input type="hidden" id="data_id" name="data_id"/>
							<h5><?php el('_comment'); ?>
								<button type="button" class="reset-comment btn btn-xs btn-default pull-right"><?php el('_clear'); ?></button></h5>
							<textarea rows="5" class="form-control" id="ask1" name="detail"></textarea>
							<br>
							<button type="submit" name="share" id="share" class="btn btn-success btn-block"><?php el('_run_application'); ?></button>
							<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?php el('_cancel'); ?></button>
						</form>
					</div>
				</div><!-- /row -->
			</div>
		</div>
	</div>
</div>
