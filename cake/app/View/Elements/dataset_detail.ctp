<?php
	foreach ($data as $dt) :
		$mng = $dt['MongoSafety'];	// $dt['Mongo'];
		$cnt = $dt['AppCount'];
		$color = Util::getLabelColor($dt['publish_status']);
?>
		<!-- 基本データ -->
		<div class="row">
			<div class="col-xs-12 col-sm-12">
			<table class="table table-bordered">
				<tbody>
			    <tr>
			      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_data_id'); ?></th>
			      <td colspan="3" class="col-xs-10 col-sm-10 col-md-10"><?php eh($dt['data_id']); ?></td>
			    </tr>
				<tr>
			      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_data_type'); ?></th>
			      <td colspan="3" class="col-xs-10 col-sm-10 col-md-10"><?php eh($dt['data_type']); ?></td>
			    </tr>
				<tr>
			      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_dataset_dir'); ?></th>
			      <td class="col-xs-5 col-sm-5 col-md-5"><?php eh($dt['dir_name']); ?></td>
			      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_original_dataset_dir'); ?></th>
			      <td class="col-xs-3 col-sm-3 col-md-3"><?php eh($dt['original_dir_name']); ?></td>
			    </tr>
			    <tr>
			      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_creator'); ?></th>
			      <td colspan="3" class="col-xs-10 col-sm-10 col-md-10"><?php eh($dt['owner']); ?></td>
			    </tr>
			    <tr>
			      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_target_group'); ?></th>
			      <td colspan="3" class="col-xs-10 col-sm-10 col-md-10"><?php eh(implode(', ', $data_shared)); echo ($sharedgroup); ?></td>
			    </tr>
			    <tr>
			      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_shared'); ?></th>
			      <td colspan="3" class="col-xs-10 col-sm-10 col-md-10">
<!-- NIJC -->
<?php echo $this->element('item_status', array('cnt' => @$cnt)); ?>
			      </td>
			    </tr>
			    <tr>
			      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_publish'); ?></th>
			      <td colspan="3" class="col-xs-10 col-sm-10 col-md-10"><span class="label label-default label-<?php echo $color; ?>"><?php el($dt['publish_status']); ?></span></td>
			    </tr>
			    <tr>
			      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_upd_date'); ?></th>
			      <td colspan="3" class="col-xs-10 col-sm-10 col-md-10"><?php eh(date('Y-m-d H:i:s', strtotime($dt['upd_date']))); ?></td>
			    </tr>
			    </tbody>
			</table>
			</div>
		</div>
		<!-- /基本データ -->

<?php if ($is_app or !$is_editable) :	// 共有・公開申請中ならば ?>
		<p class="submit mb20">
			<button class="btn btn-success" disabled="disabled">
				<i class="fa fa-pencil"></i>&nbsp; <?php el('_edit_metadata'); ?></button>
		</p>
<?php elseif ($is_editable) :	// 編集可能ならば ?>
		<p class="submit mb20">
			<a href="/Dataset/template/<?php eh($dt['data_id']); ?>" class="btn btn-success"
				<?php // if (!$user['is_applicant']) echo 'disabled="disabled"'; ?>>
				<i class="fa fa-pencil"></i>&nbsp; <?php el('_edit_metadata'); ?></a>
		</p>
<?php endif; ?>

		<!-- メタデータ一覧 -->
		<div class="panel panel-success">
			<div class="panel-heading">
				<?php el('_details_metadata'); ?>

				<a class="pull-right white" data-toggle="collapse" href="#ppp1">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>
			<div class="panel-body collapse in" id="ppp1">

<?php if ($is_app) :	// 共有/公開状態があれば ?>
				<div class="text-right">
<?php 	if (isset($cnt['Share'])) foreach ($cnt['Share'] as $k => $v) : ?>
					<!-- span class="label label-default label-<?php echo Util::getLabelColor($k); ?>" -->
					<span class="label label-default label-<?php echo $cnt['Share']; ?>">
						<?php el(Util::getStatusLabel($k)); ?></span>
<?php 	endforeach; ?>

<?php 	if (isset($cnt['Release'])) foreach ($cnt['Release'] as $k => $v) : ?>
					<span class="label label-default label-<?php echo Util::getLabelColor($k); ?>">
						<?php el('_publish'); ?><?php el($k); ?></span>
<?php 	endforeach; ?>
				</div>
				<br />
<?php endif; ?>

<?php echo $this->element('dataset_metadata', array('mng' => $dt['MongoSafety']));	// データ詳細 ?>

			</div><!-- /panelbody -->
		</div><!-- /panel -->
		<!-- /メタデータ一覧 -->
<?php
/*
		<!-- (一時的) メタデータ一覧（全部） -->
		<div class="panel panel-success">
			<div class="panel-heading">
				メタデータ詳細（全部）

				<a class="pull-right white" data-toggle="collapse" href="#ppp1">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>
			<div class="panel-body collapse in" id="ppp1">

<?php echo $this->element('dataset_metadata', array('mng' => $dt['Mongo']));	// データ詳細 ?>

			</div><!-- /panelbody -->
		</div><!-- /panel -->
		<!-- /(一時的) メタデータ一覧（全部） -->
*/
?>
<?php
	endforeach;
?>
