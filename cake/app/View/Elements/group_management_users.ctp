		<!-- ユーザー一覧 -->
		<div class="panel panel-success">
			<div class="panel-heading">
				<?php el('_users_list_this_group'); ?>

				<a class="pull-right white" data-toggle="collapse" href="#ppp1">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>
			<div class="panel-body collapse in" id="ppp1">
				<form id="form-free-user" action="" method="post">
					<div class="table-responsive">
					<!-- <input type="button" class="btn btn-mini btn-success pull-right" data-toggle="modal" data-target="#p-lg5" value="テンプレート呼び出し" data-whatever="@mdo"><br> -->
						<table class="table table-striped table-hover table-condensed tree" id="table1">
							<thead>
								<tr>
<?php //if ($is_editable) : ?>
									<!-- <th><input type="checkbox" id="chk-all" name="chk-all" value="on"></th> -->
<?php //else : ?>
									<td>#</td>
<?php //endif; ?>
									<th><?php el('_user_id'); ?></th>
									<th><?php el('_user_name'); ?></th>
									<th><?php el('_detail'); ?></th>
									<th><?php el('_home_dir'); ?></th>
									<th><?php el('_group_name'); ?></th>
									<th><?php el('_role'); ?></th>
							</tr>
							</thead>
							<tbody>
<?php
	foreach ($data_users as $i => $dt) :
?>
								<tr class="click-checked">
<?php //if ($is_editable) : ?>
									<!-- <td><input type="checkbox" name="chk[]" value="<?php eh($dt['username']); ?>"></td> -->
<?php //else : ?>
									<td><?php echo $i + 1; ?></td>
<?php //endif; ?>
									<td><?php eh($dt['username']); ?></td>
									<td><?php eh($dt['name']); ?></td>
									<td><?php eh($dt['detail']); ?></td>
									<td><?php eh($dt['home_dir']); ?></td>
									<!-- <td><?php eh($dt['upd_date']); ?></td> -->
									<td><?php echo nl2br(h($dt['group_name'])); ?></td>
									<!-- <td><?php echo nl2br(h($dt['admin'])); ?></td> -->
									<td><?php echo nl2br(h($dt['role'])); ?></td>
									<!-- <td><?php eh($dt['status']); ?></td> -->
								</tr>
<?php
	endforeach;
?>
							</tbody>
						</table>
					</div><!-- /table-responsive -->
<?php if ($is_editable) : ?>
					<div class="form-group">
						<!-- <button type="submit" name="unset_group" class="btn btn-mini btn-success"><?php el('_unset_group'); ?></button> -->
						<!-- <small class="text-success ml10"><?php el('_unset_users_from_group'); ?></small> -->
						<!-- <input type="hidden" name="id" value="<?php eh($data_group['id']); ?>"> -->
					</div>
<?php endif; ?>
				</form>
			</div><!-- /panelbody -->
		</div><!-- /panel -->
