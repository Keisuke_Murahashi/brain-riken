<!-- 公開申請 - 大サイズ用モーダル -->
<div class="modal fade" id="p-lg2">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title">公開申請</h4>
				<h5>選択したデータセットの公開を申請します。よろしいですか？</h5>
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<ul class="list-group">
							<li class="list-group-item">t1wi_000001</li>
							<li class="list-group-item">t1wi_000002</li>
							<li class="list-group-item">t1wi_000003</li>
							<li class="list-group-item">t1wi_000004</li>
							<!-- <li class="list-group-item"><strong>MetaData：</strong>2015bmindsdata_001</li> -->
						</ul>
					</div>
				</div>
				<button type="button" class="btn btn-success btn-block" data-dismiss="modal">申請</button>
				<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?php el('_cancel'); ?></button>
			</div>
		</div>
	</div>
</div>
