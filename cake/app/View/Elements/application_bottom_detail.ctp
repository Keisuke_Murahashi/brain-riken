<!-- 申請詳細モーダル -->
<div class="modal fade" id="p-lg3">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title"><?php el('_details_application'); ?></h4><br>
				<!-- <h5><?php el('_reject_selected_application'); ?></h5> -->
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<form name="app-form" class="form-group col-md-12" enctype="multipart/form-data" action="" method="post">
						<table class="table table-bordered" id="table_app3">
							<tbody>
							</tbody>
						</table>
						<br>
						<!-- <button type="submit" name="reject" class="btn btn-success btn-block">棄却</button> -->
						<button type="button" class="btn btn-success btn-block" data-dismiss="modal"><?php el('_run_confirm'); ?></button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
