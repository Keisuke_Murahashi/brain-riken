		<!-- メッセージ作成 -->
		<div class="panel panel-success">
			<div class="panel-heading " data-toggle="collapse" data-target="#ppp1">
				<i class="fa fa-envelope"></i>&nbsp;
				<?php el('_create_message'); ?>

				<a class="pull-right white" data-toggle="collapse" href="#ppp1">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>
			<div class="panel-body <?php if (!$data_reply) echo 'collapse'; ?>" id="ppp1">

				<form name="form1" id="form1" action="/Message/create/<?php echo $id ?>" class="form-horizontal" method="post">
				<div class="form-group">
						<div class="col-sm-4">
						<label><?php el('_message_type'); ?></label>
							<select id="mess_type" name="mess_type" class="form-control col-sm-3" required>
								<option value=""><?php el('_select_message_type'); ?></option>
<?php
	foreach (array('user' => l('_user_message'), 'portal' => l('_portal_message')) as $key => $name) :
		$selected = ($data_reply and $data_reply['mess_type'] == $key) ? 'selected' : '';
?>
								<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $name; ?></option>
<?php
	endforeach;
?>
							</select>
						</div>
<!-- 						<div class="col-sm-3">
							<select name="category" class="form-control col-sm-3">
								<option value=""><?php el('_select_category'); ?></option>
<?php
	foreach (array('News', 'Message') as $name) :
		$selected = ($data_reply and $data_reply['category'] == $name) ? 'selected' : '';
?>
								<option value="<?php echo $name; ?>" <?php echo $selected; ?>><?php el($name); ?></option>
<?php
	endforeach;
?>
							</select>
						</div> -->
					</div>
					<div id="target-box" class="form-group">
						<div class="col-sm-12">
							<label><?php el('_target_user'); ?></label>
						</div>
<?php echo $this->element('message_form_users');	// users ?>
					</div>
									<div class="form-group">
						<div class="col-sm-12">
							<label><?php el('_message_title'); ?></label>
							<input type="text" class="form-control" name="title" placeholder="title" value="<?php if ($data_reply) echo 'RE: '. h($data_reply['title']); ?>">
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-12">
							<label><?php el('_message_body'); ?></label>
							<textarea class="form-control" rows="8" name="body" placeholder="body" required>
<?php // print reply content
	if ($data_reply) :
?>
-----Original Message-----
From : <?php eh($data_reply['author']); ?>

Sent : <?php eh($data_reply['date']); ?>
<?php eh($data_reply['content']); ?>
<?php
	endif;
?></textarea>
<br>
					<input type="submit" name="submit" class="btn btn-mini btn-success" value="<?php el('_submit'); ?>"
						<?php // if (!$user['is_applicant']) echo 'disabled="disabled"'; ?>>
						</div>
					</div>
				</form>
			</div><!-- panelbody -->
		</div><!-- panel -->
