<!-- メッセージモーダル -->
<div class="modal fade" id="p-lg1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
			<button class="close" data-dismiss="modal">
				<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
			</button>
			<h4 class="modal-title"><?php el('_details_message'); ?></h4><br>
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<form name="mess-form" id="mess-form" class="form-group col-md-12" enctype="multipart/form-data" action="/Message/" method="post">
						<div id="mess"></div>
						<button type="submit" name="reply" class="btn btn-success btn-block"><?php el('_reply'); ?></button>
						<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?php el('_close'); ?></button>
					</form>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
