<?php
	// for Home
	$this->assign('title', l('_config'));	// title
	$this->Html->script('config', array('inline' => false));	// script

	// import css/script
	$this->set('import_script', array(
		'ui', 'cookie', 'migrate',
		'datatables', 'tableedit',
	));
?>

	<!-- コンテンツ ここから -->
		<h1 class="page-header"><span class="glyphicon glyphicon-cog">&nbsp;<?php echo $this->fetch('title'); ?></h1>

		<p id="config_data_filter" class="nav text-right">
			<a href="/Config/filter/" class="btn btn-success"><?php el('_filter_setting'); ?></a>
		</p>

<?php echo $this->element('setup_config');	// 設定一覧 ?>

<?php echo $this->element('setup_dictionary');	// 辞書一覧 ?>

	<!-- /コンテンツ ここまで -->
