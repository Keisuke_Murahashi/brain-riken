<?php
	// for Home
	$this->assign('title', l('_users_managements'));	// title
	$this->Html->script('user_management', array('inline' => false));	// script

	// import css/script
	$this->set('import_script', array(
		'ui', 'cookie', 'migrate',
		'treegrid', 'datatables', 'tableedit', 'multiselect', 'prettify',
	));

	// bottom dialog
	$this->append('bottom', $this->element('user_management_bottom'));
?>

	<!-- コンテンツ ここから -->
		<h1 class="page-header"><span class="glyphicon glyphicon-user"></span>&nbsp;<?php echo $this->fetch('title'); ?></h1>

<?php   // NIJC
	// if ( isset($user['role']) && ($user['role'] == 'SystemAdmin') )
		echo $this->element('user_management_users');	// ユーザー管理 ?>
<?php echo $this->element('user_management_group');	// グループ管理 ?>

	<!-- /コンテンツ ここまで -->
