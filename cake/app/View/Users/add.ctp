<?php
	// for Home
	$this->assign('title', l('_add_user'));	// title
?>

	<!-- コンテンツ ここから -->
		<h1 class="page-header"><?php echo $this->fetch('title'); ?></h1>

		<?php echo $this->Form->create('User', array(
			'class' => 'form-signin col-md-4 clearfix',
		)); ?>

			<p>
				<?php echo $this->Form->input('username', array(
					'type'=>'text', 'label'=> false, 'size'=>30,
					'error'=> false, 'div'=> false,
					'class'=>'form-control','placeholder'=>'User name',
					'required'=>'required','autofocus'=>'autofocus',
				)); ?>

				<?php echo $this->Form->input('password', array(
					'type'=>'password', 'label'=> false, 'size'=>30,
					'error'=> false, 'div'=> false,
					'class'=>'form-control','placeholder'=>'Password',
					'required'=>'required',
				)); ?>

				<?php echo $this->Form->input('name', array(
					'type'=>'text', 'label'=> false, 'size'=>30,
					'error'=> false, 'div'=> false,
					'class'=>'form-control','placeholder'=>'User nickname',
				)); ?>
			</p>

		<?php echo $this->Form->end(array(
				'label' => l('_sign_up'),
				'class' => 'btn btn-lg btn-success btn-block',
			)); ?>

		<div class="pt30 clear">
			<p>ログインユーザー名とパスワードの登録だけを行うことができます。</p>
			<p>基本的にデバッグ用です。</p>
		</div>
	<!-- /コンテンツ ここまで -->
