<?php
	// for Home
	$this->assign('title', l('_top'));	// title
	$this->Html->script('home', array('inline' => false));	// script
	$this->Html->script('search', array('inline' => false));	// script

	// import css/script
//	$this->set('import_script', array(
//		'ui', 'cookie', 'migrate',
//		'treegrid', 'datatables', 'tableedit', 'multiselect', 'prettify',
//	));

	// bottom dialog
	$this->append('bottom', $this->element('home_bottom'));
?>

	<!-- コンテンツ ここから -->

		<h1 class="page-header"><span class="glyphicon glyphicon-home"></span>&nbsp;<?php echo $this->fetch('title'); ?></h1>

		<div class="row"><!-- row -->

			<!-- 左側半分 -->
			<div class="col-md-6">

<?php echo $this->element('home_news');	// お知らせ ?>

			</div><!-- /左側半分 -->

			<!-- 右側半分 -->
			<div class="col-md-6">

<?php echo $this->element('home_search');	// データセット検索 ?>


			</div><!-- 右側半分 -->

		</div><!-- /row -->
<?php echo $this->element('home_asset');	// Myアセット ?>

<?php //echo $this->element('home_dataset');	// 新規登録されたデータセット ?>

	<!-- /コンテンツ ここまで -->
