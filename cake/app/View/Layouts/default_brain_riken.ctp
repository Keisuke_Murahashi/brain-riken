<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html lang="<?php echo lang(); ?>">
<head>
<?php echo $this->element('head');	// head ?>
</head>
<body class="<?php printf('%s %s %s', $this->name, $this->action, lang()); ?>">
<?php echo $this->element('nav', array('username' => ''));	// navigation ?>

<div id="container" class="container-fluid">
	<div class="row"> <!-- row -->

<?php echo $this->element('sidebar');	// sidebar ?>

<main class="<?php echo $this->name .' '. $this->action; ?>">
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

<?php echo $this->Flash->render(); ?>

<?php echo $this->fetch('content');	// contents ?>

	</div>
</main>

	</div><!--/.row-->
</div><!--/#container-->

<?php echo $this->fetch('bottom');	// bottom contents ?>
<?php echo $this->fetch('bottom1');	// bottom contents ?>
<?php echo $this->fetch('bottom2');	// bottom contents ?>
<?php echo $this->fetch('bottom3');	// bottom contents ?>
<?php echo $this->fetch('bottom4');	// bottom contents ?>

<?php echo $this->element('footer'); ?>
<?php echo $this->element('sql_dump'); ?>
</body>
</html>
