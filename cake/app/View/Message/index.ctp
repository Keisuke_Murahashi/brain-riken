<?php
	// for Home
	$this->assign('title', l('_my_messages'));	// title
	$this->Html->script('message', array('inline' => false));	// script

	// import css/script
	$this->set('import_script', array(
		'ui', 'cookie', 'migrate',
		'treegrid', 'datatables'
	));

	// bottom dialog
	$this->append('bottom', $this->element('message_bottom'));
?>

	<!-- コンテンツ ここから -->
		<h1 class="page-header"><span class="glyphicon glyphicon-envelope"></span>&nbsp;<?php echo $this->fetch('title'); ?></h1>

<?php echo $this->element('message_form');	// メッセージ作成 ?>
<?php echo $this->element('message_receive');	// 受信メッセージ ?>
<?php echo $this->element('message_list');	// メッセージ一覧 ?>

	<!-- /コンテンツ ここまで -->
