<?php
	// for Home
	$this->assign('title', l('_details_message'));	// title
	$this->Html->script('message', array('inline' => false));	// script

	// import css/script
	$this->set('import_script', array(
		'ui', 'cookie', 'migrate',
		'treegrid', 'datatables', 'tableedit',
	));

	// bottom dialog
	$this->append('bottom', $this->element('message_bottom'));
?>

	<!-- コンテンツ ここから -->
		<h1 class="page-header"><span class="glyphicon glyphicon-envelope"></span>&nbsp;<?php echo $this->fetch('title'); ?></h1>


<?php
	// icon
	$icons = array('News' => 'fa-folder', 'Message' => 'fa-envelope-o');
	$types = array('user' => l('_user_message'), 'portal' => l('_portal_message'),
					'system' => l('_system_message'), );


	foreach ($data_items as $i => $dt) :
?>
		<div class="row">
			 <div class="col-xs-12 col-sm-10">
				<!-- <ul class="list-group">
					<li class="list-group-item"><strong>mess_id : </strong><?php echo h($dt['mess_id']); ?></li>
					<li class="list-group-item"><strong>mess_type : </strong><?php echo h($types[$dt['mess_type'] ]); ?></li>
					<li class="list-group-item"><strong>target : </strong><?php echo h($dt['targets']); ?></li>
				</ul>  -->
		<table class="table table-bordered">
			<tbody>
		    <tr>
		      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_message_id'); ?></th>
		      <td class="col-xs-10 col-sm-10 col-md-10"><?php echo h($dt['mess_id']); ?></td>
		    </tr>
			<tr>
		      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_message_type'); ?></th>
		      <td class="col-xs-10 col-sm-10 col-md-10"><?php echo h(@$types[$dt['mess_type'] ]); ?></td>
		    </tr>
		    <tr>
		      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_target'); ?></th>
		      <td class="col-xs-10 col-sm-10 col-md-10"><?php echo h($dt['targets']); ?></td>
		    </tr>
		    <tr>
		      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_sender'); ?></th>
		      <td class="col-xs-10 col-sm-10 col-md-10"><?php echo h($dt['author']); ?></td>
		    </tr>
		    </tbody>
		</table>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title"><?php echo h($dt['title']); ?></h2>
					</div>
					<div class="panel-body">
						<p><?php echo nl2br(h($dt['content']), true) ?></p>
					</div>
					<div class="panel-footer text-right">
						<small class="text-muted"><i class="fa fa-clock-o"></i><?php echo h(date('Y/m/d H:i', strtotime($dt['date']))); ?>
						&nbsp;|&nbsp;
						<i class="fa fa-pencil"></i><?php echo h($dt['author']); ?>
						<!-- &nbsp;|&nbsp;
						<i class="fa <?php echo $icons[ $dt['category'] ]; ?>"></i><?php el($dt['category']); ?></small> -->
					</div>
				</div>
<?php
		// システムメッセージのときは「返信」ボタン不要
		if ($dt['mess_type'] and $dt['mess_type'] != 'system') :
?>
				<a href="/Message/reply/<?php echo h($dt['mess_id']); ?>" class="btn btn-default btn-success pull-left"><?php el('_reply'); ?></a>
<?php	endif; ?>
			</div>
		</div>
<?php
	endforeach;
?>

	<!-- /コンテンツ ここまで -->
