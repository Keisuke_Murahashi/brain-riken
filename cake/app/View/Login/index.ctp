<?php
	// for Home
	$this->assign('title', l('_login'));	// title
	$this->Html->css('signin', null, array('inline' => false));	// css
?>
<!DOCTYPE html>
<html lang="<?php echo lang(); ?>">
<head>
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
<?php echo $this->element('head');	// head ?>
</head>
<body id="Login">
<?php echo $this->Flash->render(); ?>
<div id="container" class="container">

	<main>

		<?php echo $this->Form->create('User', array(
				'class' => 'form-signin', 'type' => 'post',
				'novalidate' => true		// NIJC
		)); ?>
			<h2 class="form-signin-heading text-center">
				<span class="glyphicon glyphicon-lock"></span> <?php echo $this->fetch('title'); ?>
			</h2>

			<?php echo $this->Form->input('username', array(
					'type'=>'text', 'label'=>false,
					'error'=>false, 'div'=>false,
					'class'=>'form-control','placeholder'=>'User ID',
					'required'=>'required','autofocus'=>'autofocus',
				)); ?>

			<?php echo $this->Form->input('password', array(
					'type'=>'password', 'label'=>false,
					'error'=>false, 'div'=>false,
					'class'=>'form-control','placeholder'=>'Password',
					'required'=>'required',
				)); ?>

			<div class="radio text-center">
				<label>
					<input type="radio" name="lang" value="ja" <?php echo ($language == 'ja') ? 'checked' : ''; ?>> 日本語
				</label>
				<label class="ml10">
					<input type="radio" name="lang" value="en" <?php echo ($language == 'en') ? 'checked' : ''; ?>> English
				</label>
			</div>

		<?php echo $this->Form->end(array(
			'label' => l('_sign_in'),
			'class' => 'btn btn-lg btn-success btn-block',
		)); ?>
	<div class="text-center">
  		<div class="glyphicon glyphicon-question-sign">
			<a href="//www.bminds.brain.riken.jp/password/?a=lostpass" target="_blank"><?php el('_lost_password'); ?></a>
  		</div>
	</div>
<?php if (!Configure::read('ActiveDirectory')) : ?>
		<p class="mt20 text-center">
			<a href="/Users/add/">&laquo; 新しいユーザーを追加する &raquo;</a>
		</p>
<?php endif; ?>
	</main>

</div><!--/#container-->

<?php echo $this->element('footer'); ?>
</body>
</html>
