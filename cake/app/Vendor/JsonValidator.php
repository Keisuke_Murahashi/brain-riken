<?php
/**
 * JsonSchema\Validator 用ラッパー
 *
 * App::import('Vendor', 'JsonValidator');
 *
 * [Get JsonValidator object]
 * $validator = JsonValidator::create();
 *
 * [Get JsonValidator object with checking valid]
 * $validator = JsonValidator::check($json_data_string, $json_scheme_string);
 * print_r($validator->getErrors());
 *
 * [Get error messages only]
 * $error_messages = JsonValidator::error($json_data_string, $json_scheme_string);
 * print_r($error_messages);	// if valid, output "null"
 */
require_once("vendor/autoload.php");

class JsonValidator {

	// Get JsonValidator object
	public static function create () {
		return new JsonSchema\Validator;
	}

	// Get JsonValidator object with checking valid
	public static function check ($json_data, $json_scheme) {
		$v = new JsonSchema\Validator;
		$v->check(json_decode($json_data), json_decode($json_scheme));
		return $v;
	}

	// debug print
	public static function error ($json_data, $json_scheme) {
		$v = new JsonSchema\Validator;
		$v->check(json_decode($json_data), json_decode($json_scheme));
		return (!$v->isValid()) ? $v->getErrors() : null;
	}
}
