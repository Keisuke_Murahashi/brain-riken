<?php
	// for Home
	$this->assign('title', l('_search_result'));	// title
	$this->Html->script('search', array('inline' => false));	// script

	// import css/script
	$this->set('import_script', array(
		'ui', 'cookie', 'migrate',
		'treegrid', 'datatables', 'tableedit',
	));

	// bottom dialog
//	$this->append('bottom', $this->element('search_bottom'));
?>

	<!-- コンテンツ ここから -->
		<header class="page-header">
			<h1><span class="glyphicon glyphicon-search"></span>
				<?php echo $this->fetch('title'); ?></h1>

<?php if ($this->request->is('get')) :	// キーワード検索時 ?>
			<form name="form1" enctype="multipart/form-data" id="id_form1" action="/Search/" method="get">
				<div class="input-group">
					<input type="text" class="form-control" name="keyword" value="<?php eh($this->request->query('keyword')); ?>" placeholder="<?php el('_input_keyword'); ?>" data-maxlength="<?php echo Configure::read('Site.keyword_max_length') ?: 50; ?>">
					<span class="input-group-btn">

					<button class="btn btn-default" type="submit" name="submit1">
						<i class='glyphicon glyphicon-search'></i>
					</button>
					</span>
				</div>
			</form>
<?php endif; ?>

			<div class="result">
<?php if ($count_all) :	// カウント ?>
				<strong><?php echo $count_all; ?></strong> 
				<?php echo el('_found_items'); ?>
<?php else : ?>
				<?php echo el('_not_found'); ?>
<?php endif; ?>
			</div>
		</header>

<?php if (!$this->request->is('get')) :	// 詳細検索時 ?>

<?php echo $this->element('search_condition');	// 検索条件 ?>

<?php endif; ?>

<?php if ($this->request->is('get')) :	// キーワード検索時 ?>
		<br>

		<!-- 検索種類 -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab1" data-toggle="tab">
				<?php el('_metadata'); ?> (<?php echo count($data_metadata); ?>)</a></li>

			<li><a href="#tab2" data-toggle="tab">
				<?php el('_static_pages'); ?> (<?php echo count($data_pages); ?>)</a></li>

			<li><a href="#tab3" data-toggle="tab">
				<?php el('_documents'); ?> (<?php echo count($data_documents); ?>)</a></li>
		</ul>
<?php endif; ?>

		<div class="tab-content">
			<div class="tab-pane active" id="tab1">
				<br>
<?php echo $this->element('search_result_metadata');	// メタデータ一覧 ?>

			</div><!-- /tab-pane -->

<?php if ($this->request->is('get')) :	// キーワード検索時 ?>
			<div class="tab-pane" id="tab2">
				<br>
<?php echo $this->element('search_result_pages');	// 固定ページ ?>

			</div><!-- /tab-pane -->

			<div class="tab-pane" id="tab3">
				<br>
<?php echo $this->element('search_result_documents');	// ドキュメント一覧 ?>

			</div><!-- /tab-pane -->
<?php endif; ?>

		</div><!-- /tab-content -->

	<!-- /コンテンツ ここまで -->
