<?php
	// for Archives
	$this->assign('title', l('_archives'));	// title
	$this->Html->script('archives', array('inline' => false));	// script

	// import css/script
	$this->set('import_script', array(
		'ui', 'cookie', 'migrate',
		'treegrid', 'datatables', 'tableedit', 'multiselect', 'prettify',
	));

	// bottom dialog
	$this->append('bottom', $this->element('archives_bottom'));
?>

	<!-- コンテンツ ここから -->
		<h1 class="page-header"><?php echo $this->fetch('title'); ?></h1>
		<div class="row"><!-- row -->

			<!-- 左側半分 -->
			<div class="col-md-10">

<?php echo $this->element('archives_news');	// お知らせ ?>

			</div><!-- /左側半分 -->

			<!-- 右側半分 -->
			<div class="col-md-2 pull-right">

<?php echo $this->element('archives_monthly');	// 月別 ?>

			</div><!-- /右側半分 -->
		</div><!-- /row -->
	<!-- /コンテンツ ここまで -->
