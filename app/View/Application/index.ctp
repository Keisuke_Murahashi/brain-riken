<?php
	// for Home
	$this->assign('title', l('_applications'));	// title
	$this->Html->script('application', array('inline' => false));	// script

	// import css/script
	$this->set('import_script', array(
		'ui', 'cookie', 'migrate',
		'treegrid', 'datatables', 'tableedit',
	));

	// bottom dialog
	$this->append('bottom', $this->element('application_bottom_accept'));
	$this->append('bottom', $this->element('application_bottom_reject'));
	$this->append('bottom', $this->element('application_bottom_detail'));
	$this->append('bottom', $this->element('application_bottom_withdraw'));
	$this->append('bottom', $this->element('application_bottom_withdraw_user'));
?>

	<!-- コンテンツ ここから -->
		<h1 class="page-header"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<?php echo $this->fetch('title'); ?></h1>

<?php if ($user['is_admin']) echo $this->element('application_list');	// 申請一覧（管理者用） ?>
<?php echo $this->element('application_list2');	// 申請一覧（ユーザー用） ?>
<?php //echo $this->element('application_history');	// 承認履歴 ?>

	<!-- /コンテンツ ここまで -->
