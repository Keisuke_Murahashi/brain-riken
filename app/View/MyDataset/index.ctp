<?php
	// for Home
	$this->assign('title', l('_item_management'));	// title
	$this->Html->script('myasset', array('inline' => false));	// script

	// import css/script
	$this->set('import_script', array(
		'ui', 'cookie', 'migrate',
		'treegrid', 'datatables', 'tableedit',
	));

	// bottom dialog
	$this->append('bottom', $this->element('myasset_bottom_linkdir'));
	$this->append('bottom', $this->element('myasset_bottom_public'), array('data_exp_list' => $data_exp_list));
	$this->append('bottom', $this->element('myasset_bottom_shared'));
	$this->append('bottom', $this->element('myasset_bottom_drop'));
	$this->append('bottom', $this->element('myasset_bottom_template'));
	$this->append('bottom', $this->element('myasset_bottom_uploadmeta'));
	$this->append('bottom', $this->element('myasset_bottom_invalid'));
	$this->append('bottom', $this->element('myasset_bottom_unlink'));
?>

	<!-- コンテンツ ここから -->
		<h1 class="page-header"><span class="glyphicon glyphicon-briefcase"></span>&nbsp;<?php echo $this->fetch('title'); ?></h1>

<?php // echo $this->element('myasset_wizard_nav');	// ウィザード ?>

<?php echo $this->element('myasset_form_new');	// メタデータ登録 ?>

<?php echo $this->element('myasset_form_data');	// データ一覧 ?>

	<!-- /コンテンツ ここまで -->
