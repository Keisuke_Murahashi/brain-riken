<!-- ディレクトリ関連付け - 大サイズ用モーダル -->
<div class="modal fade" id="p-lg1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title">ID発行</h4>
				<h5>選択したディレクトリのIDを発行します。よろしいですか？</h5>
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<ul class="list-group">
							<li class="list-group-item">/user/dir1</li>
							<li class="list-group-item">/user/dir2</li>
							<li class="list-group-item">/user/dir3</li>
							<li class="list-group-item">/user/dir4</li>
							<!-- <li class="list-group-item"><strong>MetaData：</strong>2015bmindsdata_001</li> -->
						</ul>
					</div>
				</div>
				<button type="button" class="btn btn-success btn-block" data-dismiss="modal">発行</button>
				<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?php el('_cancel'); ?></button>
			</div>
		</div>
	</div>
</div>
