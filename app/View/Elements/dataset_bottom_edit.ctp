<!-- メタデータ登録・更新 - 大サイズ用モーダル -->
<div class="modal fade" id="p-lg5">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title">メタデータ登録・更新</h4>
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<ul class="list-group">
							<li class="list-group-item"><strong>Base uri：</strong>/bminds/xxx000-103993</li>
							<li class="list-group-item"><strong>data_id：</strong>xxx000-1039992</li>
							<li class="list-group-item"><strong>MetaData：</strong>2015bmindsdata_001</li>
						</ul>
					</div>
					<div class=" col-sm-8">
						<!-- <a class="btn btn-sm btn-success pull-right" href="#p-lg" data-toggle="modal"><span class="glyphicon glyphicon-wrench"></span>&nbsp; メタデータ管理</a> -->
						<!-- <a class="btn btn-sm btn-success pull-right" href="#p-lg" data-toggle="modal"><span class="glyphicon glyphicon-wrench"></span>&nbsp; メタデータ管理</a> -->
						<div class="btn-group pull-right">
							<!-- <button class="btn btn-success">id割付申請</button> -->
							<!-- <button class="btn btn-success">登録/更新</button> -->
						</div>
					</div>
				</div>

				<div class="panel panel-success">
					<div class="panel-heading">登録されたメタデータ情報
						<div class="pull-right">
							<i class="fa fa-caret-square-o-down"></i>
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table id="example1" class="table table-striped table-hover table-condensed">
								<thead>
									<tr><th><?php el('_id'); ?></th><th><?php el('_Element'); ?></th><th><?php el('_Value'); ?></th></tr>
								</thead>
								<tbody>
									<tr><td>1</td><td>scheme_id</td><td></td></tr>
									<tr><td>2</td><td>data_id</td><td>t1wi_000001</td></tr>
									<tr><td>3</td><td>title</td><td>Adult marmoset, individual brain, in vivo, T1WI 9.4 Tesla</td></tr>
									<tr><td>4</td><td>subject</td><td>subject, keyword, classification code, or key phrase describing the resource.</td></tr>
									<tr><td>5</td><td>subject_scheme</td><td></td></tr>
									<tr><td>6</td><td>scheme_uri</td><td></td></tr>
									<tr><td>7</td><td>creator</td><td></td></tr>
									<tr><td>8</td><td>contact_person</td><td></td></tr>
									<tr><td>9</td><td>uploader</td><td></td></tr>
									<tr><td>10</td><td>contributor</td><td></td></tr>
									<tr><td>11</td><td>publisher</td><td>NIJC</td></tr>
									<tr><td>12</td><td>shared_date</td><td></td></tr>
									<tr><td>13</td><td>date</td><td></td></tr>
									<tr><td>14</td><td>content_language</td><td></td></tr>
									<tr><td>15</td><td>size</td><td></td></tr>
									<tr><td>16</td><td>format</td><td></td></tr>
									<tr><td>17</td><td>version</td><td></td></tr>
									<tr><td>18</td><td>rights</td><td></td></tr>
									<tr><td>19</td><td>resource_type</td><td></td></tr>
									<tr><td>20</td><td>related_content</td><td></td></tr>
									<tr><td>21</td><td>description</td><td></td></tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
