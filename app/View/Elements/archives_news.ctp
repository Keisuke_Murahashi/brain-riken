		<!-- お知らせ -->
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#tab1" data-toggle="tab"><i class="fa fa-user"></i>&nbsp;<?php el('_user_message'); ?></a>
				<span class="count"><?php echo @$count_unread['user'] ?: 0; ?></span>
			</li>
			<li>
				<a href="#tab2" data-toggle="tab"><i class="fa fa-users"></i>&nbsp;<?php el('_portal_message'); ?></a>
				<span class="count"><?php echo @$count_unread['portal'] ?: 0; ?></span>
			</li>
			<li>
				<a href="#tab3" data-toggle="tab"><i class="fa fa-flag-o"></i>&nbsp;<?php el('_system_message'); ?></a>
				<span class="count"><?php echo @$count_unread['system'] ?: 0; ?></span>
			</li>
		</ul>

		<div class="tab-content">
			<!-- 個人宛て通知 -->
			<div class="tab-pane active fade in" id="tab1">
				<br>
<?php
	// contents
	echo $this->element('archives_news_contents', array('data' => $data_user));
?>
			</div><!-- /tab-pane -->

			<!-- 皆様へのお知らせ -->
			<div class="tab-pane fade in" id="tab2">
				<br>
<?php
	// contents
	echo $this->element('archives_news_contents', array('data' => $data_every));
?>
			</div><!-- /tab-pane -->

			<!-- システムメッセージ -->
			<div class="tab-pane fade in" id="tab3">
				<br>
<?php
	// contents
	echo $this->element('archives_news_contents', array('data' => $data_system));
?>
			</div><!-- /tab-pane -->
		</div><!-- /tab-content -->
		<!-- /お知らせ -->
