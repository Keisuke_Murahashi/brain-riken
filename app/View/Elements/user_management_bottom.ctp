<!-- options data (hide) -->
<div class="hide ta-r">
	<select id="option_roles" name="option_roles">
<?php foreach ($option_roles as $dt) : ?>
		<option value="<?php eh($dt['name']); ?>"><?php el($dt['name']); ?></option>
<?php endforeach; ?>
	</select>

	<select id="option_group" name="option_group">
<?php foreach ($option_group as $dt) : ?>
		<option value="<?php eh($dt['id']); ?>"><?php echo $dt['name']; ?></option>
<?php endforeach; ?>
	</select>
</div>
<div id="php-user_id"
     data-val="<?php el('_user_id'); ?>"></div>
<div class="hide ta-r" id="php-user_name"
     data-val="<?php el('_user_name'); ?>"></div>
<div class="hide ta-r" id="php-detail"
     data-val="<?php el('_detail'); ?>"></div>
<div class="hide ta-r" id="php-home_dir"
     data-val="<?php el('_home_dir'); ?>"></div>
<div class="hide ta-r" id="php-group_name"
     data-val="<?php el('_group_name'); ?>"></div>
<div class="hide ta-r" id="php-role"
     data-val="<?php el('_role'); ?>"></div>


