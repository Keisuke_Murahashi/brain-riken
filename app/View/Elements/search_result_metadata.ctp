				<!-- データセット一覧 -->
                        <!-- form action="<?php eh(ConnectionManager::$config->default_api['download_url_any']); ?>"  method="post" id="sidebar_download" class="nav download" -->
			<form action="/MyDataset/download" method="post" id="sidebar_download" class="nav download">
                                        <div class="submit form-inline text-right">
                                                <input type="hidden" name="username" value="<?php eh($user['username']); ?>" />
                                                <input type="hidden" name="targets" value="" />
                                                <input type="hidden" name="download_url" value="<?php eh(ConnectionManager::$config->default_api['download_url_any']); ?>" />
                                                <input type="hidden" name="language" value="<?php echo lang(); ?>" />
                                                <button type="submit" class="btn btn-success" title="<?php el('_download'); ?>">
                                                        <?php el('_download_all'); ?></button>
                                        </div>
                        </form>
				<div class="panel panel-success">
					<div class="panel-heading">
						<?php el('_result_list'); ?>	
						<!-- <div class="pull-right"><i class="fa fa-caret-square-o-down"></i></div> -->
						<!-- <button	class="pull-right btn-success" data-toggle="collapse" data-target="#p1"><i class="fa fa-caret-square-o-down"></i></button> -->
						<a class="pull-right white" data-toggle="collapse" href="#p1">
							<i class="fa fa-caret-square-o-down"></i></a>
					</div>
					<div class="panel-body collapse in" id="p1">
						<div class="table-responsive">
							<table class="table table-striped table-hover" id="table1">
								<thead>
									<tr>
										<th>#</th>
										<th><?php el('_data_id'); ?></th>
										<th><?php el('_data_type'); ?></th>
										<th><?php el('_dataset_dir'); ?></th>
										<th><?php el('_upd_date'); ?></th>
										<th><?php el('_creator'); ?></th>
										<th><?php el('_title'); ?></th>
										<th><?php el('_subject'); ?></th>
										<th><?php el('_contact_person'); ?></th>
										<th><?php el('_fixed'); ?></th>
										<!-- th><?php el('_download'); ?></th -->

										<!-- <th><?php el('_share'); ?></th> -->
										<!-- <th><?php el('_publish'); ?></th> -->
										<!-- <th><?php el('_detail'); ?></th> -->
									</tr>
								</thead>
								<tbody>
<?php
	foreach ($data_metadata as $dt) :
		$mng = $dt['Mongo'];
?>
									<tr>
										<td><input type="checkbox" name="chk1[]" value="<?php eh($dt['data_id']); ?>"></td>
										<td><a href="/Dataset/detail/<?php echo $dt['data_id']; ?>"><?php eh($dt['data_id']); ?></a></td>
										<td><?php eh($dt['data_type']); ?></td>
										<td><?php eh($dt['dir_name']); ?></td>
										<td><?php eh(date('Y-m-d H:i:s', strtotime($dt['upd_date']))); ?></td>
										<td><?php eh($dt['owner']); ?></td>
<?php echo $this->element('dataset_list_mongo', array('mng' => $mng));	// Mongoデータ共通 ?>
										<!-- <td><a href="/Dataset/detail/<?php eh($dt['data_id']); ?>" class="btn btn-xs btn-success">detail</a></td> -->
                                                                <!-- td>
                                                                <?php if($dt['dir_name'] != '') : ?>
                                                                        <button type="button" class="download btn btn-default btn-xs fa fa-download"
                                                                                data-action="<?php eh($dt['download_url']); ?>"
                                                                                data-targets="<?php eh ($dt['UserAuth']['group']); echo ":"; eh($dt['data_id']); ?>"
                                                                                data-username="<?php eh($user['owner']); ?>"
                                                                        ></button>
                                                                <?php endif; ?>
                                                                </td -->

									</tr>
<?php
	endforeach;
?>
								</tbody>
							</table>
						</div><!-- /table-responsive -->
					</div><!-- /panel-body -->
				</div><!-- /panel -->
