<!-- 大サイズ用モーダル -->
<div class="modal fade" id="p-lg1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title"><?php el('_message'); ?></h4>
				<div class="panel panel-default">
					<div class="panel-heading"><h2 class="panel-title">パリ同時テロ　逃走容疑者、翌日にベルギーのカフェで休憩も</h2></div>
					<div class="panel-body">
						<p>今月１３日のパリ同時多発テロに関与したとして国際手配されているサラ・アブデスラム容疑者が、逃走中の事件翌日、ベルギー首都ブリュッセルのカフェに立ち寄っていたことが２９日までに分かった。</p>
						<p>アブデスラム容疑者はフランス出国時に続き、ここでも当局の目をすり抜けたことになる。(CNN.co.jp)</p>
						<a href="http://news.yahoo.co.jp/pickup/6182438">記事全文</a>
					</div>
					<div class="panel-footer text-right"><small class="text-muted"><i class="fa fa-clock-o"></i>2015.11.30&nbsp; |&nbsp; <i class="fa fa-pencil"></i>Kenji Sato&nbsp; |&nbsp; <i class="fa fa-folder"></i>News</small></div>
				</div>
			</div>
		</div>
	</div>
</div>
