<!-- options data (hide) -->
<div class="hide ta-r">
	<select id="option_roles" name="option_roles">
<?php foreach ($option_roles as $dt) : ?>
		<option value="<?php eh($dt['name']); ?>"><?php eh($dt['name']); ?></option>
<?php endforeach; ?>
	</select>

	<select id="option_group" name="option_group">
<?php foreach ($option_group as $dt) : ?>
		<option value="<?php eh($dt['id']); ?>"><?php eh($dt['name']); ?></option>
<?php endforeach; ?>
	</select>
</div>

<!-- テンプレート呼び出し - 大サイズ用モーダル -->
<div class="modal fade" id="p-lg5">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title">テンプレート呼び出し</h4>

				<form name="form1" enctype="multipart/form-data" action="/Message/" method="post">
					<div class="panel panel-success">
						<div class="panel-heading">
							登録されているテンプレート一覧
							<div class="pull-right"><i class="fa fa-caret-square-o-down"></i></div>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table id="" class="table table-striped table-hover table-condensed">
									<thead>
										<tr><th>#</th><th><?php el('_id'); ?></th><th>Template Name</th><th><?php el('_data_id'); ?></th><th><?php el('_register_date'); ?></th></tr>
									</thead>
									<tbody>
										<tr><td><input type="radio" name="test"></td><td>00001</td><td>Template Name</td><td>t1wi_000001</td><td>2015/12/16 11:45:00</td></tr>
										<tr><td><input type="radio" name="test"></td><td>00002</td><td>Template Name</td><td>t1wi_000002</td><td>2015/12/16 11:45:00</td></tr>
										<tr><td><input type="radio" name="test"></td><td>00003</td><td>Template Name</td><td>t1wi_000003</td><td>2015/12/16 11:45:00</td></tr>
										<tr><td><input type="radio" name="test"></td><td>00004</td><td>Template Name</td><td>t1wi_000004</td><td>2015/12/16 11:45:00</td></tr>
										<tr><td><input type="radio" name="test"></td><td>00005</td><td>Template Name</td><td>t1wi_000005</td><td>2015/12/16 11:45:00</td></tr>
									</tbody>
								</table>
							</div>
							<input type="text" class="form-control" id="recipient-name">
						</div><!--/panel-body-->
					</div><!--/panel-->
					<button class="btn btn-success btn-block" type="submit">呼び出し</button>
					<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?php el('_cancel'); ?></button>
				</form>
			</div>
		</div>
	</div>
</div>
