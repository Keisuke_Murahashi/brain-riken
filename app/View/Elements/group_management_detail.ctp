		<!-- グループ詳細 -->
		<div class="row">
			<div class="col-xs-12 col-sm-12">
			<table class="table table-bordered">
				<tbody>
			    <tr>
			      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_group_name'); ?></th>
			      <td class="col-xs-10 col-sm-10 col-md-10"><?php eh($data_group['name']); ?></td>
			    </tr>
				<tr>
			      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_detail'); ?></th>
			      <td class="col-xs-10 col-sm-10 col-md-10"><?php eh($data_group['detail']); ?></td>
			    </tr>
			    <tr>
			      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_status'); ?></th>
			      <td class="col-xs-10 col-sm-10 col-md-10"><?php eh($data_group['status']); ?></td>
			    </tr>
			    <tr>
			      <th class="col-xs-2 col-sm-2 col-md-2 success"><?php el('_upd_date'); ?></th>
			      <td class="col-xs-10 col-sm-10 col-md-10"><?php eh(date('Y-m-d H:i:s', strtotime($data_group['upd_date']))); ?></td>
			    </tr>
			    </tbody>
			</table>
			</div>
		</div><!-- /row -->
