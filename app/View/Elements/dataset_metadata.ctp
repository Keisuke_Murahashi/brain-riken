				<div class="table-responsive"><!-- table-responsive -->
<?php
		if ($mng) :	// Mongo データがあれば
?>
					<table class="table table-striped table-hover table-condensed tree" id="example1">
						<thead>
							<tr>
								<th><?php el('_id'); ?></th>
								<th><?php el('_element'); ?></th>
								<!-- <th><?php el('_help'); ?></th> -->
								<th><?php el('_Value'); ?></th>
							</tr>
						</thead>
						<tbody>
<?php
			// Mongo データを DUMP
			$i = 0;
			$i_each = 0;
			foreach ($mng as $key => $value) :
				$i_parent = ++$i_each;

				if (is_array($value)) :
?>
							<tr class="treegrid-<?php echo $i_parent; ?>">
								<td><?php echo ++$i; ?></td>
								<td><?php eh($key); ?></td>
								<!-- <td><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></td> -->
								<td></td>
							</tr>
<?php
					foreach ($value as $key1 => $value1) :
						$i_child = 0;

						if (is_array($value1)) :
							foreach ($value1 as $key2 => $value2) :
								$i_grandchild = 0;

								if (is_array($value2)) :
?>
									<tr class="treegrid-<?php echo ++$i_each; ?> treegrid-parent-<?php echo $i_parent; ?>">
										<td><?php echo $i; ?>-<?php echo ++$i_child; ?></td>
										<td><?php eh($key2); ?></td>
										<!-- <td><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></td> -->
										<td></td>
									</tr>

<?php
									foreach ($value2 as $key3 => $value3) :
										$i_parent2 = $i_each;
										foreach ($value3 as $key4 => $value4) :
?>
											<tr class="treegrid-<?php echo ++$i_each; ?> treegrid-parent-<?php echo $i_parent2; ?>">
												<td><?php echo $i; ?>-<?php echo $i_child; ?>-<?php echo ++$i_grandchild; ?></td>
												<td><?php eh($key4); ?></td>
												<!-- <td><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></td> -->
												<td><?php eh($value4); ?></td>
											</tr>

<?php
										endforeach;

									endforeach;
								else:
?>
									<tr class="treegrid-<?php echo ++$i_each; ?> treegrid-parent-<?php echo $i_parent; ?>">
										<td><?php echo $i; ?>-<?php echo ++$i_child; ?></td>
										<td><?php eh($key2); ?></td>
										<!-- <td><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></td> -->
										<td><?php eh($value2); ?></td>
									</tr>

<?php
							endif;
							endforeach;
						endif;
					endforeach;
				else:
?>
							<tr class="treegrid-<?php echo $i_parent; ?>">
								<td><?php echo ++$i; ?></td>
								<td><?php eh($key); ?></td>
								<!-- <td><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></td> -->
								<td><?php eh($value); ?></td>
							</tr>
<?php
				endif;
			endforeach;
?>
						</tbody>
					</table>
<?php
		else :	// Mongo データがなければ
?>
					<p><?php el('_no_metadata'); ?></p>
					<br>
<?php
		endif;
?>
				</div><!-- /table-responsive -->
