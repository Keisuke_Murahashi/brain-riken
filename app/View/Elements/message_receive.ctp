		<!-- ドキュメント一覧 -->
		<div class="panel panel-success">
			<div class="panel-heading"><span class="glyphicon glyphicon-save-file"></span>&nbsp;
				<?php el('_archives', '受信メッセージ'); ?>

				<a class="pull-right white" data-toggle="collapse" href="#ppp3">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>
			<div class="panel-body collapse in" id="ppp3">

			<?php echo $this->element('archives_news');	// お知らせ ?>

			</div><!-- /panelbody -->
		</div><!-- /panel -->
