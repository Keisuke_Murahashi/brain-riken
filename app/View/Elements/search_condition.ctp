				<!-- 検索条件 -->
				<form name="selectform" enctype="multipart/form-data"
					 id="select_form2" action="/Search/" method="post" class="search_condition">
					<fieldset class="row">
						<legend><?php el('_search_conditions'); ?></legend>
						<div id="data_type_group" class="col-xs-12 col-sm-2 form-group">
							<label for="data_type"><?php el('_data_type'); ?>:</label>
							<select id="data_type" name="data_type" class="form-control"
								title="<?php el('_select_data_type_at_search'); ?>">
<?php
	$post_type = $this->request->data('data_type');
	foreach ($data_types as $type) :
		$selected = ($type == $post_type) ? ' selected' : '';
?>
								<option value="<?php eh($type); ?>"<?php echo $selected; ?>><?php eh($type); ?></option>
<?php endforeach; ?>
							</select>
						</div>
						<table class="col-xs-12 col-sm-8 form-group">
<?php
	if (!empty($conditions)) :
		foreach ($conditions as $cond) :
?>
							<tr>
								<td class="field">
									<div class="wrapper">
										<select name="field[]" class="form-control condition_field"
											title="<?php el('_select_field_at_search'); ?>">
											<option value="<?php eh($cond['field']); ?>" selected><?php eh($cond['field']); ?></option>
										</select>
									</div>
								</td>
								<td class="value">
									<div class="wrapper">
										<input type="text" name="value[]"
											class="form-control condition_value"
											placeholder="<?php el('_input_keyword'); ?>"
											value="<?php eh($cond['value']); ?>"
											title="<?php el('_input_value_at_search'); ?>"
											data-maxlength="<?php echo Configure::read('Site.keyword_max_length') ?: 50; ?>">
									</div>
								</td>
								<td class="button">
									<div class="wrapper">
										<button type="button" class="btn btn-sm btn-success add_condition">
											<i class="fa fa-plus"></i>
											<?php el('_add_condition'); ?>
										</button>
										<button type="button" class="btn btn-sm btn-success clear_condition">
											<i class="fa fa-minus"></i>
											<?php el('_clear'); ?>
										</button>
									</div>
								</td>
							</tr>

<?php
		endforeach;
	else :
?>
							<tr>
								<td class="field">
									<div class="wrapper">
										<select name="field[]" class="form-control condition_field"
											title="<?php el('_select_field_at_search'); ?>">
											<option value=""><?php el('_select_field_at_search'); ?></option>
										</select>
									</div>
								</td>
								<td class="value">
									<div class="wrapper">
										<input type="text" name="value[]"
											class="form-control condition_value"
											placeholder="<?php el('_input_keyword'); ?>"
											value=""
											title="<?php el('_input_value_at_search'); ?>"
											data-maxlength="<?php echo Configure::read('Site.keyword_max_length') ?: 50; ?>">
									</div>
								</td>
								<td class="button">
									<div class="wrapper">
										<button type="button" class="btn btn-sm btn-success add_condition">
											<i class="fa fa-plus"></i>
											<?php el('_add_condition'); ?>
										</button>
										<button type="button" class="btn btn-sm btn-success clear_condition">
											<i class="fa fa-minus"></i>
											<?php el('_clear'); ?>
										</button>
									</div>
								</td>
							</tr>
<?php
	endif;
?>						</table>
						<div class="col-xs-12 col-sm-2 form-group text-center submit">
							<div class="form-group">
								<label class="radio-inline">
									<input type="radio" name="iro" value="and"
										<?php echo $this->request->data('iro') != 'or' ? 'checked="checked"' : '' ?>>AND</label>
								<label class="radio-inline">
									<input type="radio" name="iro" value="or"
										<?php echo $this->request->data('iro') == 'or'  ? 'checked="checked"' : '' ?>>OR</label>
							<!-- /div>
							<div class="form-group" -->
								<button class="btn btn-success btn-sm ml10" type="submit" name="submit2">
									<span class="glyphicon glyphicon-search"></span>&nbsp;<?php el('_search'); ?>
								</button>
							</div>
						</div>
					</fieldset>
				</form>
