			<div id="json_direct_link">
				<!-- <a href="#" id="direct_link"><?php el('_direct_link'); ?></a> -->
				<!-- <?php el('_cache_schema_options'); ?> -->

			</div>

			<div id="json_output">
				<h2><?php el('_json_output'); ?></h2>
				<p><?php el('_form_edit_json_directly'); ?></p>
				<div class="form-group col-md-12">
					<button type="button" class='btn btn-success' id='setvalue'><?php el('_update_form'); ?></button>
					<br><br>
					<!--<button type="submit" class='btn btn-success' name="update" id="update"><?php el('_update_data'); ?></button>-->
					<textarea id='output' style='width: 100%; height: 300px; font-family: monospace;' class='form-control' name="json"></textarea>
				</div>
			</div>

			<div id="json_options">
				<h2><?php el('_options'); ?></h2>
				<div id='options_holder'>
					<div>
						<label>CSS Framework</label>
						<select id='theme_switcher' class='form-control'>
							<option value='html'>None</option>
							<option value='jqueryui'>jQuery UI</option>
							<option value='bootstrap2'>Bootstrap 2</option>
							<option value='bootstrap3'>Bootstrap 3</option>
							<option value='foundation3'>Foundation 3</option>
							<option value='foundation4'>Foundation 4</option>
							<option value='foundation5'>Foundation 5</option>
						</select>
					</div>
					<div>
						<label>Icon Library</label>
						<select id='icon_switcher' class='form-control'>
							<option value=''>None</option>
							<option value='jqueryui'>jQuery UI</option>
							<option value='bootstrap2'>Bootstrap 2 Glyphicons</option>
							<option value='bootstrap3'>Bootstrap 3 Glyphicons</option>
							<option value='foundation2'>Foundicons 2</option>
							<option value='foundation3'>Foundicons 3</option>
							<option value='fontawesome3'>FontAwesome 3</option>
							<option value='fontawesome4'>FontAwesome 4</option>
						</select>
					</div>
					<div>
						<label>Object Layout</label>
						<select id='object_layout' class='form-control'>
							<option value='normal'>normal</option>
							<option value='grid'>grid</option>
						</select>
					</div>
					<div>
						<label>Show Errors</label>
						<select id='show_errors' class='form-control'>
							<option value='interaction'>On Interaction</option>
							<option value='change'>On Field Change</option>
							<option value='always'>Always</option>
							<option value='never'>Never</option>
						</select>
					</div>
					<div>
						<label>Boolean options</label>
						<select multiple size=9 id='boolean_options' style='width: 100%;' class='form-control'>
							<option value='required_by_default'>Object properties required by default</option>
							<option value='no_additional_properties'>No additional object properties</option>
							<option value='ajax'>Allow loading schemas via Ajax</option>
							<option value='disable_edit_json'>Disable "Edit JSON" buttons</option>
							<option value='disable_collapse'>Disable collapse buttons</option>
							<option value='disable_properties'>Disable properties buttons</option>
							<option value='disable_array_add'>Disable array add buttons</option>
							<option value='disable_array_reorder'>Disable array move buttons</option>
							<option value='disable_array_delete'>Disable array delete buttons</option>
						</select>
					</div>
				</div>
			</div><!-- /json_options -->

			<div id="json_validation">
				<h2><?php el('_validation'); ?></h2>
				<p><?php el('_show_valudation_error'); ?></p>
				<textarea id='validate' style='width: 100%; height: 100px; font-family: monospace;' readonly disabled class='form-control'></textarea>
			</div><!-- /json_options -->
