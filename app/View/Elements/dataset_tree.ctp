<?php
	foreach ($data as $i => $dt) :
?>
		<div class="panel panel-success">
			<div class="panel-heading">
				<?php el('_dataset_tree'); ?>&nbsp;
				<!-- <div class="pull-right"><i class="fa fa-caret-square-o-down"></i> -->
				<a class="pull-right white" data-toggle="collapse" href="#ppp2">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>
			<div class="panel-body collapse in" id="ppp2">
				<div class="">

<?php echo $this->element('dataset_tree_list', array(
			'dataset' => $dt['DatasetTree'],
			'data_id' => $dt['data_id'],
			'dir_name' => str_replace('\\', '/', $dt['dir_name']),
));	// データツリー ?>

				</div><!-- / -->
			</div><!-- /panel-body -->
		</div><!-- /panel -->
<?php
	endforeach;
?>
