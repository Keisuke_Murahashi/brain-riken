		<!-- 辞書一覧 -->
		<div class="panel panel-success">
			<div class="panel-heading">
				<?php el('_dictionary_list'); ?>

				<a class="pull-right white" data-toggle="collapse" href="#body-dictionary">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>

			<div class="panel-body collapse in" id="body-dictionary">
				<div class="table-responsive">
					<table class="table table-striped table-hover" id="table-dictionary">
						<thead>
							<tr>
								<th>#</th>
								<th><?php el('_term'); ?></th>
								<th><?php el('_memo'); ?></th>
								<th><?php el('_update'); ?></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>New</td>
								<td></td>
								<td></td>
								<td>(new data)</td>
							</tr>
<?php
	foreach ($data_dictionary as $i => $dt) :
?>
							<tr>
								<td><?php eh($dt['id']); ?></td>
								<td><?php eh($dt['term']); ?></td>
								<td><?php eh($dt['memo']); ?></td>
								<td><?php eh(date('Y-m-d H:i:s', strtotime($dt['update']))); ?></td>
							</tr>
<?php
	endforeach;
?>
						</tbody>
					</table>
					<p class="text-right"><small><?php el('_edit_new_line_to_add_dictionary'); ?></small></p>
				</div><!-- /table-responsive -->
			</div><!-- /panel-body -->
		</div><!-- /panel -->
