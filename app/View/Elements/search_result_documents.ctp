				<!-- ドキュメント一覧 -->
				<div class="panel panel-success">
					<div class="panel-heading">
						<?php el('_result_list'); ?>	
						<a class="pull-right white" data-toggle="collapse" href="#ppp2">
							<i class="fa fa-caret-square-o-down"></i></a>
					</div>
					<div class="panel-body collapse in" id="ppp2">
						<div class="table-responsive">
							<table class="table table-striped table-hover" id="table3">
								<thead>
									<tr>
										<th>#</th>
										<th><?php el('_data_id'); ?></th>
										<th><?php el('_doc_name'); ?></th>
										<th><?php el('_creator'); ?></th>
										<th><?php el('_upd_date'); ?></th>
										<th><?php el('_doc_type'); ?></th>
										<th><?php el('_download'); ?></th>
									</tr>
								</thead>
								<tbody>
<?php
	foreach ($data_documents as $i => $dt) :
?>
									<tr>
										<td><?php echo $i + 1; ?></td>
										<td><a href="/Dataset/detail/<?php echo $dt['data_id']; ?>"><?php eh($dt['data_id']); ?></a></td>
										<td><?php eh($dt['filename']); ?></td>
										<td><?php eh($dt['owner']); ?></td>
										<td><?php eh($dt['file_date']); ?></td>
										<td class="text-center text-success"><i class="fa <?php eh($dt['icon']); ?> fa-2x" title="<?php echo $dt['filetype_s']; ?>"></i></td>
										<td><a href="/Download/?id=<?php eh($dt['data_id']); ?>&amp;file=<?php eh($dt['filepath']); ?>" class="btn btn-sm btn-success">
												<span class="glyphicon glyphicon-download-alt"></span></a></td>
									</tr>
<?php
	endforeach;
?>
								</tbody>
							</table>
							<div class="text-center">
							</div>
						</div><!-- /table-responsive -->
					</div><!-- /panel-body -->
				</div><!-- /panel -->
