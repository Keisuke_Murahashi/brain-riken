<!-- <div class="row"> --><!-- row -->
<!-- 左側半分 -->
<!-- <div class="col-md-6"> -->
<!-- メタデータ登録 -->
	<h4><i class="fa fa-download"></i>&nbsp; <?php el('_register_metadata'); ?></h4>
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tabfile" data-toggle="tab"><i class="fa fa-file-o"></i>&nbsp; <?php el('_upload_metadata'); ?></a></li>
		<li><a href="#tabform" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp; <?php el('_input_metadata'); ?></a></li>
	</ul>
	<div class="tab-content">
		<!-- ファイルで登録 -->
		<div class="tab-pane active fade in" id="tabfile">
			<br>
			<form class="form-inline mb20" enctype="multipart/form-data" action="" name="form" method="post">
			<div class="form-group">
				<div class="input-group">
					<input type="file" id="file_input" name="file_input" style="display: none;" required>
					<div class="input-group col-md-12">
						<input id="dummy_file" type="text" class="form-control input-xxlarge" placeholder="<?php el('_select_file'); ?>" disabled>
					</div>
					<span class="input-group-btn">
						<button class="btn btn-default" type="button" onclick="$('#file_input').click();"><i class="glyphicon glyphicon-folder-open"></i></button>
					</span>
					<input type="submit" name= "meta_reg" class="btn btn-mini btn-success" value="<?php el('_upload'); ?>"
						 <?php // if (!$user['is_applicant']) echo 'disabled="disabled"'; ?>>
				</div>
			</div>
			</form>
		</div>
		<br>
		<!-- フォームで登録 -->
		<div class="tab-pane fade in" id="tabform">
			<!--  <form id="id_form1" name="form1" action="" -->
			<form id="id_form1" name="form1" action="/Dataset/add_new"
			class="form-inline clearfix mb20" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<!-- <input class="input-xxlarge form-control" id="id_textBox1" type="text" placeholder="<?php el('_input_directory_name'); ?>" name="dir_name">&nbsp; -->
				<select id="Select0" class="form-control" name="data_type" required>
					<option value=""><?php el('_select_datatype'); ?></option>
<?php foreach ($data_types as $type) : ?>
					<option value="<?php eh($type); ?>"><?php eh($type); ?></option>
<?php endforeach; ?>
				</select>
				<input type="submit" name= "dir_add" class="btn btn-mini btn-success" value="<?php el('_add_dataset'); ?>"
					 <?php // if (!$user['is_applicant']) echo 'disabled="disabled"'; ?>>
			</div>
			</form>
		</div>
	</div>
        <h4><i class="fa fa-link"></i>&nbsp; <?php el('_link_dir'); ?></h4>
<div class="panel-heading"><i class="fa fa-list" style="color: green;"></i>&nbsp;<?php el('_select_item'); ?></div>
	<button class="btn btn-success" id="upddirbtn"  <?php // if (!$user['is_applicant']) echo 'disabled="disabled"'; ?>><i class="fa fa-check-square"></i>&nbsp; <?php el('_link_dir'); ?></button> &nbsp; &nbsp; &nbsp; &nbsp;
	<!-- NIJC -->
	<button class="btn btn-success" id="unlinkdirbtn"  <?php // if (!$user['is_applicant']) echo 'disabled="disabled"'; ?>><i class="fa fa-check-square-o"></i>&nbsp; <?php el('_unlink_dir'); ?></button>

	<br>
<!-- /メタデータ登録 -->
<!-- </div> -->
<!-- <div class="col-md-6"> -->
<!-- 共有・公開申請 -->
<?php if ($user['is_applicant']) : ?>
<h1 class="page-header">&nbsp;</h1>
<div style="overflow: hidden;">
	<div style="float: left; border-right: solid 1px #eee; padding-right: 50px;"><h4><i class="fa fa-share-alt"></i>&nbsp; <?php el('_share_group'); ?></h4>
	  <div class="panel-heading"><i class="fa fa-list" style="color: green;"></i>&nbsp;<?php el('_select_item'); ?></div>
	  <button class="btn btn-success" id="sharebtn"   <?php if (!$user['is_applicant']) echo 'disabled="disabled"'; ?>><i class="fa fa-share-alt"></i>&nbsp; <?php el('_apply_share'); ?></button>&nbsp;&nbsp;&nbsp;&nbsp;</div>
	  <div style="float: left; padding-left: 20px;"><h4><i class="fa fa-share-alt"></i>&nbsp; <?php el('_share_publish'); ?></h4>
	  <div class="panel-heading"><i class="fa fa-list" style="color: green;"></i>&nbsp;<?php el('_select_item'); ?></div>
	  <!-- NIJC -->
	  <button class="btn btn-success" id="publishbtn" <?php if (!$user['is_applicant'] || $user['UserAuth']['role'] != 'SystemAdmin') echo 'disabled="disabled"'; ?>><i class="fa fa-share-square-o"></i>&nbsp; <?php el('_apply_publication'); ?></button>
	</div>
</div>
<?php endif; ?>
<!-- 共有・公開申請 -->
<!-- </div> -->
<!-- </div> -->
<br><br>
