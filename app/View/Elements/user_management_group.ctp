		<!-- データセット一覧 -->
<?php if ($user['role'] == 'SystemAdmin') : ?>
                <div class="panel panel-info">
                <div class="panel-heading"><i class="fa fa-users"></i>&nbsp; <?php el('_group_managements'); ?>&nbsp;(<?php el('_for_system_admin'); ?>)
<?php else : ?>
                <div class="panel panel-success">
                        <div class="panel-heading"><i class="fa fa-users"></i>&nbsp; <?php el('_group_managements'); ?>
<?php endif; ?>
				<a class="pull-right white" data-toggle="collapse" href="#p2">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>
			<div class="panel-body collapse in" id="p2">
<?php if ($user['is_admin']) : ?>
				<form name="form1" enctype="multipart/form-data" id="id_form1"
						action="" class="form-inline clearfix" method="post">
					<!-- NIJC -->
					<?php  if ($user['UserAuth']['role'] == 'SystemAdmin') { ?>
					<div class="form-group">
						<input class="input-xxlarge form-control" id="id_textBox1" name="gp_name" type="text" placeholder="<?php el('_input_group_name'); ?>" required>&nbsp;
						<input class="input-xxlarge form-control" id="id_textBox2" name="gp_detail" type="text" placeholder="<?php el('_input_detail'); ?>">&nbsp;
						<!-- <input type="button" class="btn btn-mini btn-success" onclick="add();" value="新規追加">	-->
						<input type="submit" name= "submit1" class="btn btn-mini btn-info" value="<?php el('_new_group'); ?>">
					</div>
					<?php } ?>

					<!--div class="form-group pull-right">
						<select id="Select1" name="action" onchange="changeSelect()" class="form-control">
							<option value=""><?php el('_select_action'); ?></option>
							<option value="#p-lg1"><?php el('_valid'); ?></option>
							<option value="#p-lg2"><?php el('_invalid'); ?></option>
							<!-- <option value="#p-lg3">一括登録</option> -->
						<!-- /select -->
						<!-- <input type="button" id="testLink" class="btn btn-mini btn-primary"	data-toggle="modal" value="実行"> -->
						<!-- a class="btn btn-mini btn-info" href="" data-target="#p-lg1" data-toggle="modal"><?php el('_run'); ?></a>
					 </div -->
				</form>
<?php endif; ?>
				<br>
				<div class="table-responsive">
					<table class="table table-striped table-hover <?php echo $user['is_admin'] ? 'editable' : ''; ?>" id="table2">
						<thead>
							<tr>
								<th>#</th>
								<!-- th><?php el('_group_id'); ?></th -->
								<th><?php el('_group_name'); ?></th>
								<th><?php el('_detail'); ?></th>
								<th><?php el('_status'); ?></th>
							</tr>
						</thead>
						<tbody>
<?php
	foreach ($data_group as $i => $dt) :
?>

							<tr class="<?php echo ($user['role'] == 'SystemAdmin' || $dt['id'] == $user['group']) ? '' : 'not' ?>">
								<td data-href="/UserManagement/group/<?php eh($dt['id']); ?>"><?php eh($dt['id']); ?></td>
								<td data-href="/UserManagement/group/<?php eh($dt['id']); ?>"><?php eh($dt['name']); ?></td>
								<td><?php eh($dt['detail']); ?></td>
								<td><?php eh($dt['status']); ?></td>
							</tr>
<?php
	endforeach;
?>
						</tbody>
					</table>
				</div><!--/table-responsive-->
			</div><!--/panel-body-->
		</div><!--/panel-->
