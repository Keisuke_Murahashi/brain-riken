	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title><?php echo $this->fetch('title'); ?></title>

	<?php echo $this->element('import_script');	// 各ページに必要な css/script を assign ?>

	<?php echo $this->Html->meta('icon'); ?>

	<?php echo $this->fetch('meta'); ?>

	<?php echo $this->Html->css('font-awesome.min'); ?>

	<?php echo $this->Html->css('bootstrap.min'); ?>

	<?php echo $this->Html->css('dashboard'); ?>

	<?php echo $this->Html->css('bminds'); ?>

	<?php echo $this->fetch('css'); ?>

	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	<!--[if lt IE 9]>
		<script src="/assets/js/ie8-responsive-file-warning.js"></script>
			<![endif]-->
	<script src="/assets/js/ie-emulation-modes-warning.js"></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
