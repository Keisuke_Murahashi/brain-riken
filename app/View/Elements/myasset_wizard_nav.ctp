<!-- ウィザード -->
	<div class="row text-center mb40"><!-- row -->
		<div class="col-md-3">
			<a href="#" id="testbtn">
				<span class="fa-stack fa-3x">
					<!-- <i class="fa fa-clone fa-stack-1x text-success"></i> -->
					<i class="fa fa-download text-success"></i>
				</span>
			<p class="text-muted"><?php el('_register_metadata'); ?></p>
			</a>
		</div>
		<div class="col-md-3">
			<a href="#" id="sharebtn">
				<span class="fa-stack fa-3x">
					<i class="fa fa-share-alt fa-stack-1x text-success"></i>
				</span>
				<p class="text-muted"><?php el('_apply_shared_dataset'); ?></p>
			</a>
		</div>
		<div class="col-md-3">
			<a href="#" id="publishbtn">
				<span class="fa-stack fa-3x">
					<i class="fa fa-share-square-o fa-stack-1x text-success"></i>
				</span>
			<p class="text-muted"><?php el('_apply_public_dataset'); ?></p>
			</a>
		</div>
		<div class="col-md-3">
			<a href="#" id="upddirbtn">
				<span class="fa-stack fa-3x">
					<i class="fa fa-check-square fa-stack-1x text-success"></i>
				</span>
				<p class="text-muted"><?php el('_relate_dataset'); ?></p>
			</a>
		</div>
	</div><!-- /row -->
<!-- /ウィザード -->
