<?php
	// 対象ユーザーリスト
	$targets = array();

	// 返信のとき
	if (!empty($data_reply)) {
		$targets = isset($data_reply['targets']) ?
					explode("\n", $data_reply['targets']) :
					Hash::extract($data_reply, 'Target.{n}.target');

		$targets = array_fill_keys($targets, true);	// 連想配列に

		// 元の送信者も含める
		if (isset($data_reply['author'])) $targets[ $data_reply['author'] ]= true;

		// 自身が宛先のものは削除
		unset( $targets[ $user['username'] ] );
	}
?>
						<!--グループ選択-->
						<div id="target-group" class="col-sm-3 target-group">
							<select multiple class="form-control group" name="target_group[]" size="6">
								<option value="" selected><?php el('_all_groups') ?></option>
<?php
	foreach ($data_group as $group) :
		if (empty($group['UserAuth'])) continue;
?>
								<option value="<?php eh($group['id']); ?>"><?php eh($group['name']); ?></option>
<?php
	endforeach;
?>
							</select>
						</div>
						<!-- 全ユーザー選択 -->
						<div id="target-group-all" class="col-sm-3 target-users">
							<select multiple class="form-control users" name="target[]" size="6">
<?php
	foreach ($data_usernames as $u) :
			$selected = (array_key_exists($u['username'], $targets)) ? 'selected' : '';
?>
								<option value="<?php eh($u['username']); ?>" <?php echo $selected; ?>><?php eh($u['name']); ?></option>
<?php
	endforeach;
?>
							</select>
						</div>
<?php

	foreach ($data_group as $group) :
		if (empty($group['UserAuth'])) continue;
?>
						<!-- <?php eh($group['id']); ?> ユーザー選択 -->
						<div id="target-group-<?php eh($group['id']); ?>" class="col-sm-3 target-users">
							<select multiple class="form-control users" name="target[]" size="6">
<?php
		foreach  ($group['UserAuth'] as $user_auth) :
			if (empty($user_auth['User'])) continue;
			$u = $user_auth['User'];
			$selected = (array_key_exists($u['username'], $targets)) ? 'selected' : '';
?>
								<option value="<?php eh($u['username']); ?>" <?php echo $selected; ?>><?php eh($u['name']); ?></option>
<?php
		endforeach;
?>
							</select>
						</div>
<?php
	endforeach;
?>
