<!-- ナビゲーションバー -->
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">
			<!-- <span class="glyphicon glyphicon-cloud"></span> -->
			<i class="fa fa-cloud"></i>
				Brain/MINDS Research Portal</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<span class="glyphicon glyphicon-user"></span>&nbsp; <?php echo @$user['name']; ?></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#"><?php el('_user_name'); ?> : <?php echo @$user['username']; ?></a></li>
						<li><a href="#"><?php el('_group_name'); ?> : <?php echo @$user['group_name']; ?></a></li>
						<li><a href="#"><?php el('_home_dir'); ?> : <?php echo @$user['home_dir']; ?></a></li>
						<li><a href="#"><?php el('_language'); ?> : <?php echo lang(); ?></a></li>
						<li><a href="#"><?php el('_role'); ?> : <?php echo @$user['UserAuth']['role']; ?></a></li>
					</ul>
				</li>
				<li><a href="/Config/"><span class="glyphicon glyphicon-cog"></span>&nbsp; <?php el('_settings'); ?></a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<img src="/img/lang-<?php echo lang(); ?>.png" alt="" />&nbsp;
							<?php echo lang(); ?></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="?lang=ja"><img src="/img/lang-ja.png" alt="" /> 日本語</a></li>
						<li><a href="?lang=en"><img src="/img/lang-en.png" alt="" /> English</a></li>
					</ul>
				</li>
				<li><a href="/Login/"><span class="glyphicon glyphicon-log-out"></span>&nbsp; <?php el('_logout'); ?></a></li>
		</ul>

		</div>
	</div>
</nav><!-- /ナビゲーションバー -->
