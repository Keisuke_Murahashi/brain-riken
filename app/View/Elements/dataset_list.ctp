		<!-- データセット一覧 -->
		<div class="panel panel-success">
			<div class="panel-heading">
				<i class="fa fa-list"></i>&nbsp; <?php el('_dataset_list'); ?>

				<a class="pull-right white" data-toggle="collapse" href="#p1">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>

			<div class="panel-body collapse in" id="p1">
				<div class="table-responsive">
					<table class="table table-striped table-hover" id="table1">
						<thead>
							<tr>
								<th>#</th>
								<th><?php el('_data_id'); ?></th>
								<th><?php el('_data_type'); ?></th>
								<th><?php el('_dataset_dir'); ?></th>
								<th><?php el('_upd_date'); ?></th>
								<th><?php el('_owner'); ?></th>
								<th><?php el('_title'); ?></th>
								<th><?php el('_subject'); ?></th>
								<th><?php el('_contact_person'); ?></th>
								<!-- <th><?php el('_share'); ?></th> -->
								<!-- <th><?php el('_publish'); ?></th> -->
								<!-- <th><?php el('_detail'); ?></th> -->
							</tr>
						</thead>
						<tbody>
<?php
	foreach ($data as $i => $dt) :
		$mng = $dt['Mongo'] + array('title' => '', 'subject' => '', 'contact_person' => '');;
?>
							<tr>
								<td><input type="checkbox" name="chk1" value="<?php echo $dt['data_id']; ?>"></td>
								<td><a href="/Dataset/detail/<?php echo $dt['data_id']; ?>"><?php eh($dt['data_id']); ?></a></td>
								<td><?php eh($dt['data_type']); ?></td>
								<td><?php eh($dt['dir_name']); ?></td>
								<td><?php eh(date('Y-m-d H:i:s', strtotime($dt['upd_date']))); ?></td>
								<td><?php eh($dt['owner']); ?></td>
<?php echo $this->element('dataset_list_mongo', array('mng' => $mng));	// Mongoデータ共通 ?>
								<!-- <td><a href="/Dataset/detail/<?php echo $dt['data_id']; ?>"
									class="btn btn-xs btn-success">detail</a></td> -->
							</tr>
<?php
	endforeach;
?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
