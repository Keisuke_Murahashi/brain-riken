								<div class="list-group">
<?php
	// icon
	$icons = array('News' => 'fa-folder', 'Message' => 'fa-envelope-o');

	// new
	$new_days = floor(strtotime(date('Y-m-d')) / 86400)
	          - Configure::read('Site.message_new_days');

	foreach ($data as $dt) :
		$read = $dt['MessageRead'];
		$dt['content'] = ( mb_strlen($dt['content']) > 80 ) ? mb_substr($dt['content'], 0, 80).'...' : $dt['content'];
?>
								<!--
									<a href="/MessageDetail/?mess_id=<?php echo $dt['mess_id']; ?>" class="list-group-item">
								-->
									<div class="list-group-item clearfix">
										<i class="read fa <?php
	echo $read['res'] ? 'fa-reply' : ($read['read'] ? 'fa-check' : '')
										?>"></i>
										<a href="/Message/<?php echo $dt['mess_id']; ?>" data-mess_id="<?php echo $dt['mess_id']; ?>" class="aaa"><?php eh($dt['title']); ?></a>

										<small class="text-muted pull-right mt02">
<?php if (floor(strtotime($dt['date'])/86400) > $new_days) : ?>
											<span class="new">new</span>
<?php endif ?>
											<i class="fa fa-clock-o"></i><?php eh(date('Y-m-d H:i', strtotime($dt['date']))); ?>
											&nbsp;|&nbsp;
											<i class="fa fa-pencil"></i><?php eh($dt['author']); ?>
											<!-- &nbsp;|&nbsp;
											<i class="fa <?php echo $icons[ $dt['category'] ]; ?>"></i><?php el($dt['category']); ?> -->
										</small>
										<br>
										<small class="text-muted"><?php echo $dt['content']; ?></small>
									</div>
<?php
	endforeach;
?>
								</div>
