<!-- ディレクトリ関連付け -->
<div class="modal fade" id="p-lg1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title"><?php el('_relate_dataset'); ?></h4>
				<h5><?php el('_relate_metadata_and_directory'); ?></h5>
				<div class="row">
					<div class="col-xs-12 col-sm-12">
					<form name="share-form" class="form-group col-md-12" enctype="multipart/form-data" action="" method="post">
						<p><?php el('_target_data'); ?></p>
						<ul class="list-group">
							<li class="list-group-item"><strong></strong></li>
						</ul>
						<p><?php el('_target_directory'); ?>
							<small>( <?php echo @$user['home_dir']; ?> )</small></p>
						<div id="dir_list"></div><br>
						<input type="hidden" id="data_id" name="data_id"/>
						<button type="submit" name="upd_dir" id="upd_dir" class="btn btn-success btn-block"><?php el('_run_relate'); ?></button>
						<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?php el('_cancel'); ?></button>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
