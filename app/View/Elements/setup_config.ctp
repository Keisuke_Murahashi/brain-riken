		<!-- 設定一覧 -->
		<div class="panel panel-success">
			<div class="panel-heading">
				<?php el('_config_list'); ?>

				<a class="pull-right white" data-toggle="collapse" href="#body-config">
					<i class="fa fa-caret-square-o-down"></i></a>
			</div>

			<div class="panel-body collapse in" id="body-config">
				<div class="table-responsive">
					<table class="table table-striped table-hover" id="table-config">
						<thead>
							<tr>
								<th>#</th>
								<th><?php el('_key'); ?></th>
								<th><?php el('_value'); ?></th>
								<th><?php el('_memo'); ?></th>
								<th><?php el('_update'); ?></th>
							</tr>
						</thead>
						<tbody>
<?php
	foreach ($data_config as $i => $dt) :
?>
							<tr>
								<td><?php eh($dt['id']); ?></td>
								<td><?php eh($dt['key']); ?></td>
								<td><?php eh($dt['value']); ?></td>
								<td><?php eh($dt['memo']); ?></td>
								<td><?php eh(date('Y-m-d H:i:s', strtotime($dt['upd_date']))); ?></td>
							</tr>
<?php
	endforeach;
?>
							<tr>
								<td>New</td>
								<td>___</td>
								<td>__</td>
								<td>___</td>
								<td>(new data)</td>
							</tr>
						</tbody>
					</table>
					<p class="text-right"><small><?php el('_edit_new_line_to_add_config'); ?></small></p>
				</div><!-- /table-responsive -->
			</div><!-- /panel-body -->
		</div><!-- /panel -->
