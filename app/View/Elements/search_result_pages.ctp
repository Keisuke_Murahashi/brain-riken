				<!-- 固定ページ -->
				<div class="panel panel-success">
					<div class="panel-heading">
						<?php el('_result_list'); ?>
						<a class="pull-right white" data-toggle="collapse" href="#ppp1">
							<i class="fa fa-caret-square-o-down"></i></a>
					</div>
					<div class="panel-body collapse in" id="ppp1">
						<div class="table-responsive">
							<table class="table table-striped table-hover tree" id="table2">
								<thead>
									<tr>
										<th>#</th>
										<th><?php el('_page_title'); ?></th>
										<th><?php el('_content'); ?></th>
										<!-- <th><?php el('_create_date'); ?></th> -->
									</tr>
								</thead>
								<tbody>
<?php
	foreach ($data_pages as $i => $dt) :
?>
<tr>

										<td><?php echo $i + 1; ?></td>
										<td><a href="<?php eh($dt['urlLink']) ?>"><?php eh($dt['title']); ?></a></td>
										<td><?php echo $dt['contentDescription']; ?></td>
										<!-- <td><?php eh($dt['mimetype']); ?></td> -->
</tr>
	<?php
	endforeach;
?>
								</tbody>
							</table>
						</div><!-- /table-responsive -->
					</div><!-- /panelbody -->
				</div><!-- /panel -->
