<!-- 申請承認取り下げモーダル -->
<div class="modal fade" id="p-lg5">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only"><?php el('_close'); ?></span>
				</button>
				<h4 class="modal-title"><?php el('_withdraw_approved_application'); ?></h4>
				<h5><?php el('_withdraw_selected_approved_application'); ?></h5>
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<form name="app-form" class="form-group col-md-12" enctype="multipart/form-data" action="" method="post">
						<table class="table table-bordered" id="table_app5">
							<tbody>
							</tbody>
						</table>
						<p><?php el('_authorizer'); ?></p>
						<div id="search_data"></div>
						<br><br>
						<h5><?php el('_comment'); ?>
							<button type="button" class="reset-comment btn btn-xs btn-default pull-right"><?php el('_clear'); ?></button></h5>
						<textarea rows="5" class="form-control" name="comment"></textarea>
						<br>
						<input type="hidden" id="app_id" name="app_id"/>
						<input type="hidden" id="act_id" name="act_id"/>
						<button type="submit" name="withdraw" class="btn btn-success btn-block"><?php el('_run_withdrawal'); ?></button>
						<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?php el('_cancel'); ?></button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
