<?php if ($dataset) : ?>
					<ul>
<?php	foreach ($dataset as $filename) : ?>
						<li>
<?php		if (is_array($filename)) : ?>
							<b><?php eh(basename($filename[0]));	// dirname ?>/</b>

<?php echo $this->element('dataset_tree_list', array(
			'dataset' => $filename[1],
			'data_id' => $data_id,
			'dir_name' => $dir_name,
));	// データツリー ?>

<?php		else :

	// ダウンロードパス
	$filepath = preg_replace('!^/+!', '',
					str_ireplace($dir_name, '',
						str_replace('\\', '/', $filename)));
?>
							<a href="/Download/?id=<?php echo $data_id; ?>&amp;file=<?php eh($filepath); ?>">
								<?php eh(basename($filename));	// filename ?></a>
<?php		endif; ?>
						</li>
<?php	endforeach; ?>
					</ul>

<?php else: ?>
					<p><?php el('_no_dataset_file'); ?></p>
<?php endif; ?>
