<?php
	// for Home
	$this->assign('title', l('_details_group'));	// title
	$this->Html->script('user_management', array('inline' => false));	// script

	// import css/script
	$this->set('import_script', array(
		'ui', 'cookie', 'migrate',
		'treegrid', 'datatables', 'tableedit', 'multiselect', 'prettify',
	));

	// bottom dialog
	$this->append('bottom', $this->element('group_management_bottom'));
?>

	<!-- コンテンツ ここから -->
		<h1 class="page-header"><i class="fa fa-users"></i>&nbsp;<?php echo $this->fetch('title'); ?></h1>

<?php echo $this->element('group_management_detail');	// グループ管理 ?>

<?php echo $this->element('group_management_users');	// ユーザー管理 ?>

<?php if ($is_editable) echo $this->element('group_management_free_users');	// ユーザー管理 ?>

	<!-- /コンテンツ ここまで -->
