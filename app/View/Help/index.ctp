<?php
	$this->assign('title', l('_help'));	// title
?>
	<!-- コンテンツ ここから -->
	<!-- <h1 class="page-header"><?php echo $this->fetch('title'); ?></h1> -->
	<h1 class="page-header">FAQ（よくあるご質問）</h1>

	ご利用にあたって<br>
	<ul type="disc">
	<li>利用申請はどこにすればよいか</li>
	<li>利用ガイドライン</li>
	<li>セキュリティガイドライン</li>
	</ul>
	<hr>
	ユーザ、グループ関連<br>
	<ul type="disc">
	<li><a href="#q5">ユーザを追加するには</a></li>
	<li>グループを追加するには</li>
	</ul>
	<hr>
	アトラス関連<br>
	<ul type="disc">
	<li>
	<a href="#q1">マーモセット脳の領野名について知りたい。</a></li>
	<li>ｘｘｘｘｘｘ</li>
	</ul>
	<hr>
	メタデータ、データ登録関連<br>
	<ul type="disc">
	<li><a href="#q2">メタデータの各項目には何を入力するのか。</a></li>
	<li>メタデータだけを登録するには。</li>
	</ul>
	<hr>
	グループ間共有関連<br>
	<ul type="disc">
	<li>グループ間共有したい</li>
	<li>共有申請を取り下げたい。</li>
	</ul>
	<hr>

	<br><br>

ユーザ向け資料
1)革新脳データベース利用ガイドライン
2)革新脳データ共有・公開サーバーSAKIの利用 at a glance
 システムとして
　基本機能(貯蔵,HPC, 研究ＰＦポータル、諸ＶＭ、基本共用ツール、data portal)を図示し、
　それぞれの機能を利用するためのマニュアル とのリンクをつける。
 人として
　利用者の受け入れ体制として
　　グループとしての登録制
　　グルー内の管理者の役割
　　サーバー管理者の役割

3)研究プラットフォーム ユーザーマニュアル

 I 基本機能と使い方
  1 研究プラットフォームポータル 森井
   1-1 アイテム（メタデータとデータセット）の登録と共有　前田、竹内
   1-1-1　データセットのアップロード  ftp or ファイル転送ツール
   1-1-2 メタデータ編集・登録とデータセットとのヒモ付け
   1-1-3 データ（メタデータとデータセット）のcurationと共有
        グループ内curation, 共有
        グループ間共有の申請・承認
   1-1-4 データの公開準備と公開用ポータルへの送信
  1-2 ファイル転送　竹内
       scp
  1-3 HPC　前田
　　　概要（30日用も含む）、詳細
  1-4 pipelineVM　Alex、前田
  1-5 BiCC　森田

４）公開データポータル ユーザーマニュアル　保留

５）データベース標準化のための資料
　メタデータ作成ポリシー 前田
　メタデータ記入解説　前田
　　既存分はデータタイプ毎
　Anatomical annotation　？？
   　端川アトラス
   　パクシノスアトラス
　各種オントロジー（参照用）

６）各種申請ドキュメントフォーム
  グループ申請
  ユーザー追加登録申請
  グループ間共同研究申し合わせ
  新規サーバ構築申請（受付手続き、立ち上げ後の管理責任など）
	<br><br>
	<br><br>
	<br><br>
	<br><br>
	<a name="q1">
	Q： ガイドラインについて<br>
	A： 以下からガイドラインをダウンロードすることができます。ご一読ください。<br>
	<a href="guideline.pdf">Guideline.pdf</a>
	</a>
	<br><br>
	<a name="q1">
	Q： マーモセット脳の領野名について知りたい。<br>
	A： 以下からアトラスをダウンロードすることができます。<br>
	<!-- <a href="Structure List (Marmo list 5.5bTH).pdf">Structure List (Marmo list 5.5bTH)</a> -->
<?php echo $this->Html->link('Structure List (Marmo list 5.5bTH)', '/files/Structure List (Marmo list 5.5bTH).pdf') ?>
	</a>
	<br><br>
	<br><br>
	<br><br>
	<br><br>
	<br><br>
	<a name="q2">
	Q： メタデータの各項目には何を入力するのか。<br>
	A： 各項目の説明と入力例については、<a href="metadatadescription.pdf">こちら</a>をご参照ください。

	</a>
	<br><br>
	<a name="q5">
	Q： Registration of users <br>
	A： xxxxx

	</a>
	<br><br>
	Q： グループ間共有したい<br>
	A： ｘｘｘｘ
	<br><br>

	Q： 共有申請を取り下げたい。<br>
	A： ｘｘｘｘ

	<br><br>
	</ul>

	<div class="row"><!-- row -->
	</div><!-- /row -->

	<!-- /コンテンツ ここまで -->
