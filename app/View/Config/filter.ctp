<?php
	// for Home
	$this->assign('title', l('_filter_setting'));	// title
	$this->Html->script('config', array('inline' => false));	// script

	// import css/script
	$this->set('import_script', array(
		'ui', 'cookie', 'migrate',
		'datatables', 'tableedit',
	));
?>

	<!-- コンテンツ ここから -->
		<h1 class="page-header">
			<span class="glyphicon glyphicon-cog">&nbsp;<?php echo $this->fetch('title'); ?>
			<small>(<?php echo $group['name']; ?>)</small>
		</h1>

		<!-- データタイプ選択 -->
		<div id="config_data_filter" class="text-right">
			<a href="/Config/" class="btn btn-success"><?php el('_config'); ?></a>
		</div>

		<!-- オプション -->
<?php if ($data_group) : ?>
		<div id="config_options" class="form-inline pull-right text-right">
			<label for="config_select_group" class="control-label"><?php el('_select_group'); ?> : </label>
			<select id="config_select_group" name="group_id" class="form-control">
<?php
		foreach ($data_group as $g) :
			$selected = ($g['id'] == $group['id']) ? ' selected' : '' ;
?>
				<option value="<?php echo $g['id']; ?>"<?php echo $selected; ?>><?php eh($g['name']); ?></option>
<?php 	endforeach; ?>
			</select>
		</div>
<?php endif; ?>

		<!--タブメニュー-->
		<ul class="nav nav-tabs">
<?php
	foreach (array_keys($data_schemes) as $n => $data_type) :	// データタイプごと
?>
			<li class="<?php echo ($n == 0) ? 'active' : ''; ?>">
				<a href="#<?php echo $data_type; ?>" data-toggle="tab"><?php echo $data_type; ?></a></li>
<?php endforeach;	// データタイプごと ?>
		</ul><!-- /nav-tabs -->

		<br />

		<!-- タブ開始 -->
		<div class="tab-content">
<?php
	foreach (array_keys($data_schemes) as $n => $data_type) :	// データタイプごと
		$data_fields = $data_schemes[$data_type]['fields'];	// JSON スキーマのフィールド名
		$data_filter = $data_schemes[$data_type]['filter'];	// 各フィールドに対応する公開設定
?>

			<!-- タブ詳細（<?php echo $data_type; ?>） -->
			<div class="tab-pane <?php echo ($n == 0) ? 'active' : ''; ?>" id="<?php echo $data_type; ?>">

				<!-- 設定一覧 -->
				<form action="" class="panel panel-success filter-setting" method="post">
					<div class="panel-heading">
						<?php printf('%s: <b>%s</b>', l('_data_type'), $data_type); ?>
						<input type="hidden" name="data_type" value="<?php echo $data_type; ?>" class="data_type" />

						<a class="pull-right white" data-toggle="collapse" href="#body-<?php echo $data_type; ?>">
							<i class="fa fa-caret-square-o-down"></i></a>
					</div>

					<div class="panel-body collapse in" id="body-<?php echo $data_type; ?>">
						<div class="text-right">
							<a class="btn btn-default check-all" href="#body-<?php echo $data_type; ?>">
								<i class="fa fa-check-square-o"></i> <?php el('_check_all'); ?></a>
							<a class="btn btn-default uncheck-all" href="#body-<?php echo $data_type; ?>">
								<i class="fa fa-square-o"></i> <?php el('_uncheck_all'); ?></a>
						</div>

						<div class="table-responsive">
							<table class="table table-striped table-hover">
								<colgroup class="order"></colgroup>
								<colgroup class="check"></colgroup>
								<colgroup class="key"></colgroup>
								<colgroup class="text"></colgroup>
								<colgroup class="date"></colgroup>
								<thead>
									<tr>
										<th>#</th>
										<th><?php el('_public'); ?></th>
										<th><?php el('_key'); ?></th>
										<th><?php el('_description'); ?></th>
										<th><?php el('_update'); ?></th>
									</tr>
								</thead>
								<tbody>
<?php
	$i = 0;
	foreach ($data_req_fields as $key => $memo) :
?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td class="check">
											<i class="fa fa-check text-success"></i>
										</td>
										<td class="name"><?php echo $key; ?></td>
										<td class="text"><?php el($memo); ?></td>
										<td class="date"><?php el('_required'); ?></td>
									</tr>
<?php
	endforeach;
?>
<?php
	foreach ($data_fields as $key => $memo) :
		$filter = isset($data_filter[$key]) ? $data_filter[$key] : null;
		$checked = ($filter and $filter['value'] > 0) ? ' checked' : '';
?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td class="check">
											<input type="checkbox"
													data-id="<?php if ($filter) echo $filter['id']; ?>"
													data-type="<?php echo $data_type; ?>"
													name="<?php echo $key; ?>" value="1" class="checkbox key" <?php echo $checked ?> />
										</td>
										<td class="name"><?php echo $key; ?></td>
										<td class="text"><?php echo h($memo); ?></td>
										<td class="date"><?php if ($filter) eh(date('Y/m/d H:i:s', strtotime($filter['upd_date']))); ?></td>
									</tr>
<?php
	endforeach;
?>
								</tbody>
							</table>
							<p class="text-right"><small><?php el('_check_fields_you_want_to_publish_data'); ?></small></p>
						</div><!-- /table-responsive -->
					</div><!-- /panel-body -->
				</form><!-- /panel -->
			</div><!-- /tab-pane -->

<?php endforeach;	// データタイプごと ?>
		</div><!-- /tab-content -->
	<!-- /コンテンツ ここまで -->
