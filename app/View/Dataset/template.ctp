<?php
	// for Home
	$this->assign('title', l('_edit_metadata'));	// title
	$this->Html->script('dataset_template', array('inline' => false));	// script

	// import css/script
	$this->set('import_script', array(
		'ui', 'cookie', 'migrate',
		'treegrid', 'datatables', 'tableedit',
		'jsoneditor',
	));

	// bottom dialog
//	$this->append('bottom', $this->element('dataset_bottom_detail'));
?>

	<!-- コンテンツ ここから -->
		<h1 class="page-header hide"><?php echo $this->fetch('title'); ?></h1>

<?php echo $this->element('dataset_invalid', array('message' => $error_message));	// invalid メタデータ ?>
<!-- NIJC(20160713): added novalidate -->
		<form id="form_update" name="app-form" class="row" enctype="multipart/form-data" action="" method="post" novalidate>
			<div class='span8 col-md-8 columns eight large-8'>
				<h2><?php el('_json_editor'); ?></h2>
				<!-- <p><?php el('_editor_from_json_schema'); ?></p> -->

				<div id='editor'></div>

				<!-- 更新ボタン -->
				<p class="submit text-center">
					<input type="hidden" id="data_id" name="data_id" value="<?php echo $data_id; ?>">
					<input type="hidden" id="data_type" name="data_type" value="<?php echo $data_type; ?>">
					<button type="submit" class='btn btn-success' name="update" id="update"><?php el('_update_data'); ?></button>
				</p>
			</div>
			<div class='span4 col-md-4 columns four large-4'>

<?php echo $this->element('dataset_template_option');	// オプション ?>

			</div>
		</form><!-- /row -->

		<!-- NIJC -->
                <div class="panel panel-success">
                        <div class="panel-heading " data-toggle="collapse" data-target="#ppp1">
                                <i class="fa fa-file-text-o"></i>&nbsp;
                                <?php el('_json_schema'); ?>

                                <a class="pull-right white" data-toggle="collapse" href="#ppp1">
                                        <i class="fa fa-caret-square-o-down"></i></a>
                        </div>
                        <div class="panel-body <?php if (empty($data_reply)) echo 'collapse'; ?>" id="ppp1">
		<div class='row'>
			<div class='span12 col-md-12 columns twelve large-12'>
				<!-- <p><?php el('_form_changes_by_schema'); ?> -->

					<!-- スキーマを更新 -->
					<button class='btn btn-success' id='setschema'><?php el('_update_schema'); ?></button>
				<!-- </p> -->
 				<textarea id='schema' style='width: 100%; height: 450px; font-family: monospace;'
 						class='form-control' disabled></textarea>
			</div>
		</div><!-- /row -->

		<div class="hide">
			<textarea id="json_scheme" name="json_scheme" cols="100" rows="10"><?php echo h($json_scheme); ?></textarea>
			<textarea id="data_json" name="data_json" cols="100" rows="10"><?php echo h($data_json); ?></textarea>
		</div><!-- /hide -->

                        </div><!-- panelbody -->
                </div><!-- panel -->
	<!-- /コンテンツ ここまで -->
