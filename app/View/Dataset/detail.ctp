<?php
	// for Home
	$this->assign('title', l('_details_dataset'));	// title
	$this->Html->script('dataset', array('inline' => false));	// script

	// import css/script
	$this->set('import_script', array(
		'ui', 'cookie', 'migrate',
		'treegrid', 'datatables', 'tableedit',
	));

	// bottom dialog
//	$this->append('bottom', $this->element('dataset_bottom_detail'));
?>

	<!-- コンテンツ ここから -->
		<h1 class="page-header"><i class="fa fa-book"></i>&nbsp;<?php echo $this->fetch('title'); ?></h1>

		<?php if($data_item['dir_name'] != '') : ?>
		<!-- ダウンロードフォーム -->
<!-- NIJC(20160627)	
		<form action="<?php eh($data_item['download_url']); ?>" method="post"
			id="form_download" class="nav text-right">
-->
		<form action="/Dataset/download" method="post" id="form_download" class="nav text-right">
			<div class="form-inline">
				<input type="hidden" name="username" value="<?php eh($user['username']); ?>" />
				<input type="hidden" name="targets" value="<?php eh($data_item['download_id']); ?>" />
				<input type="hidden" name="download_url" value="<?php eh($data_item['download_url']); ?>" />
<!-- NIJC(20160630) -->
				<input type="hidden" name="language" value="<?php echo lang(); ?>" />
				<button type="submit" class="btn btn-success">
					<i class="fa fa-download"></i>&nbsp; <?php el('_download'); ?></button>
			</div>
		</form>
		<!-- /ダウンロードフォーム -->
		<?php endif; ?>

<?php echo $this->element('dataset_detail', array('data' => array($data_item)));	// データ詳細 ?>

<?php echo $this->element('dataset_invalid', array('message' => $error_message));	// invalid メタデータ ?>

<?php echo $this->element('dataset_tree', array('data' => array($data_item)));	// データツリー ?>

	<!-- /コンテンツ ここまで -->
