<?php
	// for Home
	$this->assign('title', l('_shared_dataset'));	// title
	$this->Html->script('dataset', array('inline' => false));	// script

	// import css/script
	$this->set('import_script', array(
		'ui', 'cookie', 'migrate',
		'treegrid', 'datatables', 'tableedit',
	));

	// bottom dialog
	$this->append('bottom', $this->element('dataset_bottom_linkdir'));
	$this->append('bottom', $this->element('dataset_bottom_public'));
	$this->append('bottom', $this->element('dataset_bottom_shared'));
	$this->append('bottom', $this->element('dataset_bottom_batch'));
	$this->append('bottom', $this->element('dataset_bottom_create'));
	$this->append('bottom', $this->element('dataset_bottom_edit'));
?>

	<!-- コンテンツ ここから -->
		<h1 class="page-header"><span class="glyphicon glyphicon-share"></span>&nbsp;<?php echo $this->fetch('title'); ?></h1>

<?php echo $this->element('dataset_list', array('data' => $data_shared));	// データ一覧 ?>

	<!-- /コンテンツ ここまで -->
