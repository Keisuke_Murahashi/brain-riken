<?php
/**
 * 共通関数
 *
 * App::import('Vendor', 'Util');
 *
 * $foobar = Util::foo('bar');
 */
class Util {
	// Icon class
	static public $icon_classes = array(
		'word'       => 'fa-file-word-o',
		'excel'      => 'fa-file-excel-o',
		'pdf'        => 'fa-file-pdf-o',
		'powerpoint' => 'fa-file-powerpoint-o',
		'text'       => 'fa-file-text-o',
		'default'    => 'fa-file-o',
	);

	// debug print
	public static function debug ($log, $logfile = '_debug.txt') {
		$fp = fopen(WWW_ROOT . $logfile, 'a');
		fwrite($fp, sprintf("[%s]\n%s\n\n", date('Y-m-d H:i:s'), print_r($log, true)));
		fclose($fp);
	}

	// debug print
	public static function icon_class ($type) {
		$icons = self::$icon_classes;
		return isset($icons[$type]) ? $icons[$type] : $icons['default'];
	}

	// パス内のディレクトリのみ取得 [検索開始Dir, フルパス/名前のみ, フルパスから除くパス]
	public static function scandir_dirs ($path, $want_path = false, $home_dir = '') {
		if (!isset($path)) return null;

		$list = scandir($path);
		$dirs = array();
		foreach ($list as $name) {
			if (strpos($name, '.') === 0) continue;	// ドットから始まるものはスキップ
			$filename = $path .'/'. $name;

			if (is_dir($filename)) {
				$tmp = Util::scandir_dirs($filename, $want_path, $home_dir);

				// 追加するパスをフォーマット
				$dirs[] = !$want_path ? $name
						: ( $home_dir ? str_replace($home_dir, '', $filename) : $filename );

				if ($tmp) {
					foreach ($tmp as $tmp2) array_push($dirs, $tmp2);
				}
			}
		}
		return $dirs;
	}

	// パス内のファイルのみ取得 (パス, パス付)
	public static function scandir_files ($path, $want_path = false) {
		if (!isset($path)) return null;

		$list = scandir($path);
		$files = array();
		foreach ($list as $name) {
			if (strpos($name, '.') === 0) continue;	// ドットから始まるものはスキップ
			$filename = $path .'/'. $name;

			if (is_file($filename)) {
				$files[] = $want_path ? $filename : $name;
			}
		}
		return $files;
	}

	// ディレクトリツリーを取得 (パス, パス付)
	public static function scandir_recursive ($path, $want_path = false) {
		if (!isset($path)) return null;
		if (!file_exists($path)) return null;

		$list = scandir($path);
		$files = array();
		foreach ($list as $name) {
			if (strpos($name, '.') === 0) continue;	// ドットから始まるものはスキップ
			$filename = $path .'/'. $name;

			if (is_dir($filename)) {
				if ($tmp = Util::scandir_recursive($filename, $want_path)) {
					$files[] = array($want_path ? $filename : $name, $tmp);
				} else {
					$files[] = $want_path ? $filename : $name;
				}
			} else {
				$files[] = $want_path ? $filename : $name;
			}
		}
		return $files;
	}
	// データセットの各申請の処理状況を取得
	public static function getAppInfo($data_asset){
		foreach ($data_asset as $i => $dt){
			$ap_data = $dt['AppData'];
			$sh_st1 = 0;
			$sh_st2 = 0;
			$sh_st3 = 0;
			$pb_st1 = 0;
			$pb_st2 = 0;
			$pb_st3 = 0;
			foreach ($ap_data as $j => $ap){
				// NIJC if ($ap['app_type'] == '共有') {
				if ($ap['app_type'] == 'Share') {
					switch ($ap['status']){
						// case '申請中':
						case 'Processing':
							$sh_st1++;
							break;
						// NIJC case '承認済':
						case 'Accepted':
							$sh_st2++;
							break;
						// NIJC case '棄却':
						case 'Rejected':
							$sh_st3++;
							break;
						default:
							// 処理
					}
				}elseif($ap['app_type'] == 'Release'){
					switch ($ap['status']){
						// case '申請中':
						case 'Processing':
							$pb_st1++;
							break;
						// NIJC case '承認済':
						case 'Accepted':
							$pb_st2++;
							break;
						// NIJC case '棄却':
						case 'Rejected':
							$pb_st3++;
							break;
						default:
							// 処理
					}
				}
			}
			$data_asset[$i]['sh_st1'] = $sh_st1;
			$data_asset[$i]['sh_st2'] = $sh_st2;
			$data_asset[$i]['sh_st3'] = $sh_st3;
			$data_asset[$i]['pb_st1'] = $pb_st1;
			$data_asset[$i]['pb_st2'] = $pb_st2;
			$data_asset[$i]['pb_st3'] = $pb_st3;
		}
		return $data_asset;
	}
	public static function getLabelColor($status){
		switch ($status){
			// case '申請中':
			case 'Processing':
				return 'warning';
			// case '承認済':
			case 'Accepted':
				return 'info';
			// NIJC case '棄却':
			case 'Rejected':
				return 'danger';
			case 'Withdrawn':
			// case 'Application':
			 	return 'default';
			// NIJC  case '共有':
			case 'Share':
				return 'success';
			case 'Release':
				return 'primary';
			default:
				// 処理
		}

	}

	// [Legacy] JSON データファイル名を取得
/* NIJC
	public static function getJsonData($type){
		switch ($type){
			case 'T1WI':
				return '01.data_t1wi_160306.json';
			case 'T2WI':
				return '02.data_t2wi_160306.json';
			case 'DWI':
				return '03.data_dwi_160306.json';
			case 'rs-fMRI':
				return '04.data_rs-fmri_160306.json';
			case 'Tracer':
				return '05.data_tracer_160306.json';
			case 'Document':
				return '06.data_document_160306.json';
			case 'Program_Tool':
				return '07.data_program_tool_160306.json';
			default:
				// 処理
		}

	}
	// [Legacy] JSON スキーマファイル名を取得
	public static function getJsonScheme($type){
		switch ($type){
			case 'T1WI':
				return '01.schema_t1wi_160221.json';
			case 'T2WI':
				return '02.schema_t2wi_160228.json';
			case 'DWI':
				return '03.schema_dwi_160228.json';
			case 'rs-fMRI':
				return '04.schema_rs-fmri_160228.json';
			case 'Tracer':
				return '05.schema_tracer_160412.json';
			case 'Document':
				return '06.schema_document_160228.json';
			case 'Program_Tool':
				return '07.schema_program_tool_160228.json';
			default:
				// 処理
		}
	}
*/
	// JSON スキーマから各 JSON フィールド名の連想配列に
	public static function getJsonKeys ($json) {
		$names = array();
		if (isset($json['properties']) and is_array($json['properties'])) {
			foreach ($json['properties'] as $name => $prop) {
				$type = isset($prop['type']) ? $prop['type'] : 'string';

				// オブジェクト時 (ここは通らないはず)
				if ($type == 'object') {
					$names[$name] = Util::getJsonKeys($prop);
				}
				// 配列時
				else if ($type == 'array' and isset($prop['items'])) {
					$names[$name] = Util::getJsonKeys($prop['items']);
				}
				// スカラー時
				else {
					$names[$name] = isset($prop['description']) ? $prop['description'] : '';
				}
			}
		} else {
			// 単純配列のとき
			return isset($prop['description']) ? $prop['description'] : '';
		}
		return $names;
	}

	// [Legacy] データタイプを元に、JSON スキーマから各 JSON フィールド名の連想配列に
	public static function getJsonKeysByType ($data_type) {
		// JSON スキーマファイルから各 JSON フィールド名の連想配列に
		// NIJC  return Util::getJsonKeysByFile( Util::getJsonScheme($data_type) );
		return Util::getJsonKeysByFile( DataType::getJsonScheme($data_type) );
	}

	// [Legacy] JSON スキーマファイルから各 JSON フィールド名の連想配列に
	public static function getJsonKeysByFile ($filename) {
		// JSON スキーマ
		$json_scheme = file_get_contents(JSON_SCHEME_DIR . $filename);

		// オブジェクト毎にパース
		// trueを付けると連想配列として分解して格納
		$json_raw = json_decode($json_scheme, true);

		// JSON スキーマから各 JSON フィールド名の連想配列に
		return Util::getJsonKeys($json_raw);
	}

// NIJC
        // Retrieve status label in lang table 
        public static function getStatusLabel($status){
                switch ($status){
                        // NIJC case '承認済':
                        // NIJC case '承認':
			case 'Accepted':
                                return '_approved';
                        // NIJC case '棄却':
			case 'Rejected':
                                return "_rejected";
                        // case '申請中':
			case 'Processing':
                                return '_applying';
                        // NIJC case '申請取り下げ':
			case 'Withdrawn':
                                return '_withdraw_application';
                        default:
                }
        }
        // Retrieve status label in lang table 
        public static function getAppType($status){
                switch ($status){
                        // case '共有':
			case 'Share':
                                return "_shared";
			case 'Release':
                                return '_publish';
			case 'WithrawalOfRelease':
                                return '_withdraw';
			case 'WithdrawalOfShare':
                                return '_withdraw';
                        default:
                }
        }
}
