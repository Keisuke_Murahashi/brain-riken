<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Database configuration class.
 *
 * You can specify multiple configurations for production, development and testing.
 *
 * datasource => The name of a supported datasource; valid options are as follows:
 *  Database/Mysql - MySQL 4 & 5,
 *  Database/Sqlite - SQLite (PHP5 only),
 *  Database/Postgres - PostgreSQL 7 and higher,
 *  Database/Sqlserver - Microsoft SQL Server 2005 and higher
 *
 * You can add custom database datasources (or override existing datasources) by adding the
 * appropriate file to app/Model/Datasource/Database. Datasources should be named 'MyDatasource.php',
 *
 *
 * persistent => true / false
 * Determines whether or not the database should use a persistent connection
 *
 * host =>
 * the host you connect to the database. To add a socket or port number, use 'port' => #
 *
 * prefix =>
 * Uses the given prefix for all the tables in this database. This setting can be overridden
 * on a per-table basis with the Model::$tablePrefix property.
 *
 * schema =>
 * For Postgres/Sqlserver specifies which schema you would like to use the tables in.
 * Postgres defaults to 'public'. For Sqlserver, it defaults to empty and use
 * the connected user's default schema (typically 'dbo').
 *
 * encoding =>
 * For MySQL, Postgres specifies the character encoding to use when connecting to the
 * database. Uses database default not specified.
 *
 * sslmode =>
 * For Postgres specifies whether to 'disable', 'allow', 'prefer', or 'require' SSL for the
 * connection. The default value is 'allow'.
 *
 * unix_socket =>
 * For MySQL to connect via socket specify the `unix_socket` parameter instead of `host` and `port`
 *
 * settings =>
 * Array of key/value pairs, on connection it executes SET statements for each pair
 * For MySQL : http://dev.mysql.com/doc/refman/5.6/en/set-statement.html
 * For Postgres : http://www.postgresql.org/docs/9.2/static/sql-set.html
 * For Sql Server : http://msdn.microsoft.com/en-us/library/ms190356.aspx
 *
 * flags =>
 * A key/value array of driver specific connection options.
 */
class DATABASE_CONFIG {

	public $default = array();
	public $default_mongo = array();

	// 公開用 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	public $production = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'database' => 'test2',
		'login' => 'test',
		'password' => '',
		'prefix' => '',
		'encoding' => 'utf8',
	);
	public $production_mongo = array(
		'datasource' => 'Mongodb.MongodbSource',
		'host' => 'localhost',
		'database' => 'test',
		'port' => '27017',
		'encoding' => 'utf8',
	);
	public $production_ldap = array(
		'datasource' => 'ActiveDirectory',
		'host' => '54.65.214.133',	// IP/host
		'database' => 'OU=Portal,DC=bminds,DC=blowish,DC=local',	// DN
		'login' => 'gniuser01@bminds.blowish.local',	// User
		'password' => 'gniuser-01',	// Password
		'port' => 389,
		'encoding' => 'utf8',
	);
	// 外部 API 連携 (代入変数: {$ (publish テーブルのフィールド名) } => そのフィールドの値)
	public $production_api = array(
		// [FESS]
		'FESS' => 'http://localhost:8080/fess',

		// [ダウンロード用API]
		'download_url_intragroup' =>	// 自グループ内共有時のデータセットダウンロード
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/download/intragroup',
		'download_url_intergroup' =>	// 外部グループ間共有時のデータセットダウンロード
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/download/intergroup',
		'download_url_any' =>	// 自グループ内共有と外部グループ間共有、共用のデータセットダウンロード
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/download/any',

		// [ディレクトリ移動用API]
		'move_dir_intragroup' =>	// 自グループ内共有時にデータセットのディレクトリを移動
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/move/intragroup',
		'move_dir_intergroup' =>	// 外部グループ間共有時にデータセットのディレクトリを移動
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/move/intergroup',
                'discharge_intragroup' =>       // NIJC(20160701): 未共有データセットの関連付け解除
                        'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/move/discharge_intragroup',
		'discharge_intergroup' =>	// 外部グループ間共有の状態から自グループ内共有の状態へ戻す - 20160708
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/move/discharge_intergroup',

		// [ユーザーのファイルアクセス権限設定用 API]
		'set_permission_intragroup' =>	// 自グループ内共有ディレクトリのファイルのアクセス権限を設定 - 20160708
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/set_permission/intragroup',
		'set_permission_intergroup' =>	// 外部グループ間共有ディレクトリのファイルのアクセス権限を設定 - 20160708
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/set_permission/intergroup',

		// [画像変換API]
		'public_image_convert' =>	// データセットの画像を画像変換処理を施して公開側のディスクに転送
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/imgcnv/run',

		// [公開用API]
		'public_getExperimentbyUser' =>	// User 名から公開先のExperiment を取得
			'https://hi-dev01.bminds.brain.riken.jp/api_product/groups/getExperimentbyUser/{$applicant}/',
		'public_addDataset' =>		// 指定したExperiment にdatasetを登録 （MongoDBに登録）
			'https://hi-dev01.bminds.brain.riken.jp/api_product/groups/addDataset/{$applicant}/{$experiment_id}/',
		'public_deletePublishPost' =>		// 取り下げの申請メタデータをMongoとWordPress用２つへデータから削除
			'https://hi-dev01.bminds.brain.riken.jp/api_product/groups/deletePublishPost/{$data_id}/{$applicant}/',
	);
	// /公開用 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	// 開発用 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	public $development = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'database' => 'test2',
		'login' => 'test',
		'password' => '',
		'prefix' => '',
		'encoding' => 'utf8',
	);
	public $development_mongo = array(
		'datasource' => 'Mongodb.MongodbSource',
		'host' => 'localhost',
		'database' => 'test',
		'port' => '27017',
		'encoding' => 'utf8',
	);
	public $development_ldap = array(
		'datasource' => 'ActiveDirectory',
		'host' => '54.65.214.133',	// IP/host
		'database' => 'OU=Portal,DC=bminds,DC=blowish,DC=local',	// DN
		'login' => 'gniuser01@bminds.blowish.local',	// User
		'password' => 'gniuser-01',	// Password
		'port' => 389,
		'encoding' => 'utf8',
	);
	// 外部 API 連携 (代入変数: {$ (publish テーブルのフィールド名) } => そのフィールドの値)
	public $development_api = array(
		// [FESS]
		'FESS' => 'http://localhost:8080/fess',

		// [ダウンロード用API]
		'download_url_intragroup' =>	// 自グループ内共有時のデータセットダウンロード
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/download/intragroup',
		'download_url_intergroup' =>	// 外部グループ間共有時のデータセットダウンロード
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/download/intergroup',
		'download_url_any' =>	// 自グループ内共有と外部グループ間共有、共用のデータセットダウンロード
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/download/any',

		// [ディレクトリ移動用API]
		'move_dir_intragroup' =>	// 自グループ内共有時にデータセットのディレクトリを移動
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/move/intragroup',
		'move_dir_intergroup' =>	// 外部グループ間共有時にデータセットのディレクトリを移動
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/move/intergroup',
		'discharge_intragroup' =>       // NIJC(20160701): 未共有データセットの関連付け解除
                        'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/move/discharge_intragroup',
		'discharge_intergroup' =>	// 外部グループ間共有の状態から自グループ内共有の状態へ戻す - 20160708
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/move/discharge_intergroup',

		// [ユーザーのファイルアクセス権限設定用 API]
		'set_permission_intragroup' =>	// 自グループ内共有ディレクトリのファイルのアクセス権限を設定 - 20160708
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/set_permission/intragroup',
		'set_permission_intergroup' =>	// 外部グループ間共有ディレクトリのファイルのアクセス権限を設定 - 20160708
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/set_permission/intergroup',

		// [画像変換API]
		'public_image_convert' =>	// データセットの画像を画像変換処理を施して公開側のディスクに転送
			'http://ssb.bminds.brain.riken.jp:8080/RpfFileManager/imgcnv/run',

		// [公開用API]
		'public_getExperimentbyUser' =>	// User 名から公開先のExperiment を取得
			'https://hi-dev01.bminds.brain.riken.jp/api_product/groups/getExperimentbyUser/{$applicant}/',
		'public_addDataset' =>		// 指定したExperiment にdatasetを登録 （MongoDBに登録）
			'https://hi-dev01.bminds.brain.riken.jp/api_product/groups/addDataset/{$applicant}/{$experiment_id}/',
		'public_deletePublishPost' =>		// 取り下げの申請メタデータをMongoとWordPress用２つへデータから削除
			'https://hi-dev01.bminds.brain.riken.jp/api_product/groups/deletePublishPost/{$data_id}/{$applicant}/',	// FESS
	);
	// /開発用 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	// DB 接続切り替え
	public function __construct() {
		$connection = Configure::read('database');

		if (!empty($this->{$connection})) {
			$this->default       = $this->{$connection};
			$this->default_mongo = $this->{$connection .'_mongo'};
			$this->default_ldap  = $this->{$connection .'_ldap'};
			$this->default_api   = $this->{$connection .'_api'};
		}
	}
}
