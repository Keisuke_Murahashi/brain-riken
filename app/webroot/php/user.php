<?php

// Basic example of PHP script to handle with jQuery-Tabledit plug-in.
// Note that is just an example. Should take precautions such as filtering the input data.

header('Content-Type: application/json');

$input = filter_input_array(INPUT_POST);

$mysqli = new mysqli('localhost', 'test', '', 'test2');

if (mysqli_connect_errno()) {
	echo json_encode(array('mysqli' => 'Failed to connect to MySQL: ' . mysqli_connect_error()));
	exit;
}
//
if ($input['action'] === 'edit') {
	$home_dir = mysqli_real_escape_string($mysqli, $input['home_dir']);
	//$home_dir = $input['home_dir'];
	$sql="UPDATE `user` SET detail='" . $input['detail'] . "', home_dir='" . $home_dir . "', upd_date=CURRENT_TIMESTAMP()" . " WHERE id='" . $input['user_name'] . "'";
	$mysqli->query($sql);
	$sql="UPDATE user_auth SET `group`='" . $input['group'] . "', role='" . $input['role'] . "', admin='" . $input['admin'] . "' WHERE username='" . $input['user_name'] . "'";
	$mysqli->query($sql);
} else if ($input['action'] === 'delete') {
	$mysqli->query("UPDATE test2 SET deleted='1' WHERE id='" . $input['id'] . "'");
} else if ($input['action'] === 'restore') {
	$mysqli->query("UPDATE test2 SET deleted='0' WHERE id='" . $input['id'] . "'");
}

mysqli_close($mysqli);

echo json_encode($input);
