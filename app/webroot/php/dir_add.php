<?php
	//各パラメータ取得(POST)
	$dir_name = $_POST['dir_name'];
	$data_type = $_POST['data_type'];
	$data_id = getID($data_type);
	$set_id = mb_strtolower($data_type)."_".$data_id;
	$username = $_SESSION['username'];

	$dbh = getDBH();
	$sql = "INSERT INTO dataset(dir_name, data_id, data_type, owner, upd_date) VALUES(?, ?, ?, ?, CURRENT_TIMESTAMP())";
	$stmt = $dbh->prepare($sql);
	$stmt->execute(array($dir_name, $set_id, $data_type, $username));

	//json
	// ファイルからJSONを読み込み（データタイプ毎にスキーマを用意）
	$json = file_get_contents("./lib/json");
	// 文字化けするかもしれないのでUTF-8に変換
	$json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
	// オブジェクト毎にパース
	// trueを付けると連想配列として分解して格納してくれます。
	$obj = json_decode($json,true);

	//JSONに各データを追加
	$array1 = array('dir_name' => $dir_name, 'data_type' => $data_type, 'data_id' => $set_id);
	$result = array_replace($obj, $array1);

	// Select
	$mongo = new MongoClient();
	$db = $mongo->test;
	$col = $db->test1;
	//$cursor = $col->find(array( 'aaa' => '111' ));
	$col->insert($result);