<?php

	//メタデータ登録
	// ファイルからJSONを読み込み
	$json = file_get_contents($_FILES["file_input"]["tmp_name"]);
	// 文字化けするかもしれないのでUTF-8に変換
	$json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
	// オブジェクト毎にパース
	// trueを付けると連想配列として分解して格納
	$obj = json_decode($json,true);

	//data_typeは必須項目（なければ中断）(ToDo)
	$data_type = $obj['data_type'];

	//ID発行(データタイプごと)
	$data_id = getID($data_type);
	$set_id = mb_strtolower($obj['data_type'])."_".$data_id;

	//ディレクトリパス所得
	if(isset($obj['dir_name'])){
		$dir_name = $obj['dir_name'];
	}else{
		$dir_name = "";
	}

	//json中のID置き換え
	$array1 = array('data_id' => $set_id);
	$result = array_replace($obj, $array1);

	// メタデータ登録(→Mongo)※要Validation(ToDo)
	$mongo = new MongoClient();
	$db = $mongo->test;
	$col = $db->test1;
	$col->insert($result);
	//$col->insert($obj);

	//データ登録(MySQL)
	$username = $_SESSION['username'];
	$dbh = getDBH();
	$sql = "INSERT INTO dataset(dir_name, data_id, data_type, owner, upd_date) VALUES(?, ?, ?, ?, CURRENT_TIMESTAMP())";
	$stmt = $dbh->prepare($sql);
	$stmt->execute(array($dir_name, $set_id, $data_type, $username));
	$dbh = null;