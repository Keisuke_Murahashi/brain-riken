<?php
	try{
		$dbh = getDBH();
		$username = $_SESSION['username'];
		$sql = "select app_id, data_id, app_type, status, creator, date, detail, share from app where creator = ? and status not in('承認済', '棄却')";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array($username));

		$no = 1;

		while($row = $stmt -> fetch(PDO::FETCH_ASSOC)) {
			$app_id = htmlspecialchars($row['app_id']);
			$data_id = htmlspecialchars($row['data_id']);
			$app_type = htmlspecialchars($row['app_type']);
			$status = htmlspecialchars($row['status']);
			$creator = htmlspecialchars($row['creator']);
			$date = htmlspecialchars($row['date']);
			$detail = htmlspecialchars($row['detail']);
			$share = htmlspecialchars($row['share']);
			echo "<tr><td><input type=\"radio\" name=\"rdo_app\" value=\"$app_id\"></td><td>$app_id</td><td>$data_id</td><td><span class=\"label label-default\">$app_type</span></td><td>$creator</td><td>$date</td><td><span class=\"label label-default\">$status</span></td><td><button class=\"btn btn-xs btn-success\" type=\"button\" onclick=\"location.href='DatasetDetail.php?data_id=$data_id'\">detail</button></td></tr>";
			$no = $no + 1;
		}

	}catch (PDOException $e){
		print('Error:'.$e->getMessage());
		die();
	}
	$dbh = null;