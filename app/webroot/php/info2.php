<?php
	try{
		$dbh = getDBH();
		$mess_type = "portal";
		$sql = "select a.mess_id, a.mess_type, a.title, a.date, a.author, a.category from message a where a.mess_type = ? LIMIT 5";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array($mess_type));
		while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
			//foreach ($dbh->query($sql) as $row) {
			$mess_id = htmlspecialchars($result['mess_id']);
			$mess_type = htmlspecialchars($result['mess_type']);
			$title = htmlspecialchars($result['title']);
			$date = htmlspecialchars($result['date']);
			$author = htmlspecialchars($result['author']);
			$category = htmlspecialchars($result['category']);
			switch ($category) {
				case "News":
					$fa = "fa-folder";
					break;
				case "Message":
					$fa = "fa-envelope-o";
					break;
			}
			//echo "<a href=\"MessageDetail.php?mess_id=$mess_id\" class=\"list-group-item\">$title";
			echo "<a href=\"#\" data-mess_id=\"$mess_id\" class=\"list-group-item aaa\">$title";
			echo "<small class=\"text-muted pull-right\"><i class=\"fa fa-clock-o\"></i>$date&nbsp; |&nbsp; <i class=\"fa fa-pencil\"></i>$author&nbsp; |&nbsp; <i class=\"fa $fa\"></i>$category</small>";
			echo "</a>";
		}
	}catch (PDOException $e){
		print('Error:'.$e->getMessage());
		die();
	}
	$dbh = null;