<?php
header('Content-Type: application/json');
require( "../lib/function.php" );

	$app_id = $_POST["input1"];
	$group = $_POST["input2"];

		try{
			$dbh = getDBH();

			$sql = "select a.app_id, a.data_id, a.app_type, a.status, a.creator, a.date, a.detail, a.share from app a where a.app_id = ?";
			$stmt = $dbh->prepare($sql);
			$stmt->execute(array($app_id));

			$no = 1;

			while($row = $stmt -> fetch(PDO::FETCH_ASSOC)) {
				$array_data[$no] = array(
						"app_id" => $row['app_id'],
						"data_id" => $row['data_id'],
						"app_type" => $row['app_type'],
						"status" => $row['status'],
						"creator" => $row['creator'],
						"date" => $row['date'],
						"detail" => $row['detail'],
						"share" => $row['share']
				);
				$no++;
				}
		}catch (PDOException $e){
			print('Error:'.$e->getMessage());
			die();
		}
		$dbh = null;

	echo json_encode($array_data);
?>