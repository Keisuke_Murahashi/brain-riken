<?php
header('Content-Type: application/json');
require( "../lib/function.php" );

	$user1 = $_POST["input1"];
	$group = $_POST["input2"];

		try{
			$dbh = getDBH();

			$sql = "select a.name, b.group from user a, user_auth b where a.name = b.username and b.admin = 'True'";
			$stmt = $dbh->prepare($sql);
			$stmt->execute();

			$no = 1;

			while($row = $stmt -> fetch(PDO::FETCH_ASSOC)) {
				$array_data[$no] = array(
						"name" => $row['name'],
						"group" => $row['group']
				);
				$no++;
				}
		}catch (PDOException $e){
			print('Error:'.$e->getMessage());
			die();
		}
		$dbh = null;

	echo json_encode($array_data);
?>