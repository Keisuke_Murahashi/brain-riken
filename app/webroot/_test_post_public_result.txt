[2016-06-22 08:12:42]
<?xml version="1.0" encoding="UTF-8"?>
<jnlp spec="1.7+"
		codebase="http://10.10.151.47:9090/silver-bullet/admin/">
	<information>
		<title>SilverBulletDownloadClient</title>
		<vendor>Skeed Co. Ltd.</vendor>
		<homepage href="http://www.skeed.co.jp" />
		<description kind="one-line">SkeedSilverBullet Client</description>
		<description kind="short">SkeedSilverBullet Client</description>
		<description kind="tooltip">SkeedSilverBullet Client</description>
		<icon kind="default" href="http://10.10.151.47:9090/silver-bullet/admin/public/images/skeed_transparent.gif" />
		<icon kind="splash" href="http://10.10.151.47:9090/silver-bullet/admin/public/images/skeed_silver_bullet_splash.jpg" />
		<offline-allowed/>
	</information>
	<security>
		<all-permissions />
	</security>
	<resources>
		<java version="1.7+" initial-heap-size="8M" max-heap-size="64M"/>
		<jar href="http://10.10.151.47:9090/silver-bullet/admin/public/jar/skeed-silver-bullet-simple-gui-client.jar"/>
		<property name="jnlp.packEnabled" value="true" />
	</resources>
	<application-desc main-class="com.skeedtech.skeed_silver_bullet.simple_gui_client.SimpleDownloaderMain">
		<argument>--host=10.10.151.47</argument>
		<argument>--user=murahashi</argument>
		<argument>--baseDir=/gpfs/home</argument>
		<argument>--downloadFiles=/gpfs/home/intragroup/1/dataset/0000000006</argument>
		<argument>--transactionId=16186bd0-b272-4c30-b701-9697f099c805</argument>
		<argument>--reportUrl=http://10.10.151.47:8080/RpfFileManager/report/</argument>
		<argument>--autoTerminate=true</argument>
		<argument>--language=ja</argument>
		<argument>--signature=7044e044a2c88bef124b83278327fcfcb41b840e16932194c09c6b44de09f7fd21e47181b308b47e935cfb35d24ed87f32d33dca99f5bac89be7046e98f0610f73be67ffce52f29dfdeef3ce186f2d2067ea98ae422202f7f6dfb8c085ba02f18db1bba750953343cbc5677cdf81e4dfab7bedd8827f86a70e1d72b0c04b0d1c</argument>
	</application-desc>
</jnlp>



[2016-06-22 08:18:19]
<?xml version="1.0" encoding="UTF-8"?>
<jnlp spec="1.7+"
		codebase="http://10.10.151.47:9090/silver-bullet/admin/">
	<information>
		<title>SilverBulletDownloadClient</title>
		<vendor>Skeed Co. Ltd.</vendor>
		<homepage href="http://www.skeed.co.jp" />
		<description kind="one-line">SkeedSilverBullet Client</description>
		<description kind="short">SkeedSilverBullet Client</description>
		<description kind="tooltip">SkeedSilverBullet Client</description>
		<icon kind="default" href="http://10.10.151.47:9090/silver-bullet/admin/public/images/skeed_transparent.gif" />
		<icon kind="splash" href="http://10.10.151.47:9090/silver-bullet/admin/public/images/skeed_silver_bullet_splash.jpg" />
		<offline-allowed/>
	</information>
	<security>
		<all-permissions />
	</security>
	<resources>
		<java version="1.7+" initial-heap-size="8M" max-heap-size="64M"/>
		<jar href="http://10.10.151.47:9090/silver-bullet/admin/public/jar/skeed-silver-bullet-simple-gui-client.jar"/>
		<property name="jnlp.packEnabled" value="true" />
	</resources>
	<application-desc main-class="com.skeedtech.skeed_silver_bullet.simple_gui_client.SimpleDownloaderMain">
		<argument>--host=10.10.151.47</argument>
		<argument>--user=murahashi</argument>
		<argument>--baseDir=/gpfs/home</argument>
		<argument>--downloadFiles=/gpfs/home/intragroup/1/dataset/0000000006</argument>
		<argument>--transactionId=e33f44da-5d1c-4279-bb57-f1659b1f5fad</argument>
		<argument>--reportUrl=http://10.10.151.47:8080/RpfFileManager/report/</argument>
		<argument>--autoTerminate=true</argument>
		<argument>--language=ja</argument>
		<argument>--signature=2e41fd18c8a82eb213a67a5a494cb8665c8b2b582c6dfb81d8e65b5c9b0122224692c4d4cd5b1b20782bf045bf72ca05d0829bcb5192bd3cce90a1ec74635ddfab31c6a9fb220163fd755ad4cf9b98efe547e14764175ff8ac78ee7bfd1f9ee9e088312b1719471bd3d06c4c55cab79739bc7e9dbd2e18533b5910d079262465</argument>
	</application-desc>
</jnlp>



