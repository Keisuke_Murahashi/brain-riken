// For Search

// 言語データ読み込み
if (window.Lang) Lang();

$(function(){
	if (!$.fn.DataTable) return;

	$('body.Search .table').css('width', '100%').dataTable({
		"language": {
	        "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
	    },
		bLengthChange:false
	});
});

// 検索キーワードの文字数制限 - 20160802
$(function() {
	$('input[name="keyword"], input.condition_value').on('change', checkMaxLength)
		.closest('form').on('submit', function (e) {
			var input_keyword = $('input[name="keyword"]', this).get(0);
			if (input_keyword) return checkMaxLength.call(input_keyword, e);
		});

	function checkMaxLength (e) {
		var $this = $(this);
		var text = $this.val();
		var max_length = parseInt( $this.data('maxlength') ) || 50;
		var in_table = $this.closest('tr').length;

		// エラーメッセージ要素を取得
		var $error = (!in_table) ?  $this.parent().next('.error') :
									$this.closest('tr').next('.error').find('td');

		// 文字数チェック (制限文字数内であれば true で返す)
		if (text.length <= max_length) {
			$error.slideUp();
			return true;
		}

		// エラーメッセージ要素がなければ作る
		if ($error.length == 0) {
			if (!in_table) {
				$error = $('<div>').addClass('error alert alert-warning');
				$this.parent().after( $error );
			} else {
				$error_tr = $('<tr>').addClass('error');
				$error = $('<td colspan="4">').addClass('error alert alert-warning');
				$this.closest('tr').after( $error_tr.append( $error ) );
			}
		}
		// エラーメッセージ追加
		$error.hide().text( l('error_keyword_exceeds_max_length') ).slideDown();
			// (検索キーワードは50文字以内で入力してください)

		return false;
	}
});

// 検索条件
$(function() {
	// 初期化とイベント設定
	$('#data_type').each(function () {
		resetCondSelect( $(this).val() || 'T1WI' );
	}).on('change', function () {
		resetCondSelect( $(this).val() );
	});

	function resetCondSelect (s) {
		$('select.condition_field').each(function () {
			$(this).data('cache', $(this).val());
		}).empty().hide();

		$.ajax({
			type: "POST",
			url: "/DataType/ajax_select.json",		// get JSON on PHP
			data: { type: s },
			dataType: 'json',
			cache : false,
			async: true,
			success: function(data){
				// New Controller - - - - - - - - - - - - - - - -
				$('select.condition_field').each(function () {
					var $this = $(this);
					var cache = $this.data('cache');
					$this.empty();	// clear

					// <option> 既定
					$this.append($('<option></option>')
						.attr('cond', 'Field').val('').text( l('_select_field_at_search') ));

					// <option> 作成
					for(var i in data){
						// 3階層まで （再帰しない）
						var row = data[i];
						if (row == 'data_type') continue;	// data_type は重複 .. 20160320

						$('<option></option>').appendTo($this)
							.attr('cond', row).val(row).text(row)
							.prop('selected', row == cache);
					}
				}).show('fast');

				// 検索条件追加ボタンの幅固定（次回用）
				var $td_btn = $('form.search_condition table td.button');
					$td_btn.width( $td_btn.width() );
			},
			error: function(){
				//alert("失敗しました");
			}
		});
	}

	// 検索条件の追加 (スライドしながら表示)
	$('form.search_condition tr .add_condition').on('click', function () {
		var $tr = $(this).closest('tr');
		var $clone = $tr.clone(true);
			$clone.find('.wrapper').hide();
			$clone.find('.condition_value').val('')
			$tr.after($clone);
			$clone.find('.wrapper').slideDown(350);
	});
	// 検索条件のクリア (スライドしながら非表示)
	$('form.search_condition tr .clear_condition').on('click', function () {
		var $tr = $(this).closest('tr');

		// クリアしたらなくなるときは１つ残したい
		if ($tr.siblings().length == 0) {
			$tr.find('.add_condition').triggerHandler('click');
		}
		// 行を削除 (スライド用要素があればスライドしながら削除)
		var $wrapper = $tr.find('.wrapper');
		if ($wrapper.length > 0) {
			$wrapper.slideUp(350, function () {
				var $tr = $(this).closest('tr');
				if (!$tr.data('removed')) $tr.data('removed', true).remove();
			});
		} else {
			$tr.data('removed', true).remove();
		}
	});
});