// For Message

// 言語データ読み込み
if (window.Lang) Lang();

$(function() {
    $('input[name="title"]').autocomplete({
        source: '/Dictionary/ajax.json'
    });
});

$(function() {
	$('#table_id').dataTable({
		"language": {
            "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
        },
		bLengthChange:false,
		"order": [[ 4, 'desc' ]]
	});
});

$(function(){
	$('.table_id').css('width', '100%').dataTable({
		"language": {
            "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
        },
		bLengthChange:false
	});
});

$(function() {
	// メッセージタイプでターゲットユーザー選択を表示/非表示
	$('#mess_type').on('change', function () {
		if ($(this).val() == 'user') {
			$('#target-box').slideDown('normal');
		} else {
			$('#target-box').slideUp('fast');
		}
	}).triggerHandler('change');

	// ターゲットユーザーのグループ切り替え
	$('#target-group select').on('change', function () {
		$('.target-users').hide();	// all hide

		$('option:selected', this).each(function () {
//			alert($(this).val())
			var group = $(this).val() || 'all';
			$('#target-group-'+ group).show('fast');
		});
	});

	// 各グループのターゲットユーザーの選択を同期
	$('.target-users select').not('#target-group-all select').on('change', function () {
		// 変更した select option の状態を連想配列に
		var ops = {};
		$('option', this).each(function () {
			ops[ $(this).val() ] = this.selected;
		});
		// 全ユーザーの select option に同期
		$('#target-group-all option').each(function () {
			var user = $(this).val();
			if (user in ops) this.selected = ops[user];
		});
	});

	// 全ターゲットユーザーの選択を同期
	$('#target-group-all select').on('change', function () {
		// 変更した select option の状態を連想配列に
		var ops = {};
		$('option', this).each(function () {
			ops[ $(this).val() ] = this.selected;
		});
		// 全ユーザーの select option に同期
		$('.target-users select').not(this).find('option').each(function () {
			var user = $(this).val();
			if (user in ops) this.selected = ops[user];
		});
	});

	// ターゲットユーザーを右クリックしたら選択を外す
	$('#target-box select option').on('contextmenu', function () {
		this.selected = false;
		$(this).parent().triggerHandler('change');
		return false;
	});
});

// フォーム送信前の確認
$(function () {
	$('#form1').on('submit', function () {
		var is_ok = true;
		$('.alert', this).hide();

		// メッセージタイプが「個人あて」で、
		// かつ「送信先ユーザー」が指定されていない時
		var $mess_type = $('#mess_type', this);
		if ($mess_type.val() == 'user') {
			// 選択されている数を数える
			var count = 0;
			var $target = $('select[name="target[]"]', this);
			$target.each(function () {
				var values = $(this).val();
				if (values) count += (typeof values == 'object') ? values.length : 1;
			});

			// ひとつも選択されていなければエラー
			if (count == 0) {
				is_ok = false;

				var $target_box = $('#target-box');
				if ($target_box.find('.alert').length == 0) {
					$('<div class="alert alert-warning clear">'+
						l('_error_select_target_user') +'</div>')
						.hide().appendTo($target_box).slideDown();
				} else {
					$target_box.find('.alert').slideDown();
				}
			}
		}
		return is_ok;
	});
});
