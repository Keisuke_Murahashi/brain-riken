// 翻訳データを取得
if (window.Lang) Lang();

// downloader
$(function(){
	// ダウンロード用 API を実行 (ダウンロードキュー)
	$(document).on('click', 'button.download, #sidebar_download .cancel', function (e) {
		// ダウンロード対象
		var target = $(this).data('targets') || $(this).val() || '';

		// ダウンロード予約
		download_queue.call(this, target);

		return false;
	});
/*
	// ダウンロード用 API を実行 (１つ１つダウンロード)
	$(document).on('click', 'button.download', function (e) {
		var $this = $(this);
		var $form = $('#form_download');
		var $targets = $form.find('input[name="targets"]');
		if ($form.length == 0 || $targets.length == 0 || !$this.data('action')) return;

		// パラメータを変更
		$targets.val( $this.data('targets') || $this.val() );

		// フォームの送信先を変更して送信
		$form.attr('action', $this.data('action')).submit();

		return false;
	});
*/

	// ダウンロード予約中に詳細へのリンクを辿ったときは別のウィンドウを開く
	$(document).on('click', '#p1 table a', function (e) {
		var is_downloadable = $('#sidebar_download li').length > 0;
		$(this).attr('target', is_downloadable ? '_blank' : '');
	});
/*
	// ダウンロード予約中に画面遷移しようとしたときに確認メッセージを表示
	$(window).on('beforeunload', function (e) {
		var is_downloadable = $('#sidebar_download li').length > 0;
		if (is_downloadable) return l('_cancel_reservation_for_download');
			// このページから移動すると現在のダウンロード予約がキャンセルされます。
	});
*/
	// ダウンロード予約をすべてキャンセル
	$('#sidebar_download').on('reset', function () {
		var $form = $(this);	// <form>
		$form.find('input[name="targets"]').val('');	// ダウンロードID
		$form.find('ul').slideUp(200, function () { $(this).empty(); });	// ダウンロードキューリスト
		$('button.download').removeClass('btn-success');	// ダウンロードボタン
	});

	// ダウンロード予約
	function download_queue (target) {
		var $this = $(this);
		var $form = $('#sidebar_download').fadeIn();	// <form>
		var $targets = $form.find('input[name="targets"]');	// ダウンロードID
		var $list = $form.find('ul').hide().empty();	// ダウンロードキューリスト
		var $buttons = $('button.download')		// 同じIDのボタン
						.filter('[data-targets="'+  target.replace(/^:/, '') +'"],'+
								'[data-targets=":'+ target.replace(/^:/, '') +'"]');
		if ($form.length == 0 || $targets.length == 0) return;

		// ":" が無い場合は付加
		if (target.indexOf(':') == -1) target = ':'+ target;

		// 既にダウンロードキューにあるかどうか
		var is_queue = false;

		// 重複パラメータを削除
		var old_targets = $targets.val() ? $targets.val().split("\t") : [];
		var new_targets = [];
		for (var i = 0, I = old_targets.length; i < I; i++) {
			if (target != old_targets[i]) new_targets.push( old_targets[i] );
			else is_queue = true;
		}

		// 既にダウンロードキューにあるかどうか
		if (is_queue) {
			if ($this.hasClass('btn')) $this.removeClass('btn-success');
			$buttons.not(this).removeClass('btn-success');	// 同じIDのボタン
		} else {
			new_targets.push( target );
			if ($this.hasClass('btn')) $this.addClass('btn-success');
			$buttons.not(this).addClass('btn-success');	// 同じIDのボタン
		}
		// パラメータを変更
		$targets.val( new_targets.join("\t") );

		// リストを再構築
		for (var i = 0, I = new_targets.length; i < I; i++) {
			var t = new_targets[i].split(':');
			$list.append('<li><i class="fa fa-folder'+ (t[0] ? '-o' : '') +'"></i> '+ t[1] +
							' <i class="cancel fa fa-times-circle"'+
								' data-targets="'+ new_targets[i] +'"'+
								' title="'+ l('_cancel') +'"></i></li>');
		}
		$list.slideDown(300);

		// フォームを更新
		$form.find('button:submit').hide().fadeIn(300);
	}

});

// ...
$(function(){
	$('#file_input').change(function() {
		var path = $(this).val();
			path = path.replace(/([\\\/])fakepath[\\\/]/g, '$1 ... $1');
		$('#dummy_file').val( path );
		//$('#dummy_file').val(this.files[0].name);
	});
});

$(function(){
	$('.table_id').css('width', '100%').dataTable({
		"language": {
            "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
        },
		"order": [[ 4, 'desc' ]],	// update-date 順
		bLengthChange:false
	});
});

$(function(){
	$('share-form').on('submit', function(e){
		e.preventDefault();
		$.ajax({
			url: "/MyDataset/",
			type: "POST",
			data: $(this).serialize(),
//			dataType: 'json',
			cache : false,
			async: true,
			success: function(data){
				// NIJC(20160701)
				//alert("Successfully submitted.")
			}
		});
		return false;
	});
});

$(function () {
	$('#sharebtn,#publishbtn').on('click', function(event){
		var	data_id = $("input[name='rdo1']:checked").val();
		var btntype = $(this).attr("id");
		if (!data_id){
			//alert("dataset_idが選択されていません。")
			alert(l('_select_data_id'))
			return false;
		}

		// check JSON data validation
		$.ajax({
			url: "/Dataset/ajax_validation.json",	// get JSON on PHP
			data: { id : data_id },
			dataType: 'json',
			cache : false
		}).done(function (res) {
			// is_error
			if (res && (res.code != 200 || res.status == 'error') && res.message) {
				open_modal_invalid(data_id, res.message);
			} else if (res && (res.code == 200 || res.status == 'success')) {
				open_modal_form(data_id, btntype);
			} else {
				// NIJC(20160701)
				//alert('通信に失敗しました\n\n'+ res);
				alert(l("_failed_to_communicate_to_server"));
			}
		}).fail(function(XMLHttpRequest, textStatus, errorThrown){
			// NIJC(20160701)
			//alert('通信に失敗しました\n\nError : ' + errorThrown);
			alert(l("_failed_to_communicate_to_server"));
		});
		return false;
	});

	// 承認者を取得しながら申請フォームをモーダルで開く
	function open_modal_form (data_id, btntype) {
		$.ajax({
			type: "POST",
			url: "/UserManagement/ajax.json",	// get JSON on PHP
			data: {},
			dataType: 'json',
			cache : false,
/* [done] に
			async: true,
			success: function(data){
				search_msg_data = data;
				search_html = search_json(search_msg_data, data_id, btntype);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				// NIJC(20160701)
				//alert("失敗しました");
				//alert('Error : ' + errorThrown);
				alert(l("_failed_to_communicate_to_server"));
			},
*/
			'':''
		}).done(function(data) {
			search_msg_data = data;
			search_html = search_json(search_msg_data, data_id, btntype);

			//処理
			if (btntype == "sharebtn"){
				$("#p-lg3").modal("show");
			}else if (btntype == "publishbtn"){
				$("#p-lg2").modal("show");
			}else if (btntype == "testbtn"){
				$("#p-lg6").modal("show");
			}
		}).fail(function(XMLHttpRequest, textStatus, errorThrown){
			// NIJC(20160701)			
			//alert('通信に失敗しました\n\nError : ' + errorThrown);
			alert(l("_failed_to_communicate_to_server"));
		});
	}

	function search_json(search_msg_data, data_id, btntype){

		var search_html = "";
		for (var i = 0, I = search_msg_data['authorizer'].length; i < I; i++) {
			var item = search_msg_data['authorizer'][i];
			search_html += '<option value="' + item['username'] + '">'+
								item['name'] + ' (' + item['group_name'] + ') ' + '[' + l(item['role']) + ']' + '</option>';
		}
		var search_html2 = "";
		for (var i = 0, I = search_msg_data['group'].length; i < I; i++) {
			var item = search_msg_data['group'][i];
			search_html2 += '<option value="' + item['name'] + '">'+ item['name'] +  '</option>';
		}
		if (btntype == "sharebtn"){
			var modal = $("#p-lg3");  //モーダルを取得
		}else{
			var modal = $("#p-lg2");  //モーダルを取得
		}

		search_html = '<select class="form-control col-md-12" name="select" id="auth_list">' + search_html + '</select>';
		search_html2 = '<select multiple class="form-control col-md-12" name="group[]" id="grp_list" required>' + search_html2 + '</select>';
		modal.find('.list-group-item').html(l('_data_id') +': &nbsp; ' + data_id); //モーダルのタイトルに値を表示
		modal.find('.modal-body input#data_id').val(data_id); //inputタグにも表示
		modal.find('.modal-body #search_data').html(search_html); //inputタグにも表示
		modal.find('.modal-body #search_data2').html(search_html2); //inputタグにも表示

		return search_html;
	}

	// バリデーションエラーが出た時のモーダルを開く
	function open_modal_invalid (data_id, errors) {
		var $form = $("#modal-invalid");
		var $box = $form.find('.message').empty();

		// Data-ID を付けた URL をセット
		var base_href = $form.find('form').attr('action').replace(/\/+$/, '');
		$form.find('a.submit').attr('href', base_href +'/'+ data_id);

		// メッセ―ジが文字列のとき
		if (typeof errors != 'object' || !errors.length) {
			$('<p></p>').text(errors).appendTo($box);
		} else {
			var message_list = [];
			for (var i = 0, I = errors.length; i < I; i++) {
				var error = errors[i];
				var term = error.property +' ('+ error.constraint +')';
				message_list.push( $('<dt></dt>').text( term ) );
				message_list.push( $('<dd></dd>').text( error.message ) );
			}
			$('<dl></dl>').append(message_list).appendTo($box);
		}
		$form.modal("show");
	}
});

$(function () {
	$('#upddirbtn').on('click', function(event){
	//$('#upddirbtn').click(function(){
	var	data_id = $("input[name='rdo1']:checked").val();
	if (data_id === undefined){
		//alert("dataset_idが選択されていません。")
		alert(l('_select_data_id'))
		//event.preventDefault();
		return;
	}

	$.ajax({

		type: "POST",
		url: "/UserManagement/ajax_dirs.json",	// get JSON on PHP
		data: { input1: data_id,input2: data_id },
		dataType: 'json',
		cache : false,
		async: true,
		success: function(data){
			search_msg_data = data;
			search_html = search_json(search_msg_data, data_id);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){
			// NIJC(20160701)
			//alert("失敗しました");
			//alert('Error : ' + errorThrown);
			alert(l("_failed_to_communicate_to_server"));
		}

		}).done(function(data) {
			//処理
			$("#p-lg1").modal("show");

	   });
	});

	function search_json(search_msg_data, data_id){

		var search_html2 = "";
		if (search_msg_data == null){
			// NIJC(20160701)
			//alert("対象のディレクトリが見つかりません。")
			alert(l("_failed_to_search_target_dir"));
			event.preventDefault();
			//return;
		}
		for (var i = 0, I = search_msg_data.length; i < I; i++) {
			//var row = search_msg_data[i].replace(/^[\\\/]+/, '');
			var row = search_msg_data[i];
			//search_html2 += '<li class="list-group-item"><input type="radio" name="dir" value="' + row + '">&nbsp; ' + row + '</li>';
			search_html2 += '<option value=' + row + '>&nbsp; ' + row + '</option>';
		}
		//search_html2 = '<ul class="list-group">' + search_html2 + '</ul>';
		search_html2 = '<select class="form-control" name="dir" id="dir" size=10>' + search_html2 + '</select>';
		var modal = $("#p-lg1");  //モーダルを取得
		modal.find('.list-group-item').html('data_id: &nbsp; ' + data_id); //モーダルのタイトルに値を表示
		modal.find('.modal-body input#data_id').val(data_id); //inputタグにも表示
		modal.find('.modal-body #dir_list').html(search_html2) //inputタグにも表示

		return search_html2;
	}
});

$(function() {
	  // いずれかのリストにデータが無い場合は、submitボタンを無効に
	  $('#p-lg1').on('show.bs.modal', function(event) {
		  if ($("#dir").children().length < 1) {
			  $(this).find('.modal-body #upd_dir').attr('disabled','disabled');
		  }
	  });
	  $('#p-lg2').on('show.bs.modal', function(event) {
		  if ($("#exp_list").children().length < 1 || $("#auth_list").children().length < 1) {
			  $(this).find('.modal-body #publish').attr('disabled','disabled');
		  }
	  });
	  $('#p-lg3').on('show.bs.modal', function(event) {
		  if ($("#grp_list").children().length < 1 || $("#auth_list").children().length < 1) {
			  $(this).find('.modal-body #share').attr('disabled','disabled');
		  }
	  });
	});

// NIJC(20160701)
$(function() {
	$("#unlinkdirbtn").on("click", function(event) {
		var data_id = $("input[name='rdo1']:checked").val();
		if (typeof data_id === "undefined") {
			alert(l("_select_data_id"));	
		} else {
			var selected_row = $("input[name='rdo1']:checked").parent().parent();
			var dir_name = $("input[name='rdo1_dirname']", selected_row).val();
			if (dir_name) {
				var modal_unlink = $("#modal-unlink");
				modal_unlink.find(".list-group-item").html("data_id: &nbsp; " + data_id);
				modal_unlink.find(".modal-body input#data_id").val(data_id);
				modal_unlink.modal("show");
			}
		}
	});
});

// (共有承認取り下げなどで) データがロックされている場合の動作 - 20160708
$(function () {
	// 対象のボタンのID (共有, 公開, ディレクトリ関連付け)
	var buttons = ['#sharebtn', '#publishbtn', '#upddirbtn'];

	// 既定の disabled をキャッシュ
	for (var i = 0; i < buttons.length; i++) {
		var $btn = $( buttons[i] );
		$btn.data('default_disabled', $btn.prop('disabled'));
	}

	$("input[name='rdo1']").on('change', function () {
		// ロックされているか (簡易的処理)
		var locked = $(this).data('locked') || $(this).attr('data-locked');
			locked = parseInt(locked);

		// ボタンの有効・無効 (既定で disabled のときは変更しない)
		for (var i = 0; i < buttons.length; i++) {
			var $btn = $( buttons[i] );
			if ($btn.data('default_disabled') == false) {
				$btn.prop('disabled', locked);
			}
		}
	});
});
