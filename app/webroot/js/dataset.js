/* Dataset/index */

$(function(){
	$('#file_input').change(function() {
		$('#dummy_file').val($(this).val());
	});
});


$(function(){
	$('.tree').treegrid({
		expanderExpandedClass: 'glyphicon glyphicon-minus',
		expanderCollapsedClass: 'glyphicon glyphicon-plus',
		'initialState': 'collapsed',
		'saveState': true
	});
});


$(function(){
	$('#table1').dataTable({
		"language": {
            "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
        },
		bLengthChange:false
	});
});

/* Dataset/detail */

$(function(){
	$('#example1').dataTable({
		"language": {
            "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
        },
		columnDefs: [
             { type: 'html-num-fmt', targets: [0] } // targetsにはCurrencyでソートしたい列のインデックスを渡します
		],
		"lengthMenu": [50,100]    // NIJC(20160622)
	}
	);
});


$(function() {
	$(".sample").autocomplete({
		source: "/Autocomplete.php"
	});
});

$(function() {
	$('#p-lg5').on('show.bs.modal', function (event) {
		var button = j(event.relatedTarget) //モーダルを呼び出すときに使われたボタンを取得
		var recipient = button.data('whatever') //data-whatever の値を取得

		//Ajaxの処理はここに
		var modal = j(this)  //モーダルを取得
		modal.find('.modal-title').text('New message to ' + recipient) //モーダルのタイトルに値を表示
		modal.find('.modal-body input#recipient-name').val(recipient) //inputタグにも表示
	});
});
