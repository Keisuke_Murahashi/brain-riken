$(function(){
	$('#file_input').change(function() {
		$('#dummy_file').val($(this).val());
	});
});

$(function(){
	$('.tree').treegrid({
		expanderExpandedClass: 'glyphicon glyphicon-minus',
		expanderCollapsedClass: 'glyphicon glyphicon-plus',
		'initialState': 'collapsed',
		'saveState': true
	});
});

$(function(){
	// option list of group
	var option_groups = [];
	$('#option_group option').each(function () {
		option_groups.push('"'+ this.value +'": "'+ this.text +'"');
	});
	option_groups = option_groups.join(', ');

	// option list of group
	var option_roles = [];
	$('#option_roles option').each(function () {
		option_roles.push('"'+ this.value +'": "'+ this.text +'"');
	});
	option_roles = option_roles.join(', ');
/*
	var user_id = document.getElementById('php-user_id').getAttribute('data-val');
	var name = document.getElementById('php-user_name').getAttribute('data-val');
	var detail = document.getElementById('php-detail').getAttribute('data-val');
	var home_dir = document.getElementById('php-home_dir').getAttribute('data-val');
	var group = document.getElementById('php-group_name').getAttribute('data-val');
	var role = document.getElementById('php-role').getAttribute('data-val');
*/
	var $user_table = $('#table1.editable');
	if ($user_table.length) $user_table.Tabledit({
//		url: '/php/user.php',
		url: '/UserManagement/ajax_edit_user.json',	// get JSON on PHP
		deleteButton: false,
		saveButton: true,
		autoFocus: false,
		buttons: {
			edit: {
				class: 'btn btn-sm btn-info',
				html: '<span class="glyphicon glyphicon-pencil"></span> &nbsp EDIT',
				action: 'edit'
			}
		},
		columns: {
			identifier: [1, "username"],
			editable: $user_table.hasClass('root') ? [
					[2, "name"],[3, "detail"],[4, "home_dir"]
					,[5, "group", '{'+ option_groups +'}']
					//, [7, 'admin', '{"True": "True", "False": "False"}']
					, [6, "role", '{'+ option_roles +'}']
					//, [9, 'status', '{"1": "Active", "2": "Invalid", "3": "Deleted"}']
			] : [
					[2, "name"],[3, "detail"],[4, "home_dir"]
					//,[5, "group", '{'+ option_groups +'}']
					//, [7, 'admin', '{"True": "True", "False": "False"}']
					, [6, "role", '{'+ option_roles +'}']
					//, [9, 'status', '{"1": "Active", "2": "Invalid", "3": "Deleted"}']
			]
		},
		onDraw: function() {
		},
		onSuccess: function(data, textStatus, jqXHR) {
			// NIJC console.log('success:', data);
			if (data && data.error) alert(data.error);
		},
		onFail: function(jqXHR, textStatus, errorThrown) {
			console.log('error:', textStatus, errorThrown.message);
			alert(textStatus +' : '+ errorThrown.message);
		},
		onAlways: function() {
		},
		onAjax: function(action, serialize) {
			console.log(serialize);
		}

	});
});


$(function(){
	//return;		// （一時的？）無効

	var $group_table = $('#table2.editable');
	if ($group_table.length) $group_table.Tabledit({
//		url: '/php/group.php',
		bLengthChange:false,
		url: '/UserManagement/ajax_edit_group.json',	// get JSON on PHP
		deleteButton: false,
		saveButton: true,
		autoFocus: false,
		editButton: true,
		// NIJC  hideIdentifier: true,
		buttons: {
			edit: {
				class: 'btn btn-sm btn-info',
				html: '<span class="glyphicon glyphicon-pencil"></span> &nbsp EDIT',
				action: 'edit'
			},
			delete: {
				class: 'btn btn-sm btn-danger',
				html: '<span class="glyphicon glyphicon-trash"></span>',
				action: 'delete'
			}
		},
		columns: {
			// NIJC
			identifier: [0, 'group_id'],
			editable: [[2, 'detail']]
		},
		onDraw: function() {
		},
		onSuccess: function(data, textStatus, jqXHR) {
		},
		onFail: function(jqXHR, textStatus, errorThrown) {
			alert(textStatus);
			alert(errorThrown);
		},
		onAlways: function() {
		},
		onAjax: function(action, serialize) {
		}
	});
});


// NIJC(20160701): maybe unused...
//$(function() {
//	$(".multiSelectSample1").multiselect({
//		selectedList: 100,
//		checkAllText: "全選択",
//		uncheckAllText: "全選択解除",
//		noneSelectedText: "未選択です",
//		selectedText: "# 個選択"
//	});
//});


$(function(){
	$('#table1').dataTable({
		"language": {
            "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
        },
		bLengthChange:false
	});
});

$(function(){
	$('#table2').dataTable({
		"language": {
            "url": $('body').hasClass('ja') ? "/js/jquery.dataTables.ja.json" :
                                              "/js/jquery.dataTables.en.json"
        },
		bLengthChange:false
	});
});

$(function() {
	$(".sample").autocomplete({
		source: "/Autocomplete.php"
	});
});


function changetest(obj){
	for (var i=0, len=obj.options.length; i<len; i++) {
		opt = obj.options[i];

		// check if selected
		if ( opt.selected ) {
			//alert(opt.value + opt.text + obj.dataset.user);
			param1 = obj.dataset.user;
			param2 = opt.value;
			//$.post("post.php", {input1:param1, input2:param2}, function(json){alert("パラメータを2つPOSTしました");});
			$.ajax({
				type: "POST",
				url: "/post.php",
				data: {
					input1: param1,
					input2: param2
				},
				cache : false,
				async: true,
				success: function(json){

					//alert("パラメータを2つPOSTしました");

				},
				error: function(){
					// NIJC(20160701): maybe test code...
					//alert("失敗しました");
					alert("failed");
				}
			});
		}
	}
}
