<?php
App::uses('AppMongoModel', 'Model');

class MongoData extends AppMongoModel {
	var $useTable = 'test1';	// table name
	public $primaryKey = '_id';	// primary key (ID)
}
