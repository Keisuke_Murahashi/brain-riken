<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
	public $findMethods = array('item' => true, 'items' => true);	// define custum "find"

	// find("all") などの結果を ID をキーにした連想配列にする (override)
	// .. find('all', array('as' => $id_name (or true))) の形式で指定する
	public function find($type = 'first', $query = array()) {
		$results = parent::find($type, $query);		// override find

		// as 指定がなければ返す
		if (empty($query['as'])) return $results;

		// 取得形式が "first", "item", "list", "neighbors" のときは返す
		if ($type == 'first' or $type == 'item'
		or  $type == 'list'  or $type == 'neighbors') return $results;

		// データのキーが数値以外のときは返す
		if (! ctype_digit( join('', array_keys($results)) )) return $results;

		// as が true のときは primaryKey を代わりに指定
		$as = is_bool($query['as']) ? $this->primaryKey : $query['as'];

		// 新しく ID をキーにした連想配列を作る
		$items = array();
		foreach ($results as $i => $item) {
			$id = isset($item[$as]) ? $item[$as]
			                        : $item[ $this->name ][$as];
			$items[$id] = $item;
		}
		return $items;
	}

	protected function _findItem($state, $query, $results = array()) {
		// customize query ?
		if ($state == 'before') {
			$query['limit'] = 1;
			return $query;
		}
		// customize result ?
		if (empty($results[0])) return array();

		// delete this Model name key
//		$results = Hash::extract($results, '{n}.'. $this->name);
//		return $results;

		// delete this Model name key (with related Model)
		$model_name = $this->name .'.';
		$new_result = array();
		$flat_result = Hash::flatten($results[0]);
		foreach ($flat_result as $key => $value) {
			$new_result[ str_replace($model_name, '', $key) ] = $value;
		}
		return Hash::expand($new_result);
	}

	protected function _findItems($state, $query, $results = array()) {
		if ($state == 'before') {	// customize query ?
			return $query;
		}
		// customize result ?
		if (empty($results)) return array();

		// delete this Model name key (with related Model)
		$model_name = $this->name .'.';
		$new_results = array();
		$flat_results = Hash::flatten($results);
		foreach ($flat_results as $key => $value) {
			$new_results[ str_replace($model_name, '', $key) ] = $value;
		}
		return Hash::expand($new_results);
	}


	// get next ID
	public function getNextID ($group_name = null, $want_prefix = false) {
		$max_id = 0;

		// get max data_id number
		$data = $this->find('first', array(
			'fields'=> array('MAX('. $this->primaryKey .') AS max_id'),
			'recursive' => -1,
		));
		foreach ($data as $dt) {
			if (isset($dt['max_id'])) $max_id = intval($dt['max_id'], 10);
		}
		return sprintf('%05d', $max_id + 1);
	}


	// (汎用) status を "削除" に
	public function saveAsDelete ($id) {
		if (empty($id)) return;	// ID

		// [削除] として更新
		return $this->save(array($this->primaryKey => $id, 'status' => 'deleted'));
	}

	// (汎用) status を "通常" に
	public function saveAsActive ($id) {
		if (empty($id)) return;	// ID

		// [通常] として更新
		return $this->save(array($this->primaryKey => $id, 'status' => 'active'));
	}
        // NIJC for debug
        public function sqllog(){
                $sql = $this->getDataSource()->getLog();
                $this->log($sql);
                return $sql;
        }
}
