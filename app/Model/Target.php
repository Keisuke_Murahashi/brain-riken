<?php
App::uses('AppModel', 'Model');

class Target extends AppModel {
	var $useTable = 'target';	// table name
	public $primaryKey = 'app_id';	// primary key (ID) [add]

	// POST データからデータ登録
	public function saveByPost ($post) {
		// データ登録用の連想配列を作成
		$new_data = array(
			'mess_id' => $post['mess_id'],
			'target'  => $post['target'],
		);
		// 保存
		return $this->save($new_data);
	}
}
