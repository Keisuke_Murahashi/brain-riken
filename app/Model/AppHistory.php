<?php
App::uses('AppModel', 'Model');

class AppHistory extends AppModel {
	var $useTable = 'app_history';	// table name
	public $primaryKey = 'act_id';	// primary key (ID)

	// Relation
	public $belongsTo = array(
		'AppData' => array(
			'className' => 'AppData',
			'foreignKey' => 'app_id',
		)
	);

	// Find 結果を整形
	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			// data_id をコピー
			$results[$key]['data_id'] =
				isset($val['AppData']) ?
					  $val['AppData']['data_id'] : null;
		}
		return $results;
	}

	// POST データからデータ登録
	public function saveByPost ($post, $username = null) {
		// データ登録用の連想配列を作成
		$new_data = array(
			'app_id'     => $post['app_id'],
			'action'     => $post['action'],
			'comment'    => $post['comment'],
			'authorizer' => $username,	// ログインユーザー名
		);
		// 保存
		return $this->save($new_data);
	}

	// ステータスを "承認取り下げ" に設定
	public function saveAsWithdraw ($post) {
		if (empty($post['act_id'])) return false;

		return $this->save(array(
			'act_id' => $post['act_id'],
			// NIJC 'action' => '承認取り下げ',
			'action' => 'Withdrawn',
		));
	}

	// データセットの ID を取得
	public function getAppId ($post) {
		if (empty($post['act_id'])) return false;

		$item = $this->find('item', array(
				'fields' => 'app_id', 'recursive' => -1,
				'conditions' => array('act_id' => $post['act_id']),
		));
		return isset($item['app_id']) ? $item['app_id'] : null;
	}
}
