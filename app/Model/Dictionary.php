<?php
App::uses('AppModel', 'Model');

class Dictionary extends AppModel {
	var $useTable = 'dictionary';	// table name
	public $primaryKey = 'id';	// primary key (ID) [add]

	// Tabledit による User 編集
	public function saveByLite ($post, $id) {
		// データ登録用の連想配列を作成
		$update_data = array(
//			'update' => date('Y/m/d H:i:s'),
		);
		foreach (array('term', 'memo') as $key) {
			if (array_key_exists($key, $post))
				$update_data[$key] = $post[$key];
		}
		// 更新のときは ID をセット
		if ($id) $update_data[ $this->primaryKey ] = $id;

		// データ登録 (MySQL)
		$this->save($update_data);
	}
}
