<?php
App::uses('AppModel', 'Model');

class UserAuth extends AppModel {
	var $useTable = 'user_auth';	// table name
	public $primaryKey = 'username';	// primary key (ID) [add]

	// Relation
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'username',
		),
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group',
		)
	);

	// グループの関連付けを設定
	public function saveToGroup ($user_id, $group_id) {
		$pk = $this->primaryKey;

		// データ登録用の連想配列を作成
		$new_data = array();
		if (is_array($user_id)) {
			foreach ($user_id as $id) {
				if ($id)  $new_data[] = array($pk => $id,      'group' => $group_id);
			}
		} else {
			if ($user_id) $new_data[] = array($pk => $user_id, 'group' => $group_id);
		}
		// 保存
		if ($new_data) return $this->saveAll($new_data);
		return;
	}

	// グループのユーザーのID一覧を取得
	public function getUserNamesByGroup ($group_id, $roles = null) {
		// $group_id が単一要素の配列のときは、その要素のスカラーにする (謎の SQL error 対策) - 20160708
		if (is_array($group_id) and count($group_id) == 1) {
			$group_id = reset($group_id);
		}

		// 検索条件
		$cond = array('group' => $group_id);

		// SystemAdmin 特有
		if (is_array($roles) and in_array('SystemAdmin', $roles)) {
			$cond = array('OR' => array(
				array('role' => 'SystemAdmin'),
				array('role' => $roles) + $cond,
			));
		} else if (is_array($roles)) {
			$cond += array('role' => $roles);
		}

		// 取得
		$item = $this->find('list', array(
			'fields' => array('username', 'group'), 'recursive' => -1,
			'conditions' => $cond,
		));
		return array_keys($item);
	}

	// グループの管理ユーザーのID一覧を取得 (writable)
	public function getAdminUserNamesByGroup ($group_id) {
		// NIJC return $this->getUserNamesByGroup($group_id, array('SystemAdmin', 'GroupAdmin'));
		return $this->getUserNamesByGroup($group_id, array('SystemAdmin', 'Leader', 'GroupAdmin'));
	}

	// グループの通常ユーザーのID一覧を取得 (writable, readable)
	public function getCommonUserNamesByGroup ($group_id) {
		return $this->getUserNamesByGroup($group_id,
						// NIJC array('SystemAdmin', 'GroupAdmin', 'Leader', 'Curator'));
						array('SystemAdmin', 'GroupAdmin', 'Leader', 'Reviewer'));
	}

	// グループの一般ユーザーのID一覧を取得 (readonly)
	public function getNormalUserNamesByGroup ($group_id) {
		// NIJC return $this->getUserNamesByGroup($group_id, array('Leader', 'Curator'));
		// return $this->getUserNamesByGroup($group_id, NULL);
		$item = $this->find('list', array(
			'fields' => array('username', 'group'), 'recursive' => -1,
			'conditions' => array('group' => $group_id, 'role' => NULL)));
			// (*memo: 'role' => 'Reviewer' は一般ユーザーではないのか？) - 20160708
		return array_keys($item);
	}

	// グループが未設定のユーザーのID一覧を取得 (denied) - 20160708
	public function getGuestUserNames () {
		return $this->getUserNamesByGroup($group_id, array('', null));
	}

	// グループIDを取得
	public function getGroup ($username) {
		return $this->getGroupId($username);
	}
	public function getGroupId ($username) {
		$item = $this->find('item', array(
			'fields' => 'group', 'recursive' => -1,
			'conditions' => array('username' => $username),
		));
		if (isset($item['group'])) return $item['group'];
		return;
	}

	// ユーザーの権限を取得
	public function getRole ($username) {
		$item = $this->find('item', array(
			'fields' => 'role', 'recursive' => -1,
			'conditions' => array('username' => $username),
		));
		if (isset($item['role'])) return $item['role'];
		return;
	}

	//++ ユーザー権限系

	// 管理者かどうか (システム管理者 or グループ管理者)
	public function isAdmin ($username) {
		$role = $this->getRole($username);
		return $role == 'SystemAdmin' || $role == 'GroupAdmin';
	}
	// システム管理者かどうか
	public function isSystemAdmin ($username) {
		$role = $this->getRole($username);
		return $role == 'SystemAdmin';
	}
	// グループ管理者かどうか
	public function isGroupAdmin ($username) {
		$role = $this->getRole($username);
		return $role == 'GroupAdmin';
	}
	// 投稿・登録できる通常のユーザーかどうか (ゲストユーザー以外)
	public function isCommon ($username) {
		$role = $this->getRole($username);
		return $role == 'SystemAdmin' || $role == 'GroupAdmin'
		    // NIJC || $role == 'Leader' || $role == 'Curator';
		    || $role == 'Leader' || $role == 'Reviewer';
	}
	// 投稿・登録ができないゲストユーザーかどうか
	public function isGuest ($username) {
		return ! $this->isCommon($username);
	}

}

