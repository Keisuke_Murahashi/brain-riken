<?php
App::uses('AppModel', 'Model');
App::uses('Util', 'Vendor');

class DataType extends AppModel {
	var $useTable = 'data_types';	// table name
	public $primaryKey = 'data_type';	// primary key (ID) [add]
	public $order = 'ordered';	// OrderBy

	// データタイプの一覧を取得
	public function getDataTypes () {
		$items = $this->find('items', array(
				'fields' => 'data_type', 'recursive' => -1,
				'conditions' => array('delflag' => 0),	// NIJC
		));
		return Hash::extract($items, '{n}.data_type');
	}

	// JSON スキーマのファイル名を取得
	public function getJsonScheme ($data_type) {
		$item = $this->find('item', array(
				'fields' => 'json_scheme', 'recursive' => -1,
				'conditions' => array('data_type' => $data_type, 'delflag' => 0), // NIJC
		));
		return isset($item['json_scheme']) ? $item['json_scheme'] : null;
	}

	// JSON データのファイル名を取得
	public function getJsonData ($data_type) {
		$item = $this->find('item', array(
				'fields' => 'json_data', 'recursive' => -1,
				'conditions' => array('data_type' => $data_type, 'delflag' => 0), // NIJC
		));
		return isset($item['json_data']) ? $item['json_data'] : null;
	}

	// JSON スキーマのファイル内容を取得
	public function getJsonSchemeContents ($data_type) {
		// JSON スキーマのファイル名を取得
		$filename = is_array($data_type) ? $data_type['json_scheme']
										 : $this->getJsonScheme($data_type);
		return file_get_contents(JSON_SCHEME_DIR . $filename);
	}

	// JSON データのファイル内容を取得
	public function getJsonDataContents ($data_type) {
		// JSON データのファイル名を取得
		$filename = is_array($data_type) ? $data_type['json_data']
										 : $this->getJsonData($data_type);
		return file_get_contents(JSON_DATA_DIR . $filename);
	}

	// データタイプを元に、JSON スキーマから各 JSON フィールド名の連想配列を取得
	public function getJsonKeys ($data_type) {
		// JSON スキーマ
		$json_scheme = $this->getJsonSchemeContents($data_type);

		// オブジェクト毎にパース
		// trueを付けると連想配列として分解して格納
		$json_raw = json_decode($json_scheme, true);

		// JSON スキーマから各 JSON フィールド名の連想配列に
		return Util::getJsonKeys($json_raw);
	}
}
