<?php
App::uses('AppModel', 'Model');

class Share extends AppModel {
	var $useTable = 'share';	// table name
	public $primaryKey = 'share_id';	// primary key (ID) [add]

	public $belongsTo = array(
			// グループ名取得
			'Group' => array(
					'className' => 'Group',
					'foreignKey' => 'group',
			)
	);

	// 共有データ作成、保存
	public function saveAsAccept ($post) {
		if (empty($post['data_id'])) return false;

		return $this->save(array(
			'data_id' => $post['data_id'],
			'group'   => $post['group'],
		));
	}

	// ステータスを "取り下げ" に設定
	public function saveAsWithdraw ($post) {
		if (empty($post['data_id'])) return false;
		if (empty($post['group'])) return false;

		return $this->updateAll(
				array ( 'delflg' => 1 ),
				array ( 'data_id' => $post['data_id'], 'group' => $post['group'])
		);
	}

	// 共有先グループ名を取得
	public function getSharedGroup ($data_id) {
		$item = $this->find('items', array(
				'conditions' => array('Share.data_id' => $data_id, 'Share.delflg' => 0),
				'fields' => array('Group.id', 'Group.name'), 'recursive' => 1,
		));
		$grp = array();
		foreach ($item as $val) {
			$grp[ $val['Group']['id'] ] =
				isset($val['Group']['name']) ? $val['Group']['name'] :
				                               $val['Group']['id'];
		}
		return $grp;
	}

	// 自グループ内共有のデータIDを取得
	public function getDataIdsFromIntraGroup ($group_id) {
		$list = $this->find('list', array(
				'joins' => array(array(
					'table' => 'Share', 'alias' => 'Other',
					'type' => 'LEFT',
					'conditions' => array(
						'Share.data_id = Other.data_id',
						'Share.group <> Other.group',
					),
				)),
				'conditions' => array(
					'Share.group' => $group_id,
					'Other.group' => null,
					'Share.delflg' => 0,
				),
				'fields' => array('Share.data_id'),
				'recursive' => -1,
		));
		return array_unique( array_values($list) );
	}

	// 外部グループ間共有のデータIDを取得
	public function getDataIdsFromInterGroup ($group_id) {
		$list = $this->find('list', array(
				'joins' => array(array(
					'table' => 'Share', 'alias' => 'Other',
					'type' => 'INNER',
					'conditions' => array(
						'Share.data_id = Other.data_id',
						'Share.group <> Other.group',
					),
				)),
				'conditions' => array(
					'Share.group' => $group_id,
					'Share.delflg' => 0,
					'Other.delflg' => 0,
				),
				'fields' => array('Share.data_id'),
				'recursive' => -1,
		));
		return array_unique( array_values($list) );
	}

}
