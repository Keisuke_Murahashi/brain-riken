<?php
App::uses('AppModel', 'Model');

class DataFilter extends AppModel {
	var $useTable = 'data_filter';	// table name
	public $primaryKey = 'id';	// primary key (ID) [add]

	// 公開必須なフィールド名 (共通)
	public $required_fields = array(
			'data_id'   => '_meta_data_id',
			'data_type' => '_meta_data_type',
//			'dir_name'  => '_meta_dir_name',
	);

	// 渡されたデータタイプを元に、公開可能なデータをフィルタリング
	public function filterOn ($group, $data_type, $data) {
		$safe_data = array();

		// データタイプを元に、公開可能なフィールド名を取得 (データフィルター)
		$filter = $this->find('list', array(
			'fields' => array('key', 'value'),
			'conditions' => array('data_type' => $data_type, 'value' => 1, 'group' => $group),
		));

		// 公開必須なフィールド名を追加
		foreach ($this->required_fields as $key => $memo) {
			$filter[$key] = true;
		}

		// (データが空でも、公開可能な親のフィールド名は表示したい) - 20160321
		if (true) {
			// 親のキーを取得
			$filter_array = Hash::expand($filter);
			$filter_parent_keys = array_keys($filter_array);

			// 親のキーをデータフィルターに追加
			foreach ($filter_parent_keys as $key) {
				if (empty($filter[$key])) $filter[$key] = 1;
			}
		}

		// 渡されたデータを平坦化
		$flat_data = Hash::flatten($data);

		// データフィルターをかけたメタデータを作成
		foreach ($flat_data as $key => $value) {
			// 公開可能であればコピー
			if (!empty($filter[$key])
			or  !empty($filter[ preg_replace('/\\.[0-9]+/', '', $key) ])) {
				$safe_data[$key] = $value;
			}
		}
		return Hash::expand($safe_data);	// データを再構築して返す
	}

	// グループとキーから ID を取得 (複合主キーにしたかった)
	public function getIdByKey ($group_id, $data_type, $key) {
		$item = $this->find('item', array(
			'fields' => 'id', 'recursive' => -1,
			'conditions' => array(
				'group' => $group_id, 'data_type' => $data_type, 'key' => $key,
			),
		));
		return isset($item['id']) ? $item['id'] : null;
	}

	// Tabledit による User 編集
	public function saveByLite ($post, $id) {
		// データ登録用の連想配列を作成
		$update_data = array(
//			'upd_date' => date('Y/m/d H:i:s'),
		);
		foreach (array('group', 'data_type', 'key', 'value') as $key) {
			if (array_key_exists($key, $post))
				$update_data[$key] = $post[$key];
		}
		// 更新のときは ID をセット
		if ($id) $update_data[ $this->primaryKey ] = $id;

		// データ登録 (MySQL)
		$this->create();	// insert ループに対応
		$this->save($update_data);

		// 新規のときは ID を設定
		if (!$id) $update_data[ $this->primaryKey ] = $this->getLastInsertID();

		return $update_data;
	}
}
