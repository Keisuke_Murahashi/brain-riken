<?php
App::uses('BaseAuthenticate', 'Controller/Component/Auth');

// LDAP認証
class LdapAuthenticate extends BaseAuthenticate {

	// LDAP認証
	public function authenticate(CakeRequest $request, CakeResponse $response) {
		$user = $this->_ldapAuth($request['data']['User']);

		// debug($user);
		return $user;
	}

	// LDAP認証を行なうメソッド
	function _ldapAuth($request) {
		$username = $request['username'];
		$password = $request['password'];

		// パスワードが空ならfalse
		if (!$password) return false;

		// データベース設定から情報取得
		$conf = new DATABASE_CONFIG();
		$conf = $conf->default_ldap;
		if (!$conf) return false;

		// 接続
		$cn = ldap_connect($conf['host'], $conf['port']);
		if (!$cn) {
			$_SESSION['error'] = 'LDAP サーバーが見つかりません。';
			return false;
		}
		// 認証方法を変更
		ldap_set_option($cn, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($cn, LDAP_OPT_REFERRALS, 0);

		// 管理権限でバインドしてみる
		if (@!ldap_bind($cn, $conf['login'], $conf['password'])) {
			$_SESSION['error'] = 'LDAP サーバーに接続できません。';
			ldap_close($cn);
			return false;
		}

		// 渡されたユーザ名を検索
		$find = ldap_search($cn, $conf['database'], 'samaccountname='. $username);
//		$find = ldap_search($cn, $conf['database'], '(cn=*)');

		// エントリを取得
		$entries = ldap_get_entries($cn, $find);

		// dn（ドメイン名前）が取得できてかつイニシャルがセットされていれば
		if ($entries['count'] > 0 and isset($entries[0]['dn'])) {
			$info = $entries[0];

			// 認証用データを作成
			$res = array('username' => $username);	// ログインＩＤ
			foreach ($info as $key => $val) {
				if (is_array($val)) {
					$res[$key] = isset($val[0]) ? $val[0] : null;
				}
			}

			// パスワード認証
			if (@ldap_bind($cn, $info['dn'], $password)) {
				ldap_close($cn);
				return $res;
			}
		}
		ldap_close($cn);
		return false;
	}
}
?>