<?php
App::uses('AdminController', 'Controller');
App::import('Vendor', 'Util');

class MyDatasetController extends AdminController {
	public $uses = array('Dataset', 'AppData', 'User', 'UserAuth',
							'Publish', 'Group', 'Share', 'DataType');

	public function index () {
		$username = $this->Auth->user('username');	// ログインユーザー名
		$group_id = $this->getGroupId();	// ログインユーザーのグループID (短縮)
//		print $username;

		// POST 通信のとき
		if ($this->request->is('post')) {
			return $this->_post();
		}

		// 各種データセット
		$data = array();

		// データセット (Mongo データも自動連係で取得)
		$data['myasset'] = $this->Dataset->find('items', array(
			'conditions' => array('owner' => $username),
			'order' => array('Dataset.upd_date' => 'desc'),
		));
		// グループ内共有
		$data['intragroup'] = $this->Dataset->find('items', array(
			'joins' => array(array(
					'table' => 'share', 'alias' => 'Share',
					'type' => 'INNER',
					'conditions' => array('Share.data_id = Dataset.data_id', 'Share.delflg = 0')
			)),
			'conditions' => array('NOT' => array('Dataset.owner' => $username)
					, 'Share.group' => $group_id
					, 'UserAuth.group' => $group_id),
			'order' => array('Dataset.upd_date' => 'desc'),
		));
		// グループ間共有
		$data['intergroup'] = $this->Dataset->find('items', array(
			'joins' => array(array(
					'table' => 'share', 'alias' => 'Share',
					'type' => 'INNER',
					'conditions' => array('Share.data_id = Dataset.data_id', 'Share.delflg = 0')
			)),
			'conditions' => array('NOT' => array('Dataset.owner' => $username)
					, 'AND' => array('Share.group' => $group_id
					, 'NOT' => array('UserAuth.group' => $group_id))),
			'order' => array('Dataset.upd_date' => 'desc'),
		));
		// 公開
		$data['public'] = $this->Dataset->find('items', array(
			'joins' => array(array(
					'table' => 'publish', 'alias' => 'Publish',
					'type' => 'INNER',
					'conditions' => array('Publish.data_id = Dataset.data_id')
			)),
			// NIJC 'conditions' => array('Dataset.owner' => $username, 'Publish.status' => '承認済'),
			'conditions' => array('Dataset.owner' => $username, 'Publish.status' => 'Accepted'),
			'order' => array('Dataset.upd_date' => 'desc'),
		));

		// 各データセットごとにダウンロード API 用の設定をセット
		foreach (array_keys($data) as $key) {
			foreach ($data[$key] as $i => $item) {
				// 共有先グループ名を取得
				$data_shared = $this->Share->getSharedGroup($item['data_id']);

				// データにダウンロード API 用の設定をセット
				// .. (外部グループ間共有かどうか (共有数 > 1))
				$this->Common->set_download_info($data[$key][$i], count($data_shared) > 1);
			}
		}

		// データタイプ一覧
		$data_types = $this->DataType->getDataTypes();

		// 公開ポータルより Theme、Exp、Dataset 一覧取得 (get theme, sub_theme, exp list)
		$data_exp_list = $this->_query_get_exp($username);

		// View にセット
		$this->set('data', $data);	// View にセット(Myアセット, グループ内共有, グループ間共有, 公開)
		$this->set('data_types', $data_types);	// View にセット(データタイプ)
		$this->set('data_exp_list', $data_exp_list);	// View にセット(Theme情報)
	}


	// リダイレクト
	protected function _redirect () {
		// トップページに遷移
		$this->redirect(array('controller' => 'MyDataset', 'action' => 'index'), 303);
	}

	// POST 通信のとき、振り分け
	protected function _post () {
		// メタデータ登録
		if (isset($this->request->data['meta_reg'])) $this->meta_reg();

		// ディレクトリ追加 → datasetにデータセット登録、metadataは空を登録
		if (isset($this->request->data['dir_add'])) $this->dir_add();

		// 共有申請
		if (isset($this->request->data['share'])) $this->share();

		// 公開申請
		if (isset($this->request->data['publish'])) $this->publish();

		// ディレクトリ関連付け
		if (isset($this->request->data['upd_dir'])) $this->upd_dir();

		// ディレクトリ関連付け解除
		if (isset($this->request->data['unlink_dir'])) {
			$this->unlink_dir();
		}

		// リダイレクト
		$this->_redirect();
	}

	// メタデータ登録
	public function meta_reg () {
		if (!$this->request->is('post')) $this->_redirect();	// リダイレクト

		$username = $this->Auth->user('username');	// ログインユーザー名
		$group_id = $this->getGroupId();	// ログインユーザーのグループID (短縮)

		// ログインユーザーのグループが未設定時
		if (!$group_id) return $this->Flash->error(l('_no_grp_set'));

		// アップロードファイルがあるか
		$this->request->data('file_input', @$_FILES['file_input']);
		$upfile = $this->request->data('file_input');
		if (!$upfile or !is_uploaded_file($upfile['tmp_name'])) {
			return $this->Flash->error(l('_no_upl_file'));
		}

		// ファイルからJSONを読み込み
		$json = file_get_contents($upfile['tmp_name']);
		$json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
		$post = json_decode($json, true);

		// メタデータ (JSON) がデコードできなかったエラー
		if (!$post) {
			return $this->Flash->error(l('_bad_meta_format'));
		}

/*		// (*) 任意の data_id が上書きされてしまうので、必ず新しい data_id にする .. 20160428
		// data_idが存在した場合は、上書き
		if (empty($post['data_id'])){
			$data_id = $post['data_id'];
		} else {
			$data_id = $this->Dataset->getNextID($post['data_type']);
		}
*/
		// data_id を新規作成
		$data_id = $this->Dataset->getNextID($post['data_type']);

		// JSON デコードデータからデータ登録
		$result = $this->Dataset->saveByPost($post, $username, $data_id);

		// 共有データの保存
		if ($result) {
			$result = $this->Share->save(array(
				'data_id' => $data_id,
				'group'   => $group_id,
			));
		}

		// 成功していたら、ファイル転送API と連携 (データセットディレクトリの設定があれば)
		if ($result and !empty($post['dir_name'])) {
			// 自グループ内共有としてデータセットディレクトリを移動
			$is_success = $this->_move_dir_intragroup($data_id, $post['dir_name']);

			if ($is_success) {
				$this->Flash->info(l('_reg_meta')."\n".
									l('_trans_dataset'));
			} else {
				$this->Flash->warn(l('_reg_meta')."\n".
									l('_fail_trans_dataset'));
			}
		}
		// データセットディレクトリの設定がなければ注意付きで正常終了
		else if ($result) {
			$this->Flash->info(l('_reg_meta')."\n".
								l('_no_dataset_dir_set'));
		}
		// データベース設定エラー
		else {
			$this->Flash->error(l('_err_ocur_reg_mata'));
		}
	}

	// ディレクトリ追加 → datasetにデータセット登録、metadataは空を登録
	public function dir_add () {
		if (!$this->request->is('post')) $this->_redirect();	// リダイレクト

	// ファイルからJSONを読み込み（データタイプ毎にスキーマを用意）
		$json = file_get_contents(WWW_ROOT ."lib/01.data_t1wi_160221.json");

		// 文字化けするかもしれないのでUTF-8に変換
		$json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');

		// オブジェクト毎にパース
		// trueを付けると連想配列として分解して格納
		$obj = json_decode($json, true);

		// POST データから値を上書き
		$obj['dir_name']  = $this->request->data('dir_name');
		$obj['data_type'] = $this->request->data('data_type');

		// JSON デコードデータからデータ登録
		$this->Dataset->saveByPost($obj, $this->Auth->user('username'));
	}

	// 共有申請
	public function share () {
		if (!$this->request->is('post')) $this->_redirect();	// リダイレクト

		//POSTデータ取得
		$post = $this->request->data;
		// NIJC $post['app_type'] = '共有';	// 申請タイプ設定
		$post['app_type'] = 'Share';	// 申請タイプ設定

		// ユーザーがデータを編集可能かどうかチェック
		//if (!$this->is_editable_dataset($post['data_id'])) return;

		// データがロックされている場合は返す (共有取り下げなど) - 20160708
		if ($this->Dataset->is_locked($post['data_id'])) {
			$this->Flash->error(l('_data_is_locked')); return;
		}

		// グループIDがないときは返す - 20160708
		if (empty($post['group'])) {
			$this->Flash->error(l('_ipt_grp_name')); return;
		}

		//グループIDを取得
		$groupnames = implode(",", $post['group']);

		if ($groupnames == '') 
                	return $this->Flash->error(sprintf(l('_select_group')));

		$groupids = array();
		foreach ($post['group'] as $groupname){
			// $group = $this->Group->findByName($post['group']);
			$group = $this->Group->findByName($groupname);
			$groupids[] = $group['id'];
		}

		//申請されたデータの有無を確認
		$app_data = $this->AppData->find('items', array(
				'conditions' => array(
				//'AppData.creator' => $this->Auth->user('username'),
				// NIJC 'AppData.group' => $group['id'],
				'OR' => array('AppData.group' => $groupids),
				'AppData.data_id' => $post['data_id'],
				'delflg' => 0,
				// NIJC 'AppData.app_type' => '共有',
				'AppData.app_type' => 'Share',
				// NIJC 'NOT' => array('AppData.status' => array('棄却', '取り下げ'))),
				'NOT' => array('AppData.status' => array('Rejected', 'Withdrawn'))),
				'recursive' => 2, 'callbacks' => true,
		));

		$app_id = array();
		if (empty($app_data)) {
			//申請データが無ければ登録
			// 対象のグループを設定
			// NIJC $post['group'] = $group['id'];
			// 申請情報を保存
			foreach ($groupids as $groupid) {
		 		$post['group'] = $groupid;
				$app_id[] = $this->AppData->saveByPost($post, $this->Auth->user('username'));
				$this->AppData->create();
			//	$post['app_id'] = $app_id+1;
			}
			// システムメッセージ本文作成
			$app_id = implode(",", $app_id);
			$msg = 'Application ID: '. $app_id . "\n";
			$msg .= 'Date: '. date('Y/m/d H:i:s') . "\n";
			$msg .= 'Data ID: '. $post['data_id'] . "\n";
			$msg .= 'Applicant: '. $this->Auth->user('username') . "\n";
			// $msg .= '共有先：'. $group['name'] . "\n";
			$msg .= 'Shared Group: '. $groupnames. "\n";
			$msg .= 'Comment: '. $post['detail'] . "\n";

			// システムメッセージ登録 (送信先, タイトル, メッセージ, 申請ID)
			//NIJC $this->messageTo($post['select'], '【' . $post['app_type'] .'申請】承認依頼', $msg, $app_id);
			$this->messageTo($post['select'], 'Request for Approval (' . $app_id .')', $msg, $app_id);

			// Flashメッセージ設定
			// $this->Flash->info(sprintf(l('_sent_app'), $post['app_type'], $app_id));
			$this->Flash->info(sprintf(l('_sent_app'), $app_id));
//									' （送信先）%s', $this->User->getName($post['select']);
		}else{
			//申請データがあれば中断

			// Flashメッセージ設定
			$this->Flash->error(sprintf(l('_aldy_app'), $groupnames));
			// NIJC $post['app_type'], $this->request->data('group')));
		}
	}

	// 公開申請
	public function publish () {
		if (!$this->request->is('post')) $this->_redirect();	// リダイレクト

		// POST データからデータ登録
		$post = $this->request->data;
		$post['app_type'] = 'Release';	// 申請タイプ設定

		// ユーザーがデータを編集可能かどうかチェック
		if (!$this->is_editable_dataset($post['data_id'])) return;

		// データがロックされている場合は返す (共有取り下げなど) - 20160708
		if ($this->Dataset->is_locked($post['data_id'])) {
			$this->Flash->error(l('_data_is_locked')); return;
		}

		//申請されたデータの有無を確認
		$app_data = $this->AppData->find('items', array(
				'conditions' => array(
						//'AppData.creator' => $this->Auth->user('username'),
						//'AppData.group' => $post['exp_list'],
						'AppData.data_id' => $post['data_id'],
						'AppData.app_type' => 'Release',
						'AppData.delflg' => 0,
						// NIJC 'NOT' => array('AppData.status' => array('棄却', '取り下げ'))),
						'NOT' => array('AppData.status' => array('Rejected', 'Withdrawn'))),
				'recursive' => 2, 'callbacks' => true,
		));

		// 新規作成
		if (empty($app_data)) {

			//申請データを保存
			//$post['group'] = $this->UserAuth->getGroup($post['select']);
			//$post['group'] = $post['exp_list'];
			$target = mb_substr($post['exp_list'], strpos($post['exp_list'], '|', strpos($post['exp_list'], '|', strpos($post['exp_list'], '|') + 1) + 1 ) + 1 );
			$post['group'] = $target;
			$app_id = $this->AppData->saveByPost($post, $this->Auth->user('username'));

			// 公開データ作成
			$post['app_id'] = $app_id;
			$post['group'] = $post['exp_list'];
			$app_id = $this->Publish->saveByPost($post, $this->Auth->user('username'));

			// システムメッセージ本文作成
			$msg = 'Application ID: '. $app_id . "\n";
			$msg .= 'Accepted Date: '. date('Y/m/d H:i:s') . "\n";
			$msg .= 'Data ID: '. $post['data_id'] . "\n";
			$msg .= 'Applicant: '. $this->Auth->user('username') . "\n";
			$msg .= 'Data Portal Target: '. $target . "\n";
			$msg .= 'Comment: '. $post['detail'] . "\n";

			// システムメッセージ登録 (送信先, タイトル, メッセージ, 申請ID)
			$this->messageTo($post['select'], 'Request for Approval (' . $app_id. ')', $msg, $app_id);

			// Flashメッセージ設定
			// nIJC $this->Flash->info(sprintf(l('_sent_app'), $post['app_type'], $app_id));
			$this->Flash->info(sprintf(l('_sent_app'), $app_id));
//									' （送信先）%s', $this->User->getName($post['select']);
		}else{
			// Flashメッセージ設定
			$this->Flash->error(sprintf(l('_aldy_app'), implode(",", $this->request->data('group'))));
			// nIJC $post['app_type'], $this->request->data('group')));
		}
	}

	// ディレクトリ関連付け
	public function upd_dir () {
		if (!$this->request->is('post')) $this->_redirect();	// リダイレクト

		// POST データ
		$data_id  = $this->request->data('data_id');
		$new_dir_name = $this->request->data('dir');
		//$new_dir_name = preg_replace('!^[\\\/]+!', '', $new_dir_name);
		$ori_dir_name = $this->Dataset->getOriginalDir($data_id);

		// ユーザーがデータを編集可能かどうかチェック
		if (!$this->is_editable_dataset($data_id)) return;

		// データがロックされている場合は返す (共有取り下げなど) - 20160708
		if ($this->Dataset->is_locked($data_id)) {
			$this->Flash->error(l('_data_is_locked')); return;
		}

		// データセットディレクトリの未選択エラー
		if (!$new_dir_name) return $this->Flash->error(l('_set_data_dir'));

		// (*) ディレクトリの場所・設定方法の変更 .. 20160426
		// ユーザーディレクトリを取得
		//$home_dir = $this->User->getHomeDir( $this->Auth->user('username') );
		$home_dir = sprintf(DIR_USER_HOME, $this->Auth->user('username'));

		//$this->Dataset->saveToDirName($data_id, $home_dir .'/'. $new_dir_name);

		// 自グループ内共有としてデータセットディレクトリを移動
		//$is_success = $this->_move_dir_intragroup($data_id, $home_dir .'/'. $new_dir_name, $ori_dir_name);
		$is_success = $this->_move_dir_intragroup($data_id, $new_dir_name, $ori_dir_name);

		if ($is_success) {
		// ディレクトリの関連付けを設定
			// NIJC update app data when move was in success
			// NIJC(20160713): commented out because saveToDirName is done in _move_dir_intragroup 
			//$this->Dataset->saveToDirName($data_id, $new_dir_name);
			$this->Flash->success(l('_chg_rel_data_dir')."\n".
								l('_trans_dataset'));
		} else {
			$this->Flash->error(l('_fail_upd_data_dir')."\n".
								l('_api_have_problem'));
		}
	}

	// 公開ポータルよりTheme、Exp、Dataset一覧取得
	public function _query_get_exp ($username) {
		$data_exp_list = array();

		// get contents
		$publish = array();
		$publish['applicant'] = $username;
		//$publish['applicant'] = 'test_matsuda'; // Tentative
		$url = ConnectionManager::$config->default_api['public_getExperimentbyUser'];
		$url = preg_replace_callback('/\{\$(\w+)\}/', function ($matches) use ($publish) {
			return isset($publish[ $matches[1] ]) ? $publish[ $matches[1] ] : '';
		}, $url);

		$json = curl_get_contents($url);
		if (empty($json)) return array();

		// decode JSON
		$json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
		$arr = json_decode($json, true);

		if (empty($arr) or empty($arr["meta"])) return array();

		// create target list
		$data_documents = array();
		foreach ($arr["meta"]["exp_data"] as $item) {
			$k = sprintf('%d|%d|%d', $item[   'Theme']['PostMeta']['post_id'],
									 $item['SubTheme']['PostMeta']['post_id'],
									 $item[     'Exp']['PostMeta']['post_id']);
			$v = sprintf('%s|%s|%s', $item[   'Theme']['PostMeta']['meta_value'],
									 $item['SubTheme']['PostMeta']['meta_value'],
									 $item[     'Exp']['PostMeta']['meta_value']);
			$data_exp_list[ $k .'|'. $v ] = $v;
		}
		return $data_exp_list;
	}

        // NIJC(20160627): forward download request to ssb server
        public function download() {
                // check method(post only)
                if (!$this->request->is('post')) {
			return;
                }

		// stop auto rendering
		$this->autoRender = false;

                // forward
		$url = $this->request->data['download_url'];
		unset($this->request->data['download_url']);
		$result = curl_post_contents($url, $this->request->data);

		// respond
		$this->response->type('application/x-java-jnlp-file');
		$this->response->body($result);
        }

        // NIJC(20160701)
        public function unlink_dir () {
                if (!$this->request->is('post')) {
                	return;
                }

                $data_id  = $this->request->data('data_id');
                $data_item = $this->Dataset->findById($data_id);
                if (!$this->is_editable_dataset($data_item)) {
                	return;
                }
		$ori_dir_name = $data_item['original_dir_name'];
		$owner_user = $data_item['owner'];
                $is_success = $this->_discharge_intragroup($data_id, $ori_dir_name, $owner_user);
                if ($is_success) {
                        $this->Flash->success(l('_completed_to_discharge_intragroup'));
                } else {
                        $this->Flash->error(l('_failed_to_discharge_intragroup'));
                }
        }

	// NIJC(20160714)
	public function upload() {
		// check method(post only)
		//if (!$this->request->is('post')) {
		//	return;
		//}

		// stop auto rendering
		$this->autoRender = false;

		// build parameters, send request
		$url = ConnectionManager::$config->default_api['upload_url'];
		$username = $this->Auth->user('username');
		$params = array(
			'username' => $username,
			'uploadDir' => sprintf(DIR_USER_HOME, $username)
		);
                $result = curl_post_contents($url, $params);

                // respond
                $this->response->type('application/x-java-jnlp-file');
                $this->response->body($result);
	}
}

