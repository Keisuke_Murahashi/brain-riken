<?php
App::uses('AdminController', 'Controller');

class UserManagementController extends AdminController {
	public $uses = array('Group', 'User', 'UserAuth', 'Role', 'Share');

	public function index ($id = null) {
		$username = $this->Auth->user('username');	// ログインユーザー名
//		print $username;

		// POST 通信のとき
		if ($this->request->is('post')) {
			return $this->_post($id);
		}

		// ユーザー一覧
		$data_users = $this->User->find('items', array(
			'recursive' => 2, 'callbacks' => true,
		));
		$this->set('data_users', $data_users);	// View にセット

		// グループ一覧
		$data_group = $this->Group->find('items', array('recursive' => -1));
		$this->set('data_group', $data_group);	// View にセット

		// Role 選択条件 (システム管理者のみSystemAdminを選択できる)
		$option_roles = $this->Role->find('items', array(
			'fields' => array('name'), 'recursive' => -1
		) + ($this->UserAuth->isSystemAdmin($username) ? array() : array(
			'conditions' => array('NOT' => array('name' => 'SystemAdmin'))
		)));
		$this->set('option_roles', $option_roles);	// View にセット

		// グループ選択条件
		$option_group = $this->Group->find('items', array(
			'fields' => array('id', 'name'), 'recursive' => -1
		));
		$this->set('option_group', $option_group);	// View にセット

	}

	// グループで絞り込み
	public function group ($id = null) {
		$username = $this->Auth->user('username');	// ログインユーザー名
//		print $username;

		// POST 通信のとき
		if ($this->request->is('post')) {
			return $this->_post($id);
		}
		// ID がないとき
		if (!$id) $this->_redirect();	// リダイレクト

		// ID 指定されたグループ
		$data_group = $this->Group->find('item', array(
			'conditions' => array('id' => $id), 'recursive' => 0
		));
		$this->set('data_group', $data_group);	// View にセット

		// このグループのユーザー一覧
		$data_users = $this->User->find('items', array(
			'conditions' => array('group' => $id),
			'recursive' => 2, 'callbacks' => true,
		));
		$this->set('data_users', $data_users);	// View にセット

		// グループ未設定のユーザー一覧
		$data_free_users = $this->User->find('items', array(
			'conditions' => array('OR' => array(
				array('group' => null),
				array('group' => ''),
			)),
			'recursive' => 1, 'callbacks' => true,
		));
		$this->set('data_free_users', $data_free_users);	// View にセット

		// Role 選択条件 (システム管理者のみSystemAdminを選択できる)
		$option_roles = $this->Role->find('items', array(
			'fields' => array('name'), 'recursive' => -1
		) + ($this->UserAuth->isSystemAdmin($username) ? array() : array(
			'conditions' => array('NOT' => array('name' => 'SystemAdmin'))
		)));
		$this->set('option_roles', $option_roles);	// View にセット

		// グループ選択条件
		$option_group = $this->Group->find('items', array(
			'fields' => array('id', 'name'), 'recursive' => -1
		));
		$this->set('option_group', $option_group);	// View にセット

		// ユーザーがグループを編集可能かどうかチェック
		$this->set('is_editable', $this->Group->is_editable($data_group, $username));
	}

	// For Ajax
	public function ajax ($id = null) {
		$vars = 'data_users';	// var names to set (string/array)
		$username = $this->Auth->user('username');	// ログインユーザー名
		$data_id = $this->request->data('input1');	// データID


		// 自グループのIDを取得
		$gid = $this->User->find('items', array(
			'conditions' => array('User.username' => $username),
			'recursive' => 2, 'callbacks' => true,
		));

		// 承認者一覧を取得（グループIDが自グループ かつ ロールがGroupAdmin）
		$data_users['authorizer'] = $this->User->find('items', array(
			'conditions' => array('AND' => array(
			//'NOT' => array('User.username' => $username),
			// NIJC 'UserAuth.group' => $gid[0]['group'], 'UserAuth.role' => 'GroupAdmin')),
			'UserAuth.group' => $gid[0]['group'], 'UserAuth.role' => array('GroupAdmin', 'Leader'))),
			'recursive' => 2, 'callbacks' => true,
		));

		//共有先一覧を取得（自グループ以外で未共有のグループ）※申請中かどうかは後で判断
		$data_users['group'] = $this->Group->find('items', array(
			'joins' =>  array(array(
					'table' => 'share', 'alias' => 'Share',
					'type' => 'LEFT',
					'conditions' => array('Group.id = Share.group'),
			)),
			'conditions' => array('NOT' => array('Share.data_id' => $data_id, 'Share.id' => $gid[0]['group']),
					 'NOT' => array('Group.id' => $gid[0]['group'])),
			'fields' => 'DISTINCT Group.name', 'recursive' => -1,
		));

		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}

	// For Ajax (get directories in user home directory)
	public function ajax_dirs () {
		$username = $this->Auth->user('username');	// ログインユーザー名
		$vars = 'dir_names';	// var names to set (string/array)
		$dir_names = null;	// - 20160708

		// find home_dir
		//$home_dir = $this->User->getHomeDir($username);
		$home_dir = sprintf(DIR_USER_HOME, $username);

		// scan home_dir [検索開始Dir, フルパス/名前のみ, フルパスから除くパス] - 20160708
		if (is_dir($home_dir)) {
			//$dir_names = Util::scandir_dirs($home_dir, true, $home_dir);
			$dir_names = Util::scandir_dirs($home_dir, true);
		}

		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}

	// リダイレクト
	protected function _redirect ($action = 'index', $id = null) {
		// トップページに遷移
		$this->redirect(array('controller' => 'UserManagement', 'action' => $action, $id), 303);
	}

	// POST 通信のとき、振り分け
	protected function _post ($id = null) {
		// グループ追加
		if (isset($this->request->data['submit1'])) $this->create();

		// 選択したユーザーを指定のグループに設定
		if (isset($this->request->data['set_group'])) $this->set_group($id);

		// 選択したユーザーを指定のグループから解除
		if (isset($this->request->data['unset_group'])) $this->unset_group($id);

		// リダイレクト
		$this->_redirect();
	}

	// グループ追加
	public function create () {
		if (!$this->request->is('post')) $this->_redirect();	// リダイレクト
		$username = $this->Auth->user('username');	// ログインユーザー名

		// ユーザーがグループを編集可能かどうかチェック
		if (!$this->Group->is_editable(null, $username)) {
			$this->Flash->error(l('_no_priv_edit_grp'));
			return;
		}

		// POST データに、別名と固定値を設定
		$post = $this->request->data;
		$post = array_merge($post, array(
			'name' => $post['gp_name'],
			'detail' => $post['gp_detail'],
			'admin' => 'TRUE', 'status' => 'active'
		));
		// グループ名があるか
		if (empty($post['name'])) {
			return $this->Flash->error(l('_ipt_grp_name'));
		}

		// POST データからデータ登録
		$this->Group->saveByPost($post, $this->Auth->user('username'));
	}

	// 選択したユーザーを指定のグループに設定
	public function set_group ($group_id = null) {
		$username = $this->Auth->user('username');	// ログインユーザー名

		// ID が見つからないときは返す
		if (!$group_id) $group_id = $this->request->data('id');
		if (!$group_id or !$this->request->is('post')) return;

		// ユーザーがグループを編集可能かどうかチェック
		if (!$this->Group->is_editable($group_id, $username)) {
			$this->Flash->error(l('_no_priv_edit_grp'));
			return;
		}

		// チェックしたユーザーID
		$user_ids = $this->request->data('chk');

		// グループ未設定のユーザー一覧
		$data_free_users = $this->User->find('items', array(
			'conditions' => array('OR' => array(
				array('group' => null),
				array('group' => ''),
			)),
			'fields' => array('username', 'UserAuth.group'),
			'recursive' => 0, 'as' => 'username',
		));

		// 現在もグループが未定義かどうかの整合性チェック
		if (is_array($user_ids)) {
			foreach ($user_ids as $i => $user_id) {
				if (empty($data_free_users[$user_id])) $user_ids[$i] = null;
			}
		} else {
			$user_ids = array($user_ids);
		}

		// トランザクション開始
		$Transaction = $this->Transaction->begin();
		try {
			// グループの関連付けを設定
			$this->UserAuth->saveToGroup($user_ids, $group_id);

			// [API] 新しく追加したユーザーにアクセス権限を設定
			$this->_api_set_permission($group_id, $user_ids)
				or throw_exception(l('_api_have_problem'));

			// システムメッセージ本文作成
			$msg  = sprintf("ユーザーID：%s\n", implode(', ', $user_ids));
			$msg .= sprintf("グループID：%s\n", $group_id);
			$msg .= sprintf("グループ名：%s\n", $this->Group->getName($group_id));

			// システムメッセージ登録 (送信先, タイトル, メッセージ, 申請ID)
			$this->messageTo($username, 'Set User Group (ユーザーの新規グループ設定)', $msg, $group_id);

			// (トランザクション) コミット
			$Transaction->commit();

			// 完了メッセージ
			$this->Flash->success(sprintf(l('_completed_to_set_user_group'), $group_id));

			// Group ID でリダイレクト
			$this->_redirect('group', $group_id);
		}
		// 例外発生時
		catch (Exception $e) {
			// (トランザクション) ロールバック
			$Transaction->rollback();

			// エラーメッセージ
			$this->Flash->error(sprintf(l('_error_change_user_info'), l('_set_group')) .
								($e->getMessage() ? ' ('. $e->getMessage() .')' : ''));
		}
	}

	// 選択したユーザーを指定のグループから解除
	public function unset_group ($group_id = null) {
		$username = $this->Auth->user('username');	// ログインユーザー名

		// ID が見つからないときは返す
		if (!$group_id) $group_id = $this->request->data('id');
		if (!$group_id or !$this->request->is('post')) return;

		// ユーザーがグループを編集可能かどうかチェック
		if (!$this->Group->is_editable($group_id, $username)) {
			$this->Flash->error(l('_no_priv_edit_grp'));
			return;
		}

		// チェックしたユーザーID
		$user_ids = $this->request->data('chk');

		// このグループのユーザー一覧
		$data_users = $this->User->find('items', array(
			'conditions' => array('group' => $group_id),
			'fields' => array('username', 'UserAuth.group'),
			'recursive' => 0, 'as' => 'username',
		));

		// 現在もこのグループに所属しているかどうかの整合性チェック
		if (is_array($user_ids)) {
			foreach ($user_ids as $i => $user_id) {
				if (empty($data_users[$user_id])) $user_ids[$i] = null;
			}
		}
		// グループの関連付けを設定
		$this->UserAuth->saveToGroup($user_ids, null);

		// Group ID でリダイレクト
		$this->_redirect('group', $group_id);
	}


	// Tabledit による User 編集 (Ajax)
	public function ajax_edit_user ($action = null, $id = null) {
// debug print
//Util::debug($this->request, '_json_update.txt');
		$vars = array('post', 'error');	// var names to set (string/array)
		$post = $this->request->data;	// POST データ
		$username = $this->Auth->user('username');	// ログインユーザー名
		$error = '';

		if ($this->request->is('post')) {	// Method == POST
			// 引数
			if (!$action) $action = $this->request->data('action');
			if (!$id)     $id     = $this->request->data('username');
			if (!$id)     return;

			// 編集対象ユーザーのグループと管理権限を取得
			$user = $this->User->findById($id);

			//if (!$user['group']) return;

			// システム管理者以外
			if ($action == 'edit' and !$this->UserAuth->isSystemAdmin($username)) {
				// (現)システム管理者のユーザー情報は変更できない
				if ($user['role'] == 'SystemAdmin') {
					$error .= l('_error_edit_user_info_for_system_admin');
				}
				// グループを変更できるのはシステム管理者のみ
				if (isset($post['group'])) {
					unset($post['group']);
					$error .= l('_error_change_group_with_system_admin');
				}
				// システム管理者に変更できるのはシステム管理者のみ
				if (isset($post['role']) and $post['role'] == 'SystemAdmin') {
					unset($post['role']);
					$error .= l('_error_change_authority_as_system_admin');
				}
			}

			// トランザクション開始
			$Transaction = $this->Transaction->begin();
			try {
				// action による振り分け
				// .. ユーザーがグループユーザーを編集可能かどうかチェック
				//if (!$this->Group->is_editable($user['group'], $username)) {
				if (false) {
					// ...
				} else if ($action == 'edit') {
					// エラーがなかったら更新する
					if (!$error) {
						$this->User->saveByLite($post, $id);

						// [API] 新しく追加したユーザーにアクセス権限を設定
						if (isset($post['role']) and $user['role'] != $post['role']) {	// ※"role"変更時のみ
							$this->_api_set_permission($user['group'], $id)
								or throw_exception(l('_api_have_problem'));
						}
					}
				} else if ($action == 'delete') {
//					$this->User->saveAsDelete($id);
				} else if ($action == 'restore') {
//					$this->User->saveAsActive($id);
				}

				// エラーメッセージ
				if ($error) {
					$error .= "\n\n". sprintf(l('_error_change_user_info'), l('_edit_user'));
				} else {
					// システムメッセージ本文作成
					$msg  = sprintf("ユーザーID：%s\n", $id);
					$msg .= sprintf("グループID：%s\n", $user['group']);
					$msg .= sprintf("グループ名：%s\n", $this->Group->getName($user['group']));

					// 変更対象のフィールド名
					$edit_fields = array(
						'name' => 'ユーザー名', 'detail' => '詳細',
						'home_dir' => 'ホームディレクトリ', 'role' => 'ロール',
					);
					foreach ($edit_fields as $k => $n) {
						if (empty($post[$k]) or $user[$k] == $post[$k]) {
							$msg .= sprintf($n ." : %s\n", $user[$k]);
						} else {
							$msg .= sprintf($n ." : %s -> %s\n", $user[$k], $post[$k]);
						}
					}

					// システムメッセージ登録 (送信先, タイトル, メッセージ, 申請ID)
					$this->messageTo($username, 'Edit User Info (ユーザー情報編集 (Ajax))', $msg, $id);

					// (トランザクション) コミット
					$Transaction->commit();
				}
			}	// try
			// 例外発生時
			catch (Exception $e) {
				// (トランザクション) ロールバック
				$Transaction->rollback();

				// エラーメッセージ
				$error .= "\n\n". sprintf(l('_error_change_user_info'), l('_edit_user')) .
									($e->getMessage() ? ' ('. $e->getMessage() .')' : '');
			}
		}

		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}

	// Tabledit による Group 編集 (Ajax)
	public function ajax_edit_group ($action = null, $id = null) {
		$vars = 'post';	// var names to set (string/array)
		$post = $this->request->data;	// POST データ
		$username = $this->Auth->user('username');	// ログインユーザー名

		if ($this->request->is('post')) {	// Method == POST
			// 引数
			if (!$action) $action = $this->request->data('action');
			if (!$id)     $id     = $this->request->data('group_id');
			if (!$id)     return;

			// action による振り分け
			// .. ユーザーがグループを編集可能かどうかチェック
			if (!$this->Group->is_editable($id, $username)) {
				// ...
			} else if ($action == 'edit') {
				$this->Group->saveByLite($post, $id);
			} else if ($action == 'delete') {
				$this->Group->saveAsDelete($id);
			} else if ($action == 'restore') {
				$this->Group->saveAsActive($id);
			}
		}
		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}
}
