<?php
App::uses('AdminController', 'Controller');

class LoginController extends AdminController {
	protected $autoLogin = false;
	public $autoLayout = false;
	public $uses = array('User');

	// run before each action
	public function beforeFilter() {
		parent::beforeFilter();

		if (Configure::read('ActiveDirectory')) {
			$this->Auth->authenticate = array('Ldap');
		}
		$this->Auth->allow('index', 'logout');
	}

	public function index () {
		$this->response->disableCache();	// no cache

		// 現在のパラメータ
		$username = $this->request->data('User.username')
		         ?: $this->Auth->user('username');	// ユーザー名
		$password = $this->request->data('User.password');	// パスワード
		$language = $this->request->data('lang')
		         ?:($this->Session->read('Config.language')
		            ?:   Configure::read('Config.language'));	// 言語

		// Auth Login
		if ($this->request->is('post') and $username and $password) {
			if ($this->Auth->login()) {	// 認証
				// Active Directory のとき、ユーザー情報を保存する
				if (Configure::read('ActiveDirectory')) {
					$this->User->saveByLDAP($this->Auth->user(), $username);
				}
				// トップページへ
				$this->redirect($this->Auth->redirect());
			} else {
				$error = $this->Session->read('error')
				      ?: l('_wrg_id_pass');
				$this->Session->delete('error');
				$this->Flash->warn($error);
			}
		}
		// ログイン認証不可
		else if ($this->request->is('post')) {
			$this->Flash->warn(
				$username ? l('_ipt_pass')
				          : l('_ipt_id_pass')
			);
		}
		// Logout
		else {
			$this->Auth->logout();
		}
		// View にセット
		$this->set(compact('username', 'language'));	// ユーザー名, 言語
	}

	// ログアウトでリダイレクト
	public function logout() {
		$this->redirect( $this->Auth->logout() );
	}
}
