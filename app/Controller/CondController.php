<?php
App::uses('AdminController', 'Controller');

class CondController extends AdminController {
	public $uses = array('Cond');

	// For Ajax
	public function ajax ($type = null) {
		$vars = 'data_cond';	// var names to set (string/array)

		// post data
		if (!$type) $type = $this->request->data('type');
		if (!$type) $type = $this->request->query('type');

		// get data
		$data_cond = $this->Cond->find('items', array(
			'conditions' => array('type' => $type),
		));

		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}

	// For Ajax
	public function ajax_select ($type = null) {
		$vars = 'json_keys';	// var names to set (string/array)

		// post data
		if (!$type) $type = $this->request->data('type');
		if (!$type) $type = $this->request->query('type');

		// データタイプを元に、JSON スキーマから各 JSON フィールド名の連想配列に
		$json_names = Util::getJsonKeysByType($type);
		$json_keys = array_keys( Hash::flatten($json_names) );

		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}
}
