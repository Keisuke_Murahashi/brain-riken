<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	protected $autoLogin = false;
	public $uses = array('User', 'Lang', 'Config', 'Message', 'AccessLog', 'Transaction');

	// run before each action
	public function beforeFilter() {
		parent::beforeFilter();

		// 言語設定切り替え
		$lang = $this->request->data('lang')
		     ?: $this->request->query('lang');
		if ($lang) {
			$this->Session->write('Config.language', $lang);	// 言語
		}

		// 言語ファイル読み込み
		if (Configure::check('Lang')) {
			$lang = Configure::read('Lang');
		} else {
		    $lang = $this->Lang->find('items', array('as' => 'key'));
		    Configure::write('Lang', $lang);
		}
		$this->set('Lang', $lang);

		// 設定ファイル読み込み
		if (Configure::check('Site')) {
			$config = Configure::read('Site');
		} else {
			$config = array();
		    $config_items = $this->Config->find('items', array('fields' => array('key', 'value')));
		    foreach ($config_items as $item) {
		    	$config[ $item['key'] ] = is_numeric($item['value']) ?
		    	                              intval($item['value']) : $item['value'];
		    }
		    Configure::write('Site', $config);
		}
		$this->set('Site', $config);
	}
}
