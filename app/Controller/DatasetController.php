<?php
App::uses('AdminController', 'Controller');
App::import('Vendor', 'JsonValidator');

class DatasetController extends AdminController {
	// NIJC public $uses = array('Dataset', 'MongoData', 'UserAuth', 'Share', 'DataType', 'DataFilter');
	public $uses = array('Dataset', 'MongoData', 'UserAuth', 'Share', 'DataType', 'DataFilter', 'Group');

	// POST 通信のとき、振り分け
	protected function _post ($id = null) {
		if (isset($this->request->data['update'])) $this->update($id);

		// リダイレクト
		$this->_redirect($id ? 'detail' : 'index', $id);
	}

	// データ一覧
	public function index ($id = null) {
		$username = $this->Auth->user('username');	// ログインユーザー名
//		print $username;

		// POST 通信のとき
		if ($this->request->is('post')) {
			return $this->_post($id);
		}

		// ID 指定があるときは page へ
		if (!$id) $id = $this->request->param('id');
		if ( $id) return $this->detail($id);

		// 所属グループに共有されたデータセット (Mongo データも自動連係で取得)
		$data_shared = $this->Dataset->findWithShared('items', array(
			'username' => $username,
			'order' => array('Dataset.upd_date' => 'desc'),
		));

		$this->set('data_shared', $data_shared);	// View にセット
	}

	// データ詳細
	public function detail ($id = null) {
		if (!$id) $this->_redirect();	// リダイレクト

		// var names to set (string/array)
		$vars = array('data_item', 'data_shared',
						'is_editable', 'is_app', 'error_message');

		$group_id = $this->getGroupId();	// ログインユーザーのグループID (短縮)

		// データセット (Mongo データも自動連係で取得)
		$data_item = $this->Dataset->findById($id);

		// 編集に不要な MongoDB 固有項目を削除
		foreach (array('_id', 'created', 'modified') as $key) {
			unset($data_item['Mongo'][$key]);
		}

		// データセットツリーを設定 (パス付)
		$data_item['DatasetTree'] =
			Util::scandir_recursive($this->Dataset->getDatasetDir($id), true);

		// ディレクトリパスを設定
		$data_item['DirName'] = $this->Dataset->getDatasetDir($id);

		// データが申請中/承認済であれば、表示するデータにフィルターをかける
		/*
		if ($this->Dataset->is_app($data_item)) {
			// 公開可能なデータをフィルタリング (データフィルター)
		// This is for Release, show all contents for  
			$data_item['MongoSafety'] = $this->DataFilter->filterOn($group_id, $data_item['data_type'], $data_item['Mongo']);
		} else {
		*/
			$data_item['MongoSafety'] = $data_item['Mongo'];	// メタデータをコピー
		// }

		// ユーザーがデータを編集可能かどうかチェック
		$is_editable = $this->Dataset->is_editable($data_item, $this->Auth->user('username'));

		// データが申請中/承認済かどうかチェック
		$is_app = $this->Dataset->is_app($data_item);

		// 共有先グループ名を取得
		// NIJC to retrieve shared group names 
		// NIJC $data_shared = $this->Share->getSharedGroup($id);
                foreach ($data_item['AppData'] as $groupinf) {
                        $group = $this->Group->findById($groupinf['group']);
			$data_shared[] = $group['name'];
		}
		
		// メタデータのバリデーション (返り値はエラーメッセージ)
		$error_message = $this->_metadata_validation($id, $data_item);

		// データにダウンロード API 用の設定をセット
		// .. (外部グループ間共有かどうか (共有数 > 1))
		$this->Common->set_download_info($data_item, count($data_shared) > 1);

		// View に変数をセット
		$this->set(compact($vars));
		$this->render('detail');	// set template
	}

	// メタデータ編集フォーム
	public function template ($id = null) {
		if (!$id) $this->_redirect();	// リダイレクト
		$data_id = $id;

		// var names to set (string/array)
		$vars = array('data_id', 'data_item', 'data_type', 'data_mongo',
						'data_json', 'json_scheme', 'error_message');

		// POST 通信のとき
		if ($this->request->is('post')) {
			return $this->_post($id);
		}

		// データセット (Mongo データも自動連係で取得)
		$data_item = $this->Dataset->findById($id);

		// ユーザーがデータを編集可能かどうかチェック
		if (!$this->is_editable_dataset($data_item)) return $this->_redirect('detail', $id);

		// MongoDB メタデータ
		$data_mongo = $data_item['Mongo'];
/*
		$data_mongo = $this->MongoData->find('item', array(
			'conditions' => array('data_id' => $id),
		));
*/
		// メタデータが見つからないとき
		if (empty($data_mongo)) {
			$this->Flash->error(sprintf(l('_no_meta'), $id));
			return $this->_redirect('detail', $id);
		}

		// 編集に不要な MongoDB 固有項目を削除
		foreach (array('_id', 'created', 'modified') as $key) {
			unset($data_mongo[$key]);
		}

		// JSONエンコード
		$data_json = json_encode($data_mongo, true);

		// データタイプ
		$data_type = $data_mongo['data_type'] ?: $data_item['data_type'];

		// JSON スキーマのファイル内容を取得
		$json_scheme = $this->DataType->getJsonSchemeContents($data_type);

		// メタデータのバリデーション (返り値はエラーメッセージ)
		$error_message = $this->_metadata_validation($id, $data_item);

		// View に変数をセット
		$this->set(compact($vars));
	}

	public function add_new ($data_type = null) {
		if (!$this->request->is('post')) $this->_redirect();	// リダイレクト

		// var names to set (string/array)
		$vars = array('data_id', 'data_item', 'data_type', 'data_mongo',
						'data_json', 'json_scheme', 'error_message');
		$data_id = null;
		$data_item = null;
		$error_message = null;
		$username = $this->Auth->user('username');	// ログインユーザー名

		// データタイプ
		if (!$data_type) $data_type = $this->request->data('data_type');

		// POST 通信のとき
		if ($this->request->is('post') && isset($this->request->data['update'])) {
			//ID取得して、データ登録
			$id = $this->Dataset->getNextID($data_type);
			return $this->_post($id);
		}

		// JSON データのファイル内容を取得 (初期値)
		$json = $this->DataType->getJsonDataContents($data_type);

		// 文字化けするかもしれないのでUTF-8に変換
		$json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');

		// オブジェクト毎にパース
		// trueを付けると連想配列として分解して格納
		$post = json_decode($json, true);

		// POST データから値を上書き
		//$post['dir_name']  = $this->request->data('dir_name');
		$post['data_type'] = $data_type;

		// NIJC(20160624): renamed uploader to submitter
		$post["submitter"] = $username;
		// "uploader" にユーザー名を付加
		//if (empty($post['uploader'])) {
		//	$post['uploader'] = array( array('name' => $username) );
		//} else if (empty($post['uploader'][0]['name'])) {
		//	$post['uploader'][0]['name'] = $username;
		//}

		//「id」を削除
		unset($post["_id"]);
		$data_json = json_encode($post, true);

		// JSON スキーマのファイル内容を取得
		$json_scheme = $this->DataType->getJsonSchemeContents($data_type);

		// View に変数をセット
		$this->set(compact($vars));
		$this->render('template');
	}


	// リダイレクト
	protected function _redirect ($action = 'index', $id = null) {
		// トップページに遷移
		$this->redirect(array('controller' => 'Dataset', 'action' => $action, $id), 302);
		exit;
	}

	// リダイレクト (MyDataset へ)
	protected function _my_redirect ($action = 'index') {
		// トップページに遷移
		$this->redirect(array('controller' => 'MyDataset', 'action' => $action), 302);
		exit;
	}

	// update Dataset (※data_id の変更はしない)
	public function update ($id = null) {
		// debug print
//		Util::debug($this->request, '_json_update.txt');

		$username = $this->Auth->user('username');	// ログインユーザー名
		$group_id = $this->getGroupId();	// ログインユーザーのグループID (短縮)
		$owner_name = $username;	// データセットオーナー (ログインユーザー)

		if (!$id) $this->request->data('data_id');
		if (!$id) $this->_my_redirect();	// リダイレクト

		// POSTからJSONを読み込み
		$json = $this->request->data('json');
		if (!$json) $this->_my_redirect();	// なければリダイレクト

		$json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
		$post = json_decode($json, true);	// データの整形

		// メタデータ (JSON) がデコードできなかったエラー
		if (!$post) {
			$this->Flash->error(l('_bad_meta_format'));
			$this->_my_redirect();
			exit;
		}

		// 現在のデータ
		$old_data = $this->Dataset->findById($id, array('recursive' => -1));
		// NIJC(20160628)
		// $old_ori_dir_name = $this->Dataset->getOriginalDir($id);
		$old_ori_dir_name = $old_data ? $old_data['original_dir_name'] : '';
		$old_dir_name = $old_data ? $old_data['dir_name'] : '';

		// 新規レコード？
		$is_new = !$old_data;

		// 編集の場合
		if (!$is_new) {
			// ユーザーがデータを編集可能かどうかチェック
			if (!$this->is_editable_dataset($id)) return $this->_redirect('detail', $id);

			// データの Owner をデータセットオーナーとする (変更しない)
			$owner_name = $this->Dataset->getOwner($id);
		}
		// 新規の場合は編集可能かのチェックは不要
		else {
//			$id = $this->Dataset->getNextID($this->request->data['data_type']);
		}

		// JSON からデータ登録
		// NIJC(20160628)
		//$result = $this->Dataset->saveByJSON($json, $owner_name, $id);
		$result = $this->Dataset->saveByJSON($json, $owner_name, $id, $old_dir_name);

		// 共有データの保存
		if ($result and $is_new) {
			$result = $this->Share->save(array(
				'data_id' => $id,
				'group'   => $group_id,
			));
		}

		// 成功していたら、ファイル転送API と連携 (データセットディレクトリの更新があれば)
		if ($is_new and $result and !empty($post['dir_name']) and $post['dir_name'] != $old_ori_dir_name) {
			// 自グループ内共有としてデータセットディレクトリを移動
			$is_success = $this->_move_dir_intragroup($id, $post['dir_name'], $old_ori_dir_name);

			if ($is_success) {
				$this->Flash->info(sprintf(l('_upd_meta'), $id) ."\n". l('_trans_dataset'));
			} else {
				$this->Flash->warn(sprintf(l('_upd_meta'), $id) ."\n". l('_fail_trans_dataset'));
			}
		}
		// データセットディレクトリの設定がなければ注意付きで正常終了
		else if ($result) {
			$this->Flash->info(sprintf(l('_upd_meta'), $id) ."\n".
					($is_new ? l('_no_dataset_dir_set') : ''));
		}
		// データベース設定エラー
		else {
			$this->Flash->error(sprintf(l('_err_ocur_upd_meta'), $id));
		}
		// Myアセットに移動
		$this->_my_redirect();
	}

	// Ajax で メタデータのバリデーション
	public function ajax_validation ($action = null, $id = null) {
		$vars = 'result';	// var names to set (string/array)
		$result = array();

		// Data-ID
		if (!$id) $id = $this->request->data('id');
		if (!$id) $id = $this->request->query('id');

		// データセット (Mongo データも自動連係で取得)
		$data_item = $this->Dataset->findById($id);

		// メタデータのバリデーション (返り値はエラーメッセージ)
		$error = $this->_metadata_validation($id, $data_item);

		// 出力する結果
		$result = array(
			'code'    => $error ? 403 : 200,
			'status'  => $error ? 'error' : 'success',
			'message' => $error ?: 'valid!',
		);
		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}


	// メタデータのバリデーション (返り値はエラーメッセージ)
	public function _metadata_validation ($id, $data_item = null) {
		$error = array();

		// データセット (Mongo データも自動連係で取得)
		if (empty($data_item)) $data_item = $this->Dataset->findById($id);

		// データが見つからないとき
		if (empty($data_item) or empty($data_item['data_type'])) {
			return sprintf(l('_no_data'), $id);
		}

		// ユーザーがデータを編集可能かどうかチェック
		if (!$this->Dataset->is_editable($data_item, $this->Auth->user())) {
//			return sprintf('[%s] このデータを編集する権限がありません', $id);
		}
		// データが申請中/承認済かどうかチェック
		if ($this->Dataset->is_app($data_item)) {
//			return sprintf('[%s] 既に共有/公開状態が設定されているため、このデータを処理できません', $id);
		}

		// メタデータが見つからないとき
		if (empty($data_item['Mongo'])) {
			return sprintf(l('_no_meta'), $id);
		}

		// データタイプ
		$data_type = $data_item['data_type'];

		// JSON スキーマのファイル内容を取得
		$json_scheme = $this->DataType->getJsonSchemeContents($data_type);

		// スキーマが見つからないとき
		if (empty($json_scheme)) {
			return sprintf(l('_no_json'), $data_type);
		}

		// MongoDB メタデータ
		$data_mongo = $data_item['Mongo'];

		//「id」を削除してから JSON string に
		// NIJC(20160624)
		//unset($data_mongo["_id"]);
		foreach (array('_id', 'created', 'modified') as $key) {
                        unset($data_mongo[$key]);
                }
		$data_json = json_encode($data_mongo, true);

		// バリデーション
		$valid_error = JsonValidator::error($data_json, $json_scheme);
		if ($valid_error) $error = array_merge($error, $valid_error);

		// Fixed→「False」or「」の場合は、申請不可
		if (empty($data_item['Mongo']['fixed']) or ($data_item['Mongo']['fixed'] == 'false')) {
			$error[] = array(
				'property'   => 'fixed',
				'constraint' => 'false',
				'format'     => 'boolean',
				'message'    => sprintf(l('_not_fixed_meta'), $id)
			);
		}
		return $error ? : null;
	}

        // NIJC(20160627): forward download request to ssb server
        public function download() {
                // check method(post only)
                if (!$this->request->is('post')) {
                        $this->_redirect();
                }

                // stop auto rendering
                $this->autoRender = false;

                // forward
                $url = $this->request->data['download_url'];
                unset($this->request->data['download_url']);
                $result = curl_post_contents($url, $this->request->data);

                // respond
                $this->response->type('application/x-java-jnlp-file');
                $this->response->body($result);
        }
}
