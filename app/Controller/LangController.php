<?php
App::uses('AdminController', 'Controller');

class LangController extends AdminController {
	public $uses = array();

	// For Ajax
	public function ajax () {
		$vars = 'data_lang';	// var names to set (string/array)

		// get data
		$lang = lang();
		$data_lang = array();
		$lang_items = $this->Lang->find('items', array(
			'fields' => array('key', $lang),
		));
	    foreach ($lang_items as $item) {
	    	$data_lang[ $item['key'] ] = $item[$lang];
	    }

		// serialize
		$this->set(compact($vars));
		$this->set('_serialize', $vars);
	}
}
