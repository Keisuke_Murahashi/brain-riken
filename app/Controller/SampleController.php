<?php
App::uses('AdminController', 'Controller');
App::import('Vendor', 'Util');
App::import('Vendor', 'JsonValidator');

class SampleController extends AdminController {
	public $autoLayout = false;
	public $uses = array();

	public function index () {
		$this -> autoRender = false;
		print "サンプルだよ";
	}

	// sample - JSON Validator
	public function json () {
		$this -> autoRender = false;
		print "<pre>[JSON Validator] checking\n\n ...";

		$json = <<<__JSON__
{
	"data_id": 123456,
	"subject": "test subject",
	"creator": [ { "name" : "test-san" } ],
	"date" : ["2016-03-20"]
}
__JSON__;

		// JSON Schems を"schema.json"に記述しておく
		$schema_file = WWW_ROOT .'lib/json/scheme/01.schema_t1wi_160324.json';
		$schema = file_get_contents($schema_file);

		// Check Error
		$error_messages = JsonValidator::error($json, $schema);

		// is Error ?
		if ($error_messages) {
			print "\n\n[Error]\n\n";
			print_r($error_messages);
		} else {
			print "\n\n[Valid]";
		}

		print "\n\n[JSON-DATA] (sample test)\n\n";
		print_r(json_decode($json));

		print "\n\n[JSON-SCHEMA] ($schema_file) \n\n";
		print_r(json_decode($schema));

		print "\n\n";
		print "complete";

	}


	// 公開情報を POST 送信されたときの情報をファイルに保存
	public function post_public () {
		$output = '_test_post_public.txt';
		Util::debug($this->request->data, $output);
		print '[post_public] POST data saved to "'. $output .'"';
		exit;
	}
}
