ALTER TABLE `app` ADD `delflg` INT(1) NOT NULL DEFAULT '0' ; // 公開申請の重複対応
ALTER TABLE `share` ADD `delflg` INT(1) NOT NULL DEFAULT '0' ; // 共有の取り消し対応
ALTER TABLE `app` CHANGE `group` `group` VARCHAR(1000) NULL DEFAULT NULL; 