-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016 年 2 朁E15 日 09:31
-- サーバのバージョン： 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test2`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `publish`
--

CREATE TABLE IF NOT EXISTS `publish` (
  `app_id` varchar(1000) NOT NULL,
  `data_id` varchar(1000) NOT NULL,
  `theme` varchar(1000) NOT NULL,
  `experiment` varchar(1000) NOT NULL,
  `dataset` varchar(1000) NOT NULL,
  `status` varchar(1000) NOT NULL,
  `date` date NOT NULL,
  `target` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `publish`
--

--INSERT INTO `publish` (`app_id`, `data_id`, `theme`, `experiment`, `dataset`, `status`, `date`, `target`) VALUES
--('00001', 't1wi_000002', 'theme1', 'experiment1', 'experiment1', '申請中', '2016-02-15', 'user_a'),
--('00011', 't1wi_000003', 'theme3', 'experiment3', 'experiment3', '申請中', '2016-02-15', 'user_a'),
--('00009', '', '', '', '', '承認済', '0000-00-00', ''),
--('00012', 't1wi_000004', 'theme2', 'experiment2', 'experiment2', '承認済', '2016-02-15', 'user_a'),
--('00013', 't1wi_000005', 'theme1', 'experiment1', 'experiment1', '棄却', '2016-02-15', 'user_a');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
