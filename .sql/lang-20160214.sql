-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016 年 2 朁E14 日 17:30
-- サーバのバージョン： 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test2`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `lang`
--

DROP TABLE IF EXISTS `lang`;
CREATE TABLE IF NOT EXISTS `lang` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(64) NOT NULL,
  `ja` varchar(255) DEFAULT NULL,
  `en` varchar(255) DEFAULT NULL,
  `upd_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- テーブルのデータのダンプ `lang`
--

INSERT INTO `lang` (`lang_id`, `key`, `ja`, `en`, `upd_date`) VALUES
(1, 'title_top', 'トップ', 'Top page', '2016-02-14 15:48:20'),
(2, 'title_myasset', 'Myアセット', 'My Assets', '2016-02-14 15:48:20'),
(3, 'title_user', 'ユーザー/グループ管理', 'User Management', '2016-02-14 15:50:34'),
(4, 'title_app', '申請/承認', 'Application', '2016-02-14 15:50:34'),
(5, 'title_message', 'メッセージ管理', 'Messages', '2016-02-14 15:51:42'),
(6, 'title_archive', '受信メッセージ', 'Archives', '2016-02-14 15:51:42'),
(7, 'title_dataset', '共有されたデータセット', 'Shared Dataset', '2016-02-14 15:53:11'),
(8, 'title_message_detail', 'メッセージ詳細', 'Message Detail', '2016-02-14 15:53:11');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
