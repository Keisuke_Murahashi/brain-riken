INSERT INTO `test2`.`lang` (`key`, `ja`, `en`, `page`) VALUES ('_error_edit_user_info_for_system_admin', 'システム管理者のユーザー情報は変更できません。', 'You will not be able to change the user information for the system administrator. ', 'user managements');
INSERT INTO `test2`.`lang` (`key`, `ja`, `en`, `page`) VALUES ('_error_change_group_with_system_admin', 'グループを変更できるのはシステム管理者のみです。', 'Only the system administrator can change the group.', 'user managements');
INSERT INTO `test2`.`lang` (`key`, `ja`, `en`, `page`) VALUES ('_error_change_authority_as_system_admin', '管理権限をシステム管理者に変更できるのはシステム管理者のみです。', 'Only the system administrator can change the system authority as the system administrator.', 'user managements');
INSERT INTO `test2`.`lang` (`key`, `ja`, `en`, `page`) VALUES ('_error_change_user_info', 'ユーザー情報の変更に失敗しました。エラー内容を確認し、再度お試しください。', 'Failed to change the user information . Check the error contents, please try again.', 'user managements');

