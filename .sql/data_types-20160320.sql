-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016 年 3 朁E21 日 18:46
-- サーバのバージョン： 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test2`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `data_types`
--

DROP TABLE IF EXISTS `data_types`;
CREATE TABLE IF NOT EXISTS `data_types` (
  `data_type` varchar(100) NOT NULL,
  `json_scheme` varchar(255) DEFAULT NULL,
  `json_data` varchar(255) DEFAULT NULL,
  `ordered` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`data_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `data_types`
--

INSERT INTO `data_types` (`data_type`, `json_scheme`, `json_data`, `ordered`) VALUES
('Document', '06.schema_document_160228.json', '06.data_document_160306.json', 6),
('DWI', '03.schema_dwi_160228.json', '03.data_dwi_160306.json', 3),
('Program_Tool', '07.schema_program_tool_160228.json', '07.data_program_tool_160306.json', 7),
('rs-fMRI', '04.schema_rs-fmri_160228.json', '04.data_rs-fmri_160306.json', 4),
('T1WI', '01.schema_t1wi_160221.json', '01.data_t1wi_160306.json', 1),
('T2WI', '02.schema_t2wi_160228.json', '02.data_t2wi_160306.json', 2),
('Tracer', '05.schema_tracer_160228.json', '05.data_tracer_160306.json', 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
