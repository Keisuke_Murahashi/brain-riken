INSERT INTO `lang`(`key`, `ja`, `en`, `page`) VALUES ("_title", 'タイトル', 'Title', 'search result');
INSERT INTO `lang`(`key`, `ja`, `en`, `page`) VALUES ("_content", 'コンテンツ', 'Contents', 'search result');
INSERT INTO `lang`(`key`, `ja`, `en`, `page`) VALUES ("_doc_name", 'ドキュメント名', 'DocumentName', 'search result');
INSERT INTO `lang`(`key`, `ja`, `en`, `page`) VALUES ("_download", 'ダウンロード', 'Download', 'search result');
INSERT INTO `lang`(`key`, `ja`, `en`, `page`) VALUES ("_doc_type", 'タイプ', 'DocumentType', 'search result');