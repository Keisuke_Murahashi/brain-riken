-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- ホスト: localhost
-- 生成日時: 2016 年 2 月 17 日 02:30
-- サーバのバージョン: 5.5.25a
-- PHP のバージョン: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- データベース: `test2`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `lang`
--

DROP TABLE IF EXISTS `lang`;
CREATE TABLE IF NOT EXISTS `lang` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(64) NOT NULL,
  `ja` varchar(255) DEFAULT NULL,
  `en` varchar(255) DEFAULT NULL,
  `page` varchar(100) DEFAULT NULL,
  `upd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- テーブルのデータのダンプ `lang`
--

INSERT INTO `lang` (`lang_id`, `key`, `ja`, `en`, `page`) VALUES
(1, '_site_title', 'Brain/MINDS Research Platform', 'Brain/MINDS Research Platform', 'header'),
(2, '_login', 'ログイン', 'login', 'header'),
(3, '_logout', 'ログアウト', 'logout', 'header'),
(4, '_settings', '設定', 'Settings', 'header'),
(5, '_help', 'ヘルプ', 'Help', 'header'),
(6, '_profile', 'ユーザー', 'Profile', 'header'),
(7, '_user_name', 'ユーザー名', 'User Name', 'header'),
(8, '_group_name', 'グループ名', 'Group Name', 'header'),
(9, '_home_dir', 'ホームディレクトリ', 'Home Dir', 'header'),
(10, '_language', '言語', 'Language', 'header'),
(11, '_top', 'トップ', 'Top', 'title'),
(12, '_my_assets', 'Myアセット', 'My Assets', 'title'),
(13, '_users_managements', 'ユーザー/グループ管理', 'User/Group Managements', 'title'),
(14, '_applications', '申請/承認', 'Applications', 'title'),
(15, '_my_messages', 'メッセージ管理', 'My Messages', 'title'),
(16, '_archives', '受信メッセージ', 'Archives', 'title'),
(17, '_shared_dataset', '共有されたデータセット', 'Shared Dataset', 'title'),
(19, '_received_messages', '受信メッセージ', 'Received Message', 'home'),
(20, '_user_message', '個人宛て通知', 'Personalized Messages', 'home'),
(21, '_portal_message', '皆様へのお知らせ', 'Portal Messages', 'home'),
(22, '_details_message', 'メッセージ詳細', 'Details of Message', 'home'),
(23, '_reply', '返信', 'Reply', 'home'),
(24, '_cancel', 'キャンセル', 'Cancel', 'home'),
(25, '_more', '続きを見る', 'more', 'home'),
(26, '_search', '検索', 'Search', 'home'),
(27, '_by_keyword', 'キーワード検索', 'by Keywords', 'home'),
(28, '_input_keyword', '（キーワード入力）', 'Keyword…', 'home'),
(29, '_by_conditions', '条件検索', 'by Conditions', 'home'),
(30, '_add_condition', '項目追加', 'Add this condition', 'home'),
(31, '_clear', 'クリア', 'Clear', 'home'),
(38, '_new_dataset', 'データセット追加', 'New Dataset', 'my assets'),
(39, '_upload_metadata', 'メタデータ登録', 'Upload Metadata', 'my assets'),
(40, '_apply_shared_dataset', 'データセット共有申請', 'Apply to Share Dataset', 'my assets'),
(41, '_apply_public_dataset', 'データセット公開申請', 'Apply to Publish Dataset', 'my assets'),
(42, '_withdraw_applications', '各種申請取り下げ', 'Withdraw Applications', 'my assets'),
(43, '_relate_dataset', 'データセット関連付け', 'Relate Dataset', 'my assets'),
(45, '_select_file', '（ファイル選択）', 'select file…', 'my assets'),
(46, '_upload', '登録', 'Upload', 'my assets'),
(47, '_dataset_list', 'データ一覧', 'Dataset List', 'my assets'),
(48, '_input_directory_name', '（フォルダ名を入力）', '(input Directory Name...)', 'my assets'),
(49, '_select_datatype', '（データタイプ選択）', '(select DataType)', 'my assets'),
(50, '_add_dataset', '新規追加', 'Add Dataset', 'my assets'),
(51, '_select_action', '（アクション選択）', '(select Action)', 'my assets'),
(52, '_run', '実行', 'Run', 'my assets'),
(53, '_relate_directory', 'ディレクトリ関連付け', 'Relate Directory', 'my assets'),
(55, '_relate_metadata_and_directory', '選択したメタデータとディレクトリを関連付けます。対象のディレクトリを選択してください。', 'You relate the selected metadata and the directory . Please select the target directory .', 'my assets directory'),
(56, '_target_data', '対象データ', 'Target Data', 'my assets directory'),
(57, '_target_directory', '対象ディレクトリ', 'Target Directory', 'my assets directory'),
(58, '_run_relate', '関連付け', 'Run Relation !', 'my assets directory'),
(60, '_apply_publication', '公開申請', 'Apply for Publication', 'my assets'),
(62, '_apply_publication_of_dataset', '選択したデータセットの公開を申請します。よろしいですか？', 'You apply for the publication of the selected data-set . is it OK?', 'my assets publish'),
(64, '_destination_to_publish', '公開先', 'Destination to Publish', 'my assets publish'),
(65, '_select_theme', '（テーマ選択）', '(select Theme)', 'my assets publish'),
(66, '_select_experiment', '（実験選択）', '(select Experiment)', 'my assets publish'),
(67, '_select_dataset', '（データセット選択）', '(select Dataset)', 'my assets publish'),
(68, '_authorizer', '承認者', 'Authorizer', 'my assets publish'),
(69, '_run_application', '申請', 'File Application !', 'my assets publish'),
(71, '_apply_share', '共有申請', 'Apply for Share', 'my assets'),
(73, '_share_application_of_dataset', '選択したデータセットの共有を申請します。よろしいですか？', 'You apply for the share of the selected data-set . is it OK?', 'my assets share'),
(76, '_comment', 'コメント', 'Comment', 'my assets share'),
(79, '_withdraw_application', '申請取り下げ', 'Withdraw Application', 'my assets'),
(81, '_withdraw_application_of_dataset', '選択したデータセットの申請を取り下げます。よろしいですか？', 'You withdraw the application of the selected data-set . is it OK?', 'my assets withdraw'),
(83, '_run_withdrawal', '取り下げ', 'Run Withdrawal', 'my assets withdraw'),
(85, '_set_template', 'テンプレート登録', 'Set as Template', 'my assets'),
(87, '_set_dataset_as_template', '選択したデータセットをテンプレートとして登録します。よろしいですか？', 'You register the selected data-set as a template . is it OK?', 'my assets template'),
(89, '_input_template_name', '（テンプレート名を入力）', '(input Template Name...)', 'my assets template'),
(90, '_run_template', '登録', 'Set Template !', 'my assets template'),
(92, '_show_records', '表示件数', 'Show', 'my assets'),
(93, '_ectries', '件', 'entries', 'my assets'),
(95, '_showing_records', ' ', 'Showing', 'my assets'),
(96, '_to', '～', 'to', 'my assets'),
(97, '_of', '/', 'of', 'my assets'),
(99, '_previows', '前へ', 'Previous', 'my assets'),
(100, '_next', '次へ', 'Next', 'my assets'),
(102, '_user_managements', 'ユーザー管理', 'User Managements', 'user managements'),
(112, '_group_managements', 'グループ管理', 'Group Managements', 'user managements'),
(113, '_input_group_name', '（グループ名入力）', '(input GroupName)', 'user managements'),
(114, '_input_detail', '（説明入力）', '(input Detail)', 'user managements'),
(115, '_new_group', '新規追加', 'New Group', 'user managements'),
(117, '_valid', '有効', 'Valid', 'user managements'),
(118, '_invalid', '無効', 'Invalid', 'user managements'),
(129, '_details_group', 'グループ詳細', 'Details of Group', 'group detail'),
(130, '_users_list', 'ユーザー一覧', 'Users List', 'group detail'),
(131, '_group_list', 'グループ一覧', 'Group List', 'group detail'),
(133, '_applications_list', '申請一覧', 'Applications List', 'apprications'),
(135, '_approve', '承認', 'Approve', 'apprications'),
(136, '_reject', '棄却', 'Reject', 'apprications'),
(147, '_approved_history', '承認履歴', 'Approved History', 'apprications'),
(152, '_details_application', '申請詳細', 'Details of Application', 'apprications'),
(153, '_run_confirm', '確認', 'Confirm', 'apprications'),
(154, '_approve_application', '申請承認', 'Approve the Application', 'apprications'),
(155, '_approve_selected_application', '選択した申請を承認します。よろしいですか？', 'You approve the selected application . is it OK?', ''),
(157, '_run_approval', '承認', 'Run Approval !', 'apprications'),
(158, '_reject_application', '申請棄却', 'Reject the Application', 'apprications'),
(159, '_reject_selected_application', '選択した申請を棄却します。よろしいですか？', 'You reject the selected application . is it OK?', ''),
(161, '_run_reject', '棄却', 'Run Rejection', 'apprications'),
(172, '_create_message', 'メッセージ作成', 'Create Message', 'my messages'),
(173, '_select_message_type', '（メッセージタイプ選択）', '(select Message Type)', 'my messages'),
(176, '_select_category', '（カテゴリー選択）', '(select Category)', 'my messages'),
(177, '_news', 'お知らせ', 'News', 'my messages'),
(178, '_message', 'メッセージ', 'Messege', 'my messages'),
(179, '_submit', '送信', 'Submit', 'my messages'),
(180, '_message_title', 'タイトル', 'Title', 'my messages'),
(181, '_target_user', '送信先ユーザー', 'Target Users', 'my messages'),
(182, '_message_body', 'メッセージ本文', 'Body', 'my messages'),
(183, '_past_messages', 'メッセージ送信履歴', 'Past Messages List', 'my messages'),
(207, '_year', '年', '/', 'archives'),
(208, '_month', '月', '', 'archives'),
(229, '_details_dataset', 'データセット詳細', 'Details of Dataset', 'details dataset'),
(230, '_details_metadata', 'メタデータ詳細', 'Details of MetaData', 'details dataset'),
(231, '_no_metadata', 'このデータセットに関連したメタデータはありません。', 'There is no metadata related with this data-set', 'details dataset'),
(232, '_base_uri', '基本フォルダ', 'Base URI', 'details dataset'),
(233, '_ftp', 'FTP', 'FTP', 'details dataset'),
(234, '_dataset_tree', 'データセットツリー ', 'Dataset Tree', 'details dataset'),
(235, '_no_dataset_file', 'このデータセットに関連したファイルはありません。', 'Dataset File is None', 'details dataset'),
(236, '_edit_metadata', 'メタデータ編集', 'Edit Metadata', 'json editor'),
(237, '_json_editor', 'JSON 編集フォーム', 'JSON Editor', 'json editor'),
(238, '_editor_from_json_schema', '以下は、 JSONスキーマから生成されたエディタです。', 'Below is the editor generated from the JSON Schema.', 'json editor'),
(239, '_json_schema', 'JSON スキーマ', 'Schema', 'json editor'),
(240, '_form_changes_by_schema', 'JSON スキーマを変更すると、それに合わせて編集フォームが生成されます。変更後、右のボタンをクリックします。', 'You can change the schema and see how the generated form looks. After you make changes, click…', 'json editor'),
(241, '_update_schema', 'スキーマを更新', 'Update Schema', 'json editor'),
(242, '_direct_link', 'Direct Link', 'Direct Link', ''),
(243, '_cache_schema_options', '（スキーマ、値、およびオプションを保存します）', ' (preserves schema, value, and options)', ''),
(244, '_json_output', 'JSON 出力', 'JSON Output', 'json editor'),
(245, '_form_edit_json_directly', '下記のフォームからでも、JSONデータを変更し、フォームに値を設定することができます', 'You can also make changes to the JSON here and set the value in the editor by clicking', 'json editor'),
(246, '_update_form', 'フォームを更新', 'Update Form', 'json editor'),
(247, '_update_data', 'データの保存', 'Update Data', 'json editor'),
(248, '_options', 'オプション', 'Options', 'json editor'),
(249, '_validation', '値の検証', 'Validation', 'json editor'),
(250, '_show_valudation_error', 'フォームの値に変更があった際、もし検証エラーがあればその内容を表示します。', 'This will update whenever the form changes to show validation errors if there are any.', 'json editor'),
(253, '_password', 'パスワード', 'Password', 'login'),
(254, '_cache_cookie', 'クッキーに保存', 'Remember me', 'login'),
(255, '_sign_in', 'ログイン', 'Sign in', 'login'),
(256, '_search_result', '検索結果', 'Search Result', 'search result'),
(257, '_metadata', 'メタデータ', 'Metadata', 'search result'),
(258, '_static_pages', '固定ページ', 'Static Pages', 'search result'),
(259, '_documents', 'ドキュメント', 'Documents', 'search result'),
(260, '_detail', '詳細', 'Detail', 'status'),
(261, '_new', '新規', 'New', 'status'),
(262, '_edit', '編集', 'Edit', 'status'),
(263, '_delete', '削除', 'Delete', 'status'),
(264, '_share', '共有', 'share', 'status'),
(265, '_public', '公開', 'public', 'status'),
(266, '_private', '非公開', 'private', 'status'),
(267, '_applying', '申請中', 'applying', 'status'),
(268, '_app', 'app', 'app', 'status'),
(269, '_dataset', 'dataset', 'detaset', 'status'),
(270, '_close', '閉じる', 'Close', 'status'),
(271, 'News', 'お知らせ', 'News', 'var name'),
(272, 'Message', 'メッセージ', 'Message', 'var name');


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
