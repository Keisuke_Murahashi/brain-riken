-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- ホスト: localhost
-- 生成日時: 2016 年 2 月 25 日 06:08
-- サーバのバージョン: 5.5.25a
-- PHP のバージョン: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- データベース: `test2`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `app`
--

DROP TABLE IF EXISTS `app`;
CREATE TABLE IF NOT EXISTS `app` (
  `app_id` int(11) NOT NULL AUTO_INCREMENT,
  `data_id` varchar(100) DEFAULT NULL,
  `app_type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `creator` varchar(100) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `detail` varchar(1000) DEFAULT NULL,
  `authorizer` varchar(100) DEFAULT NULL,
  `group` int(11) DEFAULT NULL,
  PRIMARY KEY (`app_id`),
  KEY `data_id` (`data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- テーブルのデータのダンプ `app`
--

INSERT INTO `app` (`app_id`, `data_id`, `app_type`, `status`, `creator`, `date`, `detail`, `authorizer`, `group`) VALUES
(1, 't1wi_000001', '共有', '申請中', 'user_a', '2016-02-21 10:01:50', 'comment', 'user_b', 2),
(2, 't1wi_000002', '共有', '承認済', 'user_b', '2016-02-21 15:02:44', 'abc', 'user_a', 1);

-- --------------------------------------------------------

--
-- テーブルの構造 `app_history`
--

DROP TABLE IF EXISTS `app_history`;
CREATE TABLE IF NOT EXISTS `app_history` (
  `act_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` int(11) NOT NULL,
  `authorizer` varchar(100) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `comment` varchar(1000) DEFAULT NULL,
  `act_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`act_id`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- テーブルのデータのダンプ `app_history`
--

INSERT INTO `app_history` (`act_id`, `app_id`, `authorizer`, `action`, `comment`, `act_date`) VALUES
(1, 1, 'user_a', '承認', 'aaa', '2016-02-01 15:00:00'),
(2, 2, 'user_a', '承認', 'abc', '2016-02-02 15:00:00'),
(3, 3, 'user_a', '承認', 'aaaaaa', '2016-02-04 15:00:00');

-- --------------------------------------------------------

--
-- テーブルの構造 `group`
--

DROP TABLE IF EXISTS `group`;
CREATE TABLE IF NOT EXISTS `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `detail` varchar(1000) DEFAULT NULL,
  `admin` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `upd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- テーブルのデータのダンプ `group`
--

INSERT INTO `group` (`id`, `detail`, `admin`, `name`, `upd_date`, `status`) VALUES
(1, 'Group A', 'FALSE', 'group_a', '2016-02-13 15:00:00', 'active'),
(2, 'Group B', 'FALSE', 'group_b', '2016-01-04 15:00:00', 'active'),
(3, 'Group C', 'TRUE', 'group_c', '2016-01-23 15:00:00', 'deleted');

-- --------------------------------------------------------

--
-- テーブルの構造 `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `mess_id` int(11) NOT NULL AUTO_INCREMENT,
  `mess_type` varchar(100) DEFAULT NULL,
  `title` varchar(1000) DEFAULT NULL,
  `content` varchar(4000) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `author` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`mess_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- テーブルのデータのダンプ `message`
--

INSERT INTO `message` (`mess_id`, `mess_type`, `title`, `content`, `date`, `author`, `category`) VALUES
(1, 'user', ' データ登録のお知らせ', ' データ登録のお知らせ\r\n データ登録のお知らせ\r\n データ登録のお知らせ', '2016-02-01 15:00:00', 'user_a', 'News'),
(2, 'portal', ' test', 'test', '2016-02-02 15:00:00', 'user_a', 'Message'),
(3, 'user', 'お知らせテスト', 'お知らせテストだよ\r\n\r\nuser_a\r\nuser_b\r\n\r\nにターゲット', '2016-02-09 15:00:00', 'user_a', 'News'),
(4, 'user', 'RE: お知らせテスト', '返信テストだよ\r\n\r\n-----Original Message-----\r\nFrom : user_a\r\nSent : 2016-02-10\r\n\r\nお知らせテストだよ\r\n\r\nuser_a\r\nuser_b\r\n\r\nにターゲット', '2016-02-16 15:00:00', 'user_a', 'News'),
(5, 'portal', 'RE: お知らせテスト 皆様', '皆様への返信テストだよ\r\n\r\n-----Original Message-----\r\nFrom : user_a\r\nSent : 2016-02-10\r\n\r\nお知らせテストだよ\r\n\r\nuser_a\r\nuser_b\r\n\r\nにターゲット', '2016-02-16 15:00:00', 'user_a', 'News'),
(6, 'user', ' データ登録のお知らせ', ' データ登録のお知らせ\r\n データ登録のお知らせ\r\n データ登録のお知らせ', '2015-12-31 15:00:00', 'user_a', 'News'),
(7, 'portal', ' test', 'test', '2016-01-02 15:00:00', 'user_a', 'Message'),
(8, 'user', 'お知らせテスト', 'お知らせテストだよ\r\n\r\nuser_a\r\nuser_b\r\n\r\nにターゲット', '2016-01-09 15:00:00', 'user_a', 'News'),
(9, 'user', 'RE: お知らせテスト', '返信テストだよ\r\n\r\n-----Original Message-----\r\nFrom : user_a\r\nSent : 2016-01-10\r\n\r\nお知らせテストだよ\r\n\r\nuser_a\r\nuser_b\r\n\r\nにターゲット', '2016-01-16 15:00:00', 'user_a', 'News'),
(10, 'portal', 'RE: お知らせテスト 皆様', '皆様への返信テストだよ\r\n\r\n-----Original Message-----\r\nFrom : user_a\r\nSent : 2016-01-10\r\n\r\nお知らせテストだよ\r\n\r\nuser_a\r\nuser_b\r\n\r\nにターゲット', '2016-01-16 15:00:00', 'user_a', 'News'),
(11, 'user', ' データ登録のお知らせ', ' データ登録のお知らせ\r\n データ登録のお知らせ\r\n データ登録のお知らせ', '2015-12-11 15:00:00', 'user_a', 'News'),
(12, 'portal', ' test', 'test', '2015-12-02 15:00:00', 'user_a', 'Message'),
(13, 'user', 'お知らせテスト', 'お知らせテストだよ\r\n\r\nuser_a\r\nuser_b\r\n\r\nにターゲット', '2015-12-09 15:00:00', 'user_a', 'News'),
(14, 'user', 'RE: お知らせテスト', '返信テストだよ\r\n\r\n-----Original Message-----\r\nFrom : user_a\r\nSent : 2015-12-10\r\n\r\nお知らせテストだよ\r\n\r\nuser_a\r\nuser_b\r\n\r\nにターゲット', '2015-12-16 15:00:00', 'user_a', 'News'),
(15, 'portal', 'RE: お知らせテスト 皆様', '皆様への返信テストだよ\r\n\r\n-----Original Message-----\r\nFrom : user_a\r\nSent : 2015-12-10\r\n\r\nお知らせテストだよ\r\n\r\nuser_a\r\nuser_b\r\n\r\nにターゲット', '2015-12-16 15:00:00', 'user_a', 'News'),
(16, 'user', ' データ登録のお知らせ', ' データ登録のお知らせ\r\n データ登録のお知らせ\r\n データ登録のお知らせ', '2015-02-01 15:00:00', 'user_a', 'News'),
(17, 'portal', ' test', 'test', '2015-02-02 15:00:00', 'user_a', 'Message'),
(18, 'user', 'お知らせテスト', 'お知らせテストだよ\r\n\r\nuser_a\r\nuser_b\r\n\r\nにターゲット', '2015-02-09 15:00:00', 'user_a', 'News'),
(19, 'user', 'RE: お知らせテスト', '返信テストだよ\r\n\r\n-----Original Message-----\r\nFrom : user_a\r\nSent : 2015-02-10\r\n\r\nお知らせテストだよ\r\n\r\nuser_a\r\nuser_b\r\n\r\nにターゲット', '2015-02-16 15:00:00', 'user_a', 'News'),
(20, 'portal', 'RE: お知らせテスト 皆様', '皆様への返信テストだよ\r\n\r\n-----Original Message-----\r\nFrom : user_a\r\nSent : 2015-02-10\r\n\r\nお知らせテストだよ\r\n\r\nuser_a\r\nuser_b\r\n\r\nにターゲット', '2015-02-16 15:00:00', 'user_a', 'News');

-- --------------------------------------------------------

--
-- テーブルの構造 `publish`
--

DROP TABLE IF EXISTS `publish`;
CREATE TABLE IF NOT EXISTS `publish` (
  `app_id` int(11) NOT NULL,
  `data_id` varchar(100) NOT NULL,
  `theme` varchar(1000) DEFAULT NULL,
  `experiment` varchar(1000) DEFAULT NULL,
  `dataset` varchar(1000) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `target` varchar(100) DEFAULT NULL,
  KEY `app_id` (`app_id`),
  KEY `data_id` (`data_id`),
  KEY `data_id_2` (`data_id`),
  KEY `app_id_2` (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `publish`
--

INSERT INTO `publish` (`app_id`, `data_id`, `theme`, `experiment`, `dataset`, `status`, `date`, `target`) VALUES
(1, 't1wi_000002', 'theme1', 'experiment1', 'experiment1', '申請中', '2016-02-14 15:00:00', 'user_a'),
(11, 't1wi_000003', 'theme3', 'experiment3', 'experiment3', '申請中', '2016-02-14 15:00:00', 'user_a'),
(12, 't1wi_000004', 'theme2', 'experiment2', 'experiment2', '承認済', '2016-02-14 15:00:00', 'user_a'),
(13, 't1wi_000005', 'theme1', 'experiment1', 'experiment1', '棄却', '2016-02-14 15:00:00', 'user_a');

-- --------------------------------------------------------

--
-- テーブルの構造 `share`
--

DROP TABLE IF EXISTS `share`;
CREATE TABLE IF NOT EXISTS `share` (
  `data_id` varchar(100) NOT NULL,
  `group` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `data_id` (`data_id`),
  KEY `group` (`group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `share`
--

INSERT INTO `share` (`data_id`, `group`, `date`) VALUES
('t2wi_000001', 1, '2016-02-13 15:00:00');

-- --------------------------------------------------------

--
-- テーブルの構造 `target`
--

DROP TABLE IF EXISTS `target`;
CREATE TABLE IF NOT EXISTS `target` (
  `mess_id` int(11) NOT NULL,
  `target` varchar(100) NOT NULL,
  KEY `mess_id` (`mess_id`),
  KEY `target` (`target`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `target`
--

INSERT INTO `target` (`mess_id`, `target`) VALUES
(1, 'user_a'),
(2, 'user_a'),
(3, 'user_a'),
(3, 'user_b'),
(4, 'user_a'),
(4, 'user_b');

-- --------------------------------------------------------

--
-- テーブルの構造 `user_auth`
--

DROP TABLE IF EXISTS `user_auth`;
CREATE TABLE IF NOT EXISTS `user_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `group` int(11) DEFAULT NULL,
  `admin` varchar(100) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `group` (`group`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- テーブルのデータのダンプ `user_auth`
--

INSERT INTO `user_auth` (`id`, `username`, `group`, `admin`, `role`) VALUES
(1, 'user_a', 1, 'False', 'role_c'),
(2, 'user_b', 2, 'True', 'role_a');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
