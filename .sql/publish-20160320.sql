-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- ホスト: localhost
-- 生成日時: 2016 年 3 月 21 日 11:21
-- サーバのバージョン: 5.5.25a
-- PHP のバージョン: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- データベース: `test2`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `publish`
--

DROP TABLE IF EXISTS `publish`;
CREATE TABLE IF NOT EXISTS `publish` (
  `app_id` int(11) NOT NULL,
  `data_id` varchar(100) NOT NULL,
  `applicant` varchar(100) DEFAULT NULL,
  `theme` varchar(1000) DEFAULT NULL,
  `sub_theme` varchar(1000) DEFAULT NULL,
  `experiment` varchar(1000) DEFAULT NULL,
  `dataset` varchar(1000) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `theme_id` int(11) DEFAULT NULL,
  `sub_theme_id` int(11) DEFAULT NULL,
  `experiment_id` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `target` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`app_id`),
  KEY `data_id` (`data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
