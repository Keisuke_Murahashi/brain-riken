--
-- Database: `test2`
--

-- --------------------------------------------------------

--
-- テーブルのデータのダンプ `lang`
--

INSERT INTO `lang` (`key`, `ja`, `en`, `page`, `upd_date`) VALUES('_applied', '申請中', 'Applied', NULL, '2016-04-25 00:00:00');
INSERT INTO `lang` (`key`, `ja`, `en`, `page`, `upd_date`) VALUES('_approved', '承認済', 'Approved', NULL, '2016-04-25 00:00:00');
INSERT INTO `lang` (`key`, `ja`, `en`, `page`, `upd_date`) VALUES('_rejected', '棄却', 'Rejected', NULL, '2016-04-25 00:00:00');
