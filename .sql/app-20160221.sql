-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016 年 2 朁E21 日 15:43
-- サーバのバージョン： 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test2`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `app`
--

CREATE TABLE IF NOT EXISTS `app` (
  `app_id` varchar(100) DEFAULT NULL,
  `data_id` varchar(1000) DEFAULT NULL,
  `app_type` varchar(1000) DEFAULT NULL,
  `status` varchar(1000) DEFAULT NULL,
  `creator` varchar(1000) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `detail` varchar(1000) DEFAULT NULL,
  `authorizer` varchar(1000) NOT NULL,
  `group` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `app`
--

-- INSERT INTO `app` (`app_id`, `data_id`, `app_type`, `status`, `creator`, `date`, `detail`, `authorizer`, `group`) VALUES
-- ('00001', 't1wi_000001', '共有', '申請中', 'user_a', '2016-02-21 10:01:50', 'comment', 'user_b', ''),
-- ('00002', 't1wi_000002', '共有', '承認済', 'user_b', '2016-02-21 15:02:44', 'abc', 'user_a', 'group_a');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
