INSERT INTO `test2`.`lang` (`key`, `ja`, `en`, `page`) VALUES ('error_keyword_exceeds_max_length', '検索キーワードは50文字以内で入力してください。', 'Search keyword Please enter up to 50 characters', 'Search');

INSERT INTO `config` (`id`, `key`, `value`, `memo`, `upd_date`) VALUES (8, 'keyword_max_length', '50', 'キーワード検索に入力できる最大文字数', '2016-08-03 02:29:42');

