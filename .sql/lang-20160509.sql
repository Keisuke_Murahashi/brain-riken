INSERT INTO `lang`(`key`, `ja`, `en`, `page`) VALUES ("_select_data_id", 'データセットIDを選択してください', 'Select the DatasetID', 'my assets');
INSERT INTO `lang`(`key`, `ja`, `en`, `page`) VALUES ("_select_data_from_waiting_list", '未処理一覧からデータを選択してください', 'Select the Data from Waiting list', 'apprications');
INSERT INTO `lang`(`key`, `ja`, `en`, `page`) VALUES ("_select_data_from_done_list", '結果一覧からデータを選択してください', 'Select the Data from Done list', 'apprications');
INSERT INTO `lang`(`key`, `ja`, `en`, `page`) VALUES ("_select_data_from_done_list", '結果一覧からデータを選択してください', 'Select the Data from Done list', 'apprications');
INSERT INTO `lang`(`key`, `ja`, `en`, `page`) VALUES ("_select_approved_data", '承認済みデータを選択してください', 'Select the Approved Data', 'apprications');
