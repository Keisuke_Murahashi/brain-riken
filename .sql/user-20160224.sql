-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- ホスト: localhost
-- 生成日時: 2016 年 2 月 25 日 00:57
-- サーバのバージョン: 5.5.25a
-- PHP のバージョン: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- データベース: `test2`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `detail` varchar(1000) DEFAULT NULL,
  `home_dir` varchar(1000) DEFAULT NULL,
  `status` varchar(1000) DEFAULT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `lang` varchar(10) DEFAULT NULL,
  `upd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- テーブルのデータのダンプ `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `detail`, `home_dir`, `status`, `name`, `lang`, `upd_date`) VALUES
(1, 'user_a', '$2a$10$R4a6PsjgNBG5lNm1G1ukh.KnTZnzD/dVktRg8Qe6oi3/eBDR9/v9.', 'User A', 'C:\\Git\\test\\dashborrd4\\baseuri', 'active', 'user_a', NULL, '2016-02-13 15:00:00'),
(2, 'user_b', '$2a$10$R4a6PsjgNBG5lNm1G1ukh.KnTZnzD/dVktRg8Qe6oi3/eBDR9/v9.', 'User B', 'c:/test/user_b', 'active', 'user_b', NULL, '2016-02-02 15:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
